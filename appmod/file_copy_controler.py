#!/usr/bin/env python
#-*- coding: utf-8 -*-

"""

"""
import os
from os import path

from PyQt5.QtWidgets import QApplication, QMenu, QMainWindow, QDialog, QLabel, QComboBox, QPushButton, QVBoxLayout, QMessageBox, QStyledItemDelegate, QItemDelegate
from PyQt5.QtGui import QIcon, QStandardItemModel, QStandardItem, QCursor
from PyQt5.QtCore import Qt, QDir, QT_TRANSLATE_NOOP, QTranslator, QLocale, QSize, QPoint, QDateTime, QVariant, QDate
from PyQt5.QtCore import Qt
from PyQt5 import uic


from applib import log
from applib import setting
from applib import dircmd
from applib import filecmd


class file_copy_controler( QDialog ) :

    def __init__(self, parent, fileItemList, pathSelected, favDirList):

        super(file_copy_controler,self).__init__()
        self.parent = parent
        self.fileActionWindow = uic.loadUi('appmod/file_copy_view.ui', self)
        self.fileItemList = fileItemList
        self.pathSelected = pathSelected
        self.favDirList = favDirList

        nbfiles = 0
        for fileItem in fileItemList  :
            nbfiles = nbfiles + 1
            self.lineEditFileNameValue.setText( fileItem.fname )

        self.setWindowTitle("Copier les fichiers : " + str( nbfiles ))

        self.extToCopyList = setting.Setting.getValue("rawsearch/rawInfoExtSearch")
        self.fileActionWindow.checkBox_pp3.setText( "Copier : "   + ', '.join(self.extToCopyList)  )
        # Désactiver la copie jpg ... si un lien à créer d'un des fichiers ne concerne pas un Nef
        extFileNotRaw = setting.Setting.getListValue("rawsearch/rawExt")
        for fileItem in self.fileItemList :
            if fileItem.ext not in ( extFileNotRaw ) :

                self.fileActionWindow.checkBox_pp3.setDisabled( True )
                self.fileActionWindow.checkBox_pp3.setChecked( False )

        self.selectDir_pushButton.clicked.connect(self.onSelectDir)
        self.ok_pushButton.clicked.connect(self.onOk)
        self.cancel_pushButton.clicked.connect(self.onCancel)

        self.dstDir = ""
        self.fileActionWindow.lineEditFileNameValue.setText( self.dstDir )


    def onOk(self) :

       if self.dstDir != "" :

            nbFCopy = 0
            for fileItem in self.fileItemList :
                log.Log.debug(log.INFO, "Copie du fichier : " + os.path.join(fileItem.filePath + "/" + fileItem.fname)  + " --> "  + os.path.join(self.dstDir  + "/" + fileItem.fname))
                myfilecmd = filecmd.filecmd(self)
                try :
                    #os.symlink(  os.path.join(fileItem.filePath + "/" + fileItem.fname) ,  os.path.join(self.dstDir  + "/" + fileItem.fname))
                    resultCopy = myfilecmd.copy( fileItem.filePath, fileItem.fname , self.dstDir, fileItem.fname, self.fileActionWindow.checkBox_overwrite.isChecked() )
                    if resultCopy is False :
                         raise Exception('Erreur de copie du fichier')
                    nbFCopy = nbFCopy + 1
                    if self.fileActionWindow.checkBox_pp3.isChecked() is True :
                        print ("COPY PP3 ?")

                        (rawDirlistSearch) = setting.Setting.getListValue("rawsearch/rawDirlistSearch")
                        i = 0
                        for rawDirlistSearchElem in rawDirlistSearch :
                            rawDirlistSearch[i] = rawDirlistSearch[i].replace( "[rawdir]" , fileItem.filePath)
                            if not rawDirlistSearch[i].endswith("/") :
                                rawDirlistSearch[i] = rawDirlistSearch[i] + "/"

                            for extToCopyElem in self.extToCopyList :
                                if os.path.exists(rawDirlistSearch[i]  + os.path.splitext(fileItem.fname)[0] + "." + extToCopyElem) is True :
                                    print ("COPY " + rawDirlistSearch[i]  + os.path.splitext(fileItem.fname)[0] + "." + extToCopyElem  +" TO " + self.dstDir  + "/" + os.path.splitext(fileItem.fname)[0] + "." + extToCopyElem)
                                    myfilecmd.copy(
                                        rawDirlistSearch[i], os.path.splitext(fileItem.fname)[0] + "." + extToCopyElem ,
                                        self.dstDir  , os.path.splitext(fileItem.fname)[0] + "." + extToCopyElem,
                                        self.fileActionWindow.checkBox_overwrite.isChecked()
                                    )
                            i = i + 1


                except Exception as e:
                    QMessageBox.warning(self, 'Erreur', "Erreur de copie du fichier : \n\n" + str(e.args[0]), QMessageBox.Yes )
                    log.Log.debug(log.INFO, "Erreur copie du fichier : " + str(e.args[0]))


            self.fileActionWindow.result_textEdit.setText("Copie des "+str(nbFCopy)+" fichiers terminée.")


    def onCancel(self) :
        self.close()

    def dialogShow( self) :
        log.Log.debug( log.INFO , "File copy view : showing window."  )
        self.fileActionWindow.exec_() # Modal

    def onSelectDir(self) :
        self.dstDir = ""

        dircmdWin = dircmd.dircmd(self)
        self.dstDir = dircmdWin.select( "Selectionnez le dossier destination", self.pathSelected,  self.favDirList)
        self.fileActionWindow.lineEditFileNameValue.setText( self.dstDir )




    def tmp() :
        dstDir = ""

        dircmdWin = dircmd.dircmd(self)
        dstDir = dircmdWin.select( "Selectionnez le dossier destination",  self.MainWindow.treeView.getPathSelected(), self.favDirList )

        if dstDir != "" :

            myfilecmd = filecmd.filecmd(self)

            for fileItemElem in self.getFileItemListSelected () :
                log.Log.debug(log.INFO, "Copy file: " + self.MainWindow.treeView.getPathSelected() + fileItemElem.fname + " ==> " +  dstDir )
                resultCmd = myfilecmd.copy(  self.MainWindow.treeView.getPathSelected(), fileItemElem.fname , dstDir , fileItemElem.fname )
                if resultCmd is not True :
                    QMessageBox.warning(self, 'Erreur', "Erreur de copie : \n\n" + myfilecmd.cmdExec + "\n\n" + "Code de retour : " + str(myfilecmd.subprocess_returncode)  + "\n" + myfilecmd.subprocess_err , QMessageBox.Yes )
                else :
                    print ("create thumb ")
                    thumbSizeWidth = int(setting.Setting.getValue("iconview/thumbSizeWidth", "100"))
                    thumbSizeHeight  = int(setting.Setting.getValue("iconview/thumbSizeHeight", "100"))
                    thumbQuality  = int(setting.Setting.getValue("iconview/thumbQuality", "30"))
                    thumbCacheDir  = setting.Setting.getValue("iconview/thumbCacheDir", "./cache")
                    overlayIconSizeW  = int(setting.Setting.getValue("iconview/overlayIconSizeW", "32"))
                    overlayIconSizeH  = int(setting.Setting.getValue("iconview/overlayIconSizeH", "32"))
                    overlayIConLink = QImage("icon/arrow_green.png")
                    overlayIConLink = overlayIConLink.scaled( QSize(overlayIconSizeW, overlayIconSizeH), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.SmoothTransformation ) # or .Qt.SmoothTransformation   /  QtCore.Qt.FastTransformation
                    overWrite = True
                    fileItemElemNew = fileItem(self, dstDir, fileItemElem.fname, self.fileExt, getExif=False)
                    fileItemElemNew.createThumb(  thumbSizeWidth, thumbSizeHeight, thumbCacheDir, thumbQuality, overWrite,  overlayIConLink, overlayIconSizeW, overlayIconSizeH )

