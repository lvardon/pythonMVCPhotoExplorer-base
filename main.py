#!/usr/bin/env python
#-*- coding: utf-8 -*-

# === Point d'entrée principal de l'application ===

"""

 * class **main**
 * class **systray**
 * def **chekImportLib**
 * def **my_exception_hook**
 * def **main** [[main.py#main]]

"""

# To make pylint aware that you want to use the new print statement and not put erroneous brackets
from __future__ import print_function

import sys
import time
from time import sleep

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow, QDialog, QLabel, QPushButton, QVBoxLayout, QMessageBox, QSplashScreen, QProgressBar
from PyQt5.QtGui import QIcon, QFont, QPixmap
from PyQt5.QtCore import Qt, QDir, QT_TRANSLATE_NOOP, QTranslator, QLocale, QFile

from applib import aes
from applib import process
from applib import locale

from applib import log
from applib import sqlite

from applib import setting
from applib.setting import Setting

from appmod.main_controler import STARTUPMODE_GUI, STARTUPMODE_SYSTRAY

# ---
class Main( object ):
    """

    Point d'entrée en mode de lancement "normal"  (par opposition au mode "systray")

    * Initialisation du log
    * Lecture des paramètres application depuis config.ini
    * Enregistrement dans le fichier de log d'informations relatives à l'environnement

    """

    def __init__(self, appSplash, parent = None):

        self.confirmOnQuit = setting.Setting.getValue( "app/confirmOnQuit", "0" )
        self.confirmOnHide = setting.Setting.getValue( "app/confirmOnHide", "0" )

        self.logfullfileName =  setting.Setting.getValue( "log/logPath", "logdefault.txt" )
        self.eraseContent = setting.Setting.getValue( "log/eraseContent", "0")
        self.outToconsole = setting.Setting.getValue( "log/outToconsole", "1" )


        if setting.Setting.fileExists is False :
            fatalMessage = "Le fichier config.ini n'a pas été trouvé, ou ne contient pas de valeur log/logfullpath (le fichier de log est : logdefault.txt). Arrêt."
            log.Log.debug(log.Log.FATAL, fatalMessage  )
            print (fatalMessage )
            msg = QMessageBox( )
            msg.setIcon(QMessageBox.Critical)
            msg.setText( fatalMessage )
            msg.setWindowTitle("Erreur critique")
            msg.setStandardButtons(QMessageBox.Ok )
            msg.exec_()
            sys.exit(1)

        else :
            log.Log.debug(log.INFO, "Lecture des paramètres de : config.ini"  )

            self.debugMode = setting.Setting.getValue('debug/configDebug', "1" )
            log.Log.debug(log.INFOSETTING, 'Config.ini : debug/configDebug = ' + self.debugMode  )
            log.Log.debug(log.INFOSETTING, 'Config.ini : log/eraseContent = ' + self.eraseContent  )
            log.Log.debug(log.INFOSETTING, 'Config.ini : app/confirmOnQuit = ' + self.confirmOnQuit  )
            log.Log.debug(log.INFOSETTING, 'Config.ini : app/confirmOnHide = ' + self.confirmOnHide  )
            log.Log.debug(log.INFOSETTING, 'Config.ini : log/outToconsole = ' + self.outToconsole  )

            # Affiche dans le log la présence, ou non, de librairies Python requises
            chekImportLib()

            # Necessaire pour afficher l'icône de l'application dans la barre des tâches (Win 7)
            import platform
            if platform.system() == "Windows":
                import ctypes
                myappid = 'MyOrganization.MyGui.1.0.0' # arbitrary string
                ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
                log.Log.debug(log.INFOSETTING, 'Os version : Windows' )
            else :
                log.Log.debug(log.INFOSETTING, 'Os version : not ms win' )

            log.Log.debug(log.INFOSETTING, 'Python version : ' + platform.python_version()  )
            log.Log.debug(log.INFOSETTING, 'Python version (majeur): ' + platform.python_version_tuple()[0]  )
            log.Log.debug(log.INFOSETTING, 'system   :'+ platform.system() )
            log.Log.debug(log.INFOSETTING, 'node     :'+ platform.node())
            log.Log.debug(log.INFOSETTING, 'release  :'+ platform.release())
            log.Log.debug(log.INFOSETTING, 'version  :'+ platform.version())
            log.Log.debug(log.INFOSETTING, 'machine  :'+ platform.machine())
            log.Log.debug(log.INFOSETTING, 'processor:'+ platform.processor())

            # Ouvre la base de données Sqlite, pour en obtenir la version
            self.dbCx = sqlite.sqlite( self, "data/database.sqlite" )
            sqliteVer = self.dbCx.getversion()
            log.Log.debug( log.INFO, "SQLite version: %s" % sqliteVer)

            # Passe la main au controleur principal (l'application)
            log.Log.debug(log.INFO, "Main Controler : init."  )
            from appmod import main_controler
            self.main_controler = main_controler.main_controler( self, appSplash, STARTUPMODE_GUI )

    # Sauvegarde de l'apparence(state et geometry) de la fenêtre principale
    def saveSetting(self) :
        """
        Enregistre les tailles et position de la fenêtre application, dans le fichier config.ini
        """
        if self.main_controler.MainWindow.saveGeometry() != None :
            setting.Setting.setValue("mainwindow/restoreGeometryValue", self.main_controler.MainWindow.saveGeometry())

        if self.main_controler.MainWindow.saveState() != None :
            setting.Setting.setValue("mainwindow/restoreStateValue", self.main_controler.MainWindow.saveState())


# ---

class SystemTrayIcon(QtWidgets.QSystemTrayIcon):
    """
    Point d'entrée en mode de lancement "systray"

    * Initialisation du log
    * Lecture des paramètres application depuis **config.ini**
    * Affichage dans *log* d'informations relatives à l'environnement
    """

    def __init__(self, app, appSplash, qicone, tooltip="", parent=None):

        super(SystemTrayIcon, self).__init__(qicone, app)

        self.app = app
        self.parent = parent

        self.confirmOnQuit = setting.Setting.getValue( "app/confirmOnQuit", "0" )
        self.confirmOnHide = setting.Setting.getValue( "app/confirmOnHide", "0" )

        self.logfullfileName =  setting.Setting.getValue( "log/logPath", "logdefault.txt" )
        self.eraseContent = setting.Setting.getValue( "log/eraseContent", "0")
        self.outToconsole = setting.Setting.getValue( "log/outToconsole", "1" )

        if setting.Setting.fileExists is False :
            fatalMessage = "Le fichier config.ini n'a pas été trouvé, ou ne contient pas de valeur log/logfullpath (le fichier de log est : logdefault.txt). Arrêt."
            log.Log.debug(log.FATAL, fatalMessage  )
            msg = QMessageBox( )
            msg.setIcon(QMessageBox.Critical)
            msg.setText( fatalMessage )
            msg.setWindowTitle("Erreur critique")
            msg.setStandardButtons(QMessageBox.Ok )
            msg.exec_()
            sys.exit(1)

        else :
            log.Log.debug(log.INFO, "Lecture des paramètres de : config.ini"  )

            self.debugMode = setting.Setting.getValue('debug/configDebug', "1" )
            log.Log.debug(log.INFOSETTING, 'Config.ini : debug/configDebug = ' + self.debugMode  )
            log.Log.debug(log.INFOSETTING, 'Config.ini : log/eraseContent = ' + self.eraseContent  )
            log.Log.debug(log.INFOSETTING, 'Config.ini : app/confirmOnQuit = ' + self.confirmOnQuit  )
            log.Log.debug(log.INFOSETTING, 'Config.ini : app/confirmOnHide = ' + self.confirmOnHide  )
            log.Log.debug(log.INFOSETTING, 'Config.ini : log/outToconsole = ' + self.outToconsole  )

            # Affiche dans le log la présence, ou non, de librairies Python requises
            chekImportLib()

            # Necessaire pour afficher l'icône de l'application dans la barre des tâches (Win 7)
            import platform
            if platform.system() == "Windows":
                import ctypes
                myappid = 'MyOrganization.MyGui.1.0.0' # arbitrary string
                ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
                log.Log.debug(log.INFOSETTING, 'Os version : Windows' )
            else :
                log.Log.debug(log.INFOSETTING, 'Os version : not ms win' )

            log.Log.debug(log.INFOSETTING, 'Python version : ' + platform.python_version()  )
            log.Log.debug(log.INFOSETTING, 'Python version (majeur): ' + platform.python_version_tuple()[0]  )
            log.Log.debug(log.INFOSETTING, 'system   :'+ platform.system() )
            log.Log.debug(log.INFOSETTING, 'node     :'+ platform.node())
            log.Log.debug(log.INFOSETTING, 'release  :'+ platform.release())
            log.Log.debug(log.INFOSETTING, 'version  :'+ platform.version())
            log.Log.debug(log.INFOSETTING, 'machine  :'+ platform.machine())
            log.Log.debug(log.INFOSETTING, 'processor:'+ platform.processor())

        if tooltip != "":
            self.setToolTip( tooltip )
            self.showMessage('Infos', tooltip)

        # Le mode systray dispose d'un menu contextuel (clic-droit sur l'icône du systray)
        self.systrayInitMenu()

        # Ouvre la base de données Sqlite, pour en obtenir la version
        self.dbCx = sqlite.sqlite( self, "data/database.sqlite" )
        sqliteVer = self.dbCx.getversion()
        log.Log.debug( log.INFO, "SQLite version: %s" % sqliteVer)

        # Passe la main au controleur principal (l'application)
        log.Log.debug(log.INFO, "Main Controler : init."  )
        from appmod import main_controler
        self.main_controler = main_controler.main_controler( self, appSplash, STARTUPMODE_SYSTRAY )

    # Sauvegarde de l'apparence (state et geometry) de la fenêtre principale
    def saveSetting(self) :
        """
        Enregistre les tailles et position de la fenêtre application, dans le fichier config.ini
        """

        if self.main_controler.MainWindow.saveGeometry() != None :
            setting.Setting.setValue("mainwindow/restoreGeometryValue", self.main_controler.MainWindow.saveGeometry())

        if self.main_controler.MainWindow.saveState() != None :
            setting.Setting.setValue("mainwindow/restoreStateValue", self.main_controler.MainWindow.saveState())



    def systrayInitMenu(self):
        """
        Menu contextuel, affiché dans le mode de lancement "systray" (menu de l'icône du systray)
        """
        menu = QtWidgets.QMenu(self.parent)

        # Menu  : "Affiche la fenêtre principale"
        app_icon = QtGui.QIcon()
        app_icon.addFile('icon/app16x16.png', QtCore.QSize(16, 16))
        app_icon.addFile('icon/app24x24.png', QtCore.QSize(24, 24))
        app_icon.addFile('icon/app32x32.png', QtCore.QSize(32, 32))
        app_icon.addFile('icon/app48x48.png', QtCore.QSize(48, 48))
        app_icon.addFile('icon/app256x256.png', QtCore.QSize(256, 256))

        mainWindowShowPopupIcon = QtGui.QIcon(app_icon)

        mainWindowShowPopupAction = QtWidgets.QAction(mainWindowShowPopupIcon, '&Ouvrir', self)
        mainWindowShowPopupAction.triggered.connect(self.onMainWindowShow)
        menu.addAction(mainWindowShowPopupAction)

        menu.addSeparator()

        # Menu : "Fermer la fenêtre et Quitter le programme"
        quitter_icone = QtGui.QIcon()
        quitter_icone.addFile('icon/app16x16.png', QtCore.QSize(16,16))
        quitter_icone.addFile('icon/app24x24.png', QtCore.QSize(24,24))
        quitter_icone.addFile('icon/app32x32.png', QtCore.QSize(32,32))
        quitter_icone.addFile('icon/app48x48.png', QtCore.QSize(48,48))
        quitter_icone.addFile('icon/app256x256.png', QtCore.QSize(256,256))

        quitterIcon = QtGui.QIcon.fromTheme("application-exit", QtGui.QIcon(quitter_icone))
        quitterAction = QtWidgets.QAction(quitterIcon, '&Quitter', self)
        quitterAction.triggered.connect(self.onQuitApp)
        menu.addAction(quitterAction)

        self.setContextMenu(menu)

    def onMainWindowShow(self):
        """
        Affiche la fenêtre principale
        """
        self.main_controler.mainwindowShow()
        self.showMessage('Infos', "Fenêtre principale affichée...")


    def onQuitApp(self, event):
        """
        Gestion de la fermeture de l'application.
        Demande eventuelle de confirmation avant fermeture du programme
        """
        if self.confirmOnQuit == "1" :

            messCloseApp = self.tr( "Fermer l'application ?", "Message demandant la confirmation de fermeture du programme")

            box = QMessageBox()
            box.setIcon(QMessageBox.Question)
            box.setWindowTitle('Confirmez')
            box.setText(messCloseApp)
            box.setStandardButtons(QMessageBox.Yes|QMessageBox.No)
            buttonY = box.button(QMessageBox.Yes) # buttonY.setText('Oui')
            buttonN = box.button(QMessageBox.No) #buttonN.setText('Non')

            box.exec_()

            if box.clickedButton() == buttonY:
                self.saveSetting()
                log.Log.debug(log.INFO ,"Main systray : close app."   )
                sys.exit(0)

            elif box.clickedButton() == buttonN:
                pass
        else :
            log.Log.debug(log.INFO, "Main systray : close app."  )
            self.saveSetting()
            sys.exit(0)

    @classmethod
    def closeEvent(cls, event):
        """
        Ignore la fermeture de l'application
        """
        event.ignore()

# ---
class chekImportLib( object ) :
    """
    Effectue quelques vérifications (affichage dans le log uniquement) sur la présence de librairies Python tièrces (prérequis)
    """
    def __init__(self):

        self.debliblist = setting.Setting.getListValue( "appdep/depliblist" )

        for libName in self.debliblist:

            if sys.version_info[0] == 2 :
                import pkgutil
                eggs_loader = pkgutil.find_loader(libName)
                loadedLib = eggs_loader is not None

            if sys.version_info[0] == 3 :
                import importlib
                loadedLib = importlib.find_loader(libName)

            if loadedLib is False :
                log.Log.debug(log.WARNING, 'check import ib : ' + libName + ' : non trouvee.' )
            else :
                log.Log.debug(log.INFO, 'check import lib : ' + libName + ' : ok'  )

# ---

# Sauvegarde du gestionnaire original d'erreurs Python
sys._excepthook = sys.excepthook

# Gestionnaire personnalisé des erreurs Python
def my_exception_hook(exctype, value, traceback):
    """
    Gestion globale des erreurs d'execution
    """
    import os
    import logging

    QApplication.restoreOverrideCursor()

    # Affiche l'erreur et son contexte
    print(exctype, value, traceback)

    # Call the normal Exception hook after
    sys._excepthook(exctype, value, traceback)

    logfullfileName =  setting.Setting.getValue( "log/logPath", "log.txt")
    logfullfileName = logfullfileName.replace('\\', os.sep)
    log.Log.debug(log.FATAL, str(exctype) + "\n" + str(value) + "\n" +  str(traceback)  )

    logging.basicConfig(filename=logfullfileName )
    logging.error( "Uncaught exception", exc_info=(exctype, value, traceback))

    # Crée un copie du fichier de log courant (le nom de la copie inclu la date et l'heure)
    log.Log.copy()

    msg = QMessageBox( )
    msg.setIcon(QMessageBox.Critical)
    msg.setText("Erreur : le détail est enregistré dans : " + str(log.Log.getFilelogCopyName())  )
    msg.setInformativeText( str(exctype) + "\n" + str(value) + "\n" +  str(traceback))
    msg.setWindowTitle("Exception non gérée")
    msg.setStandardButtons(QMessageBox.Ok )
    msg.exec_()

    sys.exit(1)

# Redirige les erreurs python non "trappées" vers cette foncton
sys.excepthook = my_exception_hook


class splash( QSplashScreen ) :
    """
    Gestion du splash screen au lancement de l'application
    """
    def __init__(self, app, parent=None):

        super(splash, self).__init__(parent)

        self.parent = parent
        self.app = app

        # Create and display the splash screen
        splash_pix = QPixmap('icon/splash.png')
        #self.splash = QSplashScreen(self.parent, splash_pix, Qt.WindowStaysOnTopHint)
        self.setPixmap(QPixmap(splash_pix))

        # adding progress bar
        self.progress = False
        if self.progress is True :
            self.progressBar = QProgressBar(self)
            self.progressBar.setGeometry(self.width() / 10 ,(8*self.height()/16), 8 * self.width() / 10, (self.height() / 10) )
            self.progressBar.move(40,self.height()-50)

        #progress_description.setHidden(False)
        #progress_description.setText('Init ...')

        self.setMask(splash_pix.mask())
        self.show()


        self.processEvent()

    def processEvent(self) :
        """
        Boucle de temporisation pour éviter de figer l'affichage de la fen^etre de l'application
        """
        start = time.time()
        while time.time() - start < 0.5:
            sleep(0.001)
            self.app.processEvents()

    def progressBarForward( self, barVal ) :
        """
        Fixe la valeur courante de la barre de progression
        """
        if self.progress is True :
            self.progressBar.setValue( barVal)

    def progressBarSetMaximum( self, maximum ) :
        """
        Fixe la valeur maximale de la barre de progression
        """
        if self.progress is True :
            self.progressBar.setMaximum(maximum)

    def closeSplash(self, timeoutMs ) :
        """
        Déclenche la disparition du splash screen, après un certain temps
        """
        #splash.finish(w)
        #splash.finish( w.main_controler)
        #QTimer::singleShot(2000, &splash, SLOT(close()))
        QtCore.QTimer.singleShot(timeoutMs, self.close)

# ---
def main():
    # === main ===
    """
    Point d'entrée principal
    """
    app = QApplication(sys.argv)

    # Cas de la version de Python inférieur à 3
    import platform
    if int(platform.python_version_tuple()[0]) < 3 :
        reload(sys)
        sys.setdefaultencoding('utf8')

    setting.Setting = setting.setting(None, "config.ini")
    logfullfileName =  setting.Setting.getValue( "log/logPath", "logdefault.txt" )
    eraseContent = setting.Setting.getValue( "log/eraseContent", "0")
    outToconsole = setting.Setting.getValue( "log/outToconsole", "1" )
    log.Log = log.log( None, logfullfileName, eraseContent, outToconsole)

    appSplash = None
    if setting.Setting.getValue( "app/splashScreenShow", "0" ) == "1":
        appSplash = splash( app )

    # Analyse des paramètres passé en ligne de commande
    import getopt
    version = '1.0'
    verbose = False
    output_filename = 'default.out'     #print ('ARGV      :', sys.argv[1:])

    options, remainder = getopt.getopt(sys.argv[1:], 'o:v', ['output=',
                                                             'verbose',
                                                             'version=',
                                                            ])

    for opt, arg in options: #print ('OPTIONS   :', options)
        if opt in ('-o', '--output'):
            output_filename = arg
        elif opt in ('-v', '--verbose'):
            verbose = True
        elif opt == '--version':
            version = arg

    # Affichage des valeurs d'arguments passé en ligne de commande
    log.Log.debug(log.INFOSETTING, 'Arg value : version = ' + version  )
    log.Log.debug(log.INFOSETTING, 'Arg value : verbose = ' + str(verbose)  )
    log.Log.debug(log.INFOSETTING, 'Arg value : output_filename = ' + output_filename  )
    for remainderValue in remainder :
        log.Log.debug(log.INFOSETTING, 'Arg value : remainderValue = ' + remainderValue  )


    # Pour une détection automatique de la langue locale
    localeName = QtCore.QLocale.system().name() # => De type en_EN ou fr_FR
    # pour une version forcée :
    #localeName = "fr_FR"
    #localeName = "en_EN"
    log.Log.debug(log.INFO, '0/3 : Locale = ' + localeName  )

    # http://forum.qtfr.org/discussion/17944/qt5-correction-du-fichier-de-traduction-en-francais-qt-fr-qm
    # http://l10n-files.qt.io/l10n-files/

    # Traduction des textes
    translator = QtCore.QTranslator(app)
    if translator.load("locale/locale_" + localeName + ".qm"):
        log.Log.debug(log.INFO, '1/3 : install locale : ' + "locale/locale_" + localeName + ".qm"  )
        app.installTranslator(translator)

    # Traductions de qt (pour qdialogQt, les boutons yes/no/cancel...)
    translator2 = QtCore.QTranslator (app)
    if translator2.load("locale/qt_fr.qm"):
    #if translator2.load("qt_" + localeName, QtCore.QLibraryInfo.location(QtCore.QLibraryInfo.TranslationsPath)):
    #if translator2.load("qt_de.qm", QtCore.QLibraryInfo.location(QtCore.QLibraryInfo.TranslationsPath)):
        log.Log.debug(log.INFO, '2/3 : install locale : ' + "locale/qt_fr.qm" )
        app.installTranslator(translator2)

    translatorQt = QtCore.QTranslator (app)
    if translatorQt.load("locale/qtbase_fr.qm"):
    #if translator2.load("qtbase_fr.qm", QtCore.QLibraryInfo.location(QtCore.QLibraryInfo.TranslationsPath)):
        log.Log.debug(log.INFO, '3/3 : install locale : ' + "locale/qtbase_fr.qm" )
        app.installTranslator(translatorQt)

    app.setFont(  QFont( setting.Setting.getValue( "mainwindow/fontName", "SansSerif" ), int(setting.Setting.getValue( "mainwindow/fontSize", "10" ) ))  )

    modeSystray = setting.setting(app, "config.ini").getValue( "app/modeSystray", "1" )
    if modeSystray == "0" :
        # Lancement en mode normal
        Main(appSplash)
    else :
        # Lancement en mode systray
        app.setQuitOnLastWindowClosed(False)
        systray_icon = QtGui.QIcon()
        systray_icon.addFile('icon/app16x16.png', QtCore.QSize(16, 16))
        systray_icon.addFile('icon/app24x24.png', QtCore.QSize(24, 24))
        systray_icon.addFile('icon/app32x32.png', QtCore.QSize(32, 32))
        systray_icon.addFile('icon/app48x48.png', QtCore.QSize(48, 48))
        systray_icon.addFile('icon/app256x256.png', QtCore.QSize(256, 256))
        trayIconApp = SystemTrayIcon(app, appSplash, QtGui.QIcon(systray_icon), "Nom/description du programme")
        log.Log.debug(log.INFO, "App chargée dans le systray" )
        trayIconApp.show()

    if setting.Setting.getValue( "app/splashScreenShow", "0" ) == "1":
        appSplash.closeSplash( 500 )

    sys.exit(app.exec_())

if __name__ == "__main__":

    main()
