<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>AudioContainerControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/audiocapture/audiocontainercontrol.cpp" line="+69"/>
        <source>RAW (headerless) file format</source>
        <translation>Format de fichier RAW (sans en-tête)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>WAV file format</source>
        <translation>Format de fichier WAV</translation>
    </message>
</context>
<context>
    <name>AudioEncoderControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/audiocapture/audioencodercontrol.cpp" line="+92"/>
        <source>Linear PCM audio data</source>
        <translation>Données audio PCM linéaire</translation>
    </message>
</context>
<context>
    <name>BbCameraAudioEncoderSettingsControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/qnx/camera/bbcameraaudioencodersettingscontrol.cpp" line="+53"/>
        <source>No compression</source>
        <translation>Aucune compression</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>AAC compression</source>
        <translation>Compression AAC</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>PCM uncompressed</source>
        <translation>PCM sans compression</translation>
    </message>
</context>
<context>
    <name>BbCameraMediaRecorderControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/qnx/camera/bbcameramediarecordercontrol.cpp" line="+100"/>
        <source>Unable to retrieve mute status</source>
        <translation>Impossible de récupérer le statut muet</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Unable to retrieve audio input volume</source>
        <translation>Impossible de récupérer le volume d&apos;entrée audio</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Unable to set mute status</source>
        <translation>Impossible de définir le statut muet</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Unable to set audio input volume</source>
        <translation>Impossible de définir le volume d&apos;entrée audio</translation>
    </message>
</context>
<context>
    <name>BbCameraSession</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/qnx/camera/bbcamerasession.cpp" line="+334"/>
        <source>Camera provides image in unsupported format</source>
        <translation>La caméra fournit l&apos;image dans un format non pris en charge</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Could not load JPEG data from frame</source>
        <translation>Impossible de charger les données JPEG à partir d&apos;une frame</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Camera not ready</source>
        <translation>La caméra n&apos;est pas prête</translation>
    </message>
    <message>
        <location line="+237"/>
        <source>Unable to apply video settings</source>
        <translation>Impossible d&apos;appliquer les paramètres vidéo</translation>
    </message>
    <message>
        <location line="+120"/>
        <source>Could not open destination file:
%1</source>
        <translation>Impossible d&apos;ouvrir le fichier de destination :
%1</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Unable to open camera</source>
        <translation>Impossible d&apos;ouvrir la caméra</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unable to retrieve native camera orientation</source>
        <translation>Impossible de récupérer l&apos;orientation native de la caméra</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Unable to close camera</source>
        <translation>Impossible de fermer la caméra</translation>
    </message>
    <message>
        <location line="+204"/>
        <source>Unable to start video recording</source>
        <translation>Impossible de démarrer l&apos;enregistrement vidéo</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Unable to stop video recording</source>
        <translation>Impossible d&apos;arrêter l&apos;enregistrement vidéo</translation>
    </message>
</context>
<context>
    <name>BbCameraVideoEncoderSettingsControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/qnx/camera/bbcameravideoencodersettingscontrol.cpp" line="+63"/>
        <source>No compression</source>
        <translation>Aucun compression</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>AVC1 compression</source>
        <translation>Compression AVC1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>H264 compression</source>
        <translation>Compression H264</translation>
    </message>
</context>
<context>
    <name>BbImageEncoderControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/qnx/camera/bbimageencodercontrol.cpp" line="+53"/>
        <source>JPEG image</source>
        <translation>Compression JPEG</translation>
    </message>
</context>
<context>
    <name>BbVideoDeviceSelectorControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/qnx/camera/bbvideodeviceselectorcontrol.cpp" line="+104"/>
        <source>Front Camera</source>
        <translation>Caméra frontale</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Rear Camera</source>
        <translation>Caméra arrière</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Desktop Camera</source>
        <translation>Caméra du bureau</translation>
    </message>
</context>
<context>
    <name>CameraBinImageCapture</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/camerabin/camerabinimagecapture.cpp" line="+86"/>
        <source>Camera not ready</source>
        <translation>La caméra n&apos;est pas prête</translation>
    </message>
</context>
<context>
    <name>CameraBinImageEncoder</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/camerabin/camerabinimageencoder.cpp" line="+66"/>
        <source>JPEG image</source>
        <translation>Image JPEG</translation>
    </message>
</context>
<context>
    <name>CameraBinRecorder</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/camerabin/camerabinrecorder.cpp" line="+218"/>
        <source>QMediaRecorder::pause() is not supported by camerabin2.</source>
        <translation>QMediaRecorder : pause() n&apos;est pas supportée par camerabin2.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Service has not been started</source>
        <translation>Le service n&apos;a pas été démarré</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Recording permissions are not available</source>
        <translation>Les permissions d&apos;enregistrement ne sont pas disponibles</translation>
    </message>
</context>
<context>
    <name>CameraBinSession</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/camerabin/camerabinsession.cpp" line="+976"/>
        <source>Camera error</source>
        <translation>Erreur de caméra</translation>
    </message>
</context>
<context>
    <name>DSCameraSession</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/directshow/camera/dscamerasession.cpp" line="+625"/>
        <source>Camera not ready for capture</source>
        <translation>La caméra n&apos;est pas prête pour l&apos;enregistrement</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Could not save image to file.</source>
        <translation>Impossible d&apos;enregistrer l&apos;image dans le fichier.</translation>
    </message>
</context>
<context>
    <name>MFPlayerSession</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/wmf/player/mfplayersession.cpp" line="+203"/>
        <source>Invalid stream source.</source>
        <translation>Source de flux invalide.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Attempting to play invalid Qt resource.</source>
        <translation>Tentative de jouer une ressource Qt invalide.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The system cannot find the file specified.</source>
        <translation>Le système ne peut pas trouver le fichier spécifié.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The specified server could not be found.</source>
        <translation>Le serveur spécifié est introuvable.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Unsupported media type.</source>
        <translation>Type de média non supporté.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Failed to load source.</source>
        <translation>Impossible de charger la source.</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Cannot create presentation descriptor.</source>
        <translation>Impossible de créer le descripteur de présentation.</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Failed to get stream count.</source>
        <translation>Impossible de récupérer le nombre de flux.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Failed to create topology.</source>
        <translation>Impossible de créer la topologie.</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Unable to play any stream.</source>
        <translation>Impossible de jouer aucun flux.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Unable to play.</source>
        <translation>Impossible de jouer.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Failed to set topology.</source>
        <translation>Impossible de définir la topologie.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Unknown stream type.</source>
        <translation>Type de flux inconnu.</translation>
    </message>
    <message>
        <location line="+553"/>
        <source>Failed to stop.</source>
        <translation>Impossible d&apos;arrêter.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>failed to start playback</source>
        <translation>Impossible de démarrer la lecture</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Failed to pause.</source>
        <translation>Impossible de mettre en pause.</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Unable to create mediasession.</source>
        <translation>Impossible de créer une session de média.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unable to pull session events.</source>
        <translation>Impossible d&apos;envoyer les événements de session.</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Failed to seek.</source>
        <translation>Impossible de rechercher.</translation>
    </message>
    <message>
        <location line="+393"/>
        <source>Media session non-fatal error.</source>
        <translation>Erreur non fatale de la session de média.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Media session serious error.</source>
        <translation>Erreur sérieuse de la session de média.</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Unsupported media, a codec is missing.</source>
        <translation>Le média n&apos;est pas supporté, un codec est manquant.</translation>
    </message>
</context>
<context>
    <name>QAndroidAudioEncoderSettingsControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/android/src/mediacapture/qandroidaudioencodersettingscontrol.cpp" line="+54"/>
        <source>Adaptive Multi-Rate Narrowband (AMR-NB) audio codec</source>
        <translation>Codec audio Adaptive Multi-Rate Narrowband (AMR-NB)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Adaptive Multi-Rate Wideband (AMR-WB) audio codec</source>
        <translation>Codec audio Adaptive Multi-Rate Wideband (AMR-WB)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>AAC Low Complexity (AAC-LC) audio codec</source>
        <translation>Codec audio AAC Low Complexity (AAC-LC)</translation>
    </message>
</context>
<context>
    <name>QAndroidCameraSession</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/android/src/mediacapture/qandroidcamerasession.cpp" line="+311"/>
        <source>Camera cannot be started without a viewfinder.</source>
        <translation>La caméra ne peut pas être démarrée sans viseur.</translation>
    </message>
    <message>
        <location line="+217"/>
        <source>Camera not ready</source>
        <translation>La caméra n&apos;est pas prête</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Drive mode not supported</source>
        <extracomment>Drive mode is the camera&apos;s shutter mode, for example single shot, continuos exposure, etc.</extracomment>
        <translation>Mode d&apos;enregistrement non pris en charge</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Failed to capture image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+83"/>
        <source>Camera preview failed to start.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Could not open destination file: %1</source>
        <translation>Impossible d&apos;ouvrir le fichier de destination : %1</translation>
    </message>
</context>
<context>
    <name>QAndroidImageEncoderControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/android/src/mediacapture/qandroidimageencodercontrol.cpp" line="+57"/>
        <source>JPEG image</source>
        <translation>Image JPEG</translation>
    </message>
</context>
<context>
    <name>QAndroidMediaContainerControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/android/src/mediacapture/qandroidmediacontainercontrol.cpp" line="+67"/>
        <source>MPEG4 media file format</source>
        <translation>Format de fichier média MPEG4</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>3GPP media file format</source>
        <translation>Format de fichier média 3GPP</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>AMR NB file format</source>
        <translation>Format de fichier AMR NB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>AMR WB file format</source>
        <translation>Format de fichier AMR WB</translation>
    </message>
</context>
<context>
    <name>QAndroidVideoEncoderSettingsControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/android/src/mediacapture/qandroidvideoencodersettingscontrol.cpp" line="+72"/>
        <source>H.263 compression</source>
        <translation>Compression H.263</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>H.264 compression</source>
        <translation>Compression H.264</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>MPEG-4 SP compression</source>
        <translation>Compression MPEG-4 SP</translation>
    </message>
</context>
<context>
    <name>QAudioDecoder</name>
    <message>
        <location filename="../../qtmultimedia/src/multimedia/audio/qaudiodecoder.cpp" line="+147"/>
        <location line="+58"/>
        <source>The QAudioDecoder object does not have a valid service</source>
        <translation>L&apos;objet QAudioDecoder ne dispose pas d&apos;un service valide</translation>
    </message>
</context>
<context>
    <name>QCamera</name>
    <message>
        <location filename="../../qtmultimedia/src/multimedia/camera/qcamera.cpp" line="+114"/>
        <location line="+97"/>
        <source>The camera service is missing</source>
        <translation>Le service caméra est manquant</translation>
    </message>
</context>
<context>
    <name>QCameraImageCapture</name>
    <message>
        <location filename="../../qtmultimedia/src/multimedia/camera/qcameraimagecapture.cpp" line="+541"/>
        <location line="+22"/>
        <source>Device does not support images capture.</source>
        <translation>Le périphérique ne supporte pas la capture d&apos;images.</translation>
    </message>
</context>
<context>
    <name>QDeclarativeAudio</name>
    <message>
        <location filename="../../qtmultimedia/src/imports/multimedia/qdeclarativeaudio.cpp" line="+397"/>
        <source>volume should be between 0.0 and 1.0</source>
        <translation>le volume doit être entre 0.0 et 1.0</translation>
    </message>
</context>
<context>
    <name>QGstreamerAudioDecoderSession</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/audiodecoder/qgstreameraudiodecodersession.cpp" line="+221"/>
        <source>Cannot play stream of type: &lt;unknown&gt;</source>
        <translation>Impossible de lire le flux de type : &lt;inconnu&gt;</translation>
    </message>
</context>
<context>
    <name>QGstreamerAudioEncode</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/mediacapture/qgstreameraudioencode.cpp" line="+89"/>
        <source>Raw PCM audio</source>
        <translation>Audio Raw PCM</translation>
    </message>
</context>
<context>
    <name>QGstreamerAudioInputSelector</name>
    <message>
        <location filename="../../qtmultimedia/src/gsttools/qgstreameraudioinputselector.cpp" line="+101"/>
        <source>System default device</source>
        <translation>Périphérique par défaut du système</translation>
    </message>
</context>
<context>
    <name>QGstreamerCameraControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/mediacapture/qgstreamercameracontrol.cpp" line="+120"/>
        <source>State not supported.</source>
        <translation>État non supporté.</translation>
    </message>
</context>
<context>
    <name>QGstreamerCaptureSession</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/mediacapture/qgstreamercapturesession.cpp" line="+225"/>
        <source>Could not create an audio source element</source>
        <translation>Impossible de créer un élément source audio</translation>
    </message>
    <message>
        <location line="+402"/>
        <source>Failed to build media capture pipeline.</source>
        <translation>Impossible de construire le pipeline de capture du média.</translation>
    </message>
</context>
<context>
    <name>QGstreamerImageCaptureControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/mediacapture/qgstreamerimagecapturecontrol.cpp" line="+68"/>
        <source>Not ready to capture</source>
        <translation>Pas prêt pour la capture</translation>
    </message>
</context>
<context>
    <name>QGstreamerImageEncode</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/mediacapture/qgstreamerimageencode.cpp" line="+66"/>
        <source>JPEG image encoder</source>
        <translation>Encodeur d&apos;image JPEG</translation>
    </message>
</context>
<context>
    <name>QGstreamerPlayerControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/mediaplayer/qgstreamerplayercontrol.cpp" line="+392"/>
        <source>Attempting to play invalid user stream</source>
        <translation>Tentative de jouer un flux utilisateur invalide</translation>
    </message>
</context>
<context>
    <name>QGstreamerPlayerSession</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/mediaplayer/qgstreamerplayersession.cpp" line="+1176"/>
        <location line="+125"/>
        <source>Cannot play stream of type: &lt;unknown&gt;</source>
        <translation>Impossible de jouer le flux de type : &lt;inconnu&gt;</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>UDP source timeout</source>
        <translation>La source UDP ne répond pas</translation>
    </message>
    <message>
        <location line="+471"/>
        <source>Media is loaded as a playlist</source>
        <translation>Le média est chargé comme une liste de lecture</translation>
    </message>
</context>
<context>
    <name>QGstreamerRecorderControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/mediacapture/qgstreamerrecordercontrol.cpp" line="+160"/>
        <location line="+21"/>
        <source>Service has not been started</source>
        <translation>Le service n&apos;a pas été démarré</translation>
    </message>
    <message>
        <location line="+103"/>
        <source>Not compatible codecs and container format.</source>
        <translation>Format de codecs et de conteneur incompatible.</translation>
    </message>
</context>
<context>
    <name>QGstreamerVideoInputDeviceControl</name>
    <message>
        <location filename="../../qtmultimedia/src/multimedia/gsttools_headers/qgstreamervideoinputdevicecontrol_p.h" line="+72"/>
        <source>Main camera</source>
        <translation>Caméra principale</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Front camera</source>
        <translation>Caméra frontale</translation>
    </message>
</context>
<context>
    <name>QMediaPlayer</name>
    <message>
        <location filename="../../qtmultimedia/src/multimedia/playback/qmediaplayer.cpp" line="+346"/>
        <source>Attempting to play invalid Qt resource</source>
        <translation>Tente de jouer une ressource Qt invalide</translation>
    </message>
    <message>
        <location line="+505"/>
        <source>The QMediaPlayer object does not have a valid service</source>
        <translation>L&apos;objet QMediaPlayer ne dispose pas d&apos;un service valide</translation>
    </message>
</context>
<context>
    <name>QMediaPlaylist</name>
    <message>
        <location filename="../../qtmultimedia/src/multimedia/playback/qmediaplaylist.cpp" line="+517"/>
        <location line="+61"/>
        <source>Could not add items to read only playlist.</source>
        <translation>Impossible d&apos;ajouter des éléments à une liste de lecture en lecture seule.</translation>
    </message>
    <message>
        <location line="-42"/>
        <location line="+61"/>
        <source>Playlist format is not supported</source>
        <translation>Le format de liste de lecture n&apos;est pas supporté</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>The file could not be accessed.</source>
        <translation>Impossible d&apos;accéder au fichier.</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Playlist format is not supported.</source>
        <translation>Le format de liste de lecture n&apos;est pas supporté.</translation>
    </message>
</context>
<context>
    <name>QMultimediaDeclarativeModule</name>
    <message>
        <location filename="../../qtmultimedia/src/imports/multimedia/multimedia.cpp" line="+88"/>
        <source>CameraCapture is provided by Camera</source>
        <translation>CameraCapture est fourni par Camera</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>CameraRecorder is provided by Camera</source>
        <translation>CameraRecorder est fourni par Camera</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>CameraExposure is provided by Camera</source>
        <translation>CameraExposure est fourni par Camera</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>CameraFocus is provided by Camera</source>
        <translation>CameraFocus est fourni par Camera</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>CameraFlash is provided by Camera</source>
        <translation>CameraFlash est fourni par Camera</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+17"/>
        <source>CameraImageProcessing is provided by Camera</source>
        <translation>CameraImageProcessing est fourni par Camera</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>CameraViewfinder is provided by Camera</source>
        <translation>CameraViewFinder est fourni par Camera</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>%1 playlist type is unknown</source>
        <translation type="vanished">Le type de playlist %1 est inconnu</translation>
    </message>
    <message>
        <source>invalid line in playlist file</source>
        <translation type="vanished">ligne invalide dans le fichier de playlist</translation>
    </message>
    <message>
        <source>Empty file provided</source>
        <translation type="vanished">Fichier vide fourni</translation>
    </message>
</context>
<context>
    <name>QPlaylistFileParser</name>
    <message>
        <location filename="../../qtmultimedia/src/multimedia/playback/playlistfileparser.cpp" line="+294"/>
        <source>%1 playlist type is unknown</source>
        <translation type="unfinished">Le type de playlist %1 est inconnu</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>invalid line in playlist file</source>
        <translation type="unfinished">ligne invalide dans le fichier de playlist</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Empty file provided</source>
        <translation type="unfinished">Fichier vide fourni</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>%1 does not exist</source>
        <translation>%1 n&apos;existe pas</translation>
    </message>
</context>
<context>
    <name>QWinRTCameraImageCaptureControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/winrt/qwinrtcameraimagecapturecontrol.cpp" line="+156"/>
        <source>Camera not ready</source>
        <translation>La caméra n&apos;est pas prête</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>Invalid photo data length.</source>
        <translation>La longueur des données photo est invalide.</translation>
    </message>
    <message>
        <location line="+74"/>
        <source>Image saving failed</source>
        <translation>Échec de l&apos;enregistrement de l&apos;image</translation>
    </message>
</context>
<context>
    <name>QWinRTImageEncoderControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/winrt/qwinrtimageencodercontrol.cpp" line="+60"/>
        <source>JPEG image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
