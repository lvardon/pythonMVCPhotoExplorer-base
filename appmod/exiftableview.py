#!/usr/bin/env python
#-*- coding: utf-8 -*-


"""
Tableview widget exif display
"""

import os.path
try:
    from PyQt5.QtCore import Qt, QRectF, pyqtSignal, QT_VERSION_STR, QPointF, QSizeF, QVariant, QAbstractTableModel, QSortFilterProxyModel
    from PyQt5.QtGui import QImage, QPixmap, QPainterPath, QTransform, QPainter, QFont, QColor, QBrush
    from PyQt5.QtWidgets import QGraphicsView, QGraphicsScene, QFileDialog, QApplication, QMenu, QFrame, QLabel, QDesktopWidget, QGraphicsTextItem, QGraphicsItem, QTableView
except ImportError:
    try:
        from PyQt4.QtCore import Qt, QRectF, pyqtSignal, QT_VERSION_STR
        from PyQt4.QtGui import QGraphicsView, QGraphicsScene, QImage, QPixmap, QPainterPath, QFileDialog, QMenu
    except ImportError:
        raise ImportError("exif_tableView: Requires PyQt5 or PyQt4.")


author__ = "moiàfai.com"
version__ = '0.1.0'

from applib import log
from applib import sqlite

from applib import setting


COL_exiflib = 0
COL_exifval = 1


class exiftableview(QTableView):


    def __init__(self , parent=None):

        super(exiftableview, self).__init__(parent)

        self.horizontalHeader().setStretchLastSection(True)
        self.clearSelection()
        self.setSortingEnabled(True)

    def setData(self, arrayData) :

        exifModel = exifTableModel(arrayData, self)
        self.setModel(exifModel)
        if len(arrayData) > 0 :
            self.resizeColumnToContents(COL_exifval )
        else :
            exifModel.removeRows(0, exifModel.rowCount(self))
        #self.horizontalHeader().show()

    def columnCount(self, parent):
        if len(self.headerdata) > 0:
            return len(self.headerdata)
        return 0



class exifTableModel(QAbstractTableModel):

    def __init__(self, datain, parent=None, *args):
        QAbstractTableModel.__init__(self, parent, *args)
        self.arraydata = datain
        self.headerdata = ["Tag exif", "Valeur" ]

    def rowCount(self, parent):
        return len(self.arraydata)

    def columnCount(self, parent):
        if len(self.arraydata) > 0 :
            return len(self.arraydata[0])
        else :
            return len(self.headerdata)

    def data(self, index, role):
        if not index.isValid():
            return QVariant()
        elif role != Qt.DisplayRole:
            return QVariant()
        return QVariant(self.arraydata[index.row()][index.column()])

    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return QVariant(self.headerdata[col])
