#!/usr/bin/env python
#-*- coding: utf-8 -*-

"""
Recherche de doublons
"""
import os

from PIL import Image
#from imagehash import imagehash
#import imagehash


# ok : import ImageChops
# ok : import ImageStat


from PyQt5.QtWidgets import QApplication, QStyle, QMenu, QMainWindow, QDialog, QFileDialog, QLabel,QLineEdit, QComboBox, QPushButton, QVBoxLayout, QMessageBox, QStyledItemDelegate, QItemDelegate, QTreeView, QListView, QAbstractItemView
from PyQt5.QtGui import QPainter, QIcon, QImage, QPalette, QPen, QBrush, QStandardItemModel, QStandardItem, QCursor, QDesktopServices, QColor, QPixmap, QFont
from PyQt5 import QtCore
from PyQt5.QtCore import Qt, QDir, QStorageInfo, QT_TRANSLATE_NOOP, QTranslator, QLocale, QSize, QPoint, QDateTime, QVariant, QDate, QAbstractTableModel, QAbstractListModel, QUrl, QSortFilterProxyModel,  QStandardPaths, QModelIndex, QItemSelectionModel
from PyQt5 import uic

from applib import log
from applib import setting
from applib import dircmd

from appmod.fileitem import fileItem, COL_selection, COL_thumb, COL_key, COL_ino, COL_fname, COL_dev, COL_ext, COL_islink, COL_linktarget, \
    COL_fsize, COL_fsizeh, COL_c_date, COL_m_date, COL_a_date, COL_access_w, COL_mime, COL_ftype, COL_crc32, COL_sha1, \
    COL_exifinfos, COL_isimmutable, COL_width, COL_height, COL_trtstatus, COL_trtlib

from appmod.fileitem import IMG_COMPARE_AVG, IMG_COMPARE_GRADIENT, IMG_COMPARE_PERCEPTUAL, IMG_COMPARE_WAVELET

SEARCH_BY_NAME  = 0
SEARCH_BY_NAME_CREATION_DATE = 1
SEARCH_BY_NAME_CREATION_DATE_EXIF = 2
SEARCH_BY_SHA1  = 3
SEARCH_BY_MD5  = 4
SEARCH_BY_CRC32  = 5
SEARCH_BY_HASH_AVG = 6
SEARCH_BY_HASH_GRADIENT = 7
SEARCH_BY_HASH_PERCEPTUAL = 8
SEARCH_BY_HASH_WAVELET = 9




class searchDuplicate( QDialog ) :

    def __init__(self, parent, initialdir, favDirList, fileList):

        super(searchDuplicate,self).__init__()

        self.parent = parent
        self.initialdir = initialdir
        self.favDirList = favDirList
        self.fileExt =  self.fileExt = setting.Setting.getListValue("app/fileExt")
        self.lastDstDir = setting.Setting.getValue("finddup/lastDstDir")

        self.sensitive = 50

        self.dupWindow = uic.loadUi('appmod/duplicate_view.ui', self)

        self.progressBar.hide()

        self.setWindowTitle("Recherche de doublons")

        self.ok_pushButton.clicked.connect(self.onOk)

        self.dupWindow.pushButtonSelectSrc.clicked.connect( self.onActionSelectSrcDir )

        self.dstDir = ""
        self.dupWindow.pushButtonSelectDst.clicked.connect( self.onActionSelectDstDir )

        self.dupWindow.pushButton_search.clicked.connect( self.onActionSearch )

        #self.dupWindow.listView_src.setViewMode(QListView.IconMode)

        self.dupWindow.listView_src.setSpacing( int(setting.Setting.getValue("finddup/cellSpacing", "12") ))

        self.dupWindow.listView_result.setSpacing( int(setting.Setting.getValue("finddup/cellSpacing", "12") ))

        """
        self.dupWindow.listView_src.setSpacing( int(setting.Setting.getValue("finddup/cellSpacing", "4") ))
        self.dupWindow.listView_src.setWordWrap(True)
        self.dupWindow.listView_src.setWrapping(True)
        self.dupWindow.listView_src.setFont(QFont(setting.Setting.getValue("finddup/fontName", "Arial"), int(setting.Setting.getValue("finddup/fontSize", "9"))))
        """

        self.dupWindow.lineEditSrc.setText(self.initialdir)

        self.dupMethod = "0"
        self.dupWindow.comboBox_method.addItem( "Par nom du fichier" , "0")
        self.dupWindow.comboBox_method.addItem( "Par nom et date de création fichier" , "1")
        self.dupWindow.comboBox_method.addItem( "Par nom et date de création exif" ,"2")
        self.dupWindow.comboBox_method.addItem( "Par Hash : Sha1", "3")
        self.dupWindow.comboBox_method.addItem( "Par Hash : Md5", "4")
        self.dupWindow.comboBox_method.addItem( "Par Hash : Crc32", "5")
        #self.dupWindow.comboBox_method.addItem( "Par contenu : dhash", "6")
        #self.dupWindow.comboBox_method.addItem( "Par contenu : ham", "7")
        self.dupWindow.comboBox_method.addItem( "Par contenu : hash moyen", "6")
        self.dupWindow.comboBox_method.addItem( "Par contenu : hash gradient", "7")
        self.dupWindow.comboBox_method.addItem( "Par contenu : hash perceptual", "8")
        self.dupWindow.comboBox_method.addItem( "Par contenu : hash wavelet", "9")

        self.dupWindow.comboBox_method.currentIndexChanged.connect(self.onComboBoxMethodChanged)

        self.dupWindow.comboBox_method.setCurrentIndex(self.dupWindow.comboBox_method.findData( str(self.dupMethod)))
        self.onComboBoxMethodChanged( self.dupWindow.comboBox_method.currentIndex() )
        self.dupWindow.comboBox_method.setMaximumSize(250,60)



        self.dupWindow.lineEdit_sensitive.setText( str(self.sensitive) )
        self.dupWindow.lineEdit_sensitive.editingFinished.connect(self.onSensitiveChanged)

        # --------------------- Source
        self.fileList = fileList
        #self.fileList = self.getfileListFromDir( self.initialdir )        # Datas, tableau de "filItems"

        self.listDataSrc = listData( self )

        self.listDataSrc.model = mylistModelSrc(self.fileList, self)     # QAbstractTableModel
        #self.listDataSrc.model.loadData(self.fileList)

        self.proxySrc = listViewSortProxySrc(self)                # QSortFilterProxyModel
        self.proxySrc.setSourceModel(self.listDataSrc.model)

        self.listDataSrc.setProxyModel( self.proxySrc )
        self.dupWindow.listView_src.setModel(self.proxySrc)    # QlistView

        self.dupWindow.listView_src.selectionModel().currentChanged.connect(self.onActionSrcSelectionChanged)

        # -------------------- Resultats
        self.dupWindow.lineEditDst.setText(self.lastDstDir)

        self.fileListResult = {}                                        # Dict de tableaux
        self.initSearchResults()

        self.listDataResult = listData( self )

        self.listDataResult.model = mylistModelDst( [], self)     # QAbstractTableModel

        self.proxyResult = listViewSortProxyDst(self)                # QSortFilterProxyModel
        self.proxyResult.setSourceModel(self.listDataResult.model)

        self.listDataResult.setProxyModel( self.proxyResult )
        self.dupWindow.listView_result.setModel(self.proxyResult)    # QlistView

        self.dupWindow.listView_result.setContextMenuPolicy(Qt.CustomContextMenu)
        self.dupWindow.listView_result.customContextMenuRequested.connect(self.menuContextImageDst)

        self.setWindowSetTitle( len(self.fileList), -1 )

    def onOk(self) :
        self.fileExt = setting.Setting.setValue("finddup/lastDstDir", self.dupWindow.lineEditDst.text() )
        self.close()

    def dialogShow( self) :
        log.Log.debug( log.INFO , "Duplicate view : showing window."  )
        self.dupWindow.exec_() # Modal

    def setWindowSetTitle(self, numberOfFiles, numberOfDup):
        """
        Affiche le titre de la fenêtre principale
        """
        labelWin = "Recherche de doublons pour : " + str(numberOfFiles) + " images. "
        if numberOfDup >= 0 :
            labelWin = labelWin + str(numberOfDup) + " doublons trouvés."

        self.setWindowTitle( labelWin )
        #self.setWindowTitle(titleMessage + " (" + str(numberOfFiles) + ")")

    def onComboBoxMethodChanged(self, index):
        #print (self.dupWindow.comboBox_method.itemData(index))
        if int(self.dupWindow.comboBox_method.itemData(index)) in (SEARCH_BY_NAME, SEARCH_BY_NAME_CREATION_DATE, SEARCH_BY_NAME_CREATION_DATE_EXIF, SEARCH_BY_SHA1, SEARCH_BY_MD5, SEARCH_BY_CRC32) :
            self.dupWindow.lineEdit_sensitive.setDisabled( True )
        else :
            self.dupWindow.lineEdit_sensitive.setDisabled( False )

    def onActionSrcSelectionChanged( self, current, previous ) :
        """
        Evènement : Selection d'une image source, affichage des doublons corespondants
        """
        indexlineSelected = current.row()
        if indexlineSelected >=0 and indexlineSelected < len(self.fileListResult) :
            self.dupWindow.listView_result.clearSelection()
            self.listDataResult.model.loadData( self.fileListResult[ str(indexlineSelected)] )

    def onSensitiveChanged(self) :
        if self.dupWindow.lineEdit_sensitive.isModified():
            try :
                self.sensitive = float(self.dupWindow.lineEdit_sensitive.text())
            except :
                self.sensitive = 50

        self.dupWindow.lineEdit_sensitive.setText( str(self.sensitive) )

    def initSearchResults( self) :
        """
        Initialisation du tableau des résultats
        """
        imaIndex = 0
        for fileItemElem in self.proxySrc.getDataListSorted( self.fileExt ) :
            self.fileListResult.update({ str(imaIndex) : [ ] })
            imaIndex = imaIndex + 1


    def onActionSearch (self) :
        """
        Evènement : executer la recherche
        """
        print ("      --- self.onActionSearch :")
        targetRootDir = self.dupWindow.lineEditDst.text()
        print ("in : ... " + targetRootDir)
        recurvive = self.dupWindow.checkBox_recursive.isChecked()
        print ("recurvive = " + str(recurvive))
        selectedMethod = self.dupWindow.comboBox_method.currentIndex()
        print ("method : ... " + str(selectedMethod) )
        sensitive = self.dupWindow.lineEdit_sensitive.text()
        print ("sensitive : ... " + str(sensitive) )

        if os.path.isdir(targetRootDir) is False :
            QMessageBox.critical(self, "Attention", "Renseignez le dossier dans lequel rechercher les doublons.", QMessageBox.Cancel)
            return

        if len(self.proxySrc.getDataListSorted( self.fileExt ) ) <= 0 :
            QMessageBox.critical(self, "Attention", "Fournir au moins une image pour laquelle rechercher les doublons.", QMessageBox.Cancel)
            return

        QApplication.setOverrideCursor(Qt.WaitCursor)

        # Raz du tableau des résultats
        self.initSearchResults()

        # Calcul du nombre de fichiers à comparer, afin de proposer une Progress Bar

        fileCount = 0
        if recurvive is True :
            for root, dirs, files in os.walk(targetRootDir):
                fileCount += len(files)
        else :
            fileCount = len(next(os.walk(targetRootDir))[2])

        print ("fileCount=" + str(fileCount))
        print ("src len = " + str(self.proxySrc.rowCount() ) )
        #self.progressBar.setMaximum(fileCount * len(self.fileList))
        self.progressBar.setMaximum(fileCount * self.proxySrc.rowCount())
        self.progressBar.setValue(0)
        self.progressBar.show()


        # Recherche des doublons pour chacun des fichiers source
        imaIndex = 0
        self.fileIndex = 0
        for fileItemElem in self.proxySrc.getDataListSorted( self.fileExt ) :
            print (" search : #" + str(imaIndex) + ": " + fileItemElem.fname   )
            #self.progressBar.setValue(imaIndex)
            self.onActionSearchRecurse (imaIndex, fileItemElem, targetRootDir , recurvive, selectedMethod, sensitive)
            #fileIndex = fileIndex + self.proxySrc.rowCount()

            imaIndex = imaIndex + 1


        # Selectionner la première image source
        self.dupWindow.listView_src.selectionModel().clear()
        index = self.listDataSrc.model.index(0, 0, QModelIndex())
        self.dupWindow.listView_src.selectionModel().select(index, QItemSelectionModel.Select )

        # Afficher les doublons correspondant à la première image de la liste
        self.listDataResult.model.loadData( self.fileListResult[ "0" ] )

        self.progressBar.hide()

        QApplication.restoreOverrideCursor()

    def onActionSearchRecurse (self, imaIndex, fileItemElem, targetDir, recurvive, selectedMethod, sensitive) :
        """
        Recherche resursive des doublons dans ce dossier, pour une image donnée
        """
        print ("Enter : " + targetDir )

        nbDupFound = 0

        thumbSizeWidth = int(setting.Setting.getValue("iconview/thumbSizeWidth", "100"))
        thumbSizeHeight  = int(setting.Setting.getValue("iconview/thumbSizeHeight", "100"))
        thumbQuality  = int(setting.Setting.getValue("iconview/thumbQuality", "30"))
        thumbCacheDir  = setting.Setting.getValue("iconview/thumbCacheDir", "./cache")
        overlayIconSizeW  = int(setting.Setting.getValue("iconview/overlayIconSizeW", "32"))
        overlayIconSizeH  = int(setting.Setting.getValue("iconview/overlayIconSizeH", "32"))
        overlayIConLink = QImage("icon/arrow_green.png")
        overlayIConLink = overlayIConLink.scaled( QSize(overlayIconSizeW, overlayIconSizeH), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.SmoothTransformation ) # or .Qt.SmoothTransformation   /  QtCore.Qt.FastTransformation
        overWrite = False

        for fileName in os.listdir(targetDir):

            fileNameFull = os.path.join(targetDir, fileName)

            # Traiter les sous-dossiers
            if os.path.isdir(fileNameFull):
                if recurvive is True :
                    self.onActionSearchRecurse ( imaIndex, fileItemElem, fileNameFull, recurvive, selectedMethod, sensitive)

            # Traiter les fichiers
            if os.path.isfile(fileNameFull):

                self.fileIndex = self.fileIndex + 1
                self.progressBar.setValue( self.fileIndex)
                similarity = -1

                compareResult = False
                #print( "check #" + str(self.fileIndex ) + " -" + fileItemElem.filePath + "/" + fileItemElem.fname + " with file : " + fileNameFull )
                #print( "check #" + str(self.fileIndex ) + " -" + fileItemElem.fname + " vs  " + targetfileItem.fname )

                # Ne pas comparer le fichier avec lui-même
                if fileItemElem.filePath + "/" + fileItemElem.fname == fileNameFull :
                    continue

                targetfileItem = fileItem(self, targetDir, fileName, self.fileExt, getExif=True)

                # Effectuer la comparaison selon la méthode retenue
                if selectedMethod == SEARCH_BY_NAME  :
                    if fileItemElem.fname == fileName :
                        similarity = 100
                        compareResult = True

                elif selectedMethod == SEARCH_BY_NAME_CREATION_DATE  :
                    if fileItemElem.fname == fileName :
                        if fileItemElem.c_date == targetfileItem.c_date :
                            similarity = 100
                            compareResult = True

                elif selectedMethod == SEARCH_BY_SHA1  :
                    if fileItemElem.get_sha1() == targetfileItem.get_sha1() :
                        similarity = 100
                        compareResult = True

                elif selectedMethod == SEARCH_BY_MD5  :
                    if fileItemElem.get_md5() == targetfileItem.get_md5() :
                        similarity = 100
                        compareResult = True

                elif selectedMethod == SEARCH_BY_CRC32  :
                    if fileItemElem.get_crc32() == targetfileItem.get_crc32() :
                        similarity = 100
                        compareResult = True

                elif selectedMethod == SEARCH_BY_NAME_CREATION_DATE_EXIF  :
                    if fileItemElem.fname == fileName :
                        if fileItemElem.get_ExifCreationDateTimeStamp() == targetfileItem.get_ExifCreationDateTimeStamp() :
                            similarity = 100
                            compareResult = True
                    """
                    elif selectedMethod == SEARCH_BY_DHASH  :
                        if fileItemElem.get_dhash == targetfileItem.get_dhash():
                            similarity = 100
                            compareResult = True

                    elif selectedMethod == SEARCH_BY_HAM  :
                        img1 = fileItemElem.filePath + "/" + fileItemElem.fname
                        img2 = targetfileItem.filePath + "/" + targetfileItem.fname
                        hash1 = self.avhash(img1)
                        hash2 = self.avhash(img2)
                        dist = self.hamming(hash1, hash2)
                        print ("hash(%s) = %d\nhash(%s) = %d\nhamming distance = %d\nsimilarity = %d%%" % (img1, hash1, img2, hash2, dist, (64 - dist) * 100 / 64))
                        if  (64 - dist) * 100 / 64 >= self.sensitive :
                            similarity = (64 - dist) * 100 / 64
                            compareResult = True
                            print ("***************** ok")
                    """
                elif selectedMethod == SEARCH_BY_HASH_AVG :
                    imgfile1 = fileItemElem.filePath + "/" + fileItemElem.fname
                    imgfile2 = targetfileItem.filePath + "/" + targetfileItem.fname
                    similarityValue = targetfileItem.imageDiffCompare(imgfile1, imgfile2, IMG_COMPARE_AVG)
                    if  similarityValue >= self.sensitive :
                        similarity = similarityValue
                        compareResult = True

                elif selectedMethod == SEARCH_BY_HASH_GRADIENT :
                    imgfile1 = fileItemElem.filePath + "/" + fileItemElem.fname
                    imgfile2 = targetfileItem.filePath + "/" + targetfileItem.fname
                    similarityValue = targetfileItem.imageDiffCompare(imgfile1, imgfile2, IMG_COMPARE_GRADIENT)
                    if  similarityValue >= self.sensitive :
                        similarity = similarityValue
                        compareResult = True

                elif selectedMethod == SEARCH_BY_HASH_PERCEPTUAL :
                    imgfile1 = fileItemElem.filePath + "/" + fileItemElem.fname
                    imgfile2 = targetfileItem.filePath + "/" + targetfileItem.fname
                    similarityValue = targetfileItem.imageDiffCompare(imgfile1, imgfile2, IMG_COMPARE_PERCEPTUAL)
                    if  similarityValue >= self.sensitive :
                        similarity = similarityValue
                        compareResult = True

                elif selectedMethod == SEARCH_BY_HASH_WAVELET :
                    imgfile1 = fileItemElem.filePath + "/" + fileItemElem.fname
                    imgfile2 = targetfileItem.filePath + "/" + targetfileItem.fname
                    similarityValue = targetfileItem.imageDiffCompare(imgfile1, imgfile2, IMG_COMPARE_WAVELET)
                    if  similarityValue >= self.sensitive :
                        similarity = similarityValue
                        compareResult = True

                # Si le résultat de la comparaison est positif : ajouter ce fichier à la liste des résultats
                # créer la vignette correspondand au doublon
                if compareResult is True :
                    nbDupFound = nbDupFound + 1
                    newFileItem = fileItem(self, targetDir, fileName, self.fileExt, getExif=True)
                    newFileItem.similarity = similarity

                    print ("Add : " + newFileItem.fname + " sim=" + str(newFileItem.similarity))

                    self.fileListResult.update({ str(imaIndex) : self.fileListResult[str(imaIndex)] + [ newFileItem ] })

                    newFileItem.createThumb(  thumbSizeWidth, thumbSizeHeight, thumbCacheDir, thumbQuality, overWrite,  overlayIConLink, overlayIconSizeW, overlayIconSizeH )
                    #self.createThumbForFile( newFileItem, thumbSizeWidth, thumbSizeHeight, thumbCacheDir, thumbQuality, overWrite,  overlayIConLink, overlayIconSizeW, overlayIconSizeH )

        self.setWindowSetTitle( len(self.fileList), nbDupFound )

        # Trier la liste des resultats par similarité
        print ("--- sort list for im index --- " + str(imaIndex))
        self.fileListResult[str(imaIndex)].sort( key=lambda x: x.similarity, reverse=True )
        imIdx = 0
        for imgDup in self.fileListResult[str(imaIndex)] :
            print (imgDup.fname + " sim=" + str(imgDup.similarity))
            imIdx = imIdx + 1
        print ("---" )

        """
        imIdx = 0
        for fileItemElem in self.proxySrc.getDataListSorted( self.fileExt ) :
            print ("--- sort --- " + str(imIdx))
            print (self.fileListResult[imIdx])
            self.fileListResult[imIdx].sort(key=lambda x: x.similarity, reverse=True)
            imIdx = imIdx + 1
        """
    def x_createThumbForFile( self, newFileItem , thumbSizeWidth, thumbSizeHeight, thumbCacheDir, thumbQuality, overWrite, overlayIConLink, overlayIconSizeW, overlayIconSizeH) :
        """
        Créer la vignette d'une image

        sudo apt-get install qt5-image-formats-plugins
        """
        linkAttr = ""
        if newFileItem.islink is True :
            linkAttr="_l"

        thumbFileName = newFileItem.key + linkAttr + ".JPG"
        overlayIConExt = QImage("icon/ext_"+newFileItem.ext+".png")
        overlayIConExt = overlayIConExt.scaled( QSize(overlayIconSizeW, overlayIconSizeH), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.FastTransformation ) # or .Qt.SmoothTransformation   /  QtCore.Qt.FastTransformation

        if not os.path.isfile( thumbCacheDir + thumbFileName ) or overWrite is True :

        #if overWrite is True or not os.path.isfile( thumbCacheDir + "/" + thumbFileName  )  :

            baseQimage = QImage( newFileItem.filePath + "/" + newFileItem.fname)
            if not baseQimage.isNull() :
                baseQimage = baseQimage.scaled( QSize(thumbSizeHeight, thumbSizeWidth), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.FastTransformation ) # or .Qt.SmoothTransformation   /  QtCore.Qt.FastTransformation

                overlayQimage = QImage( baseQimage.size(), QImage.Format_ARGB32_Premultiplied)

                painter = QPainter( overlayQimage )
                self.drawOverlayIconInit( painter , overlayQimage, baseQimage )

                if newFileItem.islink is True :
                    self.drawOverlayIcon( painter, overlayQimage.width() -32, overlayQimage.height()-32, overlayIConLink )

                self.drawOverlayIcon( painter, 0,0 , overlayIConExt )

                painter.end()

                overlayQimage.save( thumbCacheDir + "/" + thumbFileName  , "JPG", thumbQuality)
            else :
                log.Log.debug(log.INFO, "Erreur de création de vignettes (format incorrect): " +  pathName + "/" + fileName )

    def drawOverlayIconInit(self, painter, imageWithOverlay, baseImage ) :
        """
        Préparer le dessin d'une icône
        """
        painter.setCompositionMode(QPainter.CompositionMode_Source)
        painter.fillRect(imageWithOverlay.rect(), Qt.transparent)
        painter.setCompositionMode(QPainter.CompositionMode_SourceOver)
        painter.drawImage(0, 0, baseImage)

    def drawOverlayIcon(self, painter, posX, posY, overlayIConImage ) :
        """
        Dessiner une icône en surimpression
        """
        painter.setCompositionMode(QPainter.CompositionMode_SourceOver)
        painter.drawImage( posX , posY, overlayIConImage)

    def onActionSelectDstDir(self) :

        dstDir = ""

        dircmdWin = dircmd.dircmd(self)
        dstDir = dircmdWin.select( "Selectionnez le dossier destination", self.initialdir, self.favDirList )

        if dstDir != "" :

            self.dstDir = dstDir
            self.dupWindow.lineEditDst.setText(self.dstDir)
            # Raz du tableau des résultats

            self.fileListResult = {}
            self.initSearchResults()

            #self.listDataResult.model.layoutAboutToBeChanged.emit()
            #self.listDataResult.model.loadData( {} )
            #self.listDataResult.model.layoutChanged.emit()

            #self.listDataResult = listData( self )
            #self.listDataResult.model = mylistModelDst( [], self)     # QAbstractTableModel
            #self.listDataResult.model.loadData(self.listDataResult)



    def onActionSelectSrcDir(self) :

        srcDir = ""

        dircmdWin = dircmd.dircmd(self)
        srcDir = dircmdWin.select( "Selectionnez le dossier source", self.initialdir, self.favDirList )

        if srcDir != "" :

            self.srcDir = srcDir
            self.dupWindow.lineEditSrc.setText(self.srcDir)
            self.fileList = self.getfileListFromDir(self.srcDir )
            self.listDataSrc.model.loadData(self.fileList)
            self.setWindowSetTitle( len(self.fileList), -1 )

    def getfileListFromDir(self, pathName):

        QApplication.setOverrideCursor(Qt.WaitCursor)

        self.processCancel = False

        log.Log.debug(log.INFO, "read dir " + pathName)

        fileList = []
        if os.path.isdir(pathName):
            try:
                # Nombre de fichiers, afin de proposer une Progress Bar
                onlyfilesCount = len(next(os.walk(pathName))[2])
                self.progressBar.setMaximum(onlyfilesCount)
                self.progressBar.show()
                self.progressBar.setValue(0)
                barVal = 0

                files = os.listdir(pathName)
                log.Log.debug(log.INFO, "read dir start : " + str(onlyfilesCount) + " files.")
                for fileName in files:

                    QApplication.processEvents()
                    if self.processCancel == True :
                        return fileList

                    if os.path.isfile(pathName + "/" + fileName):
                        try :
                            newFileItem = fileItem(self, pathName, fileName, self.fileExt, getExif=True)
                            if newFileItem.ext in self.fileExt :
                                fileList.append(  newFileItem )

                        except Exception as e:
                            log.Log.debug(log.ERROR, "Error in fileItem : %s:" % e.args[0] + " / pathName=" + pathName)

                        barVal = barVal + 1
                        self.progressBar.setValue( barVal)
                        #self.splash.progressBarForward( barVal )
                        #self.splash.processEvent()

                log.Log.debug(log.INFO, "read dir done : " + str(len(fileList)) + " files.")

            except Exception as e:
                log.Log.debug(log.ERROR, "Error %s:" % e.args[0] + " / pathName=" + pathName)

            #finally:
        else :
            log.Log.debug(log.INFO, "non trouvé : " + pathName)

        self.progressBar.hide()
        QApplication.restoreOverrideCursor()

        return fileList


    def menuContextImageDst(self, point):
        """
        Affichage du menu contextuel pour les icônes et/ou fichiers
        """

        index = self.dupWindow.listView_result.indexAt(point)
        source_index = self.proxyResult.mapToSource(index)
        currentKeyValue = self.listDataResult.model.getDataLineKey( source_index.row() )
        newFileItem = self.listDataResult.model.getFileItemFromKey( currentKeyValue )
        if newFileItem == [] :
            return


        self.menuImage = QMenu(self)

        self.menuImage.clear()

        actionShowDet = self.menuImage.addAction('Détail : ' + newFileItem.fname)
        actionShowDet.triggered.connect(self.onActionShowDet)

        self.menuImage.addSeparator()
        actionRename = self.menuImage.addAction('Rename')
        actionDelete = self.menuImage.addAction('Delete')
        actionMove = self.menuImage.addAction('Move')

        self.menuImage.popup(self.dupWindow.listView_result.mapToGlobal(point))

    def onActionShowDet(self) :
        print ("onActionShowDet")


    def avhash(self, im):
        """
        if not isinstance(im, Image.Image):
            im = Image.open(im)
        im = im.resize((8, 8), Image.ANTIALIAS).convert('L')
        avg = reduce(lambda x, y: x + y, im.getdata()) / 64.
        return reduce(lambda x, (y, z): x | (z << y),
                      enumerate(map(lambda i: 0 if i < avg else 1, im.getdata())),
                      0)
        """
        return 0

    def hamming(self, h1, h2):
        h, d = 0, h1 ^ h2
        while d:
            h += 1
            d &= d - 1
        return h


"""
Implement specifics columns sort
"""

class listViewSortProxySrc(QSortFilterProxyModel):

    def __init__(self, parent=None):

        super(listViewSortProxySrc, self).__init__(parent)

        self.setFilterCaseSensitivity(Qt.CaseInsensitive)
        self.setSortCaseSensitivity(Qt.CaseInsensitive)
        self.setSortLocaleAware(True)

        self.colFilterList = {}

    def lessThan(self, left, right):

        col = left.column()

        dataleft = left.data()
        dataright = right.data()

        if col == COL_selection:
            dataleft = self.sourceModel().data(left, Qt.UserRole + 1)
            dataright = self.sourceModel().data(right, Qt.UserRole + 1)

        if col == COL_fname:
            dataleft = self.remove_accents(dataleft).lower()
            dataright = self.remove_accents(dataright).lower()

        elif col == COL_fsizeh:
            dataleft = self.sourceModel().data(left, Qt.UserRole + 1)
            dataright = self.sourceModel().data(right, Qt.UserRole + 1)
            pass

        elif col == COL_c_date:
            dataleft = self.sourceModel().data(left, Qt.UserRole + 1)
            dataright = self.sourceModel().data(right, Qt.UserRole + 1)
            pass

        return dataleft < dataright

    def remove_accents(self, input_str):
        import unicodedata
        import platform
        if platform.system() == "Windows":
            return unicodedata.normalize('NFD', input_str).encode('ascii', 'ignore')
        else:
            nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
            return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])

    def getColFilter (self , colIndex) :
        if colIndex in self.colFilterList :
            return self.colFilterList[colIndex]
        else :
            return ""

    def setColFilter (self , colIndex , strRegFilter) :
        self.colFilterList[colIndex] = strRegFilter

    def unSetColFilter (self , colIndex ) :
        if colIndex in self.colFilterList :
            del( self.colFilterList[colIndex])

    def filterAcceptsRow(self, source_row_number, source_parent):

        for colFilterKey  in self.colFilterList :
            indexData = self.sourceModel().index(source_row_number, colFilterKey)
            valueColCurrent = self.sourceModel().data(indexData, Qt.UserRole)
            expression = QRegExp(self.colFilterList[colFilterKey])
            index = expression.indexIn(str(valueColCurrent))
            if index < 0:
                return False

        return QSortFilterProxyModel.filterAcceptsRow(self, source_row_number, source_parent)

    # call : self.table_proxy_model.dumpSortedData()
    def dumpSortedData( self ) :

        print ("data sorted :::::::::")
        row = 0
        while row < self.rowCount() :

            indexData = self.index(row, COL_ino)
            ino = self.data(indexData, Qt.UserRole)
            #print ("ino = " + str(ino))

            indexData = self.index(row, COL_key)
            ino = self.data(indexData, Qt.UserRole)
            #print ("key = " + str(ino))

            fname = self.data( self.index(row, COL_fname) , Qt.UserRole)
            selection = self.data( self.index(row, COL_selection) , Qt.UserRole)
            fsizeh = self.data( self.index(row, COL_fsizeh) , Qt.UserRole)
            c_date = self.data( self.index(row, COL_c_date) , Qt.UserRole)

            print ( str(row) + " : #" + " -> fname=" + str(fname) + " selection=" + str(selection) + " fsizeh=" +str(fsizeh))

            row = row + 1

    def selectAll(self , fileExt= [] ) :

        row = 0
        while row < self.rowCount() :

            rowKey = self.data( self.index(row, COL_key), Qt.UserRole)
            indexItem = self.sourceModel().getFileItemFromKey(rowKey)
            if indexItem.ext in fileExt :
                indexItem.selection = True
                print ("select : "+ indexItem.fname )

            row = row + 1

    def getDataListSorted(self , fileExt= [] ) :

        self.itemListSorted = []

        row = 0
        while row < self.rowCount() :

            rowKey = self.data( self.index(row, COL_key), Qt.UserRole)
            #print (rowKey)

            indexItem = self.sourceModel().getFileItemFromKey(rowKey)
            #print (indexItem.fname)

            #if indexItem.ext in fileExt :
            self.itemListSorted.append( indexItem )
                #print ("from support, add  "+ indexItem.fname +"   rowKey = " +  str(rowKey))

            row = row + 1

        return self.itemListSorted

    def geFirstDataItemSorted( self , fileExt= []  ):
        """
        Retourne le premier Fileitem image dans le dossier
        """
        indexItem = None
        row = 0
        while row < self.rowCount() :

            indexKey = self.index(row, COL_key)
            rowKey = self.data(indexKey, Qt.UserRole)
            indexItem = self.sourceModel().getFileItemFromKey(rowKey)
            if indexItem.ext in fileExt :
                break
            row = row + 1

        return indexItem


class listViewSortProxyDst(QSortFilterProxyModel):

    def __init__(self, parent=None):

        super(listViewSortProxyDst, self).__init__(parent)

        self.setFilterCaseSensitivity(Qt.CaseInsensitive)
        self.setSortCaseSensitivity(Qt.CaseInsensitive)
        self.setSortLocaleAware(True)

        self.colFilterList = {}

    def lessThan(self, left, right):

        col = left.column()

        dataleft = left.data()
        dataright = right.data()

        if col == COL_selection:
            dataleft = self.sourceModel().data(left, Qt.UserRole + 1)
            dataright = self.sourceModel().data(right, Qt.UserRole + 1)

        if col == COL_fname:
            dataleft = self.remove_accents(dataleft).lower()
            dataright = self.remove_accents(dataright).lower()

        elif col == COL_fsizeh:
            dataleft = self.sourceModel().data(left, Qt.UserRole + 1)
            dataright = self.sourceModel().data(right, Qt.UserRole + 1)
            pass

        elif col == COL_c_date:
            dataleft = self.sourceModel().data(left, Qt.UserRole + 1)
            dataright = self.sourceModel().data(right, Qt.UserRole + 1)
            pass

        return dataleft < dataright

    def remove_accents(self, input_str):
        import unicodedata
        import platform
        if platform.system() == "Windows":
            return unicodedata.normalize('NFD', input_str).encode('ascii', 'ignore')
        else:
            nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
            return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])

    def getColFilter (self , colIndex) :
        if colIndex in self.colFilterList :
            return self.colFilterList[colIndex]
        else :
            return ""

    def setColFilter (self , colIndex , strRegFilter) :
        self.colFilterList[colIndex] = strRegFilter

    def unSetColFilter (self , colIndex ) :
        if colIndex in self.colFilterList :
            del( self.colFilterList[colIndex])

    def filterAcceptsRow(self, source_row_number, source_parent):

        for colFilterKey  in self.colFilterList :
            indexData = self.sourceModel().index(source_row_number, colFilterKey)
            valueColCurrent = self.sourceModel().data(indexData, Qt.UserRole)
            expression = QRegExp(self.colFilterList[colFilterKey])
            index = expression.indexIn(str(valueColCurrent))
            if index < 0:
                return False

        return QSortFilterProxyModel.filterAcceptsRow(self, source_row_number, source_parent)

    def dumpSortedData( self ) :

        print ("data sorted :::::::::")
        row = 0
        while row < self.rowCount() :

            indexData = self.index(row, COL_ino)
            ino = self.data(indexData, Qt.UserRole)
            #print ("ino = " + str(ino))

            indexData = self.index(row, COL_key)
            ino = self.data(indexData, Qt.UserRole)
            #print ("key = " + str(ino))

            fname = self.data( self.index(row, COL_fname) , Qt.UserRole)
            selection = self.data( self.index(row, COL_selection) , Qt.UserRole)
            fsizeh = self.data( self.index(row, COL_fsizeh) , Qt.UserRole)
            c_date = self.data( self.index(row, COL_c_date) , Qt.UserRole)

            print ( str(row) + " : #" + " -> fname=" + str(fname) + " selection=" + str(selection) + " fsizeh=" +str(fsizeh))

            row = row + 1

    def selectAll(self , fileExt= [] ) :

        row = 0
        while row < self.rowCount() :

            rowKey = self.data( self.index(row, COL_key), Qt.UserRole)
            indexItem = self.sourceModel().getFileItemFromKey(rowKey)
            if indexItem.ext in fileExt :
                indexItem.selection = True
                print ("select : "+ indexItem.fname )

            row = row + 1

    def getDataListSorted(self , fileExt= [] ) :

        self.itemListSorted = []

        row = 0
        while row < self.rowCount() :

            rowKey = self.data( self.index(row, COL_key), Qt.UserRole)
            #print (rowKey)

            indexItem = self.sourceModel().getFileItemFromKey(rowKey)
            #print (indexItem.fname)

            if indexItem.ext in fileExt :
                self.itemListSorted.append( indexItem )
                #print ("from support, add  "+ indexItem.fname +"   rowKey = " +  str(rowKey))

            row = row + 1

        return self.itemListSorted

    def geFirstDataItemSorted( self , fileExt= []  ):
        """
        Retourne le premier Fileitem image dans le dossier
        """
        indexItem = None
        row = 0
        while row < self.rowCount() :

            indexKey = self.index(row, COL_key)
            rowKey = self.data(indexKey, Qt.UserRole)
            indexItem = self.sourceModel().getFileItemFromKey(rowKey)
            if indexItem.ext in fileExt :
                break
            row = row + 1

        return indexItem


class mylistModelSrc(QAbstractTableModel):
    """
    Modèle de données des vues fichier détails et icones

    Qt.UserRole   : retourne la donnée brute (sans transformation)
    Qt.UserRole+1 : retourne la donnée pour le tri
    """
    def __init__(self, datain, parent=None):

        QAbstractTableModel.__init__(self, parent)
        self.arraydata = datain
        """
        self.headerdata = [ "COL_thumb", "COL_selection", "COL_key", "COL_ino", "COL_fname", "COL_dev", "COL_ext", "COL_islink",
                           "COL_linktarget", "COL_fsize", "COL_fsizeh", "COL_c_date", "COL_m_date", "COL_a_date",
                           "COL_access_w", "COL_mime", "COL_ftype", "COL_crc32", "COL_sha1", "COL_exifinfos", "COL_isimmutable" , "COL_width", "COL_height", "COL_trtstatus", "COL_trtlib"]
        """
        self.headerdata = setting.Setting.getListValue("listview/colHeaderNames")
        self.colFilterList = {}

        self.iconSizeWidth = int(setting.Setting.getValue("finddup/iconSizeWidth", "100"))
        self.iconSizeHeight = int(setting.Setting.getValue("finddup/iconSizeHeight", "100"))
        self.cellWidth = int(setting.Setting.getValue("finddup/cellWidth", "164"))
        self.cellHeight = int(setting.Setting.getValue("finddup/cellHeight", "128"))
        self.cellPadding = float(setting.Setting.getValue("finddup/cellPadding", "1.2"))

        self.iconSizeWidthIni = self.iconSizeWidth
        self.iconSizeHeightIni = self.iconSizeHeight
        self.cellWidthIni = self.cellWidth
        self.cellHeightIni = self.cellHeight

        self.thumbQuality  = int(setting.Setting.getValue("iconview/thumbQuality", "30"))
        self.thumbCacheDir  = setting.Setting.getValue("iconview/thumbCacheDir", "./cache")

        self.c_date_format_out = setting.Setting.getValue("listview/c_date_format_out", "dddd d MMMM yyyy hh:mm")
        self.m_date_format_out = setting.Setting.getValue("listview/c_date_format_out", "dddd d MMMM yyyy hh:mm")
        self.a_date_format_out = setting.Setting.getValue("listview/c_date_format_out", "dddd d MMMM yyyy hh:mm")

        self.fileExt =  self.fileExt = setting.Setting.getListValue("app/fileExt")

        self.setMode = 1

    def setColFilter (self , colIndex , filterActive = True) :
        self.colFilterList[colIndex] = filterActive

    def unSetColFilter (self , colIndex ) :
        if colIndex in self.colFilterList :
            del( self.colFilterList[colIndex])

    def zoom( self, zoomFactor) :

        self.layoutAboutToBeChanged.emit()

        self.iconSizeWidth = round(self.iconSizeWidth * zoomFactor)
        self.iconSizeHeight = round(self.iconSizeHeight * zoomFactor)

        font = QFont( setting.Setting.getValue("finddup/fontName", "Arial") , int(setting.Setting.getValue("finddup/fontSize", "10")))
        fm = QFontMetrics(font)
        #legendPixelsWide = fm.width(  setting.Setting.getValue("iconview/iconLabelFormat", "XXX XXXX XXXX") )
        legendPixelsHigh = fm.height()

        self.cellHeight = legendPixelsHigh + self.iconSizeHeight
        self.cellWidth = self.iconSizeWidth * self.cellPadding

        self.layoutChanged.emit()

    def zoomReset( self ) :

        self.layoutAboutToBeChanged.emit()

        self.iconSizeWidth = int(setting.Setting.getValue("finddup/iconSizeWidthInitial", "140"))
        self.iconSizeHeight = int(setting.Setting.getValue("finddup/iconSizeHeightInitial", "110"))

        self.cellHeight = int(setting.Setting.getValue("finddup/cellHeightInitial", "110"))
        self.cellWidth = int(setting.Setting.getValue("finddup/cellWidthInitial", "110"))

        self.layoutChanged.emit()

    def setViewMode(self, mode) :
        """
        Stockage du mode actuel de vue  : list (qtableview) or icons (qlistview)
        """
        self.setMode = mode

    def getViewMode(self) :
        """
        Retourne le mode actuel de vue  : list (qtableview) or icons (qlistview)
        """
        return self.setMode


    def getDataLineKey(self, row ) :
        """
        Retourne l'id (key) d'une ligne de données
        """
        if row >= 0 and row < self.rowCount(self) :
            return str(self.arraydata[row].key)
        else :
            return None

    def getDataByRowNumber(self, rowNumber) :
        return self.arraydata[ rowNumber ]

    def rowCount(self, parent):
        return len(self.arraydata)

    def columnCount(self, parent):
        if len(self.headerdata) > 0:
            return len(self.headerdata)
        return 0

    def data(self, index, role):

        if not index.isValid():
            return QVariant()

        if index.column() == COL_selection:

            if role == Qt.CheckStateRole :
                if self.arraydata[index.row()].selection is True :
                    return Qt.Checked
                else :
                    return Qt.Unchecked

            return QVariant()

        if index.column() == COL_thumb:

            if role == Qt.DisplayRole:
                #return self.arraydata[index.row()].iconLabel
                return self.arraydata[index.row()].fetch_label( setting.Setting.getValue("iconview/iconLabelFormat") )

            if role == Qt.UserRole:
                return self.arraydata[index.row()].ino

            if role == Qt.UserRole+1:
                return self.arraydata[index.row()].fname

            if role == Qt.DecorationRole:

                iconW = self.iconSizeWidth
                iconH = self.iconSizeHeight

                linkAttr = ""
                if self.arraydata[index.row()].islink is True :
                    linkAttr="_l"

                if os.path.isfile( self.thumbCacheDir + "/" + self.arraydata[index.row()].key + linkAttr + ".JPG"    ) :
                    pixmap = QPixmap(  "data/thumbs/" + self.arraydata[index.row()].key + linkAttr + ".JPG" , "JPG" )

                    #if self.arraydata[index.row()].access_w == False or  self.arraydata[index.row()].isimmutable == True  :
                    if self.arraydata[index.row()].isimmutable is True  :
                        pixmap_icon = QPixmap( QImage("icon/icon_lock.png") )
                        pixmap_icon = pixmap_icon.scaled( QSize(18, 18), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.FastTransformation )
                        painter = combined = QPainter(pixmap)
                        painter.setOpacity(0.8)
                        painter.drawPixmap(QPoint(pixmap.width() - 18,  0  ), pixmap_icon)
                        painter.end()

                    pixmap_resized = pixmap.scaled( QSize(iconH, iconW), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.FastTransformation )

                else :
                    if os.path.isfile( "icon/ext_" + self.arraydata[index.row()].ext + ".png"  ) :
                        pixmap = QPixmap("icon/ext_" + self.arraydata[index.row()].ext + ".png" , "PNG")
                        pixmap_resized = pixmap.scaled( QSize(iconH*0.4, iconW*0.4), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.FastTransformation )
                    else :
                        pixmap = QPixmap("icon/ext_generic.png" , "PNG")
                        pixmap_resized = pixmap.scaled( QSize(iconH*0.4, iconW*0.4), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.FastTransformation )

                return pixmap_resized

            if role == Qt.SizeHintRole:
                cellH = self.cellWidth
                cellW = self.cellHeight

                return QSize( cellW, cellH )

        if index.column() == COL_key:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].key
            if role == Qt.UserRole:
                return self.arraydata[index.row()].key

        if index.column() == COL_ino:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].ino
            if role == Qt.UserRole:
                return self.arraydata[index.row()].ino

        if index.column() == COL_fname:

            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].fname
            if role == Qt.UserRole:
                return self.arraydata[index.row()].fname
            if role == Qt.TextAlignmentRole:
                return Qt.AlignLeft | Qt.AlignVCenter

        if index.column() == COL_dev:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].dev
            if role == Qt.UserRole:
                return self.arraydata[index.row()].dev

        if index.column() == COL_fsize:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].fsize
            if role == Qt.UserRole:
                return self.arraydata[index.row()].fsize

        if index.column() == COL_fsizeh:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].fsizeh
            if role == Qt.UserRole:
                return self.arraydata[index.row()].fsizeh
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].fsize

        if index.column() == COL_ext:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].ext
            if role == Qt.UserRole:
                return self.arraydata[index.row()].ext

        if index.column() == COL_c_date:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].c_date.strftime( self.c_date_format_out )
            if role == Qt.UserRole:
                return self.arraydata[index.row()].c_date_stamp
            if role == Qt.UserRole+1:
                return self.arraydata[index.row()].c_date_stamp

        if index.column() == COL_m_date:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].m_date.strftime(  self.m_date_format_out )
            if role == Qt.UserRole:
                return self.arraydata[index.row()].m_date_stamp
            if role == Qt.UserRole+1:
                return self.arraydata[index.row()].m_date_stamp

        if index.column() == COL_a_date:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].a_date.strftime( self.a_date_format_out)
            if role == Qt.UserRole:
                return self.arraydata[index.row()].a_date_stamp
            if role == Qt.UserRole+1:
                return self.arraydata[index.row()].a_date_stamp

        if index.column() == COL_islink:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].islink
            if role == Qt.UserRole:
                return self.arraydata[index.row()].islink
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].islink

        if index.column() == COL_linktarget:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].linktarget
            if role == Qt.UserRole:
                return self.arraydata[index.row()].linktarget
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].linktarget

        if index.column() == COL_access_w:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].access_w
            if role == Qt.UserRole:
                return self.arraydata[index.row()].access_w
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].access_w

        if index.column() == COL_mime:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].mime
            if role == Qt.UserRole:
                return self.arraydata[index.row()].mime
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].mime

        if index.column() == COL_ftype:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].ftype
            if role == Qt.UserRole:
                return self.arraydata[index.row()].ftype
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].ftype

        if index.column() == COL_crc32:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].crc32
            if role == Qt.UserRole:
                return self.arraydata[index.row()].crc32
            if role == Qt.UserRole + 1 :
                return self.arraydata[index.row()].crc32

        if index.column() == COL_sha1:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].sha1
            if role == Qt.UserRole:
                return self.arraydata[index.row()].sha1
            if role == Qt.UserRole +1 :
                return self.arraydata[index.row()].sha1

        if index.column() == COL_exifinfos:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].exifinfos
            if role == Qt.UserRole:
                return self.arraydata[index.row()].exifinfos
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].exifinfos

        if index.column() == COL_isimmutable:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].isimmutable
            if role == Qt.UserRole:
                return self.arraydata[index.row()].isimmutable
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].isimmutable

        if index.column() == COL_width:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].width
            if role == Qt.UserRole:
                return self.arraydata[index.row()].width
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].width

        if index.column() == COL_height:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].height
            if role == Qt.UserRole:
                return self.arraydata[index.row()].height
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].height

        if index.column() == COL_trtstatus:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].trtStatus
            if role == Qt.UserRole:
                return self.arraydata[index.row()].trtStatus

        if index.column() == COL_trtlib:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].trtLib
            if role == Qt.UserRole:
                return self.arraydata[index.row()].trtLib

        return QVariant()

    def flags(self, index):

        if not index.isValid():
            return None

        #if index.column() == COL_selection or index.column() == COL_thumb:
        #    if self.arraydata[index.row()].ext in self.fileExt :
        #        return Qt.ItemIsEnabled  | Qt.ItemIsUserCheckable

        return Qt.ItemIsEnabled | Qt.ItemIsSelectable

    def headerData(self, col, orientation, role):

        if self.colFilterList is not None :
            for colFilterKey in self.colFilterList :
                if col == colFilterKey :
                    if orientation == Qt.Horizontal and role == Qt.DecorationRole:
                        return QPixmap("icon/filter_blue.png")

        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return QVariant(self.headerdata[col])

        return QVariant()

    def sort(self, Ncol, order):
        try:
            self.layoutAboutToBeChanged.emit()
            self.arraydata = sorted(self.arraydata, key=operator.itemgetter(Ncol))
            if order == Qt.DescendingOrder:
                self.arraydata.reverse()
            self.layoutChanged.emit()

        except TypeError as e:
            print('[*] Sort on empty column')


    def loadData(self, fileData):
        """
        Charge les données et actualise l'affichage
        """
        self.layoutAboutToBeChanged.emit()
        self.arraydata = fileData
        self.layoutChanged.emit()


    def getFileItemFromKey(self, idElem ):
        """
        Retourne le Fileitem pour un id (key) donné.
        """
        for itemElem in self.arraydata:
            if itemElem.key == idElem:
                return itemElem
        return []


class mylistModelDst(QAbstractTableModel):
    """
    Modèle de données des vues fichier détails et icones

    Qt.UserRole   : retourne la donnée brute (sans transformation)
    Qt.UserRole+1 : retourne la donnée pour le tri
    """
    def __init__(self, datain, parent=None):

        QAbstractTableModel.__init__(self, parent)
        self.arraydata = datain

        self.headerdata = setting.Setting.getListValue("listview/colHeaderNames")
        self.colFilterList = {}

        self.iconSizeWidth = int(setting.Setting.getValue("finddup/iconSizeWidth", "100"))
        self.iconSizeHeight = int(setting.Setting.getValue("finddup/iconSizeHeight", "100"))
        self.cellWidth = int(setting.Setting.getValue("finddup/cellWidth", "164"))
        self.cellHeight = int(setting.Setting.getValue("finddup/cellHeight", "128"))
        self.cellPadding = float(setting.Setting.getValue("finddup/cellPadding", "1.2"))

        self.iconSizeWidthIni = self.iconSizeWidth
        self.iconSizeHeightIni = self.iconSizeHeight
        self.cellWidthIni = self.cellWidth
        self.cellHeightIni = self.cellHeight

        self.thumbQuality  = int(setting.Setting.getValue("iconview/thumbQuality", "30"))
        self.thumbCacheDir  = setting.Setting.getValue("iconview/thumbCacheDir", "./cache")

        self.c_date_format_out = setting.Setting.getValue("listview/c_date_format_out", "dddd d MMMM yyyy hh:mm")
        self.m_date_format_out = setting.Setting.getValue("listview/c_date_format_out", "dddd d MMMM yyyy hh:mm")
        self.a_date_format_out = setting.Setting.getValue("listview/c_date_format_out", "dddd d MMMM yyyy hh:mm")

        self.fileExt =  self.fileExt = setting.Setting.getListValue("app/fileExt")

        self.setMode = 1

    def setColFilter (self , colIndex , filterActive = True) :
        self.colFilterList[colIndex] = filterActive

    def unSetColFilter (self , colIndex ) :
        if colIndex in self.colFilterList :
            del( self.colFilterList[colIndex])

    def zoom( self, zoomFactor) :

        self.layoutAboutToBeChanged.emit()

        self.iconSizeWidth = round(self.iconSizeWidth * zoomFactor)
        self.iconSizeHeight = round(self.iconSizeHeight * zoomFactor)

        font = QFont( setting.Setting.getValue("finddup/fontName", "Arial") , int(setting.Setting.getValue("finddup/fontSize", "10")))
        fm = QFontMetrics(font)
        #legendPixelsWide = fm.width(  setting.Setting.getValue("iconview/iconLabelFormat", "XXX XXXX XXXX") )
        legendPixelsHigh = fm.height()

        self.cellHeight = legendPixelsHigh + self.iconSizeHeight
        self.cellWidth = self.iconSizeWidth * self.cellPadding

        self.layoutChanged.emit()

    def zoomReset( self ) :

        self.layoutAboutToBeChanged.emit()

        self.iconSizeWidth = int(setting.Setting.getValue("finddup/iconSizeWidthInitial", "140"))
        self.iconSizeHeight = int(setting.Setting.getValue("finddup/iconSizeHeightInitial", "110"))

        self.cellHeight = int(setting.Setting.getValue("finddup/cellHeightInitial", "110"))
        self.cellWidth = int(setting.Setting.getValue("finddup/cellWidthInitial", "110"))

        self.layoutChanged.emit()

    def setViewMode(self, mode) :
        """
        Stockage du mode actuel de vue  : list (qtableview) or icons (qlistview)
        """
        self.setMode = mode

    def getViewMode(self) :
        """
        Retourne le mode actuel de vue  : list (qtableview) or icons (qlistview)
        """
        return self.setMode


    def getDataLineKey(self, row ) :
        """
        Retourne l'id (key) d'une ligne de données
        """
        if row >= 0 and row < self.rowCount(self) :
            return str(self.arraydata[row].key)
        else :
            return None

    def getDataByRowNumber(self, rowNumber) :
        return self.arraydata[ rowNumber ]

    def rowCount(self, parent):
        return len(self.arraydata)

    def columnCount(self, parent):
        if len(self.headerdata) > 0:
            return len(self.headerdata)
        return 0

    def data(self, index, role):

        if not index.isValid():
            return QVariant()

        if index.column() == COL_selection:

            if role == Qt.CheckStateRole :
                if self.arraydata[index.row()].selection is True :
                    return Qt.Checked
                else :
                    return Qt.Unchecked

            return QVariant()

        if index.column() == COL_thumb:

            if role == Qt.DisplayRole:
                # return self.arraydata[index.row()].iconLabel
                # self.iconLabel = self.fetch_label( setting.Setting.getValue("iconview/iconLabelFormat") )
                return self.arraydata[index.row()].fetch_label( setting.Setting.getValue("finddup/iconLabelFormat") )


            if role == Qt.UserRole:
                return self.arraydata[index.row()].ino

            if role == Qt.UserRole+1:
                return self.arraydata[index.row()].fname

            if role == Qt.DecorationRole:

                iconW = self.iconSizeWidth
                iconH = self.iconSizeHeight

                linkAttr = ""
                if self.arraydata[index.row()].islink is True :
                    linkAttr="_l"

                if os.path.isfile( self.thumbCacheDir + "/" + self.arraydata[index.row()].key + linkAttr + ".JPG"    ) :
                    pixmap = QPixmap(  "data/thumbs/" + self.arraydata[index.row()].key + linkAttr + ".JPG" , "JPG" )

                    #if self.arraydata[index.row()].access_w == False or  self.arraydata[index.row()].isimmutable == True  :
                    if self.arraydata[index.row()].isimmutable is True  :
                        pixmap_icon = QPixmap( QImage("icon/icon_lock.png") )
                        pixmap_icon = pixmap_icon.scaled( QSize(18, 18), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.FastTransformation )
                        painter = combined = QPainter(pixmap)
                        painter.setOpacity(0.8)
                        painter.drawPixmap(QPoint(pixmap.width() - 18,  0  ), pixmap_icon)
                        painter.end()

                    pixmap_resized = pixmap.scaled( QSize(iconH, iconW), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.FastTransformation )

                else :
                    if os.path.isfile( "icon/ext_" + self.arraydata[index.row()].ext + ".png"  ) :
                        pixmap = QPixmap("icon/ext_" + self.arraydata[index.row()].ext + ".png" , "PNG")
                        pixmap_resized = pixmap.scaled( QSize(iconH*0.4, iconW*0.4), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.FastTransformation )
                    else :
                        pixmap = QPixmap("icon/ext_generic.png" , "PNG")
                        pixmap_resized = pixmap.scaled( QSize(iconH*0.4, iconW*0.4), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.FastTransformation )

                return pixmap_resized

            if role == Qt.SizeHintRole:
                cellH = self.cellWidth
                cellW = self.cellHeight

                return QSize( cellW, cellH )

        if index.column() == COL_key:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].key
            if role == Qt.UserRole:
                return self.arraydata[index.row()].key

        if index.column() == COL_ino:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].ino
            if role == Qt.UserRole:
                return self.arraydata[index.row()].ino

        if index.column() == COL_fname:

            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].fname
            if role == Qt.UserRole:
                return self.arraydata[index.row()].fname
            if role == Qt.TextAlignmentRole:
                return Qt.AlignLeft | Qt.AlignVCenter

        if index.column() == COL_dev:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].dev
            if role == Qt.UserRole:
                return self.arraydata[index.row()].dev

        if index.column() == COL_fsize:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].fsize
            if role == Qt.UserRole:
                return self.arraydata[index.row()].fsize

        if index.column() == COL_fsizeh:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].fsizeh
            if role == Qt.UserRole:
                return self.arraydata[index.row()].fsizeh
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].fsize

        if index.column() == COL_ext:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].ext
            if role == Qt.UserRole:
                return self.arraydata[index.row()].ext

        if index.column() == COL_c_date:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].c_date.strftime( self.c_date_format_out )
            if role == Qt.UserRole:
                return self.arraydata[index.row()].c_date_stamp
            if role == Qt.UserRole+1:
                return self.arraydata[index.row()].c_date_stamp

        if index.column() == COL_m_date:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].m_date.strftime(  self.m_date_format_out )
            if role == Qt.UserRole:
                return self.arraydata[index.row()].m_date_stamp
            if role == Qt.UserRole+1:
                return self.arraydata[index.row()].m_date_stamp

        if index.column() == COL_a_date:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].a_date.strftime( self.a_date_format_out)
            if role == Qt.UserRole:
                return self.arraydata[index.row()].a_date_stamp
            if role == Qt.UserRole+1:
                return self.arraydata[index.row()].a_date_stamp

        if index.column() == COL_islink:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].islink
            if role == Qt.UserRole:
                return self.arraydata[index.row()].islink
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].islink

        if index.column() == COL_linktarget:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].linktarget
            if role == Qt.UserRole:
                return self.arraydata[index.row()].linktarget
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].linktarget

        if index.column() == COL_access_w:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].access_w
            if role == Qt.UserRole:
                return self.arraydata[index.row()].access_w
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].access_w

        if index.column() == COL_mime:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].mime
            if role == Qt.UserRole:
                return self.arraydata[index.row()].mime
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].mime

        if index.column() == COL_ftype:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].ftype
            if role == Qt.UserRole:
                return self.arraydata[index.row()].ftype
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].ftype

        if index.column() == COL_crc32:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].crc32
            if role == Qt.UserRole:
                return self.arraydata[index.row()].crc32
            if role == Qt.UserRole + 1 :
                return self.arraydata[index.row()].crc32

        if index.column() == COL_sha1:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].sha1
            if role == Qt.UserRole:
                return self.arraydata[index.row()].sha1
            if role == Qt.UserRole +1 :
                return self.arraydata[index.row()].sha1

        if index.column() == COL_exifinfos:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].exifinfos
            if role == Qt.UserRole:
                return self.arraydata[index.row()].exifinfos
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].exifinfos

        if index.column() == COL_isimmutable:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].isimmutable
            if role == Qt.UserRole:
                return self.arraydata[index.row()].isimmutable
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].isimmutable

        if index.column() == COL_width:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].width
            if role == Qt.UserRole:
                return self.arraydata[index.row()].width
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].width

        if index.column() == COL_height:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].height
            if role == Qt.UserRole:
                return self.arraydata[index.row()].height
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].height

        if index.column() == COL_trtstatus:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].trtStatus
            if role == Qt.UserRole:
                return self.arraydata[index.row()].trtStatus

        if index.column() == COL_trtlib:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].trtLib
            if role == Qt.UserRole:
                return self.arraydata[index.row()].trtLib

        return QVariant()

    def flags(self, index):

        if not index.isValid():
            return None

        #if index.column() == COL_selection or index.column() == COL_thumb:
        #    if self.arraydata[index.row()].ext in self.fileExt :
        #        return Qt.ItemIsEnabled  | Qt.ItemIsUserCheckable

        return Qt.ItemIsEnabled | Qt.ItemIsSelectable

    def headerData(self, col, orientation, role):

        if self.colFilterList is not None :
            for colFilterKey in self.colFilterList :
                if col == colFilterKey :
                    if orientation == Qt.Horizontal and role == Qt.DecorationRole:
                        return QPixmap("icon/filter_blue.png")

        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return QVariant(self.headerdata[col])

        return QVariant()

    def sort(self, Ncol, order):
        try:
            self.layoutAboutToBeChanged.emit()
            self.arraydata = sorted(self.arraydata, key=operator.itemgetter(Ncol))
            if order == Qt.DescendingOrder:
                self.arraydata.reverse()
            self.layoutChanged.emit()

        except TypeError as e:
            print('[*] Sort on empty column')


    def loadData(self, fileData):
        """
        Charge les données et actualise l'affichage
        """
        self.layoutAboutToBeChanged.emit()
        self.arraydata = fileData
        self.layoutChanged.emit()


    def getFileItemFromKey(self, idElem ):
        """
        Retourne le Fileitem pour un id (key) donné.
        """
        for itemElem in self.arraydata:
            if itemElem.key == idElem:
                return itemElem
        return []


class listData( object ) :

    def __init__(self, parent):

        self.parent = parent
        self.clear()
        self.proxy = None

    def setProxyModel(self, list_proxy_model) :
        #self.list_proxy_model = list_proxy_model
        self.proxy = list_proxy_model

    def clear(self) :
        self.model = None
        self.proxy = None

    def setTableModel( self, tablemodel) :
        self.model = tablemodel

    def getFileCountByExt( self, extSearch, caseSensitive = False ) :

        proxyModel = self.proxy
        if caseSensitive is False :
            extSearch = extSearch.upper()

        rowRet = 0
        row = 0
        while row < self.list_proxy_model.rowCount() :

            indexData = self.list_proxy_model.index(row, COL_ext)
            ext = self.list_proxy_model.data(indexData, Qt.UserRole)

            if caseSensitive is False :
                if ext.upper() == extSearch :
                    rowRet = rowRet + 1
            else :
                if ext == extSearch :
                    rowRet = rowRet + 1


            row = row + 1

        return rowRet





