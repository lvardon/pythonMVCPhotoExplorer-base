# -*- coding: utf-8 -*-

"""
Manipulations de fichiers : renomer, supprimer ...

"""

#from PyQt5.QtWidgets import QMessageBox

import os
import os.path
import subprocess

from applib import log
from applib import setting

class filecmd( object ) :

    def __init__(self, parent  ) :

        self.parent = parent

        self.fileRenameCmd = openwithValues = setting.Setting.getValue("filecommand/fileRename")
        self.fileDelCmd = openwithValues = setting.Setting.getValue("filecommand/fileDel")
        self.fileMoveCmd = openwithValues = setting.Setting.getValue("filecommand/fileMove")
        self.fileCopyCmd = openwithValues = setting.Setting.getValue("filecommand/fileCopy")

        self.subprocess_err = ""
        self.subprocess_returncode = None

    def formatCmd( self, cmdExec, srcdir, filename , dstdir = "", filenamenew="" ) :

        cmdExec = cmdExec.replace( "[srcdir]", srcdir )
        cmdExec = cmdExec.replace( "[filename]", filename )
        cmdExec = cmdExec.replace( "[dstdir]", dstdir )
        cmdExec = cmdExec.replace( "[filenamenew]", filenamenew )
        return cmdExec

    def remove( self, srcdir, filename  ) :

        self.subprocess_err = ""
        self.subprocess_returncode = None

        print ("1" + srcdir )
        print ("2" + filename)
        print ("Command = " + self.fileDelCmd)

        self.cmdExec = self.formatCmd( self.fileDelCmd , srcdir, filename , "", "" )

        try:

            for line in self.run_command(self.cmdExec):
                print ("process : " + line )

            if self.subprocess_returncode != 0 :
                return False

        except Exception as e:
            log.Log.debug(log.INFO, "Erreur : " + str(e.args[0]))
            return False

        return True

    def copy( self, srcdir, filename , dstdir, filenamenew , overWriteExisting = False) :

        self.subprocess_err = ""
        self.subprocess_returncode = None

        uniqFileName = self.getUniqFileName( dstdir  + "/" +  filenamenew )
        print ("1" + srcdir )
        print ("2" + filename)
        print ("3" + dstdir)
        print ("4" + os.path.basename(uniqFileName))
        print ("Command = " + self.fileCopyCmd)

        if overWriteExisting == False :
            self.cmdExec = self.formatCmd( self.fileCopyCmd , srcdir, filename , dstdir, os.path.basename(uniqFileName) )
        else :
            self.cmdExec = self.formatCmd( self.fileCopyCmd , srcdir, filename , dstdir, filenamenew )

        try:

            for line in self.run_command(self.cmdExec):
                print ("process : " + line )

            if self.subprocess_returncode != 0 :
                return False

        except Exception as e:
            log.Log.debug(log.INFO, "Erreur : " + str(e.args[0]))
            return False

        return True

    def move( self, srcdir, filename , dstdir, filenamenew) :

        self.subprocess_err = ""
        self.subprocess_returncode = None

        uniqFileName = self.getUniqFileName( dstdir  + "/" +  filenamenew )
        print ("1" + srcdir )
        print ("2" + filename)
        print ("3" + dstdir)
        print ("4" + os.path.basename(uniqFileName))
        print ("Command = " + self.fileMoveCmd)

        self.cmdExec = self.formatCmd( self.fileMoveCmd , srcdir, filename , dstdir, os.path.basename(uniqFileName) )

        try:

            for line in self.run_command(self.cmdExec):
                print ("process : " + line )

            if self.subprocess_returncode != 0 :
                return False

        except Exception as e:
            log.Log.debug(log.INFO, "Erreur : " + str(e.args[0]))
            return False

        return True

    def getUniqFileName(self, path):
        """
        Compose un nom de fichier unique, à partir d'un nom de fichier donné
        """
        path      = os.path.expanduser(path)

        if not os.path.exists(path):
            return path

        root, ext = os.path.splitext(os.path.expanduser(path))
        dir       = os.path.dirname(root)
        fname     = os.path.basename(root)
        candidate = fname+ext
        index     = 1
        ls        = set(os.listdir(dir))
        while candidate in ls:
             candidate = "{}_{}{}".format(fname,index,ext)
             index    += 1
        return os.path.join(dir,candidate)

    def run_command(self, command):
        """
        Execute une commande Os
        """
        self.subprocess_err = ""
        self.subprocess_returncode = None

        p = subprocess.Popen(command,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             shell=True)
        # Read stdout from subprocess until the buffer is empty !
        for line in iter(p.stdout.readline, b''):
            if line: # Don't print blank lines
                yield line

        # This ensures the process has completed, AND sets the 'returncode' attr
        while p.poll() is None:
            time.sleep(.1) #Don't waste CPU-cycles

        # Empty STDERR buffer
        err = p.stderr.read()

        self.subprocess_returncode = p.returncode

        if p.returncode != 0:
           # The run_command() function is responsible for logging STDERR
            log.Log.debug(log.ERROR, "Error %s:" % err + " / cmd=" + command)
            self.subprocess_err = err
