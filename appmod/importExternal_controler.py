#!/usr/bin/env python
#-*- coding: utf-8 -*-

import os
import fnmatch
import subprocess
import time

from PyQt5.QtWidgets import QApplication, QStyle, QMenu, QMainWindow, QDialog, QFileDialog, QLabel,QLineEdit,  QComboBox, QPushButton, QVBoxLayout, QMessageBox, QStyledItemDelegate, QItemDelegate, QTreeView
from PyQt5.QtGui import QIcon, QPalette, QPen, QBrush, QStandardItemModel, QStandardItem, QCursor, QDesktopServices, QColor, QPixmap
from PyQt5 import QtCore
from PyQt5.QtCore import Qt, QDir,  QStorageInfo, QT_TRANSLATE_NOOP, QTranslator, QLocale, QSize, QPoint, QDateTime, QVariant, QDate, QAbstractTableModel, QUrl, QSortFilterProxyModel,  QStandardPaths
from PyQt5 import uic

from applib import log
from applib import setting
from applib import dircmd

from appmod.fileitem import fileItem, COL_selection, COL_thumb, COL_key, COL_ino, COL_fname, COL_dev, COL_ext, COL_islink, COL_linktarget, \
    COL_fsize, COL_fsizeh, COL_c_date, COL_m_date, COL_a_date, COL_access_w, COL_mime, COL_ftype, COL_crc32, COL_sha1, \
    COL_exifinfos, COL_isimmutable, COL_width, COL_height, COL_trtstatus, COL_trtlib

from appmod.fileitem import TRT_STATUS_WAIT, TRT_STATUS_ERROR, TRT_STATUS_OK, TRT_STATUS_IGNORE, TRT_STATUS_IGNORE_NOCMD, TRT_STATUS_IGNORE_EXISTS, TRT_STATUS_WARNING

class importExternal( QDialog ) :
    """
    Dialogue d'importation/copie de fichiers depuis un support externe
    """

    def __init__(self, parent, initialDir, favDirList):

        super(importExternal,self).__init__(parent)

        self.parent = parent
        self.initialDir = initialDir
        self.favDirList = favDirList
        self.DetailWindow = uic.loadUi('appmod/importExternal_view.ui', self)

        self.setWindowTitle("Importation de fichiers")

        self.DetailWindow.pushButtonOk.clicked.connect(self.onOk)
        self.DetailWindow.pushButtonCopy.clicked.connect(self.onCopy)
        self.DetailWindow.pushButtonCheck.clicked.connect(self.onCheck)

        self.DetailWindow.pushButtonSelectSrc.clicked.connect( self.onActionSelectSrcDir )
        self.DetailWindow.pushButtonSelectDst.clicked.connect( self.onActionSelectDstDir )

        self.DetailWindow.lineEditDest.textChanged.connect(self.onDstDirChanged)
        self.DetailWindow.lineEditSrc.textChanged.connect(self.onSrcDirChanged)

        self.DetailWindow.lineEditPassword.setEchoMode(QLineEdit.Password)

        self.DetailWindow.tableViewSrc.horizontalHeader().setStretchLastSection(True)
        self.DetailWindow.tableViewSrc.setSortingEnabled(True)
        self.DetailWindow.radioButton_Md5.setChecked(True)

        self.subprocess_err = ""
        self.subprocess_returncode = None

        # Paramètres d'importation
        self.fileExt = setting.Setting.getListValue("app/fileExt")

        self.srcDir = setting.Setting.getValue("importimage/srcDir", "/")
        self.dstDir = setting.Setting.getValue("importimage/dstDir", "/")

        # Commandes pour protéger / déproteger les images et dossiers
        self.commandBeforeCopy = setting.Setting.getValue("importimage/commandBeforeCopy", "")
        self.commandAfterCopy = setting.Setting.getValue("importimage/commandAfterCopy", "")

        # Commandes de copie des fichiers, par extention
        self.copyExtCommand = {
                                "NEF" : setting.Setting.getValue("importimage/commandCopyNEF", "")  ,
                                "JPG" : setting.Setting.getValue("importimage/commandCopyJPG", "") ,
                                "jpg" : setting.Setting.getValue("importimage/commandCopyjpg", ""),
                                "tiff" : setting.Setting.getValue("importimage/commandCopytiff", "")
                                }

        # Onglet paramètres
        paramHeader = ["Type de fichier", "Commande de copie/importation"]

        paramTableData =[]
        for paramKey in  self.copyExtCommand :
            paramTableData.append(  [ paramKey, self.copyExtCommand[ paramKey ] ] )

        self.DetailWindow.lineEdit_checkTemplate.setText(setting.Setting.getValue("importimage/fileCheckTemplate", "") )
        self.checkTemplate = self.DetailWindow.lineEdit_checkTemplate.text( )

        self.paramModel = paramTabledata(paramTableData, paramHeader)
        self.DetailWindow.tableView_param.setModel(self.paramModel)
        self.DetailWindow.tableView_param.resizeColumnToContents(0)

        self.DetailWindow.textEdit_paramBeforeCommand.setText( self.commandBeforeCopy )
        self.DetailWindow.textEdit_paramAfterCommand.setText( self.commandAfterCopy )

        # Charger la liste des fichiers présent sur le support
        self.loadFileList()

        self.DetailWindow.tabWidget.currentChanged.connect(self.onChangeTab)

    def onChangeTab(self, tabIndex ) :

        if tabIndex == 0 :
            self.commandBeforeCopy = self.DetailWindow.textEdit_paramBeforeCommand.toPlainText()
            setting.Setting.setValue("importimage/commandBeforeCopy" , self.commandBeforeCopy)

            self.commandAfterCopy = self.DetailWindow.textEdit_paramAfterCommand.toPlainText( )
            setting.Setting.setValue("importimage/commandAfterCopy" , self.commandAfterCopy)

            self.checkTemplate = self.DetailWindow.lineEdit_checkTemplate.text( )
            setting.Setting.setValue("importimage/fileCheckTemplate" , self.checkTemplate)

            row = 0
            while row < self.paramModel.rowCount(self)  :
                col = 0
                copyExt =  self.paramModel.data(self.paramModel.index(row, 0), Qt.DisplayRole)
                copyCmd =  self.paramModel.data(self.paramModel.index(row, 1), Qt.DisplayRole)
                setting.Setting.setValue("importimage/commandCopy" + copyExt, copyCmd)
                self.copyExtCommand[ copyExt ] = copyCmd
                col = col + 1
                row = row + 1

    def onDstDirChanged(self) :

        if not os.path.isdir(self.DetailWindow.lineEditDest.text()):
            self.DetailWindow.pushButtonOk.setEnabled(False)
        else :
            self.DetailWindow.pushButtonOk.setEnabled(True)

    def onSrcDirChanged(self) :

        if not os.path.isdir(self.DetailWindow.lineEditSrc.text()):
            self.DetailWindow.pushButtonOk.setEnabled(False)
        else :
            self.DetailWindow.pushButtonOk.setEnabled(True)

    def onActionSelectDstDir(self) :

        dstDir = ""

        dircmdWin = dircmd.dircmd(self)
        dstDir = dircmdWin.select( "Selectionnez le dossier destination", self.initialDir, self.favDirList )

        if dstDir != "" :
            self.dstDir = dstDir
            self.DetailWindow.lineEditDest.setText(self.dstDir)

    def onActionSelectSrcDir(self) :

        srcDir = ""

        dircmdWin = dircmd.dircmd(self)
        srcDir = dircmdWin.select( "Selectionnez le dossier destination", self.initialDir, self.favDirList )

        if srcDir != "" :

            self.srcDir = srcDir
            self.DetailWindow.lineEditSrc.setText(self.srcDir)
            self.loadFileList()
            self.DetailWindow.tableViewSrc.resizeColumnToContents(COL_selection)
            self.DetailWindow.tableViewSrc.resizeColumnToContents(COL_fname)
            self.DetailWindow.tableViewSrc.resizeColumnToContents(COL_ext)
            self.DetailWindow.tableViewSrc.resizeColumnToContents(COL_fsizeh)
            self.DetailWindow.tableViewSrc.resizeColumnToContents(COL_c_date)


    def run_command(self, command):
        """
        Execute une commande Os
        """

        self.subprocess_err = ""
        self.subprocess_returncode = None

        p = subprocess.Popen(command,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             shell=True)
        # Read stdout from subprocess until the buffer is empty !
        for line in iter(p.stdout.readline, b''):
            if line: # Don't print blank lines
                yield line

        # This ensures the process has completed, AND sets the 'returncode' attr
        while p.poll() is None:
            time.sleep(.1) #Don't waste CPU-cycles

        # Empty STDERR buffer
        err = p.stderr.read()

        self.subprocess_returncode = p.returncode

        if p.returncode != 0:
           # The run_command() function is responsible for logging STDERR
            log.Log.debug(log.ERROR, "Error %s:" % err + " / cmd=" + command)
            self.subprocess_err = err


    def loadFileList(self) :

        self.DetailWindow.lineEditSrc.setText(self.srcDir)
        self.DetailWindow.lineEditDest.setText(self.dstDir)

        header = setting.Setting.getListValue("listview/colHeaderNames")

        self.fileList = self.getfileListFromDir(self.srcDir )
        self.tableModel = fileTableModel(self.fileList, header, self)

        # Files model custom sort
        self.table_proxy_model = tableviewSortProxyImport(self)
        self.table_proxy_model.setSourceModel(self.tableModel)
        self.DetailWindow.tableViewSrc.setModel(self.table_proxy_model)

        col = 0
        for colElem in self.tableModel.headerdata :
            self.DetailWindow.tableViewSrc.horizontalHeader().hideSection(col)
            col = col + 1

        self.DetailWindow.tableViewSrc.horizontalHeader().showSection(COL_selection)
        self.DetailWindow.tableViewSrc.horizontalHeader().showSection(COL_fname)
        self.DetailWindow.tableViewSrc.horizontalHeader().showSection(COL_ext)
        self.DetailWindow.tableViewSrc.horizontalHeader().showSection(COL_fsizeh)
        self.DetailWindow.tableViewSrc.horizontalHeader().showSection(COL_c_date)
        self.DetailWindow.tableViewSrc.horizontalHeader().showSection(COL_trtstatus)
        self.DetailWindow.tableViewSrc.horizontalHeader().showSection(COL_trtlib)


        self.DetailWindow.tableViewSrc.resizeColumnToContents(COL_selection)
        self.DetailWindow.tableViewSrc.resizeColumnToContents(COL_fname)
        self.DetailWindow.tableViewSrc.resizeColumnToContents(COL_ext)
        self.DetailWindow.tableViewSrc.resizeColumnToContents(COL_fsizeh)
        self.DetailWindow.tableViewSrc.resizeColumnToContents(COL_c_date)


    def onCancel(self) :
        self.close()

    def onOk(self) :
        setting.Setting.setValue("importimage/srcDir", self.srcDir)
        setting.Setting.setValue("importimage/dstDir", self.dstDir )

        setting.Setting.setValue("importimage/commandBeforeCopy", self.DetailWindow.textEdit_paramBeforeCommand.toPlainText())
        setting.Setting.setValue("importimage/commandAfterCopy", self.DetailWindow.textEdit_paramAfterCommand.toPlainText( ))

        row = 0
        while row < self.paramModel.rowCount(self)  :
            col = 0
            copyExt =  self.paramModel.data(self.paramModel.index(row, 0), Qt.DisplayRole)
            copyCmd =  self.paramModel.data(self.paramModel.index(row, 1), Qt.DisplayRole)
            setting.Setting.setValue("importimage/commandCopy" + copyExt, copyCmd)
            col = col + 1
            row = row + 1

        self.close()

    def onCopy(self) :
        self.executeImport( True )

    def onCheck(self) :
        self.executeImport( False )

    def executeImport (self, doImport = False ) :

        # Check password
        if self.commandBeforeCopy.find( "[password]" ) >=0  :
            if self.DetailWindow.checkBox_beforecommand.isChecked()  :
                if self.DetailWindow.lineEditPassword.text()  == "" :
                    QMessageBox.critical(self, "Mot de passe manquant", "Le mot de passe n'est pas renseigné.", QMessageBox.Cancel)
                    self.DetailWindow.textEditLog.setText("")
                    self.DetailWindow.textEditLog.append( "Traitement stoppé : le mot de passe n'est pas renseigné."  )
                    self.progressBar.hide()
                    self.DetailWindow.pushButtonCopy.setEnabled(True)
                    self.DetailWindow.pushButtonCheck.setEnabled(True)
                    return
        if self.commandAfterCopy.find( "[password]" ) >=0  :
            if self.DetailWindow.checkBox_aftercommand.isChecked() :
                if self.DetailWindow.lineEditPassword.text()  == "" :
                    QMessageBox.critical(self, "Mot de passe manquant", "Le mot de passe n'est pas renseigné.", QMessageBox.Cancel)
                    self.DetailWindow.textEditLog.setText("")
                    self.DetailWindow.textEditLog.append( "Traitement stoppé : le mot de passe n'est pas renseigné." )
                    self.progressBar.hide()
                    self.DetailWindow.pushButtonCopy.setEnabled(True)
                    self.DetailWindow.pushButtonCheck.setEnabled(True)
                    return

        # Init ui
        self.DetailWindow.pushButtonCopy.setEnabled(False)
        self.DetailWindow.pushButtonCheck.setEnabled(False)
        self.DetailWindow.textEditLog.setText("")

        # Clear trtlib
        self.tableModel.layoutAboutToBeChanged.emit()
        for fileItemCurrent in self.table_proxy_model.getDataListSorted( self.fileExt ) :
            fileItemCurrent.trtStatus = TRT_STATUS_WAIT
            fileItemCurrent.trtLib = ""
        self.tableModel.layoutChanged.emit()

        # Nombre de fichiers, afin de proposer une Progress Bar
        nbFilesToTalcopy = 0
        nbFilesProgressBar = 0
        for fileItemCurrent in self.table_proxy_model.getDataListSorted( self.fileExt ) :
            nbFilesToTalcopy = nbFilesToTalcopy + 1
            if fileItemCurrent.selection is True :
                if fileItemCurrent.ext in self.copyExtCommand:
                    nbFilesProgressBar =  nbFilesProgressBar + 1

        self.progressBar.setMaximum(nbFilesProgressBar)
        self.progressBar.show()
        self.progressBar.setValue(0)

        #
        self.tableModel.layoutAboutToBeChanged.emit()

        QApplication.setOverrideCursor(Qt.WaitCursor)

        # Executer la commande avant copie
        if self.DetailWindow.checkBox_beforecommand.isChecked():
            self.command = self.commandBeforeCopy.replace( "[dstDir]" , self.dstDir )
            self.DetailWindow.textEditLog.append("Commande pré-copie : " + self.command)
            if doImport is True :
                self.command = self.command.replace( "[password]" , self.DetailWindow.lineEditPassword.text() )
                for line in self.run_command(self.command):
                    print ("before process : " + line )
                if self.subprocess_returncode != 0 :
                    self.command = self.commandBeforeCopy.replace( "[dstDir]" , self.dstDir )
                    reply = QMessageBox.question(self,
                                         "Erreur ", "Une erreur est survenue dans l'execution de la commande : \n\nCode de retour : " + str(self.subprocess_returncode) + "\n\n" + self.command + "\n\n" + self.subprocess_err + "\n\nContinuer le traitement ?",
                                         QMessageBox.Yes, QMessageBox.No)
                    if reply == QMessageBox.No:
                        self.DetailWindow.textEditLog.append( "Traitement stoppé." )
                        self.progressBar.hide()
                        self.DetailWindow.pushButtonCopy.setEnabled(True)
                        self.DetailWindow.pushButtonCheck.setEnabled(True)
                        QApplication.restoreOverrideCursor()
                        return

        nbFilesTocopy = 0
        nbfileCopied = 0
        nbfileTrt = 0
        nbfileErr = 0
        nbfileSrc = 0
        nbfileIgnUnselect = 0
        nbfileIgnNocmd = 0
        nbfileIgnExists = 0
        #nbfileExists=0

        for fileItemCurrent in self.table_proxy_model.getDataListSorted( self.fileExt ) :

            fileItemCurrent.trtLib = ""

            QApplication.processEvents()

            if fileItemCurrent.selection is True :

                if fileItemCurrent.ext in self.copyExtCommand:

                    # Fichiers destination, de même nom que le fichier source
                    dstFileItemList = []
                    for targetFileFullName in self.locate(fileItemCurrent.fname, self.dstDir):
                        (pathName, fileName) = os.path.split(targetFileFullName)
                        dstFileItem = fileItem(self, pathName, fileName )
                        dstFileItemList.append( dstFileItem )

                    hashEqual = False
                    if self.DetailWindow.radioButton_Md5.isChecked() or self.DetailWindow.radioButton_Crc32.isChecked() or self.DetailWindow.radioButton_Sha1.isChecked() :

                        # Empreinte, pour comparer fichier source et destination
                        if len(dstFileItemList) > 0 :
                            if self.DetailWindow.radioButton_Crc32.isChecked() :
                                if dstFileItemList[0].get_crc32() == fileItemCurrent.get_crc32() :
                                    hashEqual = True
                            if self.DetailWindow.radioButton_Md5.isChecked() :
                                if dstFileItemList[0].get_md5() == fileItemCurrent.get_md5() :
                                    hashEqual = True
                            if self.DetailWindow.radioButton_Sha1.isChecked() :
                                if dstFileItemList[0].get_sha1() == fileItemCurrent.get_sha1() :
                                    hashEqual = True

                    #if hashEqual is True :
                    #    nbfileExists = nbfileExists + 1

                    # Copie seulemement si les hash sont différents, ou bien si la copie est forcée.
                    if hashEqual is False or self.DetailWindow.radioButton_Force.isChecked() :

                        # Formatage de la commande à appliquer au fichier source
                        commandValue = self.copyExtCommand[fileItemCurrent.ext]
                        commandValueFmt = self.replaceTag( commandValue , fileItemCurrent)

                        try :

                            nbfileSrc  = nbfileSrc + 1
                            self.DetailWindow.textEditLog.append( fileItemCurrent.fname + " : "  + commandValueFmt )

                            DstFileFullName = self.dstDir + "/" + fileItemCurrent.fname

                            # Executer la commande de copie ...
                            copyErr = 0
                            if doImport is True :

                                for line in self.run_command(commandValueFmt):
                                    print ("process : " + line )

                                # Vérifier le fichier copié  (uniquement si la copie est forcée, ou bien si le fichier n'existait pas)
                                fileCheckTemplate = self.replaceTag(self.checkTemplate, fileItemCurrent)
                                self.DetailWindow.textEditLog.append( "Vérification de la copie : " + fileCheckTemplate )
                                (pathName, fileName) = os.path.split(fileCheckTemplate)
                                dstFileItemCopy = fileItem(self, pathName, fileName )
                                if dstFileItemCopy.is_immutable(DstFileFullName) is True or dstFileItemCopy.access_w is False :
                                    copyErr  = 3
                                else :
                                    if self.DetailWindow.radioButton_Crc32.isChecked() :
                                        if dstFileItemCopy.get_crc32() != fileItemCurrent.get_crc32() :
                                            copyErr  = 1
                                    if self.DetailWindow.radioButton_Md5.isChecked() :
                                        if dstFileItemCopy.get_md5() != fileItemCurrent.get_md5() :
                                            copyErr  = 1
                                    if self.DetailWindow.radioButton_Sha1.isChecked() :
                                        if dstFileItemCopy.get_sha1() != fileItemCurrent.get_sha1() :
                                            self.DetailWindow.textEditLog.append( dstFileItemCopy.get_sha1() + "/" + fileItemCurrent.get_sha1() )
                                            copyErr  = 1

                            else :
                                if fileItemCurrent.is_immutable(DstFileFullName) is True or os.access(DstFileFullName, os.W_OK) is True :
                                    copyErr  = 2


                            if copyErr == 0 :
                                fileItemCurrent.trtStatus = TRT_STATUS_OK
                                fileItemCurrent.trtLib = "Copié."
                                nbfileCopied = nbfileCopied + 1

                            if copyErr == 1 :
                                fileItemCurrent.trtStatus = TRT_STATUS_ERROR
                                fileItemCurrent.trtLib = "Erreur de copie : le fichier copié n'existe pas ou n'a mal été copié corrctement(hash différents)."
                                bfileErr = nbfileErr + 1

                            if copyErr == 2 :
                                fileItemCurrent.trtStatus = TRT_STATUS_WARNING
                                fileItemCurrent.trtLib = "Le fichier ou dossier de destination n'est pas accessible en écriture."

                            if copyErr == 3 : # when docopy == True
                                fileItemCurrent.trtStatus = TRT_STATUS_ERROR
                                fileItemCurrent.trtLib = "Le fichier ou dossier de destination n'est pas accessible en écriture."

                            nbfileTrt = nbfileTrt + 1

                        except Exception as e:
                            log.Log.debug(log.ERROR, "Error %s:" % e.args[0] )
                            fileItemCurrent.trtStatus = TRT_STATUS_ERROR
                            fileItemCurrent.trtLib = "EXCEPTION : Erreur de copie  : " + str(e.args[0])
                            self.DetailWindow.textEditLog.append( fileItemCurrent.fname + " : Erreur commande : "  + commandValueFmt )
                            nbfileErr = nbfileErr + 1

                    else :

                        fileItemCurrent.trtStatus = TRT_STATUS_IGNORE_EXISTS
                        fileItemCurrent.trtLib = "Ignoré : le fichier existe déjà. Les hash sont identiques. " + str(len(dstFileItemList)) + " exemplaires."
                        self.DetailWindow.textEditLog.append( fileItemCurrent.fname + " : " + fileItemCurrent.trtLib  )
                        nbfileIgnExists = nbfileIgnExists + 1


                    nbFilesTocopy = nbFilesTocopy + 1
                    self.progressBar.setValue(nbFilesTocopy)

                else :
                        fileItemCurrent.trtStatus = TRT_STATUS_IGNORE_NOCMD
                        fileItemCurrent.trtLib = "Ignoré : pas de commande de copie pour cette extention : " + fileItemCurrent.ext
                        self.DetailWindow.textEditLog.append( fileItemCurrent.fname + " : " + fileItemCurrent.trtLib  )
                        nbfileIgnNocmd = nbfileIgnNocmd + 1
            else :
                    fileItemCurrent.trtStatus = TRT_STATUS_IGNORE
                    fileItemCurrent.trtLib = "Ignoré : non selectionné."
                    self.DetailWindow.textEditLog.append( fileItemCurrent.fname + " : " + fileItemCurrent.trtLib  )
                    nbfileIgnUnselect = nbfileIgnUnselect + 1


        if self.DetailWindow.checkBox_aftercommand.isChecked():
            self.command = self.commandAfterCopy.replace( "[dstDir]" , self.dstDir )
            self.DetailWindow.textEditLog.append("Commande post-copie : " + self.command)

            if doImport is True :
                self.command = self.command.replace( "[password]" , self.DetailWindow.lineEditPassword.text() )
                for line in self.run_command(self.command):
                    print ("after process : " + line )
                if self.subprocess_returncode != 0 :
                    self.command = self.commandAfterCopy.replace( "[dstDir]" , self.dstDir )
                    QMessageBox.warning(self, "Erreur ", "Une erreur est survenue dans l'execution de la commande : \n\nCode de retour : " + str(self.subprocess_returncode) + "\n\n" + self.command + "\n\n" + self.subprocess_err, QMessageBox.Cancel)


        self.DetailWindow.textEditLog.append( "" )
        self.DetailWindow.textEditLog.append("---- Résultat du traitement -----")

        self.DetailWindow.textEditLog.append( "Fichiers identifiés sur le support (total) : " + str(nbFilesToTalcopy) )
        self.DetailWindow.textEditLog.append( "Fichiers ignorés (non selectionnés) : " + str(nbfileIgnUnselect))
        self.DetailWindow.textEditLog.append( "Fichiers ignorés (extention sans commande) : " + str(nbfileIgnNocmd))
        #self.DetailWindow.textEditLog.append( "Fichiers ignorés (existe déjà) : " + str(nbfileIgnExists))
        if self.DetailWindow.radioButton_Force.isChecked() :
            self.DetailWindow.textEditLog.append( "Fichiers ignorés (existants sur la destination) : " + str(nbfileIgnExists) + " (copie forcée)")
        else :
            self.DetailWindow.textEditLog.append( "Fichiers ignorés (existants sur la destination) : " + str(nbfileIgnExists))

        self.DetailWindow.textEditLog.append( "" )

        self.DetailWindow.textEditLog.append( "Fichiers à traiter (selectionnés et extention prise en charge, ou hash différents) : " + str(nbfileSrc))
        self.DetailWindow.textEditLog.append( "Fichiers à copier (tentatives de copies réalisées) : " + str(nbfileTrt))


        if doImport == True :
            self.DetailWindow.textEditLog.append( "Fichiers copiés (nouveaux sur la destination) : " + str(nbfileCopied) )
            self.DetailWindow.textEditLog.append( "Fichiers en erreur : " + str(nbfileErr))
        else :
            self.DetailWindow.textEditLog.append( "Fichiers copiés (nouveaux sur la destination) : " + str(nbfileCopied) + " (simulation)")
            self.DetailWindow.textEditLog.append( "Fichiers en erreur : ? (simulation)")
        self.DetailWindow.textEditLog.append( "" )

        #taux = ((( nbfileCopied + nbfileIgn) / nbFilesToTalcopy ) - nbfileErr ) * 100
        #self.DetailWindow.textEditLog.append( "<b>Taux de réussite : </b>" + str(taux) + "%" )


        self.progressBar.hide()
        self.DetailWindow.pushButtonCopy.setEnabled(True)
        self.DetailWindow.pushButtonCheck.setEnabled(True)

        self.tableModel.layoutChanged.emit()

        QApplication.restoreOverrideCursor()

    def locate(self, pattern, root=os.curdir):
        '''Locate all files matching supplied filename pattern in and below
        supplied root directory.'''
        for path, dirs, files in os.walk(os.path.abspath(root)):
            for filename in fnmatch.filter(files, pattern):
                yield os.path.join(path, filename)


    def replaceTag(self, commandLine , fileItem) :

        commandLinefmt = commandLine

        commandLinefmt = commandLinefmt.replace( "[srcDir]" , self.srcDir )
        commandLinefmt = commandLinefmt.replace( "[fileNameOnly]" , fileItem.fname)
        commandLinefmt = commandLinefmt.replace( "[fileNameExt]" , fileItem.ext )
        commandLinefmt = commandLinefmt.replace( "[fileNameFull]" , self.srcDir + "/" + fileItem.fname )
        commandLinefmt = commandLinefmt.replace( "[fileBaseName]" ,  fileItem.fname)
        commandLinefmt = commandLinefmt.replace( "[dstDir]" , self.dstDir )

        import datetime
        cdateY = datetime.datetime.fromtimestamp( fileItem.c_date_stamp ).strftime('%Y')
        cdateM = datetime.datetime.fromtimestamp( fileItem.c_date_stamp ).strftime('%m')
        cdateD = datetime.datetime.fromtimestamp( fileItem.c_date_stamp ).strftime('%Y-%m-%d %H:%M:%S')
        cdated = datetime.datetime.fromtimestamp( fileItem.c_date_stamp ).strftime('%d')
        cdateh = datetime.datetime.fromtimestamp( fileItem.c_date_stamp ).strftime('%H')
        cdatei = datetime.datetime.fromtimestamp( fileItem.c_date_stamp ).strftime('%M')
        cdates = datetime.datetime.fromtimestamp( fileItem.c_date_stamp ).strftime('%S')
        commandLinefmt = commandLinefmt.replace( "[fileCDateMonth]" , cdateM )
        commandLinefmt = commandLinefmt.replace( "[fileCDateYear]" , cdateY )

        commandLinefmt = commandLinefmt.replace( "[exifCDateMonth]" , fileItem.tagDateTimeOriginalMonth)
        commandLinefmt = commandLinefmt.replace( "[exifCDateYear]" , fileItem.tagDateTimeOriginalYear)
        commandLinefmt = commandLinefmt.replace( "[exifCDateDay]" , fileItem.tagDateTimeOriginalDay)

        return commandLinefmt

    def dialogShow( self) :
        log.Log.debug( log.INFO , "import_external_view : showing window."  )
        self.DetailWindow.exec_() # Modal

    def getfileListFromDir(self, pathName):

        self.processCancel = False

        log.Log.debug(log.INFO, "read dir " + pathName)

        fileList = []
        if os.path.isdir(pathName):
            try:
                # Nombre de fichiers, afin de proposer une Progress Bar
                onlyfilesCount = len(next(os.walk(pathName))[2])
                self.progressBar.setMaximum(onlyfilesCount)
                self.progressBar.show()
                self.progressBar.setValue(0)
                barVal = 0

                files = os.listdir(pathName)
                log.Log.debug(log.INFO, "read dir start : " + str(onlyfilesCount) + " files.")
                for fileName in files:

                    QApplication.processEvents()
                    if self.processCancel == True :
                        return fileList

                    if os.path.isfile(pathName + "/" + fileName):
                        try :
                            newFileItem = fileItem(self, pathName, fileName, self.fileExt, getExif=True)
                            if newFileItem.ext in self.fileExt :
                                fileList.append(  newFileItem )

                        except Exception as e:
                            log.Log.debug(log.ERROR, "Error in fileItem : %s:" % e.args[0] + " / pathName=" + pathName)

                        barVal = barVal + 1
                        self.progressBar.setValue( barVal)
                        #self.splash.progressBarForward( barVal )
                        #self.splash.processEvent()

                log.Log.debug(log.INFO, "read dir done : " + str(len(fileList)) + " files.")

            except Exception as e:
                log.Log.debug(log.ERROR, "Error %s:" % e.args[0] + " / pathName=" + pathName)

            #finally:
        else :
            log.Log.debug(log.INFO, "non trouvé : " + pathName)

        self.progressBar.hide()
        return fileList



class fileTableModel (QAbstractTableModel):

    def __init__(self, datain, headerdata, parent=None, *args):
        """ datain: a list of lists
            headerdata: a list of strings
        """
        super(fileTableModel, self).__init__(parent)

        self.arraydata = datain
        self.headerdata = headerdata

        self.thumbCacheDir  = setting.Setting.getValue("iconview/thumbCacheDir", "./cache")
        self.iconSizeWidth = int(setting.Setting.getValue("iconview/iconSizeWidth", "100"))
        self.iconSizeHeight = int(setting.Setting.getValue("iconview/iconSizeHeight", "100"))

        self.c_date_format_out = setting.Setting.getValue("listview/c_date_format_out", "dddd d MMMM yyyy hh:mm")

        self.fileExt = setting.Setting.getListValue("app/fileExt")

    def rowCount(self, parent):
        return len(self.arraydata)

    def columnCount(self, parent):
        if len(self.headerdata) > 0:
            return len(self.headerdata)
        return 0

    def data(self, index, role):

        if not index.isValid():
            return QVariant()

        if role == Qt.BackgroundRole :

            if self.arraydata[index.row()].selection is True :
                if self.arraydata[index.row()].trtStatus == TRT_STATUS_ERROR :
                    return QColor(234, 82, 82)
                else :
                    return QColor(178, 255, 179)
            else :
                if self.arraydata[index.row()].trtStatus == TRT_STATUS_ERROR :
                    return QColor(234, 82, 82)
                else :
                    return QColor(211, 211, 211)

        if index.column() == COL_selection:

            if role == Qt.DisplayRole:
                return QVariant("")

            if role == Qt.CheckStateRole :

                if self.arraydata[index.row()].ext not in self.fileExt :
                    return QVariant("")

                if self.arraydata[index.row()].selection is True :
                    return QVariant(Qt.Checked)
                else:
                    return QVariant(Qt.Unchecked)

            if role == Qt.UserRole :
                if self.arraydata[index.row()].selection is True :
                    return True
                else:
                    return False

            if role == Qt.UserRole + 1:
                if self.arraydata[index.row()].selection is True :
                    return 1
                else:
                    return 0

        if index.column() == COL_thumb:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].iconLabel
            if role == Qt.UserRole:
                return self.arraydata[index.row()].ino
            if role == Qt.UserRole+1:
                return self.arraydata[index.row()].fname
            if role == Qt.DecorationRole:

                linkAttr = ""
                if self.arraydata[index.row()].islink == True :
                    linkAttr="_l"

                if os.path.isfile( self.thumbCacheDir + "/" + self.arraydata[index.row()].key + linkAttr + ".JPG"    ) :
                    pixmap = QPixmap(  "data/thumbs/" + self.arraydata[index.row()].key + linkAttr + ".JPG" , "JPG" )

                    if self.arraydata[index.row()].isimmutable == True  :
                        pixmap_icon = QPixmap( QImage("icon/icon_lock.png") )
                        pixmap_icon = pixmap_icon.scaled( QSize(18, 18), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.FastTransformation )
                        painter = combined = QPainter(pixmap)
                        painter.setOpacity(0.8);
                        painter.drawPixmap(QPoint(pixmap.width() - 18,  0  ), pixmap_icon)
                        painter.end()

                    pixmap_resized = pixmap.scaled( QSize(self.iconSizeHeight, self.iconSizeWidth), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.FastTransformation )

                else :
                    if os.path.isfile( "icon/ext_" + self.arraydata[index.row()].ext + ".png"  ) :
                        pixmap = QPixmap("icon/ext_" + self.arraydata[index.row()].ext + ".png" , "PNG")
                        pixmap_resized = pixmap.scaled( QSize(self.iconSizeHeight*0.4, self.iconSizeWidth*0.4), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.FastTransformation )
                    else :
                        pixmap = QPixmap("icon/ext_generic.png" , "PNG")
                        pixmap_resized = pixmap.scaled( QSize(self.iconSizeHeight*0.4, self.iconSizeWidth*0.4), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.FastTransformation )

                return pixmap_resized


            if role == Qt.SizeHintRole:
                return QSize( self.cellWidth, self.cellHeight )

        if index.column() == COL_key:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].key
            if role == Qt.UserRole:
                return self.arraydata[index.row()].key

        if index.column() == COL_ino:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].ino
            if role == Qt.UserRole:
                return self.arraydata[index.row()].ino

        if index.column() == COL_fname:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].fname
            if role == Qt.UserRole:
                return self.arraydata[index.row()].fname
            if role == Qt.TextAlignmentRole:
                return Qt.AlignLeft | Qt.AlignVCenter

        if index.column() == COL_dev:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].dev
            if role == Qt.UserRole:
                return self.arraydata[index.row()].dev

        if index.column() == COL_fsize:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].fsize
            if role == Qt.UserRole:
                return self.arraydata[index.row()].fsize

        if index.column() == COL_fsizeh:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].fsizeh
            if role == Qt.UserRole:
                return self.arraydata[index.row()].fsizeh
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].fsize

        if index.column() == COL_ext:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].ext
            if role == Qt.UserRole:
                return self.arraydata[index.row()].ext

        if index.column() == COL_c_date:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].c_date.strftime( self.c_date_format_out )
            if role == Qt.UserRole:
                return self.arraydata[index.row()].c_date_stamp
            if role == Qt.UserRole+1:
                return self.arraydata[index.row()].c_date_stamp

        if index.column() == COL_m_date:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].m_date.strftime(  self.m_date_format_out )
            if role == Qt.UserRole:
                return self.arraydata[index.row()].m_date_stamp
            if role == Qt.UserRole+1:
                return self.arraydata[index.row()].m_date_stamp

        if index.column() == COL_a_date:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].a_date.strftime( self.a_date_format_out)
            if role == Qt.UserRole:
                return self.arraydata[index.row()].a_date_stamp
            if role == Qt.UserRole+1:
                return self.arraydata[index.row()].a_date_stamp

        if index.column() == COL_islink:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].islink
            if role == Qt.UserRole:
                return self.arraydata[index.row()].islink

        if index.column() == COL_linktarget:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].linktarget
            if role == Qt.UserRole:
                return self.arraydata[index.row()].linktarget

        if index.column() == COL_access_w:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].access_w
            if role == Qt.UserRole:
                return self.arraydata[index.row()].access_w

        if index.column() == COL_mime:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].mime
            if role == Qt.UserRole:
                return self.arraydata[index.row()].mime

        if index.column() == COL_ftype:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].ftype
            if role == Qt.UserRole:
                return self.arraydata[index.row()].ftype

        if index.column() == COL_crc32:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].crc32
            if role == Qt.UserRole:
                return self.arraydata[index.row()].crc32

        if index.column() == COL_sha1:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].sha1
            if role == Qt.UserRole:
                return self.arraydata[index.row()].sha1

        if index.column() == COL_exifinfos:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].exifinfos
            if role == Qt.UserRole:
                return self.arraydata[index.row()].exifinfos

        if index.column() == COL_isimmutable:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].isimmutable
            if role == Qt.UserRole:
                return self.arraydata[index.row()].isimmutable

        if index.column() == COL_width:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].width
            if role == Qt.UserRole:
                return self.arraydata[index.row()].width

        if index.column() == COL_height:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].height
            if role == Qt.UserRole:
                return self.arraydata[index.row()].height

        if index.column() == COL_trtstatus:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].trtStatus
            if role == Qt.UserRole:
                return self.arraydata[index.row()].trtStatus

        if index.column() == COL_trtlib:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].trtLib
            if role == Qt.UserRole:
                return self.arraydata[index.row()].trtLib

        return QVariant()

    def getFileItemFromKey(self, idElem ):
        for itemElem in self.arraydata:
            if itemElem.key == idElem:
                return itemElem
        return []

    def setData(self, index, value, role):

        if not index.isValid():
            return False

        if index.column() == COL_selection:

            if self.arraydata[index.row()].ext in self.fileExt :

                if role == Qt.CheckStateRole:

                    self.layoutAboutToBeChanged.emit()

                    #print ("check/uncheck row : " + str(index.row()) + " / " + self.arraydata[index.row()].fname)

                    if value == Qt.Checked:
                        self.arraydata[index.row()].selection = True

                    else:
                        self.arraydata[index.row()].selection = False

                    self.layoutChanged.emit()

                    return True

                else:
                    print ("Set data with no CheckStateRole")

        return False

    def flags(self, index):

        if not index.isValid():
            return None

        if index.column() == COL_selection:
            if self.arraydata[index.row()].ext in self.fileExt :

                return Qt.ItemIsEnabled  | Qt.ItemIsUserCheckable

        return Qt.ItemIsEnabled


    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return QVariant(self.headerdata[col])
        return QVariant()

    def sort(self, Ncol, order):
        """Sort table by given column number.
        """
        self.layoutAboutToBeChanged.emit()
        self.arraydata = sorted(self.arraydata, key=operator.itemgetter(Ncol))
        if order == Qt.DescendingOrder:
            self.arraydata.reverse()
        self.layoutChanged.emit()



"""
Implement specifics columns sort
"""

class tableviewSortProxyImport(QSortFilterProxyModel):

    def __init__(self, parent=None):

        super(tableviewSortProxyImport, self).__init__(parent)

        self.setFilterCaseSensitivity(Qt.CaseInsensitive)
        self.setSortCaseSensitivity(Qt.CaseInsensitive)
        self.setSortLocaleAware(True)

        self.colFilterList = {}

    def lessThan(self, left, right):

        col = left.column()

        dataleft = left.data()
        dataright = right.data()

        if col == COL_selection:
            dataleft = self.sourceModel().data(left, Qt.UserRole + 1)
            dataright = self.sourceModel().data(right, Qt.UserRole + 1)

        if col == COL_fname:
            dataleft = self.remove_accents(dataleft).lower()
            dataright = self.remove_accents(dataright).lower()

        elif col == COL_fsizeh:
            dataleft = self.sourceModel().data(left, Qt.UserRole + 1)
            dataright = self.sourceModel().data(right, Qt.UserRole + 1)
            pass

        elif col == COL_c_date:
            dataleft = self.sourceModel().data(left, Qt.UserRole + 1)
            dataright = self.sourceModel().data(right, Qt.UserRole + 1)
            pass


        return dataleft < dataright

    def remove_accents(self, input_str):
        import unicodedata
        import platform
        if platform.system() == "Windows":
            return unicodedata.normalize('NFD', input_str).encode('ascii', 'ignore')
        else:
            nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
            return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])

    def getColFilter (self , colIndex) :
        if colIndex in self.colFilterList :
            return self.colFilterList[colIndex]
        else :
            return ""

    def setColFilter (self , colIndex , strRegFilter) :
        self.colFilterList[colIndex] = strRegFilter

    def unSetColFilter (self , colIndex ) :
        if colIndex in self.colFilterList :
            del( self.colFilterList[colIndex])

    def filterAcceptsRow(self, source_row_number, source_parent):

        for colFilterKey  in self.colFilterList :
            indexData = self.sourceModel().index(source_row_number, colFilterKey)
            valueColCurrent = self.sourceModel().data(indexData, Qt.UserRole)
            expression = QRegExp(self.colFilterList[colFilterKey])
            index = expression.indexIn(str(valueColCurrent))
            if index < 0:
                return False

        return QSortFilterProxyModel.filterAcceptsRow(self, source_row_number, source_parent)

    # call : self.table_proxy_model.dumpSortedData()
    def dumpSortedData( self ) :

        print ("data sorted :::::::::")
        row = 0
        while row < self.rowCount() :

            indexData = self.index(row, COL_ino)
            ino = self.data(indexData, Qt.UserRole)
            print ("ino = " + str(ino))

            indexData = self.index(row, COL_key)
            ino = self.data(indexData, Qt.UserRole)
            print ("key = " + str(ino))

            fname = self.data( self.index(row, COL_fname) , Qt.UserRole)
            selection = self.data( self.index(row, COL_selection) , Qt.UserRole)
            fsizeh = self.data( self.index(row, COL_fsizeh) , Qt.UserRole)
            c_date = self.data( self.index(row, COL_c_date) , Qt.UserRole)

            print ( str(row) + " : #" + " -> fname=" + str(fname) + " selection=" + str(selection) + " fsizeh=" +str(fsizeh))

            row = row + 1

    def selectAll(self , fileExt= [] ) :

        row = 0
        while row < self.rowCount() :

            rowKey = self.data( self.index(row, COL_key), Qt.UserRole)
            indexItem = self.sourceModel().getFileItemFromKey(rowKey)
            if indexItem.ext in fileExt :
                indexItem.selection = True
                print ("select : "+ indexItem.fname )

            row = row + 1



    def getDataListSorted(self , fileExt= [] ) :

        self.itemListSorted = []

        row = 0
        while row < self.rowCount() :

            rowKey = self.data( self.index(row, COL_key), Qt.UserRole)
            indexItem = self.sourceModel().getFileItemFromKey(rowKey)
            if indexItem.ext in fileExt :
                self.itemListSorted.append( indexItem )
                #print ("from support, add  "+ indexItem.fname +"   rowKey = " +  str(rowKey))

            row = row + 1

        return self.itemListSorted

    def geFirstDataItemSorted( self , fileExt= []  ):
        """
        Retourne le premier Fileitem image dans le dossier
        """
        indexItem = None
        row = 0
        while row < self.rowCount() :

            indexKey = self.index(row, COL_key)
            rowKey = self.data(indexKey, Qt.UserRole)
            indexItem = self.sourceModel().getFileItemFromKey(rowKey)
            if indexItem.ext in fileExt :
                break
            row = row + 1

        return indexItem


class paramTabledata(QAbstractTableModel):
    def __init__(self, list, headers = [], parent = None):
        QAbstractTableModel.__init__(self, parent)
        self.list = list
        self.headers = headers

    def rowCount(self, parent):
        return len(self.list)

    def columnCount(self, parent):
        return len(self.list[0])

    def flags(self, index):
        return Qt.ItemIsEditable | Qt.ItemIsEnabled | Qt.ItemIsSelectable

    def data(self, index, role):
        if role == Qt.EditRole:
            row = index.row()
            column = index.column()
            return self.list[row][column]

        if role == Qt.DisplayRole:
            row = index.row()
            column = index.column()
            value = self.list[row][column]
            return value

    def setData(self, index, value, role = Qt.EditRole):
        if role == Qt.EditRole:
            row = index.row()
            column = index.column()
            self.list[row][column] = value
            self.dataChanged.emit(index, index)
            return True
        return False

    def headerData(self, section, orientation, role):

        if role == Qt.DisplayRole:

            if orientation == Qt.Horizontal:

                if section < len(self.headers):
                    return self.headers[section]
                else:
                    return "not implemented"
            else:
                return "item %d" % section
