#!/usr/bin/env python
#-*- coding: utf-8 -*-


"""
image viewer widget for a QPixmap in a QGraphicsView scene with mouse zooming and panning.
"""

import os.path
try:
    from PyQt5.QtCore import Qt, QRectF, pyqtSignal, QT_VERSION_STR, QPointF, QPoint, QSizeF
    from PyQt5.QtGui import QImage, QPixmap, QPainterPath, QTransform, QPainter, QFont, QColor, qRgb, QBrush
    from PyQt5.QtWidgets import QGraphicsView, QGraphicsScene, QFileDialog, QApplication, QMenu, QFrame, QLabel, QDesktopWidget, QGraphicsTextItem, QGraphicsItem
except ImportError:
    try:
        from PyQt4.QtCore import Qt, QRectF, pyqtSignal, QT_VERSION_STR
        from PyQt4.QtGui import QGraphicsView, QGraphicsScene, QImage, QPixmap, QPainterPath, QFileDialog, QMenu
    except ImportError:
        raise ImportError("ImageViewer: Requires PyQt5 or PyQt4.")


author__ = "moiàfai.com"
version__ = '0.1.0'

from applib import log
from applib import sqlite

from applib import setting


class imageviewer(QGraphicsView):

    # Mouse button signals emit image scene (x, y) coordinates.
    # !!! For image (row, column) matrix indexing, row = y and column = x.
    leftMouseButtonPressed = pyqtSignal(float, float)

    def __init__(self , parent=None):

        super(imageviewer, self).__init__(parent)

        self.parent = parent

        self.pathSelected = ""
        self.favDirList = ""

        self.widgetWidth = 0
        self.widgetHeight = 0

        # Zoom image
        self.autoAdjustZoom = True
        self.NormalZoom = False
        self.FitZoom = False

        self.pixmap = None
        self.textImageItem = None
        self.currentItemIndex = None
        self.fileName = None
        self.fileItemList= []
        self.lastRotate = 0
        self.exifRotate = True

        # Ratio d'affichage image
        self.aspectRatio = Qt.KeepAspectRatio

        self.setContextMenuPolicy(Qt.DefaultContextMenu)

        """
        QPainter.Antialiasing	0x01	Indicates that the engine should antialias edges of primitives if possible.
        QPainter.TextAntialiasing	0x02	Indicates that the engine should antialias text if possible. To forcibly disable antialiasing for text, do not use this hint. Instead, set QFont.NoAntialias on your font's style strategy.
        QPainter.SmoothPixmapTransform	0x04	Indicates that the engine should use a smooth pixmap transformation algorithm (such as bilinear) rather than nearest neighbor.
        QPainter.HighQualityAntialiasing	0x08	An OpenGL-specific rendering hint indicating that the engine should use fragment programs and offscreen rendering for antialiasing.
        QPainter.NonCosmeticDefaultPen
        """
        self.renderAntialiasing = True
        self.renderSmoothPixmapTransform = True
        self.setRenderHint( QPainter.Antialiasing, self.renderAntialiasing)
        self.setRenderHint( QPainter.SmoothPixmapTransform, self.renderSmoothPixmapTransform )

        self.infoTextTemplate = setting.Setting.getValue( "diaporama/infoTextTemplate" , "[fname]" )
        self.infoTextPos = int(setting.Setting.getValue( "diaporama/infoTextPos" , "4" ))
        self.infoTextOffset = int(setting.Setting.getValue( "diaporama/infoTextOffset" , "30" ))
        self.infoTextDisplay = int(setting.Setting.getValue( "diaporama/infoTextDisplay" , "1" ))
        self.infoTextfont = setting.Setting.getValue( "diaporama/infoTextfont" , "Arial" )
        self.infoTextfontSize = int(setting.Setting.getValue( "diaporama/infoTextfontSize" , "12" ))
        self.infoTextColor=setting.Setting.getValue( "diaporama/infoTextColor" , "#0000030" )
        self.infoTextBgColor=setting.Setting.getValue( "diaporama/infoTextBgColor" , "#FFFF10" )
        self.bgColor = QColor( setting.Setting.getValue( "diaporama/bgColor" , "#100000" ) )
        self.bgColorCurrent = self.bgColor

        self.setupUi()

        self.fullScreenMode = False

    def setupUi(self)  :

        # Open Gl option
        #from PyQt5.QtOpenGL import *
        #self.setViewport(QGLWidget())

        self.scene = QGraphicsScene( self )
        self.setScene(self.scene)
        self.scene.setBackgroundBrush(self.bgColor)
        #self.setCacheMode(QGraphicsView.CacheBackground)
        #self.setOptimizationFlag( QGraphicsView.DontAdjustForAntialiasing )


    def setPaths( self, pathSelected,  favDirList ) :
        self.pathSelected = pathSelected
        self.favDirList = favDirList

    def setMainWindow ( self , mainWindow ) :
        self.mainWindow = mainWindow

    def setExifTableView( self, exifTableViewWidget) :

        self.exifTableViewWidget = exifTableViewWidget

    def contextMenuEvent(self, event):

        menu = QMenu()

        actionAdjustToWindow = menu.addAction('Zoom : ajuster à la taille de la fenêtre')
        actionAdjustToWindow.setCheckable(True)
        actionAdjustToWindow.setChecked(self.FitZoom)
        actionAdjustToWindow.triggered.connect(self.onActionAjustToWindow)

        actionZoom1 = menu.addAction('Zoom 1:1')
        actionZoom1.setCheckable(True)
        actionZoom1.setChecked(self.NormalZoom)
        actionZoom1.triggered.connect(self.onActionActionZoom1)

        actionToggleAdjust = menu.addAction('Zoom automatique')
        actionToggleAdjust.setCheckable(True)
        actionToggleAdjust.setChecked(self.autoAdjustZoom)
        actionToggleAdjust.triggered.connect(self.onActionToggleAdjust)

        menu.addSeparator()
        actionRotate = menu.addAction('Rotation 90°')
        actionRotate.triggered.connect(self.onActionRotate)

        actionExifRotate = menu.addAction('Rotation auto exif')
        actionExifRotate.setCheckable(True)
        actionExifRotate.setChecked(self.exifRotate)
        actionExifRotate.triggered.connect(self.onActionExifRotate)

        menu.addSeparator()

        if self.fullScreenMode == True :
            #actionFullScreen.setEnabled(False)
            actionFullScreen = menu.addAction('Quitter le plein écran')
            actionFullScreen.triggered.connect(self.onActionFullScreenExit)
        else :
            actionFullScreen = menu.addAction('Plein écran')
            actionFullScreen.triggered.connect(self.onActionFullScreenEnter)


        menu.addSeparator()
        actionDisplayTextInfo = menu.addAction('Afficher les informations')
        actionDisplayTextInfo.setCheckable(True)
        actionDisplayTextInfo.setChecked(self.infoTextDisplay)
        actionDisplayTextInfo.triggered.connect(self.onActionDisplayTextInfo)

        menu.addSeparator()

        self.actionDisplayRatioFill = menu.addAction('Ratio : respecter (100% affiché)')
        self.actionDisplayRatioFill.setCheckable(True)
        self.actionDisplayRatioFill.setChecked(self.infoTextDisplay)
        self.actionDisplayRatioFill.triggered.connect(self.onActionDisplayRatioFill)

        self.actionDisplayRatioItgnore = menu.addAction('Ratio : ignorer (100% affiché)')
        self.actionDisplayRatioItgnore.setCheckable(True)
        self.actionDisplayRatioItgnore.setChecked(self.infoTextDisplay)
        self.actionDisplayRatioItgnore.triggered.connect(self. onActionDisplayRatioItgnore)

        self.actionDisplayRatioExpand = menu.addAction('Ratio : étendre (< 100% affiché)')
        self.actionDisplayRatioExpand.setCheckable(True)
        self.actionDisplayRatioExpand.setChecked(self.infoTextDisplay)
        self.actionDisplayRatioExpand.triggered.connect(self.onActionDisplayRatioExpand)

        self.actionDisplayRatioFill.setChecked(False)
        self.actionDisplayRatioItgnore.setChecked(False)
        self.actionDisplayRatioExpand.setChecked(False)

        if self.aspectRatio == Qt.KeepAspectRatio : self.actionDisplayRatioFill.setChecked(True)
        if self.aspectRatio == Qt.IgnoreAspectRatio : self.actionDisplayRatioItgnore.setChecked(True)
        if self.aspectRatio == Qt.KeepAspectRatioByExpanding : self.actionDisplayRatioExpand.setChecked(True)


        menu.addSeparator()
        menuOptionBgcolor = menu.addMenu('Couleur de fond ...')
        self.menuOptionBgcolorBlack = menuOptionBgcolor.addAction('Noir')
        self.menuOptionBgcolorBlack.setCheckable(True)
        self.menuOptionBgcolorBlack.triggered.connect(  lambda bgColor = "#000000" :  self.onActionSetBgColor( "#000000" )  )

        self.menuOptionBgcolorGray = menuOptionBgcolor.addAction('Gris')
        self.menuOptionBgcolorGray.setCheckable(True)
        self.menuOptionBgcolorGray.triggered.connect(  lambda bgColor = "#777777" :  self.onActionSetBgColor( "#777777" )  )

        self.menuOptionBgcolorWhite = menuOptionBgcolor.addAction('Blanc')
        self.menuOptionBgcolorWhite.setCheckable(True)
        self.menuOptionBgcolorWhite.triggered.connect(  lambda bgColor = "#FFFFFF" :  self.onActionSetBgColor( "#FFFFFF" )  )

        self.menuOptionBgcolorCustom = menuOptionBgcolor.addAction('Personnalisée')
        self.menuOptionBgcolorCustom.setCheckable(True)
        self.menuOptionBgcolorCustom.triggered.connect(  lambda bgColor = "" :  self.onActionSetBgColor( self.bgColor)  )

        self.menuOptionBgcolorBlack.setChecked(False)
        self.menuOptionBgcolorGray.setChecked(False)
        self.menuOptionBgcolorWhite.setChecked(False)
        self.menuOptionBgcolorCustom.setChecked(False)

        if self.bgColorCurrent == "#000000" : self.menuOptionBgcolorBlack.setChecked(True)
        if self.bgColorCurrent == "#777777" : self.menuOptionBgcolorGray.setChecked(True)
        if self.bgColorCurrent == "#FFFFFF" : self.menuOptionBgcolorWhite.setChecked(True)
        if self.bgColorCurrent == self.bgColor : self.menuOptionBgcolorCustom.setChecked(True)

        #actionDisplayRenderAntialiasing.setCheckable(True)
        #actionDisplayRenderAntialiasing.setChecked(self.renderAntialiasing)
        #actionDisplayRenderAntialiasing.triggered.connect(self.onActionDisplayRenderAntialiasing)

        """
        self.setRenderHints(QPainter.Antialiasing|QPainter.SmoothPixmapTransform)
        menu.addSeparator()
        actionDisplayRenderAntialiasing = menu.addAction('Antialiasing')
        actionDisplayRenderAntialiasing.setCheckable(True)
        actionDisplayRenderAntialiasing.setChecked(self.renderAntialiasing)
        actionDisplayRenderAntialiasing.triggered.connect(self.onActionDisplayRenderAntialiasing)

        actionDisplayRenderSmoothPixmapTransform = menu.addAction('SmoothPixmapTransform')
        actionDisplayRenderSmoothPixmapTransform.setCheckable(True)
        actionDisplayRenderSmoothPixmapTransform.setChecked(self.renderSmoothPixmapTransform)
        actionDisplayRenderSmoothPixmapTransform.triggered.connect(self.onActionDisplayRenderSmoothPixmapTransform)
        """

        menu.addSeparator()
        actionCreateLink = menu.addAction('Créer un lien')
        actionCreateLink.triggered.connect( self.onActionCreateLink)

        actionRename = menu.addAction('Renommer')
        actionRename.triggered.connect( self.onActionFileRename)
        if self.fileItemList[self.currentItemIndex].isimmutable == True or self.fileItemList[self.currentItemIndex].access_w == False :
            actionRename.setEnabled( False )

        actionDelete = menu.addAction('Supprimer ...')
        actionDelete.triggered.connect( self.onActionFileDel  )
        if self.fileItemList[self.currentItemIndex].isimmutable == True or self.fileItemList[self.currentItemIndex].access_w == False :
            actionDelete.setEnabled( False )

        actionMove = menu.addAction('Déplacer')
        actionMove.triggered.connect( self.onActionFileMove)
        if self.fileItemList[self.currentItemIndex].isimmutable == True or self.fileItemList[self.currentItemIndex].access_w == False :
            actionMove.setEnabled( False )

        actionCopy = menu.addAction('Copier')
        actionCopy.triggered.connect( self.onActionFileCopy)

        menu.exec_(event.globalPos())


    def onActionCreateLink( self) :
        """
        Créer un lien symbolique pour les fichiers de la selection
        """
        from appmod import file_linkcreate_controler
        getFileItemListSelected = [ self.fileItemList[self.currentItemIndex] ]
        self.file_linkcreate_controler = file_linkcreate_controler.file_linkcreate_controler( self , getFileItemListSelected, self.pathSelected, self.favDirList  )
        self.file_linkcreate_controler.dialogShow()

    def onActionFileRename( self) :
        """
        Renommer un fichier
        """
        from appmod import file_rename_controler
        getFileItemListSelected = [ self.fileItemList[self.currentItemIndex] ]
        self.file_rename_controler = file_rename_controler.file_rename_controler( self , getFileItemListSelected )
        self.file_rename_controler.dialogShow()

    def onActionFileDel( self) :
        """
        Supprimer les fichiers de la selection
        """
        from appmod import file_delete_controler
        getFileItemListSelected = [ self.fileItemList[self.currentItemIndex] ]
        self.file_delete_controler = file_delete_controler.file_delete_controler( self , getFileItemListSelected)
        self.file_delete_controler.dialogShow()

    def onActionFileMove( self) :
        """
        Déplacer les fichiers de la selection
        """
        from appmod import file_move_controler
        getFileItemListSelected = [ self.fileItemList[self.currentItemIndex] ]
        self.file_move_controler = file_move_controler.file_move_controler( self.mainWindow, getFileItemListSelected, self.pathSelected, self.favDirList)
        self.file_move_controler.dialogShow()

    def onActionFileCopy( self, fileName) :
        """
        Copier les fichiers de la selection
        """
        from appmod import file_copy_controler
        getFileItemListSelected = [ self.fileItemList[self.currentItemIndex] ]
        self.file_copy_controler = file_copy_controler.file_copy_controler( self , getFileItemListSelected, self.pathSelected, self.favDirList )
        self.file_copy_controler.dialogShow()


    def onActionSetBgColor( self, bgColor )  :
        self.scene.setBackgroundBrush(QColor(bgColor))
        self.bgColorCurrent = bgColor

    """
    def onActionDisplayRenderAntialiasing(self) :
        if self.renderAntialiasing == True :
            self.renderAntialiasing = False
        else :
            self.renderAntialiasing = True

        self.setRenderHint( QPainter.Antialiasing, self.renderAntialiasing)
        self.displayRefreshImage()

    def onActionDisplayRenderSmoothPixmapTransform( self) :
        if self.renderSmoothPixmapTransform == True :
            self.renderSmoothPixmapTransform = False
        else :
            self.renderSmoothPixmapTransform = True

        self.setRenderHint( QPainter.SmoothPixmapTransform, self.renderSmoothPixmapTransform )
        self.displayRefreshImage()
    """

    def onActionDisplayRatioFill( self ) :
        """
        Qt.KeepAspectRatio: Fit in viewport using aspect ratio.
        """
        self.aspectRatio = Qt.KeepAspectRatio

        self.actionDisplayRatioFill.setChecked(True)
        self.actionDisplayRatioItgnore.setChecked(False)
        self.actionDisplayRatioExpand.setChecked(False)

        self.displayRefreshImage()

    def onActionDisplayRatioItgnore( self ) :
        """
         Qt.IgnoreAspectRatio: Fit to viewport.
        """
        self.aspectRatio = Qt.IgnoreAspectRatio

        self.actionDisplayRatioFill.setChecked(False)
        self.actionDisplayRatioItgnore.setChecked(True)
        self.actionDisplayRatioExpand.setChecked(False)

        self.displayRefreshImage()

    def onActionDisplayRatioExpand( self ) :
        """
        Qt.KeepAspectRatioByExpanding: Fill viewport using aspect ratio.
        """
        self.aspectRatio = Qt.KeepAspectRatioByExpanding

        self.actionDisplayRatioFill.setChecked(False)
        self.actionDisplayRatioItgnore.setChecked(False)
        self.actionDisplayRatioExpand.setChecked(True)

        self.displayRefreshImage()

    def onActionExifRotate(self) :
        if self.exifRotate == False :
            self.exifRotate = True
        else :
            self.exifRotate = False

        self.displayRefreshImage()

    def onActionDisplayTextInfo(self) :
        if self.infoTextDisplay == 1 :
            self.infoTextDisplay = 0
            if self.textImageItem != None :
                try:
                    self.scene.removeItem (self.textImageItem)
                except :
                    pass
        else :
            self.infoTextDisplay = 1
            self.displayImageInfoLabel()

    def onActionAjustToWindow(self) :
        self.ensureVisible ( self.scene.sceneRect() )
        self.fitInView( self.scene.sceneRect(), self.aspectRatio)
        self.NormalZoom = False
        self.FitZoom = True
        self.autoAdjustZoom = False

    def onActionActionZoom1(self) :
        self.NormalZoom = True
        self.FitZoom = False
        self.autoAdjustZoom = False
        self.setTransform(QTransform())

    def onActionToggleAdjust(self) :
        if self.autoAdjustZoom == False :

            self.NormalZoom = False
            self.FitZoom = False
            self.autoAdjustZoom = True

            self.ensureVisible ( self.scene.sceneRect() )
            self.fitInView( self.scene.sceneRect(), self.aspectRatio)
        else :
            self.NormalZoom = False
            self.FitZoom = False
            self.autoAdjustZoom = False

    def onActionFullScreenExit(self) :
        self.setWindowFlags(Qt.Widget)
        self.show()
        #self.setStyleSheet("QGraphicsView { border: 0px solid ; }")

        self.fullScreenMode = False

    def onActionFullScreenEnter(self) :

        self.widgetWidth = self.width()
        self.widgetHeight = self.height()

        self.setWindowFlags(Qt.Window)
        self.setFrameShape(QFrame.NoFrame)


        self.showFullScreen()
        #self.setStyleSheet("QGraphicsView:focus {border: 10px solid red;}")

        #self.setStyleSheet( "QGraphicsView { border-style: none; }" )
        #self.setStyleSheet("border-width: 0px; border-style: solid")
        #self.setStyleSheet("border: 0px")
        #self.setStyleSheet("QGraphicsView { border: 1px solid black; }")

        self.fullScreenMode = True

        # fix : test is fullscreen == true :
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

    def onActionRotate(self) :
        self.rotate(90)
        if self.autoAdjustZoom == True :
            self.ensureVisible ( self.scene.sceneRect() )
            self.fitInView( self.scene.sceneRect(), self.aspectRatio)

    def displayImageInfoLabel( self ) :
        """
        Affiche un label d'information sur l'image actuelle (nom , taille ...)
        """

        if self.currentItemIndex != None :

            textImageInfo = self.getImageInfo( self.fileItemList[self.currentItemIndex] )
            if self.textImageItem != None :
                try:
                    self.scene.removeItem (self.textImageItem)
                except :
                    pass
            #textImageInfo.setDefaultTextColor(QColor(10, 100, 0))
            textImageInfo.setFlag(QGraphicsItem.ItemIgnoresTransformations, True)
            self.textImageItem = self.scene.addItem(textImageInfo)
            self.textImageItem  = textImageInfo


    def setUpDisplay(self, filePath, fileName , fileItemList):
        """
        point d'entrée principal de premier affichage d'une image d'un dossier
        """

        self.scene.clear()
        self.currentItemIndex = None
        self.fileItemList = None
        self.exifTableViewWidget.setData( [] )

        if filePath != None and fileName != None :

            self.fileName = fileName
            pixmap = QPixmap( filePath + "/" + fileName )
            self.pixmap = pixmap
            self.fileItemList = fileItemList

            if self.fileItemList != None :

                row = -1
                self.currentItemIndex = -1
                for fileItem in self.fileItemList :
                    row = row + 1

                    if fileItem.fname == self.fileName :
                        self.currentItemIndex = row
                        break

                if self.exifRotate == True :
                    self.pixmap = self.exifRotateView( self.pixmap , self.fileItemList[self.currentItemIndex].get_orientation() )

                self.imItem = self.scene.addPixmap(self.pixmap)

                self.scene.setSceneRect(0, 0, self.pixmap.width(), self.pixmap.height())

                if self.autoAdjustZoom == True :
                    self.ensureVisible ( self.scene.sceneRect() )
                    self.fitInView( self.scene.sceneRect(), self.aspectRatio)

                if self.infoTextDisplay == 1 :
                    self.displayImageInfoLabel()

            self.displayExifTableView()


    def getImageInfo(self , fileItem ) :

        infoText = self.infoTextTemplate

        infoText = infoText.replace("[fname]", self.fileItemList[self.currentItemIndex].fname)
        infoText = infoText.replace("[zoom]", str( round( self.transform().m11() * 1  ,1)  ) )
        infoText = infoText.replace("[pixw]", str(self.fileItemList[self.currentItemIndex].width) )
        infoText = infoText.replace("[pixh]", str(self.fileItemList[self.currentItemIndex].width) )
        infoText = infoText.replace("[imgnum]", str(self.currentItemIndex+1) )
        infoText = infoText.replace("[imgcount]", str(len(self.fileItemList)) )



        textImageInfo = QGraphicsTextItem( "infoText" )

        #textImageInfo.setTextWidth( self.scene.width() )

        textImageInfo.setHtml( "<div style=\"background-color: "+str(self.infoTextBgColor)+"; color:"+str(self.infoTextColor)+"\">" + infoText + "</div>" )

        #textImageInfo.setHtml( "<div style=\"<div style=\"background:rgba(255, 255, 255, 50%); color:rgba(255,255,255)\">" + infoText + "</div>" )
        #QColor backgroundColor = palette().light().color();
        #backgroundColor.setAlpha(200);
        #textImageInfo.setDefaultTextColor (QColor(0,100,200))

        textImageInfo.setFont(QFont(self.infoTextfont,self.infoTextfontSize))

        testImageW = textImageInfo.boundingRect().width()
        testImageH = textImageInfo.boundingRect().height()

        sceneRect = self.scene.sceneRect()

        if self.infoTextPos == 1 :
            right = 0 +self.infoTextOffset
            top =  0 + self.infoTextOffset
            textImageInfo.setPos(QPointF(right,top))
        if self.infoTextPos == 2 :
            right = sceneRect.right() - testImageW - self.infoTextOffset
            top =  0 + self.infoTextOffset
            textImageInfo.setPos(QPointF(right,top))
        if self.infoTextPos == 3 :
            right = sceneRect.right() - testImageW -self.infoTextOffset
            top =  sceneRect.bottom() - testImageH -self.infoTextOffset
            textImageInfo.setPos(QPointF(right,top))
        if self.infoTextPos == 4 :
            right = 0 + self.infoTextOffset
            top =  sceneRect.bottom() - testImageH - self.infoTextOffset
            textImageInfo.setPos(QPointF(right,top))

        return textImageInfo


    def resizeEvent(self, evt=None):

        QApplication.setOverrideCursor(Qt.WaitCursor)

        if self.autoAdjustZoom == True :
            self.ensureVisible ( self.scene.sceneRect() )
            self.fitInView( self.scene.sceneRect(), self.aspectRatio)

        if self.infoTextDisplay == 1 :
            self.displayImageInfoLabel()

        super(imageviewer, self).resizeEvent(evt)

        QApplication.restoreOverrideCursor()

    def keyPressEvent(self, event):

        if event.key() == Qt.Key_F1 :

            print ("***** fname = " + str(self.fileItemList[self.currentItemIndex].fname))
            print ("Image wxh   : " + str(self.fileItemList[self.currentItemIndex].width) + " x " + str(self.fileItemList[self.currentItemIndex].height))
            print ("Pixmap wxh  : " + str(self.pixmap.size().width()) + " x " + str(self.pixmap.size().height()))
            print ("Scene wxh   : " + str(self.scene.width()) + " x " + str(self.scene.height())) # Taille du pixmap
            print ("Ecran wxh   : " + str(self.viewport().width()) + " x " + str(self.viewport().height()))
            centerS = self.mapToScene( self.viewport().rect()).boundingRect().center()
            print ("Image center (view coordinate) x/y = " + str(centerS.x()) + "/" + str(centerS.y()))
            print ("Scale x/y   : " + str(self.transform().m11()) + "/" + str(self.transform().m22()))
            print ("visibleArea wxh : " + str( self.geometry().width() ) + " x " + str(self.geometry().height()))
            geoBoundRect = self.mapToScene( self.viewport().geometry()).boundingRect()
            print ("viewport().geometry()).boundingRect mapped wxh : " + str(geoBoundRect.width()) + " x " + str(geoBoundRect.height()))
            print ("Widget wxh  : " + str(self.widgetWidth) + " x " + str(self.widgetHeight))
            print ("-------------")

        if event.key() == Qt.Key_Escape :

            self.setWindowFlags(Qt.Widget)
            self.show()
            self.fullScreenMode = False

        if event.key() == Qt.Key_Left  or event.key() == Qt.Key_Up:
            self.displayPrevImage()

        if event.key() == Qt.Key_Right or event.key() == Qt.Key_Space or event.key() == Qt.Key_Down :
            self.displayNextImage()



    def wheelEvent(self,event):
        """
        Zoom in or out of the view.
        Previous, Next Image
        """
        modifiers = QApplication.keyboardModifiers()

        if modifiers == Qt.ControlModifier:

            zoomInFactor = 1.1
            zoomOutFactor = 1 / zoomInFactor

            # Save the scene pos
            oldPos = self.mapToScene(event.pos())

            # Zoom
            if event.angleDelta().y() > 0:
                zoomFactor = zoomInFactor
            else:
                zoomFactor = zoomOutFactor
            self.scale(zoomFactor, zoomFactor)

            # Get the new position
            newPos = self.mapToScene(event.pos())

            # Move scene to old position
            delta = newPos - oldPos
            self.translate(delta.x(), delta.y()) # Change les coordonnées de l'origine

            self.FitZoom = False
            self.NormalZoom = False
            self.autoAdjustZoom = False

            if self.infoTextDisplay == 1 :
                self.displayImageInfoLabel()

            return

        if event.angleDelta().y() > 0:
            self.displayPrevImage()
        else :
            self.displayNextImage()

    def mousePressEvent(self, event):
        """
        Déplacement de l'image à l'aide de la souris + appui sur la touche Ctrl
        """
        scenePos = self.mapToScene(event.pos())
        if event.button() == Qt.LeftButton:
            self.setDragMode(QGraphicsView.ScrollHandDrag)
            self.leftMouseButtonPressed.emit(scenePos.x(), scenePos.y())

        QGraphicsView.mousePressEvent(self, event)


    def displayPixMapByIndex(self, imgIndex) :

        self.pixmap = QPixmap( self.fileItemList[imgIndex].filePath + "/" + self.fileItemList[imgIndex].fname )
        if self.exifRotate == True :
            self.pixmap = self.exifRotateView( self.pixmap , self.fileItemList[imgIndex].get_orientation())
        #self.imItem.setTransformationMode(Qt.SmoothTransformation)
        #self.imItem.setTransformationMode(Qt.FastTransformation)

        self.imItem.setPixmap(self.pixmap)
        self.scene.setSceneRect(0, 0, self.pixmap.width(), self.pixmap.height())
        if self.autoAdjustZoom == True :
            self.ensureVisible ( self.scene.sceneRect() )
            self.fitInView( self.scene.sceneRect(), self.aspectRatio)

    def displayPrevImage( self ):

        if self.currentItemIndex == None : return

        QApplication.setOverrideCursor(Qt.WaitCursor)
        self.currentItemIndex= self.currentItemIndex - 1
        if self.currentItemIndex < 0 :
            self.currentItemIndex = 0

        self.displayPixMapByIndex(self.currentItemIndex)

        if self.infoTextDisplay == 1 :
            self.displayImageInfoLabel()

        self.displayExifTableView()

        QApplication.restoreOverrideCursor()


    def displayRefreshImage( self ):

        if self.currentItemIndex == None : return

        QApplication.setOverrideCursor(Qt.WaitCursor)

        self.displayPixMapByIndex(self.currentItemIndex)

        if self.infoTextDisplay == 1 :
            self.displayImageInfoLabel()

        self.displayExifTableView()

        QApplication.restoreOverrideCursor()

    def displayNextImage( self ):

        if self.currentItemIndex == None : return

        QApplication.setOverrideCursor(Qt.WaitCursor)
        self.currentItemIndex = self.currentItemIndex + 1
        if self.currentItemIndex > len(self.fileItemList)-1 :
            self.currentItemIndex = 0

        self.displayPixMapByIndex(self.currentItemIndex)

        if self.infoTextDisplay == 1 :
            self.displayImageInfoLabel()

        self.displayExifTableView()

        QApplication.restoreOverrideCursor()


    def displayExifTableView( self ) :
        """
            Trie puis affiche, dans un tableView, les informations Exif de l'image actuelle
        """
        if self.fileItemList != None :

            """keylist = self.fileItemList[self.currentItemIndex].exif.keys()
            for key in keylist:
                print ("%s: %s" % (key, self.fileItemList[self.currentItemIndex].exif[key]))
            """

            exifArrSorted=[]
            keylist = self.fileItemList[self.currentItemIndex].exif.keys()
            #keylist=sorted(keylist, key=lambda x: x[0]) # python 2 & 3 compatible sort
            keylist=sorted(keylist) # python 2 & 3 compatible sort
            for key in keylist:
                #print "%s: %s" % (key, self.fileItemList[self.currentItemIndex].exif[key])
                exifArrSorted.append( [ key, self.fileItemList[self.currentItemIndex].exif[key] ] )


            #self.exifTableViewWidget.deleteRows()
            #self.exifTableViewWidget.setData( [ [] ] )

            #if len(exifArrSorted) > 0 :
            self.exifTableViewWidget.setData( exifArrSorted )

            """currentHeaderState = self.exifTableViewWidget.horizontalHeader().saveState()
            self.exifTableViewWidget.setData( [ [] ] )
            self.exifTableViewWidget.horizontalHeader().restoreState( currentHeaderState )
            """

    def exifRotateView(self, pixmap , orientation) :
        """
            Effectue une rotation du pixmap, selon le tag exif "Rotation"

            There are 8 possible EXIF orientation values, numbered 1 to 8.

                1 : 0 degrees – the correct orientation, no adjustment is required.
                2 : 0 degrees, mirrored – image has been flipped back-to-front.
                3 : 180 degrees – image is upside down.
                4 : 180 degrees, mirrored – image is upside down and flipped back-to-front.
                5 : 90 degrees – image is on its side.
                6 : 90 degrees, mirrored – image is on its side and flipped back-to-front.
                7 : 270 degrees – image is on its far side.
                8 : 270 degrees, mirrored – image is on its far side and flipped back-to-front.
        """

        try :

            pixmapRet = pixmap

            if orientation == "1" :
                self.lastRotate = 0

            if orientation == "2" :
                self.lastRotate = 0
                pixmapRet = pixmap.transformed(QTransform().scale(-1, 1))

            if orientation == "3" :
                self.lastRotate = 180
                pixmapRet = pixmap.transformed(QTransform().rotate(self.lastRotate))

            if orientation == "4" :
                self.lastRotate = 180
                pixmapRet = pixmap.transformed(QTransform().scale(1, -1))

            if orientation == "5" :
                self.lastRotate = 90
                pixmapRet = pixmap.transformed(QTransform().rotate(self.lastRotate))
                pixmapRet = pixmapRet.transformed(QTransform().scale(-1, 1))

            if  orientation == "6" :
                self.lastRotate = 90
                pixmapRet = pixmap.transformed(QTransform().rotate(self.lastRotate))

            if orientation == "7" :
                self.lastRotate = 270
                pixmapRet = pixmap.transformed(QTransform().rotate(self.lastRotate))
                pixmapRet = pixmapRet.transformed(QTransform().scale(-1, 1))

            if orientation == "8" :
                self.lastRotate = 270
                pixmapRet = pixmap.transformed(QTransform().rotate(self.lastRotate))
                pixmapRet = pixmapRet.transformed(QTransform().scale(1, 1))

            return pixmapRet

        except :
            pass


