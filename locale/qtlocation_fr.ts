<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>QDeclarativeGeoMap</name>
    <message>
        <location filename="../../qtlocation/src/imports/location/qdeclarativegeomap.cpp" line="+184"/>
        <location line="+1"/>
        <source>No Map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+129"/>
        <source>Plugin does not support mapping.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QDeclarativeGeoRouteModel</name>
    <message>
        <location filename="../../qtlocation/src/imports/location/qdeclarativegeoroutemodel.cpp" line="+329"/>
        <source>Plugin does not support routing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+246"/>
        <source>Cannot route, plugin not set.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Cannot route, route manager not set.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Not enough waypoints for routing.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QDeclarativeGeocodeModel</name>
    <message>
        <location filename="../../qtlocation/src/imports/location/qdeclarativegeocodemodel.cpp" line="+153"/>
        <source>Cannot geocode, plugin not set.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Cannot geocode, geocode manager not set.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cannot geocode, valid query not set.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+146"/>
        <source>Plugin does not support (reverse) geocoding.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QGeoRouteParserOsrmV4</name>
    <message>
        <location filename="../../qtlocation/src/location/maps/qgeorouteparserosrmv4.cpp" line="+133"/>
        <source>Go straight.</source>
        <translation type="unfinished">Continuer tout droit.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Go straight onto %1.</source>
        <translation type="unfinished">Continuer tout droit sur %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Turn slightly right.</source>
        <translation type="unfinished">Tourner légèrement à droite.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Turn slightly right onto %1.</source>
        <translation type="unfinished">Tourner légèrement à droite sur %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Turn right.</source>
        <translation type="unfinished">Tourner à droite.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Turn right onto %1.</source>
        <translation type="unfinished">Tourner à droite sur %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Make a sharp right.</source>
        <translation type="unfinished">Faire un virage serré à droite.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Make a sharp right onto %1.</source>
        <translation type="unfinished">Faire un virage serré à droite sur %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>When it is safe to do so, perform a U-turn.</source>
        <translation type="unfinished">Lorsque c&apos;est sûr de le faire, effectuer un demi-tour.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Make a sharp left.</source>
        <translation type="unfinished">Faire un virage serré à gauche.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Make a sharp left onto %1.</source>
        <translation type="unfinished">Faire un virage serré à gauche sur %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Turn left.</source>
        <translation type="unfinished">Tourner à gauche.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Turn left onto %1.</source>
        <translation type="unfinished">Tourner à gauche sur %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Turn slightly left.</source>
        <translation type="unfinished">Tourner légèrement à gauche.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Turn slightly left onto %1.</source>
        <translation type="unfinished">Tourner légèrement à gauche sur %1.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reached waypoint.</source>
        <translation type="unfinished">Étape atteinte.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Head on.</source>
        <translation type="unfinished">Continuer dans cette direction.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Head onto %1.</source>
        <translation type="unfinished">Se diriger vers %1.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter the roundabout.</source>
        <translation type="unfinished">Entrer sur le rond-point.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the roundabout take the first exit.</source>
        <translation type="unfinished">Au rond-point, prendre la première sortie.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout take the first exit onto %1.</source>
        <translation type="unfinished">Au rond-point, prendre la première sortie vers %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the roundabout take the second exit.</source>
        <translation type="unfinished">Au rond-point, prendre la seconde sortie.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout take the second exit onto %1.</source>
        <translation type="unfinished">Au rond-point, prendre la seconde sortie vers %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the roundabout take the third exit.</source>
        <translation type="unfinished">Au rond-point, prendre la troisième sortie.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout take the third exit onto %1.</source>
        <translation type="unfinished">Au rond-point, prendre la troisième sortie vers %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the roundabout take the fourth exit.</source>
        <translation type="unfinished">Au rond-point, prendre la quatrième sortie.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout take the fourth exit onto %1.</source>
        <translation type="unfinished">Au rond-point, prendre la quatrième sortie vers %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the roundabout take the fifth exit.</source>
        <translation type="unfinished">Au rond-point, prendre la cinquième sortie.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout take the fifth exit onto %1.</source>
        <translation type="unfinished">Au rond-point, prendre la cinquième sortie vers %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the roundabout take the sixth exit.</source>
        <translation type="unfinished">Au rond-point, prendre la sixième sortie.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout take the sixth exit onto %1.</source>
        <translation type="unfinished">Au rond-point, prendre la sixième sortie vers %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the roundabout take the seventh exit.</source>
        <translation type="unfinished">Au rond-point, prendre la septième sortie.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout take the seventh exit onto %1.</source>
        <translation type="unfinished">Au rond-point, prendre la septième sortie vers %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the roundabout take the eighth exit.</source>
        <translation type="unfinished">Au rond-point, prendre la huitième sortie.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout take the eighth exit onto %1.</source>
        <translation type="unfinished">Au rond-point, prendre la huitième sortie vers %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the roundabout take the ninth exit.</source>
        <translation type="unfinished">Au rond-point, prendre la neuvième sortie.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout take the ninth exit onto %1.</source>
        <translation type="unfinished">Au rond-point, prendre la neuvième sortie vers %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Leave the roundabout.</source>
        <translation type="unfinished">Quitter le rond-point.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Leave the roundabout onto %1.</source>
        <translation type="unfinished">Quitter le rond-point vers %1.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stay on the roundabout.</source>
        <translation type="unfinished">Rester sur le rond-point.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Start at the end of the street.</source>
        <translation type="unfinished">Commencer à la fin de la rue.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Start at the end of %1.</source>
        <translation type="unfinished">Commencer à la fin de %1.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>You have reached your destination.</source>
        <translation type="unfinished">Vous avez atteint votre destination.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Don&apos;t know what to say for &apos;%1&apos;</source>
        <translation type="unfinished">Je ne sais quoi dire pour &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>QGeoRouteParserOsrmV5</name>
    <message>
        <location filename="../../qtlocation/src/location/maps/qgeorouteparserosrmv5.cpp" line="+99"/>
        <source>North</source>
        <extracomment>Always used in &quot;Head %1 [onto &lt;street name&gt;]&quot;</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>East</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>South</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>West</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>first</source>
        <comment>roundabout exit</comment>
        <extracomment>always used in &quot; and take the %1 exit [onto &lt;street name&gt;]&quot;</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>second</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>third</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>fourth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>fifth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>sixth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>seventh</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>eighth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ninth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>tenth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>eleventh</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>twelfth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>thirteenth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>fourteenth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>fifteenth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>sixteenth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>seventeenth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>eighteenth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>nineteenth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>twentieth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source> and take the %1 exit</source>
        <extracomment>Always appended to one of the following strings: - &quot;Enter the roundabout&quot; - &quot;Enter the rotary&quot; - &quot;Enter the rotary &lt;rotaryname&gt;&quot;</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source> and take the %1 exit onto %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>You have arrived at your destination, straight ahead</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>You have arrived at your destination, on the left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>You have arrived at your destination, on the right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>You have arrived at your destination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+252"/>
        <location line="+220"/>
        <source>Continue straight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-470"/>
        <source>Continue straight on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Continue left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Continue left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+213"/>
        <source>Continue slightly left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-211"/>
        <source>Continue slightly left on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Continue right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Continue right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Continue slightly right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Continue slightly right on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+113"/>
        <location line="+104"/>
        <location line="+198"/>
        <source>Make a U-turn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-413"/>
        <location line="+113"/>
        <location line="+104"/>
        <location line="+198"/>
        <source>Make a U-turn onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-412"/>
        <location line="+222"/>
        <location line="+30"/>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-250"/>
        <location line="+252"/>
        <source>Continue on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-242"/>
        <source>Head %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Head %1 onto %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Depart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Depart onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>At the end of the road, turn left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the end of the road, turn left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>At the end of the road, turn right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the end of the road, turn right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>At the end of the road, make a U-turn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the end of the road, make a U-turn onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the end of the road, continue straight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the end of the road, continue straight onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the end of the road, continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the end of the road, continue onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Take the ferry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>At the fork, take a sharp left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the fork, take a sharp left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the fork, turn left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the fork, turn left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>At the fork, keep left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the fork, keep left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the fork, take a sharp right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the fork, take a sharp right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the fork, turn right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the fork, turn right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>At the fork, keep right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the fork, keep right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>At the fork, continue straight ahead</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the fork, continue straight ahead onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the fork, continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the fork, continue onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Merge sharply left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Merge sharply left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Merge left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Merge left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Merge slightly left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Merge slightly left on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Merge sharply right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Merge sharply right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Merge right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Merge right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Merge slightly right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Merge slightly right on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Merge straight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Merge straight on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Merge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Merge onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Take a sharp left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Take a sharp left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+202"/>
        <source>Turn left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-200"/>
        <location line="+202"/>
        <source>Turn left onto %1</source>
        <translation type="unfinished">Tourner à gauche sur %1. {1?}</translation>
    </message>
    <message>
        <location line="-196"/>
        <source>Continue slightly left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Take a sharp right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Take a sharp right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+198"/>
        <source>Turn right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-196"/>
        <location line="+198"/>
        <source>Turn right onto %1</source>
        <translation type="unfinished">Tourner à droite sur %1. {1?}</translation>
    </message>
    <message>
        <location line="-194"/>
        <source>Contine slightly right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Contine slightly right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+220"/>
        <source>Continue straight onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-215"/>
        <source>Continue onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Continue on the left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Continue on the left on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Contine on the right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Contine on the right on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Take the ramp on the left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Take the ramp on the left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Take the ramp on the right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Take the ramp on the right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Take the ramp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Take the ramp onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Get off the bike and push</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Get off the bike and push onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Enter the rotary</source>
        <extracomment>This string will be prepended to &quot; and take the &lt;nth&gt; exit [onto &lt;streetname&gt;]</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Enter the roundabout</source>
        <extracomment>This string will be prepended to &quot; and take the &lt;nth&gt; exit [onto &lt;streetname&gt;]</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>At the roundabout, continue straight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout, continue straight on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>At the roundabout, turn left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout, turn left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>At the roundabout, turn right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout, turn right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>At the roundabout, turn around</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout, turn around onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the roundabout, continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout, continue onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Take the train</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Go straight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Go straight onto %1</source>
        <translation type="unfinished">Continuer tout droit sur %1. {1?}</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Turn slightly left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Turn slightly left onto %1</source>
        <translation type="unfinished">Tourner légèrement à gauche sur %1. {1?}</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Turn slightly right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Turn slightly right onto %1</source>
        <translation type="unfinished">Tourner légèrement à droite sur %1. {1?}</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Turn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Turn onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+29"/>
        <source> and continue straight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source> and continue straight onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source> and make a sharp left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source> and make a sharp left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source> and turn left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source> and turn left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source> and make a slight left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source> and make a slight left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source> and make a sharp right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source> and make a sharp right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source> and turn right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source> and turn right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source> and make a slight right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source> and make a slight right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source> and make a U-turn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source> and make a U-turn onto %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QGeoRouteReplyOsm</name>
    <message>
        <source>Go straight.</source>
        <translation type="vanished">Continuer tout droit.</translation>
    </message>
    <message>
        <source>Go straight onto %1.</source>
        <translation type="vanished">Continuer tout droit sur %1.</translation>
    </message>
    <message>
        <source>Turn slightly right.</source>
        <translation type="vanished">Tourner légèrement à droite.</translation>
    </message>
    <message>
        <source>Turn slightly right onto %1.</source>
        <translation type="vanished">Tourner légèrement à droite sur %1.</translation>
    </message>
    <message>
        <source>Turn right.</source>
        <translation type="vanished">Tourner à droite.</translation>
    </message>
    <message>
        <source>Turn right onto %1.</source>
        <translation type="vanished">Tourner à droite sur %1.</translation>
    </message>
    <message>
        <source>Make a sharp right.</source>
        <translation type="vanished">Faire un virage serré à droite.</translation>
    </message>
    <message>
        <source>Make a sharp right onto %1.</source>
        <translation type="vanished">Faire un virage serré à droite sur %1.</translation>
    </message>
    <message>
        <source>When it is safe to do so, perform a U-turn.</source>
        <translation type="vanished">Lorsque c&apos;est sûr de le faire, effectuer un demi-tour.</translation>
    </message>
    <message>
        <source>Make a sharp left.</source>
        <translation type="vanished">Faire un virage serré à gauche.</translation>
    </message>
    <message>
        <source>Make a sharp left onto %1.</source>
        <translation type="vanished">Faire un virage serré à gauche sur %1.</translation>
    </message>
    <message>
        <source>Turn left.</source>
        <translation type="vanished">Tourner à gauche.</translation>
    </message>
    <message>
        <source>Turn left onto %1.</source>
        <translation type="vanished">Tourner à gauche sur %1.</translation>
    </message>
    <message>
        <source>Turn slightly left.</source>
        <translation type="vanished">Tourner légèrement à gauche.</translation>
    </message>
    <message>
        <source>Turn slightly left onto %1.</source>
        <translation type="vanished">Tourner légèrement à gauche sur %1.</translation>
    </message>
    <message>
        <source>Reached waypoint.</source>
        <translation type="vanished">Étape atteinte.</translation>
    </message>
    <message>
        <source>Head on.</source>
        <translation type="vanished">Continuer dans cette direction.</translation>
    </message>
    <message>
        <source>Head onto %1.</source>
        <translation type="vanished">Se diriger vers %1.</translation>
    </message>
    <message>
        <source>Enter the roundabout.</source>
        <translation type="vanished">Entrer sur le rond-point.</translation>
    </message>
    <message>
        <source>At the roundabout take the first exit.</source>
        <translation type="vanished">Au rond-point, prendre la première sortie.</translation>
    </message>
    <message>
        <source>At the roundabout take the first exit onto %1.</source>
        <translation type="vanished">Au rond-point, prendre la première sortie vers %1.</translation>
    </message>
    <message>
        <source>At the roundabout take the second exit.</source>
        <translation type="vanished">Au rond-point, prendre la seconde sortie.</translation>
    </message>
    <message>
        <source>At the roundabout take the second exit onto %1.</source>
        <translation type="vanished">Au rond-point, prendre la seconde sortie vers %1.</translation>
    </message>
    <message>
        <source>At the roundabout take the third exit.</source>
        <translation type="vanished">Au rond-point, prendre la troisième sortie.</translation>
    </message>
    <message>
        <source>At the roundabout take the third exit onto %1.</source>
        <translation type="vanished">Au rond-point, prendre la troisième sortie vers %1.</translation>
    </message>
    <message>
        <source>At the roundabout take the fourth exit.</source>
        <translation type="vanished">Au rond-point, prendre la quatrième sortie.</translation>
    </message>
    <message>
        <source>At the roundabout take the fourth exit onto %1.</source>
        <translation type="vanished">Au rond-point, prendre la quatrième sortie vers %1.</translation>
    </message>
    <message>
        <source>At the roundabout take the fifth exit.</source>
        <translation type="vanished">Au rond-point, prendre la cinquième sortie.</translation>
    </message>
    <message>
        <source>At the roundabout take the fifth exit onto %1.</source>
        <translation type="vanished">Au rond-point, prendre la cinquième sortie vers %1.</translation>
    </message>
    <message>
        <source>At the roundabout take the sixth exit.</source>
        <translation type="vanished">Au rond-point, prendre la sixième sortie.</translation>
    </message>
    <message>
        <source>At the roundabout take the sixth exit onto %1.</source>
        <translation type="vanished">Au rond-point, prendre la sixième sortie vers %1.</translation>
    </message>
    <message>
        <source>At the roundabout take the seventh exit.</source>
        <translation type="vanished">Au rond-point, prendre la septième sortie.</translation>
    </message>
    <message>
        <source>At the roundabout take the seventh exit onto %1.</source>
        <translation type="vanished">Au rond-point, prendre la septième sortie vers %1.</translation>
    </message>
    <message>
        <source>At the roundabout take the eighth exit.</source>
        <translation type="vanished">Au rond-point, prendre la huitième sortie.</translation>
    </message>
    <message>
        <source>At the roundabout take the eighth exit onto %1.</source>
        <translation type="vanished">Au rond-point, prendre la huitième sortie vers %1.</translation>
    </message>
    <message>
        <source>At the roundabout take the ninth exit.</source>
        <translation type="vanished">Au rond-point, prendre la neuvième sortie.</translation>
    </message>
    <message>
        <source>At the roundabout take the ninth exit onto %1.</source>
        <translation type="vanished">Au rond-point, prendre la neuvième sortie vers %1.</translation>
    </message>
    <message>
        <source>Leave the roundabout.</source>
        <translation type="vanished">Quitter le rond-point.</translation>
    </message>
    <message>
        <source>Leave the roundabout onto %1.</source>
        <translation type="vanished">Quitter le rond-point vers %1.</translation>
    </message>
    <message>
        <source>Stay on the roundabout.</source>
        <translation type="vanished">Rester sur le rond-point.</translation>
    </message>
    <message>
        <source>Start at the end of the street.</source>
        <translation type="vanished">Commencer à la fin de la rue.</translation>
    </message>
    <message>
        <source>Start at the end of %1.</source>
        <translation type="vanished">Commencer à la fin de %1.</translation>
    </message>
    <message>
        <source>You have reached your destination.</source>
        <translation type="vanished">Vous avez atteint votre destination.</translation>
    </message>
    <message>
        <source>Don&apos;t know what to say for &apos;%1&apos;</source>
        <translation type="vanished">Je ne sais quoi dire pour &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>QGeoServiceProviderFactoryMapbox</name>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/mapbox/qgeoserviceproviderpluginmapbox.cpp" line="+64"/>
        <source>Mapbox plugin requires &apos;mapbox.map_id&apos; and &apos;mapbox.access_token&apos; parameters.
Please visit https://www.mapbox.com</source>
        <translation>Le plugin Mapbox a requiert les paramètres &apos;mapbox.map_id&apos; et &apos;mapbox.access_token&apos;.
Plus d&apos;informations sur https://www.mapbox.com</translation>
    </message>
</context>
<context>
    <name>QGeoTileFetcherNokia</name>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/nokia/qgeotilefetcher_nokia.cpp" line="+100"/>
        <source>Mapping manager no longer exists</source>
        <translation>Le gestionnaire de cartographie n&apos;existe plus</translation>
    </message>
</context>
<context>
    <name>QGeoTiledMapOsm</name>
    <message>
        <source>Tiles Courtesy of &lt;a href=&apos;http://www.mapquest.com/&apos;&gt;MapQuest&lt;/a&gt;&lt;br/&gt;Data &amp;copy; &lt;a href=&apos;http://www.openstreetmap.org/copyright&apos;&gt;OpenStreetMap&lt;/a&gt; contributors</source>
        <translation type="vanished">Tiles Courtesy of &lt;a href=&apos;http://www.mapquest.com/&apos;&gt;MapQuest&lt;/a&gt;&lt;br/&gt;Data &amp;copy; &lt;a href=&apos;http://www.openstreetmap.org/copyright&apos;&gt;OpenStreetMap&lt;/a&gt; contributors</translation>
    </message>
    <message>
        <source>Maps &amp;copy; &lt;a href=&apos;http://www.thunderforest.com/&apos;&gt;Thunderforest&lt;/a&gt;&lt;br/&gt;Data &amp;copy; &lt;a href=&apos;http://www.openstreetmap.org/copyright&apos;&gt;OpenStreetMap&lt;/a&gt; contributors</source>
        <translation type="vanished">Maps &amp;copy; &lt;a href=&apos;http://www.thunderforest.com/&apos;&gt;Thunderforest&lt;/a&gt;&lt;br/&gt;Data &amp;copy; &lt;a href=&apos;http://www.openstreetmap.org/copyright&apos;&gt;OpenStreetMap&lt;/a&gt; contributors</translation>
    </message>
    <message>
        <source>&amp;copy; &lt;a href=&apos;http://www.openstreetmap.org/copyright&apos;&gt;OpenStreetMap&lt;/a&gt; contributors</source>
        <translation type="vanished">&amp;copy; &lt;a href=&apos;http://www.openstreetmap.org/copyright&apos;&gt;OpenStreetMap&lt;/a&gt; contributors</translation>
    </message>
</context>
<context>
    <name>QGeoTiledMappingManagerEngineMapbox</name>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/mapbox/qgeotiledmappingmanagerenginemapbox.cpp" line="+57"/>
        <source>Custom</source>
        <translation>Personnalisée</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Mapbox custom map</source>
        <translation>Carte Mapbox personnalisée</translation>
    </message>
</context>
<context>
    <name>QGeoTiledMappingManagerEngineNokia</name>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/nokia/qgeotiledmappingmanagerengine_nokia.cpp" line="+75"/>
        <source>Street Map</source>
        <translation>Carte Routière</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+11"/>
        <source>Normal map view in daylight mode</source>
        <translation>Affichage normal de la carte en mode diurne</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Satellite Map</source>
        <translation>Carte Satellite</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Satellite map view in daylight mode</source>
        <translation>Afficher la carte satellite en mode diurne</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Terrain Map</source>
        <translation>Carte du Relief</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Terrain map view in daylight mode</source>
        <translation>Afficher la carte du Relief en mode diurne</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hybrid Map</source>
        <translation>Carte Hybride</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Satellite map view with streets in daylight mode</source>
        <translation>Afficher la carte satellite avec les routes en mode diurne</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transit Map</source>
        <translation>Carte de Transit</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Color-reduced map view with public transport scheme in daylight mode</source>
        <translation>Afficher la carte en couleurs réduites avec le plan des transports en commun en mode diurne</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Gray Street Map</source>
        <translation>Carte Routière Grise</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Color-reduced map view in daylight mode</source>
        <translation>Afficher la carte en couleurs réduites en mode diurne</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mobile Street Map</source>
        <translation>Carte Routière Mobile</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Mobile normal map view in daylight mode</source>
        <translation>Afficher la carte normale en mode diurne</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mobile Terrain Map</source>
        <translation>Carte du Relief Mobile</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Mobile terrain map view in daylight mode</source>
        <translation>Afficher la carte de relief en mode diurne</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mobile Hybrid Map</source>
        <translation>Carte Hybride Mobile</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Mobile satellite map view with streets in daylight mode</source>
        <translation>Afficher la carte satellite mobile avec les routes en mode diurne</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mobile Transit Map</source>
        <translation>Carte de Transit Mobile</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Mobile color-reduced map view with public transport scheme in daylight mode</source>
        <translation>Afficher la carte mobile avec les transports en commun en couleurs réduites en mode diurne</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mobile Gray Street Map</source>
        <translation>Carte Routière Mobile Grise</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Mobile color-reduced map view in daylight mode</source>
        <translation>Afficher la carte mobile en couleurs réduites en mode diurne</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Custom Street Map</source>
        <translation>Carte Routière Personnalisée</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Night Street Map</source>
        <translation>Carte Routière Nocturne</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Normal map view in night mode</source>
        <translation>Afficher la carte normale en mode nocturne</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mobile Night Street Map</source>
        <translation>Carte Routière Nocturne Mobile</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Mobile normal map view in night mode</source>
        <translation>Afficher la carte normale en mode nocturne</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Gray Night Street Map</source>
        <translation>Carte Routière Nocturne Grise</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Color-reduced map view in night mode (especially used for background maps)</source>
        <translation>Atténuation des couleurs de la carte en mode nuit (particulièrement utile pour les cartes de fond)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mobile Gray Night Street Map</source>
        <translation>Carte Routière Nocturne Grise Mobile</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Mobile color-reduced map view in night mode (especially used for background maps)</source>
        <translation>Atténuation des couleurs de la carte en mode nuit pour mobile (particulièrement utile pour les cartes de fond)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Pedestrian Street Map</source>
        <translation>Carte Routière Piétonne</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Pedestrian map view in daylight mode</source>
        <translation>Carte piétonne en mode diurne</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mobile Pedestrian Street Map</source>
        <translation>Carte Routière Piétonne Mobile</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Mobile pedestrian map view in daylight mode for mobile usage</source>
        <translation>Afficher la carte piétonne mobile en mode diurne pour un usage portable</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Pedestrian Night Street Map</source>
        <translation>Carte Routière Piétonne Nocturne</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Pedestrian map view in night mode</source>
        <translation>Afficher la carte piétonne en mode nocturne</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mobile Pedestrian Night Street Map</source>
        <translation>Carte Routière Nocturne Piétonne Mobile</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Mobile pedestrian map view in night mode for mobile usage</source>
        <translation>Afficher la carte piétonne mobile en mode nocturne pour un usage portable</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Car Navigation Map</source>
        <translation>Carte de navigation voiture</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Normal map view in daylight mode for car navigation</source>
        <translation>Vue normale de la carte en mode jour pour la navigation voiture</translation>
    </message>
</context>
<context>
    <name>QGeoTiledMappingManagerEngineOsm</name>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/osm/qgeotiledmappingmanagerengineosm.cpp" line="+73"/>
        <source>Street Map</source>
        <translation>Carte Routère</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Street map view in daylight mode</source>
        <translation>Afficher la carte des rues en mode diurne</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Satellite Map</source>
        <translation>Carte Satellite</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Satellite map view in daylight mode</source>
        <translation>Afficher la carte satellite en mode diurne</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cycle Map</source>
        <translation>Carte cycliste</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Cycle map view in daylight mode</source>
        <translation>Afficher la carte cycliste en mode diurne</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Transit Map</source>
        <translation>Carte de transit</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Public transit map view in daylight mode</source>
        <translation>Afficher la carte de transit en mode diurne</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Night Transit Map</source>
        <translation>Carte de transit de nuit</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Public transit map view in night mode</source>
        <translation>Afficher la carte de transit en mode nocturne</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Terrain Map</source>
        <translation>Carte du relief</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Terrain map view</source>
        <translation>Carte du relief</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Hiking Map</source>
        <translation>Carte de randonnée</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Hiking map view</source>
        <translation>Carte de randonnée</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Custom URL Map</source>
        <translation>URL de carte personnalisée</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Custom url map view set via urlprefix parameter</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QPlaceManagerEngineOsm</name>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/osm/qplacemanagerengineosm.cpp" line="+56"/>
        <source>Aeroway</source>
        <translation>Piste aérienne</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amenity</source>
        <translation>Équipement</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Building</source>
        <translation>Bâtiment</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Highway</source>
        <translation>Route</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Historic</source>
        <translation>Monument historique</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Land use</source>
        <translation>Territoire</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Leisure</source>
        <translation>Loisir</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Man made</source>
        <translation>Construction</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Natural</source>
        <translation>Nature</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Place</source>
        <translation>Emplacement</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Railway</source>
        <translation>Voie ferrée</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shop</source>
        <translation>Magasin</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Tourism</source>
        <translation>Tourisme</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Waterway</source>
        <translation>Voies navigables</translation>
    </message>
    <message>
        <location line="+244"/>
        <source>Network request error</source>
        <translation>Erreur de réseau</translation>
    </message>
</context>
<context>
    <name>QPlaceSearchReplyOsm</name>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/osm/qplacesearchreplyosm.cpp" line="+101"/>
        <source>Communication error</source>
        <translation>Erreur de communication</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Response parse error</source>
        <translation>Erreur d&apos;analyse de la réponse</translation>
    </message>
</context>
<context>
    <name>QtLocationQML</name>
    <message>
        <location filename="../../qtlocation/src/imports/location/error_messages.cpp" line="+44"/>
        <source>Plugin property is not set.</source>
        <translation>Le module n&apos;est pas configuré.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Plugin Error (%1): %2</source>
        <translation>Erreur du Module (%1) : %2</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Plugin Error (%1): Could not instantiate provider</source>
        <translation>Erreur de plugin (%1) : Impossible d&apos;instancier le fournisseur</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Plugin is not valid</source>
        <translation>Le module n&apos;est pas valide</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to initialize categories</source>
        <translation>Impossible d&apos;initialiser les catégories</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to create request</source>
        <translation>Impossible de créer la requête</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Index &apos;%1&apos; out of range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/nokia/qgeoerror_messages.cpp" line="+42"/>
        <source>Qt Location requires app_id and token parameters.
Please register at https://developer.here.com/ to get your personal application credentials.</source>
        <translation>Qt Location requiert les paramètres app_id et token.
Veuillez vous enregistrer https://developer.here.com/ pour obtenir les informations de connexion à votre espace application.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Saving places is not supported.</source>
        <translation>L&apos;enregistrement des lieux n&apos;est pas supporté.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Removing places is not supported.</source>
        <translation>La suppression des lieux n&apos;est pas supportée.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Saving categories is not supported.</source>
        <translation>L&apos;enregistrement des catégories n&apos;est pas supporté.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Removing categories is not supported.</source>
        <translation>La suppression des catégories n&apos;est pas supportée.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error parsing response.</source>
        <translation>Erreur d&apos;analyse de la réponse.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Network error.</source>
        <translation>Erreur de réseau.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Request was canceled.</source>
        <translation>La requête a été annulée.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The response from the service was not in a recognizable format.</source>
        <translation>La réponse provenant du service n&apos;était pas dans un format reconnu.</translation>
    </message>
</context>
</TS>
