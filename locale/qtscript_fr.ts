<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>QScriptBreakpointsModel</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptbreakpointsmodel.cpp" line="+447"/>
        <source>ID</source>
        <translation>Identifiant</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Location</source>
        <translation>Emplacement</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Condition</source>
        <translation>Condition</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ignore-count</source>
        <translation>Nombre d&apos;ignorés</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Single-shot</source>
        <translation>Single-shot</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Hit-count</source>
        <translation>Nombre de coups</translation>
    </message>
</context>
<context>
    <name>QScriptBreakpointsWidget</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptbreakpointswidget.cpp" line="+290"/>
        <source>New</source>
        <translation>Créer</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
</context>
<context>
    <name>QScriptDebugger</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptdebugger.cpp" line="+878"/>
        <location line="+1033"/>
        <source>Go to Line</source>
        <translation>Aller à la ligne</translation>
    </message>
    <message>
        <location line="-1032"/>
        <source>Line:</source>
        <translation>Ligne :</translation>
    </message>
    <message>
        <location line="+791"/>
        <source>Interrupt</source>
        <translation>Interrompre</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Shift+F5</source>
        <translation>Shift+F5</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Continue</source>
        <translation>Continuer</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Step Into</source>
        <translation>Pas à pas entrant</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>F11</source>
        <translation>F11</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Step Over</source>
        <translation>Pas à pas principal</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>F10</source>
        <translation>F10</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Step Out</source>
        <translation>Pas à pas sortant</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Shift+F11</source>
        <translation>Shift+F11</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Run to Cursor</source>
        <translation>Exécuter jusqu&apos;au curseur</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ctrl+F10</source>
        <translation>Ctrl+F10</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Run to New Script</source>
        <translation>Exécuter jusqu&apos;au nouveau script</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Toggle Breakpoint</source>
        <translation>Basculer le point d&apos;arrêt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>F9</source>
        <translation>F9</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Clear Debug Output</source>
        <translation>Effacer la sortie de débogage</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Clear Error Log</source>
        <translation>Effacer le journal d&apos;erreurs</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Clear Console</source>
        <translation>Effacer la console</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>&amp;Find in Script...</source>
        <translation>&amp;Chercher dans le script...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Find &amp;Next</source>
        <translation>Rechercher le &amp;suivant</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>F3</source>
        <translation>F3</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Find &amp;Previous</source>
        <translation>Rechercher le &amp;précédent</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Shift+F3</source>
        <translation>Shift+F3</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Debug</source>
        <translation>Déboguer</translation>
    </message>
</context>
<context>
    <name>QScriptDebuggerCodeFinderWidget</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptdebuggercodefinderwidget.cpp" line="+133"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Previous</source>
        <translation>Précédent</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Case Sensitive</source>
        <translation>Sensible à la casse</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Whole words</source>
        <translation>Mots complets</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&lt;img src=&quot;:/qt/scripttools/debugging/images/wrap.png&quot;&gt;&amp;nbsp;Search wrapped</source>
        <translation>&lt;img src=&quot;:/qt/scripttools/debugging/images/wrap.png&quot;&gt;&amp;nbsp;La recherche est revenue au début</translation>
    </message>
</context>
<context>
    <name>QScriptDebuggerLocalsModel</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptdebuggerlocalsmodel.cpp" line="+889"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
</context>
<context>
    <name>QScriptDebuggerStackModel</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptdebuggerstackmodel.cpp" line="+153"/>
        <source>Level</source>
        <translation>Niveau</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Location</source>
        <translation>Emplacement</translation>
    </message>
</context>
<context>
    <name>QScriptEdit</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptedit.cpp" line="+413"/>
        <source>Toggle Breakpoint</source>
        <translation>Basculer le point d&apos;arrêt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Disable Breakpoint</source>
        <translation>Désactiver le point d&apos;arrêt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Enable Breakpoint</source>
        <translation>Activer le point d&apos;arrêt</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Breakpoint Condition:</source>
        <translation>Condition du point d&apos;arrêt :</translation>
    </message>
</context>
<context>
    <name>QScriptEngineDebugger</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptenginedebugger.cpp" line="+516"/>
        <source>Loaded Scripts</source>
        <translation>Scripts chargés</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Breakpoints</source>
        <translation>Points d&apos;arrêt</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Stack</source>
        <translation>Pile</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Locals</source>
        <translation>Variables locales</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Console</source>
        <translation>Console</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Debug Output</source>
        <translation>Sortie de débogage</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Error Log</source>
        <translation>Journal d&apos;erreurs</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>View</source>
        <translation>Affichage</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Qt Script Debugger</source>
        <translation>Débogueur de script Qt</translation>
    </message>
</context>
<context>
    <name>QScriptNewBreakpointWidget</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptbreakpointswidget.cpp" line="-223"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
</TS>
