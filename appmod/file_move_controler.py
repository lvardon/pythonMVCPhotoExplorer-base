#!/usr/bin/env python
#-*- coding: utf-8 -*-

"""

"""
import os
from os import path

from PyQt5.QtWidgets import QApplication, QMenu, QMainWindow, QDialog, QLabel, QComboBox, QPushButton, QVBoxLayout, QMessageBox, QStyledItemDelegate, QItemDelegate
from PyQt5.QtGui import QIcon, QStandardItemModel, QStandardItem, QCursor
from PyQt5.QtCore import Qt, QDir, QT_TRANSLATE_NOOP, QTranslator, QLocale, QSize, QPoint, QDateTime, QVariant, QDate
from PyQt5.QtCore import Qt
from PyQt5 import uic

from applib import log
from applib import setting
from applib import dircmd
from applib import filecmd

class file_move_controler( QDialog ) :

    def __init__(self, parent, fileItemList, pathSelected, favDirList):

        super(file_move_controler,self).__init__()
        self.parent = parent
        self.fileActionWindow = uic.loadUi('appmod/file_move_view.ui', self)
        self.fileItemList = fileItemList
        self.pathSelected = pathSelected
        self.favDirList = favDirList

        nbfiles = 0
        for fileItem in fileItemList  :
            nbfiles = nbfiles + 1
            self.lineEditFileNameValue.setText( fileItem.fname )

        self.setWindowTitle("Déplacer les fichiers : " + str( nbfiles ))

        self.extToCopyList = setting.Setting.getValue("rawsearch/rawInfoExtSearch")
        self.fileActionWindow.checkBox_pp3.setText( "Déplacer : "   + ', '.join(self.extToCopyList)  )
        # Désactiver la copie jpg ... si un lien à créer d'un des fichiers ne concerne pas un Nef
        extFileNotRaw = setting.Setting.getListValue("rawsearch/rawExt")
        for fileItem in self.fileItemList :
            if fileItem.ext not in ( extFileNotRaw ) :

                self.fileActionWindow.checkBox_pp3.setDisabled( True )
                self.fileActionWindow.checkBox_pp3.setChecked( False )

        self.selectDir_pushButton.clicked.connect(self.onSelectDir)
        self.ok_pushButton.clicked.connect(self.onOk)
        self.cancel_pushButton.clicked.connect(self.onCancel)

        self.dstDir = ""
        self.fileActionWindow.lineEditFileNameValue.setText( self.dstDir )


    def onOk(self) :
       if self.dstDir != "" :

            nbFCopy = 0
            for fileItem in self.fileItemList :
                log.Log.debug(log.INFO, "Déplacement du fichier : " + os.path.join(fileItem.filePath + "/" + fileItem.fname)  + " --> "  + os.path.join(self.dstDir  + "/" + fileItem.fname))
                myfilecmd = filecmd.filecmd(self)
                try :
                    #os.symlink(  os.path.join(fileItem.filePath + "/" + fileItem.fname) ,  os.path.join(self.dstDir  + "/" + fileItem.fname))
                    resultCopy = myfilecmd.move( fileItem.filePath, fileItem.fname , self.dstDir, fileItem.fname )
                    if resultCopy is False :
                         raise Exception('Erreur de déplacement du fichier')

                    self.parent.tableData.tableModel.removeDataByKey( fileItem.key )
                    self.parent.MainWindow.nav_tableview.clearSelection()
                    self.parent.MainWindow.nav_iconview.clearSelection()

                    nbFCopy = nbFCopy + 1
                    if self.fileActionWindow.checkBox_pp3.isChecked() is True :
                        print ("COPY PP3 ?")

                        (rawDirlistSearch) = setting.Setting.getListValue("rawsearch/rawDirlistSearch")
                        i = 0
                        for rawDirlistSearchElem in rawDirlistSearch :
                            rawDirlistSearch[i] = rawDirlistSearch[i].replace( "[rawdir]" , fileItem.filePath)
                            if not rawDirlistSearch[i].endswith("/") :
                                rawDirlistSearch[i] = rawDirlistSearch[i] + "/"

                            for extToCopyElem in self.extToCopyList :
                                if os.path.exists(rawDirlistSearch[i]  + os.path.splitext(fileItem.fname)[0] + "." + extToCopyElem) is True :
                                    print ("COPY " + rawDirlistSearch[i]  + os.path.splitext(fileItem.fname)[0] + "." + extToCopyElem  +" TO " + self.dstDir  + "/" + os.path.splitext(fileItem.fname)[0] + "." + extToCopyElem)
                                    myfilecmd.copy(
                                        rawDirlistSearch[i], os.path.splitext(fileItem.fname)[0] + "." + extToCopyElem ,
                                        self.dstDir  , os.path.splitext(fileItem.fname)[0] + "." + extToCopyElem,
                                        self.fileActionWindow.checkBox_overwrite.isChecked()
                                    )
                            i = i + 1


                except Exception as e:
                    QMessageBox.warning(self, 'Erreur', "Erreur de déplacement du fichier : \n\n" + str(e.args[0]), QMessageBox.Yes )
                    log.Log.debug(log.INFO, "Erreur déplacement du fichier : " + str(e.args[0]))


            self.fileActionWindow.result_textEdit.setText("Copie des "+str(nbFCopy)+" fichiers terminée.")


    def onCancel(self) :
        self.close()

    def dialogShow( self) :
        log.Log.debug( log.INFO , "File move view : showing window."  )
        self.fileActionWindow.exec_() # Modal

    def onSelectDir(self) :
        self.dstDir = ""

        dircmdWin = dircmd.dircmd(self)
        self.dstDir = dircmdWin.select( "Selectionnez le dossier destination", self.pathSelected,  self.favDirList)
        self.fileActionWindow.lineEditFileNameValue.setText( self.dstDir )



    def tmp() :
        dstDir = ""

        dircmdWin = dircmd.dircmd(self)
        dstDir = dircmdWin.select( "Selectionnez le dossier destination", self.MainWindow.treeView.getPathSelected(), self.favDirList )

        if dstDir != "" :

            myfilecmd = filecmd.filecmd(self)

            for fileItem in self.getFileItemListSelected () :
                log.Log.debug(log.INFO, "Move file: " + self.MainWindow.treeView.getPathSelected() + fileItem.fname + " ==> " +  dstDir )
                resultCmd = myfilecmd.move(  self.MainWindow.treeView.getPathSelected(), fileItem.fname , dstDir , fileItem.fname )
                if resultCmd is not True :
                    QMessageBox.warning(self, 'Erreur', "Erreur de déplacement : \n\n" + myfilecmd.cmdExec + "\n\n" + "Code de retour : " + str(myfilecmd.subprocess_returncode)  + "\n" + myfilecmd.subprocess_err , QMessageBox.Yes )
                else :
                    log.Log.debug(log.INFO, "Move file key : " + str(fileItem.key) )
                    self.tableData.tableModel.removeDataByKey( fileItem.key )

            self.MainWindow.nav_tableview.clearSelection()
            self.MainWindow.nav_iconview.clearSelection()
