#!/usr/bin/python3
# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import QApplication, QMessageBox, QLabel, QWidget, QPushButton, QComboBox, QLineEdit, QGridLayout, QDialog, QScrollArea
from PyQt5.QtGui import QPixmap, QImage, QColor
from PyQt5.QtCore import QDir, Qt



from applib.module import PLUGIN_ACTIVE,PLUGIN_NAME,PLUGIN_DESCRIPTION,PLUGIN_VERSION,PLUGIN_MENULIB,PLUGIN_MENUTOOLTIP,PLUGIN_STATUTBARTOOLTIP, PLUGIN_MENU_UPPER,PLUGIN_MENU_TOOLBAR,PLUGIN_MENU_ICONVIEW,PLUGIN_MENU_DETAILVIEW,PLUGIN_SHORTCUT, PLUGIN_ICONPATH, PLUGIN_ORDER

import os


class poUpImage(object):

    def __init__(self):
        self.active = True
        self.error = ""

    def main( self, self_parent, currentPath, fileItemList, fileExt ) :

        for fileItem in fileItemList :
            Qima = QImage( currentPath + "/" + fileItem.fname)

            self.imagePopUpViewer( Qima, fileItem.fname )

            break  # N'afficher que la première image de la selection

        return 0

    def imagePopUpViewer( self, QimagePtr , imageName = ""  ) :
        """
        Affiche l'image à l'utilisateur dans un pop-up
        """
        self.imgV = SimpleImagePopUpViewer()
        self.imgV.setWindowTitle( imageName )
        self.imgV.openImage( QimagePtr )

    def isActive( self ) :
        return  self.getInfo( PLUGIN_ACTIVE )

    def getInfo( self, typeInfo) :
        if typeInfo == PLUGIN_ACTIVE :
            return self.active
        elif typeInfo == PLUGIN_NAME :
            return "Popup image"
        elif  typeInfo == PLUGIN_DESCRIPTION :
            return "Affiche l'image en taille réelle, dans un pop-up"
        elif  typeInfo == PLUGIN_VERSION :
            return "1.0.0"
        elif  typeInfo == PLUGIN_MENULIB :
            return "Affiche l'image en taille réelle, dans un pop-up"
        elif  typeInfo == PLUGIN_MENUTOOLTIP :
            return "Affiche l'image en taille réelle, dans un pop-up"
        elif  typeInfo == PLUGIN_STATUTBARTOOLTIP :
            return "Affiche l'image en taille réelle, dans un pop-up"
        elif  typeInfo == PLUGIN_SHORTCUT :
            return "Ctrl+"
        elif  typeInfo == PLUGIN_ICONPATH :
            return "icon/tool_module_show.png"
        elif  typeInfo == PLUGIN_MENU_UPPER :
            return True
        elif  typeInfo == PLUGIN_MENU_TOOLBAR :
            return True
        elif  typeInfo == PLUGIN_MENU_ICONVIEW :
            return True
        elif  typeInfo == PLUGIN_MENU_DETAILVIEW :
            return True
        elif  typeInfo == PLUGIN_ORDER :
            return 100

class SimpleImagePopUpViewer(QDialog):

    def __init__(self):

        super(SimpleImagePopUpViewer, self).__init__()

        self.setGeometry(100, 100, 250, 150)
        self.setWindowTitle('Image')

        self.imageLabel = QLabel()
        self.buttonCancel = QPushButton("Fermer", self)
        self.buttonCancel.clicked.connect(self.onCancel)

        self.grid = QGridLayout(self)

        self.area = QScrollArea(self)
        self.area.setWidgetResizable(True)

        self.area.setWidget(self.imageLabel)
        self.grid.addWidget(self.area, 1, 0)

        self.grid.addWidget(self.buttonCancel, 2, 0)

        self.setLayout(self.grid)

    def onCancel(self) :
        self.close()

    def openImage( self, image  ) :

        QApplication.setOverrideCursor(Qt.WaitCursor)

        self.imgPix = QPixmap.fromImage(image)
        self.imageLabel.setPixmap(self.imgPix)
        self.imageLabel.setAlignment(Qt.AlignCenter)

        dialogWidth = self.imgPix.width() + 100
        dialogHeight= self.imgPix.height() + 100
        self.resize(dialogWidth, dialogHeight)

        QApplication.restoreOverrideCursor()


        self.exec_()




