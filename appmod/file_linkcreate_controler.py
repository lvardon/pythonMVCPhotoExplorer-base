#!/usr/bin/env python
#-*- coding: utf-8 -*-

"""

"""

import os
from os import path

from PyQt5.QtWidgets import QApplication, QMenu, QMainWindow, QDialog, QLabel, QComboBox, QPushButton, QVBoxLayout, QMessageBox, QStyledItemDelegate, QItemDelegate
from PyQt5.QtGui import QIcon, QStandardItemModel, QStandardItem, QCursor
from PyQt5.QtCore import Qt, QDir, QT_TRANSLATE_NOOP, QTranslator, QLocale, QSize, QPoint, QDateTime, QVariant, QDate
from PyQt5.QtCore import Qt
from PyQt5 import uic


from applib import log
from applib import setting
from applib import dircmd
from applib import filecmd


class file_linkcreate_controler( QDialog ) :

    def __init__(self, parent, fileItemList, pathSelected,  favDirList):

        super(file_linkcreate_controler,self).__init__()
        self.parent = parent
        self.fileItemList = fileItemList
        self.pathSelected = pathSelected
        self.favDirList = favDirList

        self.fileActionWindow = uic.loadUi('appmod/file_linkcreate_view.ui', self)

        nbfiles = 0
        for fileItem in self.fileItemList  :
            nbfiles = nbfiles + 1
            self.lineEditFileNameValue.setText( fileItem.fname )

        self.setWindowTitle("Créer un lien : " + str( nbfiles ))

        self.extToCopyList = setting.Setting.getValue("rawsearch/rawInfoExtSearch")
        self.fileActionWindow.checkBox_pp3.setText( "Copier : "   + ', '.join(self.extToCopyList)  )
        # Désactiver la copie jpg ... si un lien à créer d'un des fichiers ne concerne pas un Nef
        extFileNotRaw = setting.Setting.getListValue("rawsearch/rawExt")
        for fileItem in self.fileItemList :
            if fileItem.ext not in ( extFileNotRaw ) :

                self.fileActionWindow.checkBox_pp3.setDisabled( True )
                self.fileActionWindow.checkBox_pp3.setChecked( False )


        self.selectDir_pushButton.clicked.connect(self.onSelectDir)
        self.ok_pushButton.clicked.connect(self.onOk)
        self.cancel_pushButton.clicked.connect(self.onCancel)

        self.dstDir = ""
        self.fileActionWindow.lineEditFileNameValue.setText( self.dstDir )

    def onOk(self) :

        if self.dstDir != "" :

            nbLink = 0
            for fileItem in self.fileItemList :
                log.Log.debug(log.INFO, "Création du lien : " + os.path.join(fileItem.filePath + "/" + fileItem.fname)  + " --> "  + os.path.join(self.dstDir  + "/" + fileItem.fname))
                try :
                    os.symlink(  os.path.join(fileItem.filePath + "/" + fileItem.fname) ,  os.path.join(self.dstDir  + "/" + fileItem.fname))
                    nbLink = nbLink + 1
                    if self.fileActionWindow.checkBox_pp3.isChecked() is True :
                        print ("COPY PP3 ?")

                        myfilecmd = filecmd.filecmd(self)
                        (rawDirlistSearch) = setting.Setting.getListValue("rawsearch/rawDirlistSearch")
                        i = 0
                        for rawDirlistSearchElem in rawDirlistSearch :
                            rawDirlistSearch[i] = rawDirlistSearch[i].replace( "[rawdir]" , fileItem.filePath)
                            if not rawDirlistSearch[i].endswith("/") :
                                rawDirlistSearch[i] = rawDirlistSearch[i] + "/"

                            for extToCopyElem in self.extToCopyList :
                                if os.path.exists(rawDirlistSearch[i]  + os.path.splitext(fileItem.fname)[0] + "." + extToCopyElem) is True :
                                    print ("COPY " + rawDirlistSearch[i]  + os.path.splitext(fileItem.fname)[0] + "." + extToCopyElem  +" TO " + self.dstDir  + "/" + os.path.splitext(fileItem.fname)[0] + "." + extToCopyElem)
                                    myfilecmd.copy(
                                        rawDirlistSearch[i], os.path.splitext(fileItem.fname)[0] + "." + extToCopyElem ,
                                        self.dstDir  , os.path.splitext(fileItem.fname)[0] + "." + extToCopyElem
                                    )
                            i = i + 1


                except Exception as e:
                    QMessageBox.warning(self, 'Erreur', "Erreur de création du lien : \n\n" + str(e.args[0]), QMessageBox.Yes )
                    log.Log.debug(log.INFO, "Erreur de création du lien : " + str(e.args[0]))


            self.fileActionWindow.result_textEdit.setText("Création des "+str(nbLink)+" liens terminée.")

    def onCancel(self) :
        self.close()

    def dialogShow( self) :
        log.Log.debug( log.INFO , "File create link view : showing window."  )
        self.fileActionWindow.exec_() # Modal

    def onSelectDir(self) :
        self.dstDir = ""

        dircmdWin = dircmd.dircmd(self)
        self.dstDir = dircmdWin.select( "Selectionnez le dossier destination", self.pathSelected,  self.favDirList)
        self.fileActionWindow.lineEditFileNameValue.setText( self.dstDir )



