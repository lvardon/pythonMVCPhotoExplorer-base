# Photo-Explorer #

Version : pré-alpha<br>

![app appscreenshoot](icon/appscreenshoot.png)

## Update ##

### V. 19/07/2017 ###

 * Module d'ajout de signature-filigramme dans les images
 * Ajout option de recherche de doublons (gestion de la liste de doublons et méthodes de comparaison non encore implémentées)
 * Ajout option d'importations/copies de fichiers
 * Ajout d'un historique des dossiers consultés (possibilité d'effacement de la liste encore manquante ...)
 * Ajout de la gestion de dossiers favoris , affiché en couleurs paramétrable dans config.ini (possibilité d'effacement de la liste encore manquante ...)
 * Utilisation d'icônes dans la barre d'outils principale

### V. 24/06/2017 ###

 * Gestion du ratio d'affichage de l'image (ignorer, remplir ...)
 * Colonne affiché/cachée en mode liste n'est pas consistante - corrigé
 * Afficher l'image #1 par défaut en entrée de dossier - corrigé
 * Paraméter dans config.ini la couleur d'arrière plan d'affichage image (gris, noir ...)  - fait
 * Ajout de la possibilté de stopper par Esc des process longs de lecture de dossier, création vignettes.

### V. 22/06/2017 ###

 * Système de filtrage des fichiers en mode liste
 * Tab 2 : afficher "en grand" la photo selectionnée + avec détails (exif ...)
 * Système de "modules" pour des traitements personnalisés de selections d'images (système de plugins)
 * Selection multiple sur la vue "icones"
 * Zoom+/zoom- sur la vue icône
 * Nouvelles possibilités de paramètrages dans config.ini
 * Ajout d'un splash screen
 * Rotation automatique de l'image selon exif
 * Affichage plein écran des images

### Version initiale 06/2017 ###

 * Treeview de navigation
 * Affichage des images en mode liste (détails fichiers)
 * Architecture générale de l'application (organisation des dossiers...)

## To do Fix ##

 * Corriger les fonctions du Tree view  "expand all"/"collapse all" (ne fonctionnne pas toujours comme attendu)
 * Corriger la traduction des libellés des boutons Qt (Yes/No) des boites de dialogues (main.py) - il manque un fichier de traduction Qt5 ?
 * Completer les remplacements des méta-informations dans les chaines d'infos ([fname], zoom, n° image  etc...)
 * Formater les valeur de certains tags exif ( temps d'expositin etc.)
 * Supprimer le filet blanc autour de l'image en mode plein écran
 * Regression : try-catch des erreurs non prévues (ds main.py)
 * Nettoyer/refactorer le code de appmod.main_controler.py
 * Implémenter les commandes dans les menus

## To do ##

 * Implementer un outil de gestion des vignettes en cache (nettoyage des vignettes ...)
 * Vignettes : proposer de créer les vignettes de toute une arborescence (avec sub dir) + proposer d'écraser, ou non, les vignettes existantes
 * Implementer un système de gestion des favoris (dossiers)
 * Permettre d'annoter / noter etc ... chaque image ( base sqlite ; clé = valeur Ino du fichier)
 * Remplacer les textes par des icônes dans la barre d'outil principale (avec tooltips)
 * Documentation
 * Timer en mode plein écran (mode diaporama)
 * Systeme de fitrage rapide (n'afficher que le jpg, que les Nef etc .)
 * ...

## Installation ##
 Prise en charge étendue de formats d'image ( tiff ... ):
 * sudo apt-get install qt5-image-formats-plugins

