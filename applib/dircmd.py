# -*- coding: utf-8 -*-

"""
Manipulations de dossiers : selection ...

"""

from PyQt5.QtWidgets import QApplication, QStyle, QMenu, QMainWindow, QDialog, QFileDialog, QLabel,QLineEdit, QComboBox, QPushButton, QVBoxLayout, QMessageBox, QStyledItemDelegate, QItemDelegate, QTreeView, QListView, QAbstractItemView
from PyQt5.QtGui import QPainter, QIcon, QImage, QPalette, QPen, QBrush, QStandardItemModel, QStandardItem, QCursor, QDesktopServices, QColor, QPixmap, QFont
from PyQt5 import QtCore
from PyQt5.QtCore import Qt, QDir, QStorageInfo, QT_TRANSLATE_NOOP, QTranslator, QLocale, QSize, QPoint, QDateTime, QVariant, QDate, QAbstractTableModel, QAbstractListModel, QUrl, QSortFilterProxyModel,  QStandardPaths, QModelIndex

import os
import os.path

from applib import log
from applib import setting

from appmod.fileitem import fileItem, COL_selection, COL_thumb, COL_key, COL_ino, COL_fname, COL_dev, COL_ext, COL_islink, COL_linktarget, \
    COL_fsize, COL_fsizeh, COL_c_date, COL_m_date, COL_a_date, COL_access_w, COL_mime, COL_ftype, COL_crc32, COL_sha1, \
    COL_exifinfos, COL_isimmutable, COL_width, COL_height, COL_trtstatus, COL_trtlib


class dircmd( object ) :

    def __init__(self, parent  ) :

        self.parent = parent


    def select( self , messageBox="Selectionnez le dossier", initialDir="" , favDirList=[] ) :
        """
        Boite de selection d'un dossier.
        Propose la liste des dossiers favoris.
        """
        filterData=""
        dstDir = initialDir

        dialog = QFileDialog(self.parent,  messageBox, initialDir, filterData )

        storageList = []

        storageList.append( QUrl.fromLocalFile(QStandardPaths.standardLocations(QStandardPaths.HomeLocation)[0]) )
        storageList.append( QUrl(QStandardPaths.standardLocations(QStandardPaths.DataLocation)[0]) )

        storageVolume = QStorageInfo()
        for storageItem in storageVolume.mountedVolumes() :
            storageList.append( QUrl.fromLocalFile( str(storageItem.rootPath()) ) )

        i=0
        for favElem in favDirList.getAll() :
            storageList.append( QUrl.fromLocalFile( favElem["key"] ) )
            i= i + 1

        dialog.setSidebarUrls( storageList )

        dialog.setFileMode(QFileDialog.DirectoryOnly)

        if dialog.exec_() == QDialog.Accepted:
            dstDir = dialog.selectedFiles()[0]

        return dstDir
