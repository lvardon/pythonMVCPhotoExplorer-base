#!/usr/bin/env python
#-*- coding: utf-8 -*-


"""
Widget de navigation dans les dossiers , sous forme de treeview

"""

import operator
import os
from os.path import basename
from PyQt5 import QtCore, QtGui
from PyQt5 import uic

from PyQt5.QtCore import Qt, QProcess, QRegExp, QSize, QPoint, QVariant, QItemSelectionModel, QItemSelection, QAbstractTableModel, QSortFilterProxyModel
from PyQt5.QtGui import QPainter, QImage, QPixmap, QFont, QFontMetrics, QStandardItemModel, QStandardItem, QIcon
from PyQt5.QtWidgets import QAction, QActionGroup, QApplication, QFileSystemModel, QListView, QTableView, QTreeView, QLabel, \
    QMenu, QMainWindow, QMessageBox, QAbstractItemView, QFileDialog, QGraphicsView, QGraphicsScene

from applib import log
from applib import setting

class dirNavTreeView( QTreeView ) :

    """def __init__(self , parent=None):

        super(dirNavTreeView, self).__init__()

        self.parent = parent
        self.setupUi()
        self.setContextMenuPolicy(Qt.DefaultContextMenu)
    """
    def setupUi(self)  :
        #self.show()
        pass

    def init(self , parent, dirIndex, rootIndex, dir_model ) :

        self.parent = parent
        self.dir_model = dir_model

        self.expandAll()
        self.setCurrentIndex(dirIndex)
        #rootIndex = self.dir_model.setRootPath(self.defaultRoot)
        self.setRootIndex(rootIndex)

        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.showTreeviewMenu)

        self.clicked.connect(self.parent.onTreeNodeClicked)
        self.setSortingEnabled(True)
        self.setColumnHidden(1, True)  # Hide 'size' column
        self.setColumnHidden(2, True)  # Hide 'type' column

        self.path_selected = "/"

    def openPath( self, newPath ) :

        self.setPathSelected ( newPath )

        index = self.dir_model.index(newPath)

        self.selectionModel().setCurrentIndex(index, QItemSelectionModel.Select)
        self.selectionModel().clear()

        #self.treeExpandNode( True )
        self.parent.treeViewOpenPath( newPath )


    def setPathSelected (self , path_selected ) :
        self.path_selected = path_selected

    def getPathSelected (self  ) :
        return self.path_selected


    # Context menu treeview
    def showTreeviewMenu(self, point ) :

        self.menuTreeview = QMenu(self)
        makeThumbs = self.menuTreeview.addAction("Créer les vignettes")
        makeThumbs.triggered.connect(lambda: self.onActionConstructThumbs())

        makeThumbsRec = self.menuTreeview.addAction("Créer les vignettes (récursif)")
        makeThumbsRec.triggered.connect(lambda: self.onActionConstructThumbsRec())

        self.menuTreeview.addSeparator()

        makeThumbsForce = self.menuTreeview.addAction("Créer les vignettes - forcer")
        makeThumbsForce.triggered.connect(lambda: self.onActionConstructThumbsForce())


        favAdd = self.menuTreeview.addAction("Ajouter ce dossier aux favoris")
        favAdd.triggered.connect(lambda: self.onActionAddDirToFavorites())

        favRemove = self.menuTreeview.addAction("Retirer ce dossier des favoris")
        favRemove.triggered.connect(lambda: self.onActionRemoveDirFromFavorites())


        #self.parent.popup(self.parent.MainWindow.treeView.mapToGlobal(point))
        #self.menuTreeview.popup(self.MainWindow.treeView.mapToGlobal(point))
        self.menuTreeview.popup(self.mapToGlobal(point))

    def onActionConstructThumbs(self) :
        self.parent.constructThumbs( makeThumbSubDirs = False )

    def onActionConstructThumbsForce(self) :
        self.parent.constructThumbs( makeThumbSubDirs = False, overWriteExistsThumbs = True )

    def onActionConstructThumbsRec(self) :
        self.parent.constructThumbs( makeThumbSubDirs = True )

    def onActionAddDirToFavorites(self) :
        self.parent.addDirToFavorites( self.getPathSelected()  )

    def onActionRemoveDirFromFavorites(self) :
        self.parent.removeDirFromFavorites( self.getPathSelected()  )



    def treeExpandNode(self, expandMode):

        QApplication.setOverrideCursor(Qt.WaitCursor)

        dirIndex = self.dir_model.index(self.path_selected)
        prevRootIndex = self.rootIndex()

        self.treeExpandAllNodes(dirIndex, expandMode)

        self.setRootIndex(prevRootIndex)
        self.scrollTo(prevRootIndex)
        self.resizeColumnToContents(0)

        QApplication.restoreOverrideCursor()

    def treeExpandAllNodes(self, dirIndex, expandMode):
        """
        Recursive expand
        """
        self.expand(dirIndex)  # expand the item
        self.setExpanded(dirIndex, expandMode)  # expand the item
        for i in range(self.dir_model.rowCount(dirIndex)):
            # fetch all the sub-folders
            child = dirIndex.child(i, 0)
            if self.dir_model.isDir(child):
                self.dir_model.setRootPath(self.dir_model.filePath(child))
                nextIndex = self.dir_model.index(self.dir_model.filePath(child))
                self.treeExpandAllNodes(nextIndex, expandMode)
