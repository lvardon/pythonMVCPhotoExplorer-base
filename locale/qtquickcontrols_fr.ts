<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>DefaultColorDialog</name>
    <message>
        <location filename="../../qtquickcontrols/src/dialogs/DefaultColorDialog.qml" line="+277"/>
        <source>Hue</source>
        <translation>Teinte</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Saturation</source>
        <translation>Saturation</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Luminosity</source>
        <translation>Luminosité</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Alpha</source>
        <translation>Alpha</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>DefaultDialogWrapper</name>
    <message>
        <location filename="../../qtquickcontrols/src/dialogs/DefaultDialogWrapper.qml" line="+115"/>
        <source>Show Details...</source>
        <translation>Afficher les détails</translation>
    </message>
</context>
<context>
    <name>DefaultFileDialog</name>
    <message>
        <location filename="../../qtquickcontrols/src/dialogs/DefaultFileDialog.qml" line="+145"/>
        <source>Go up to the folder containing this one</source>
        <translation>Remonter vers le dossier contenant celui-ci</translation>
    </message>
    <message>
        <location line="+121"/>
        <source>Remove favorite</source>
        <translation>Supprimer le favori</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Add the current directory as a favorite</source>
        <translation>Ajouter le dossier actuel aux favoris</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Filename</source>
        <translation>Nom du fichier</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Type</source>
        <comment>file type (extension)</comment>
        <translation>Type</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Size</source>
        <comment>file size</comment>
        <translation>Taille</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Modified</source>
        <comment>last-modified time</comment>
        <translation>Modifié</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Accessed</source>
        <comment>last-accessed time</comment>
        <translation>Accédé</translation>
    </message>
    <message>
        <location line="+65"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Choose</source>
        <translation>Choisir</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Save</source>
        <translation>Sauver</translation>
    </message>
</context>
<context>
    <name>DefaultFontDialog</name>
    <message>
        <location filename="../../qtquickcontrols/src/dialogs/DefaultFontDialog.qml" line="+117"/>
        <source>Font</source>
        <translation>Police</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+81"/>
        <source>Weight</source>
        <translation>Poids</translation>
    </message>
    <message>
        <location line="-80"/>
        <location line="+154"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location line="-141"/>
        <source>Font Family</source>
        <translation>Famille de Polices</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Thin</source>
        <translation>Fin</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ExtraLight</source>
        <translation>Très léger</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Light</source>
        <translation>Léger</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Medium</source>
        <translation>Medium</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>DemiBold</source>
        <translation>Semi gras</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bold</source>
        <translation>Gras</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ExtraBold</source>
        <translation>Très gras</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Black</source>
        <translation>Ultra gras</translation>
    </message>
    <message>
        <location line="+90"/>
        <source>Style</source>
        <translation>Style</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Italic</source>
        <translation>Italique</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Underline</source>
        <translation>Souligné</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Overline</source>
        <translation>Trait suscrit</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Strikeout</source>
        <translation>Barré</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Writing System</source>
        <translation>Système d&apos;Écriture</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Sample</source>
        <translation>Exemple</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>DefaultMessageDialog</name>
    <message>
        <location filename="../../qtquickcontrols/src/dialogs/DefaultMessageDialog.qml" line="+139"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Save All</source>
        <translation>Tout Enregistrer</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Retry</source>
        <translation>Réessayer</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Ignore</source>
        <translation>Ignorer</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Yes to All</source>
        <translation>Oui à Tout</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>No to All</source>
        <translation>Non à Tout</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Discard</source>
        <translation>Ne pas tenir compte</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Reset</source>
        <translation>Réinitialiser</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Restore Defaults</source>
        <translation>Restaurer les valeurs par defaut</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Abort</source>
        <translation>Abandonner</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show Details...</source>
        <translation>Afficher les détails...</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Hide Details</source>
        <translation>Masquer les détails</translation>
    </message>
</context>
<context>
    <name>EditMenu_base</name>
    <message>
        <location filename="../../qtquickcontrols/src/controls/Private/EditMenu_base.qml" line="+48"/>
        <source>&amp;Undo</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Redo</source>
        <translation>&amp;Rejouer</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Cu&amp;t</source>
        <translation>Co&amp;uper</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>&amp;Copy</source>
        <translation>&amp;Copier</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>&amp;Paste</source>
        <translation>Co&amp;ller</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Clear</source>
        <translation>Nettoyer</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Select All</source>
        <translation>Sélectionner tout</translation>
    </message>
</context>
<context>
    <name>EditMenu_ios</name>
    <message>
        <source>Cut</source>
        <translation type="vanished">Couper</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="vanished">Copier</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="vanished">Coller</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Supprimer</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="vanished">Sélectionner</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation type="vanished">Sélectionner tout</translation>
    </message>
</context>
</TS>
