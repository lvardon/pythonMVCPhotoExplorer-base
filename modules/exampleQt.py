#!/usr/bin/python3
# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import QApplication, QMessageBox


from applib.module import PLUGIN_ACTIVE,PLUGIN_NAME,PLUGIN_DESCRIPTION,PLUGIN_VERSION,PLUGIN_MENULIB,PLUGIN_MENUTOOLTIP,PLUGIN_STATUTBARTOOLTIP, PLUGIN_MENU_UPPER,PLUGIN_MENU_TOOLBAR,PLUGIN_MENU_ICONVIEW,PLUGIN_MENU_DETAILVIEW,PLUGIN_SHORTCUT, PLUGIN_ICONPATH, PLUGIN_ORDER

import os


class moduleQt(object):

    def __init__(self):
        self.active = True
        self.error = ""

    def main( self, self_parent, currentPath, fileItemList, fileExt ) :

        QMessageBox.information( self_parent, "Script dir = ", os.path.dirname(os.path.abspath(__file__)), QMessageBox.Ok )
        QMessageBox.information( self_parent, "Module dir = ", os.getcwd(), QMessageBox.Ok )

        imageListSelected = ""
        for fileItem in fileItemList :
            imageListSelected = imageListSelected + fileItem.fname + " :" + fileItem.fsizeh + "\n"

        QMessageBox.information( self_parent, "Qt Exemple", "Selection de "+str(len(fileItemList)) +" fichiers :\n\n" + imageListSelected  + "\nDossier : " + currentPath , QMessageBox.Ok )

        return 0

    def isActive( self ) :
        return  self.getInfo( PLUGIN_ACTIVE )

    def getInfo( self, typeInfo) :
        if typeInfo == PLUGIN_ACTIVE :
            return self.active
        elif typeInfo == PLUGIN_NAME :
            return "Exemple de module Qt"
        elif  typeInfo == PLUGIN_DESCRIPTION :
            return "Exemple simple de module Qt"
        elif  typeInfo == PLUGIN_VERSION :
            return "1.0.0"
        elif  typeInfo == PLUGIN_MENULIB :
            return "Exemple Qt"
        elif  typeInfo == PLUGIN_MENUTOOLTIP :
            return "Executer le module exemple Qt"
        elif  typeInfo == PLUGIN_STATUTBARTOOLTIP :
            return "module exemple Qt"
        elif  typeInfo == PLUGIN_SHORTCUT :
            return "Ctrl+P"
        elif  typeInfo == PLUGIN_ICONPATH :
            return "icon/statut_green.png"
        elif  typeInfo == PLUGIN_MENU_UPPER :
            return True
        elif  typeInfo == PLUGIN_MENU_TOOLBAR :
            return True
        elif  typeInfo == PLUGIN_MENU_ICONVIEW :
            return True
        elif  typeInfo == PLUGIN_MENU_DETAILVIEW :
            return True
        elif  typeInfo == PLUGIN_ORDER :
            return 500


