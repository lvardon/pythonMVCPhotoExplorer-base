#!/usr/bin/env python
#-*- coding: utf-8 -*-

"""

"""

from PyQt5.QtWidgets import QApplication, QMenu, QMainWindow, QDialog, QLabel, QComboBox, QPushButton, QVBoxLayout, QMessageBox, QStyledItemDelegate, QItemDelegate
from PyQt5.QtGui import QIcon, QStandardItemModel, QStandardItem, QCursor
from PyQt5.QtCore import Qt, QDir, QT_TRANSLATE_NOOP, QTranslator, QLocale, QSize, QPoint, QDateTime, QVariant, QDate
from PyQt5.QtCore import Qt
from PyQt5 import uic


from applib import log

from applib import setting

class filtercol_controler( QDialog ) :
        
    def __init__(self, parent):
        
        super(filtercol_controler,self).__init__()
        self.parent = parent        
        self.DetailWindow = uic.loadUi('appmod/filtercol_view.ui', self)        

        self.setWindowTitle("Filtrage")              

        self.ok_pushButton.clicked.connect(self.onOk)               

    def onOk(self) :
        self.close()

    def dialogShow( self) :        
        log.Log.debug( log.INFO , "Ars view : showing window."  )                 
        self.DetailWindow.exec_() # Modal

