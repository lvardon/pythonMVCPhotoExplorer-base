<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>QCLuceneResultWidget</name>
    <message>
        <location filename="../../qttools/src/assistant/help/qhelpsearchresultwidget.cpp" line="+102"/>
        <source>Search Results</source>
        <translation>Résultats de la recherche</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Note:</source>
        <translation>Note :</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The search results may not be complete since the documentation is still being indexed.</source>
        <translation>Les résultats de la recherche pourraient ne pas être complets car l&apos;indexation de la documentation est en cours.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Your search did not match any documents.</source>
        <translation>Votre recherche ne correspond à aucun document.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>(The reason for this might be that the documentation is still being indexed.)</source>
        <translation>(Il est possible que cela soit dû au fait que la documentation est en cours d&apos;indexation.)</translation>
    </message>
</context>
<context>
    <name>QHelp</name>
    <message>
        <location filename="../../qttools/src/assistant/help/qhelp_global.cpp" line="+54"/>
        <source>Untitled</source>
        <translation>Sans titre</translation>
    </message>
</context>
<context>
    <name>QHelpCollectionHandler</name>
    <message>
        <location filename="../../qttools/src/assistant/help/qhelpcollectionhandler.cpp" line="+71"/>
        <source>The collection file &apos;%1&apos; is not set up yet.</source>
        <translation>Le fichier collection &apos;%1&apos; n&apos;est pas encore prêt.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Cannot load sqlite database driver.</source>
        <translation>Impossible de charger le pilote de la base de données sqlite.</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+52"/>
        <source>Cannot open collection file: %1</source>
        <translation>Impossible d&apos;ouvrir le fichier collection : %1</translation>
    </message>
    <message>
        <location line="-40"/>
        <source>Cannot create tables in file %1.</source>
        <translation>Impossible de créer les tables dans le fichier %1.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>The collection file &apos;%1&apos; already exists.</source>
        <translation>Le fichier collection &apos;%1&apos; existe déjà.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot create directory: %1</source>
        <translation>Impossible de créer le répertoire : %1</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Cannot copy collection file: %1</source>
        <translation>Impossible de copier le fichier collection : %1</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>Unknown filter &apos;%1&apos;.</source>
        <translation>Filtre &apos;%1&apos; inconnu.</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Cannot register filter %1.</source>
        <translation>Impossible d&apos;enregistrer le filtre %1.</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Cannot open documentation file %1.</source>
        <translation>Impossible d&apos;ouvrir le fichier de documentation %1.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid documentation file &apos;%1&apos;.</source>
        <translation>Fichier de documentation invalide &apos;%1&apos;.</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>The namespace %1 was not registered.</source>
        <translation>L&apos;espace de nommage %1 n&apos;est pas enregistré.</translation>
    </message>
    <message>
        <location line="+120"/>
        <source>Namespace %1 already exists.</source>
        <translation>L&apos;espace de nommage %1 existe déjà.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot register namespace &apos;%1&apos;.</source>
        <translation>Impossible d&apos;enregistrer l&apos;espace de nommage &apos;%1&apos;.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Cannot open database &apos;%1&apos; to optimize.</source>
        <translation>Impossible d&apos;ouvrir la base de données &apos;%1&apos; pour optimisation.</translation>
    </message>
</context>
<context>
    <name>QHelpDBReader</name>
    <message>
        <location filename="../../qttools/src/assistant/help/qhelpdbreader.cpp" line="+91"/>
        <source>Cannot open database &apos;%1&apos; &apos;%2&apos;: %3</source>
        <extracomment>The placeholders are: %1 - The name of the database which cannot be opened %2 - The unique id for the connection %3 - The actual error string</extracomment>
        <translation>Impossible d&apos;ouvrir la base de données &apos;%1&apos; &apos;%2&apos; : %3</translation>
    </message>
</context>
<context>
    <name>QHelpEngineCore</name>
    <message>
        <location filename="../../qttools/src/assistant/help/qhelpenginecore.cpp" line="+115"/>
        <source>Cannot open documentation file %1: %2.</source>
        <translation>Impossible d&apos;ouvrir le fichier de documentation %1 : %2.</translation>
    </message>
    <message>
        <location line="+404"/>
        <source>The specified namespace does not exist.</source>
        <translation>L&apos;espace de nommage spécifié n&apos;existe pas.</translation>
    </message>
</context>
<context>
    <name>QHelpGenerator</name>
    <message>
        <location filename="../../qttools/src/assistant/help/qhelpgenerator.cpp" line="+151"/>
        <source>Invalid help data.</source>
        <translation>Données d&apos;aide invalides.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>No output file name specified.</source>
        <translation>Aucun nom de fichier de sortie spécifié.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The file %1 cannot be overwritten.</source>
        <translation>Impossible d&apos;écraser le fichier %1.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Building up file structure...</source>
        <translation>Construction de la structure de fichiers en cours...</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Cannot open data base file %1.</source>
        <translation>Impossible d&apos;ouvrir le fichier de base de données %1.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Cannot register namespace %1.</source>
        <translation>Impossible d&apos;enregistrer l&apos;espace de nommage %1.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Insert custom filters...</source>
        <translation>Insertion de filtres personnalisés...</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Insert help data for filter section (%1 of %2)...</source>
        <translation>Insertion de données d&apos;aide pour la section filtre (%1 sur %2)...</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Documentation successfully generated.</source>
        <translation>Documentation générée avec succès.</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Some tables already exist.</source>
        <translation>Certaines tables existent déjà.</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Cannot create tables.</source>
        <translation>Impossible de créer les tables.</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Cannot register virtual folder.</source>
        <translation>Impossible d&apos;enregistrer le répertoire virtuel.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Insert files...</source>
        <translation>Insertion des fichiers...</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>The file %1 does not exist! Skipping it.</source>
        <translation>Le fichier %1 n&apos;existe pas ! Fichier non pris en compte.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot open file %1! Skipping it.</source>
        <translation>Impossible d&apos;ouvrir le fichier %1 ! Fichier non pris en compte.</translation>
    </message>
    <message>
        <location line="+135"/>
        <source>The filter %1 is already registered.</source>
        <translation>Le filtre %1 est déjà enregistré.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cannot register filter %1.</source>
        <translation>Impossible d&apos;enregistrer le filtre %1.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Insert indices...</source>
        <translation>Insertion des index...</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Insert contents...</source>
        <translation>Insertion du contenu...</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot insert contents.</source>
        <translation>Impossible d&apos;insérer le contenu.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Cannot register contents.</source>
        <translation>Impossible d&apos;enregistrer le contenu.</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>File &apos;%1&apos; does not exist.</source>
        <translation>Le fichier &apos;%1&apos; n&apos;existe pas.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>File &apos;%1&apos; cannot be opened.</source>
        <translation>Le fichier &apos;%1&apos; ne peut être ouvert.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>File &apos;%1&apos; contains an invalid link to file &apos;%2&apos;</source>
        <translation>Le fichier &apos;%1&apos; contient un lien invalide vers le fichier &apos;%2&apos;</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Invalid links in HTML files.</source>
        <translation>Liens invalides dans les fichiers HTML.</translation>
    </message>
</context>
<context>
    <name>QHelpProject</name>
    <message>
        <location filename="../../qttools/src/assistant/help/qhelpprojectdata.cpp" line="+80"/>
        <source>Unknown token in file &quot;%1&quot;.</source>
        <translation>Élément inconnu dans le fichier &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Unknown token. Expected &quot;QtHelpProject&quot;.</source>
        <translation>Élément inconnu. &quot;QtHelpProject&quot; était attendu.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Error in line %1: %2</source>
        <translation>Erreur à la ligne %1 : %2</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Virtual folder has invalid syntax in file: &quot;%1&quot;</source>
        <translation>Le répertoire virtuel a une syntaxe invalide dans le fichier : &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Namespace &quot;%1&quot; has invalid syntax in file: &quot;%2&quot;</source>
        <translation>L&apos;espace de nommage &quot;%1&quot; a une syntaxe invalide dans le fichier : &quot;%2&quot;</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Missing namespace in QtHelpProject file: &quot;%1&quot;</source>
        <translation>Espace de nommage manquant dans le fichier QtHelpProject : &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Missing virtual folder in QtHelpProject file: &quot;%1&quot;</source>
        <translation>Répertoire virtuel manquant dans le fichier QtHelpProject : &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+233"/>
        <source>The input file %1 could not be opened.</source>
        <translation>Le fichier d&apos;entrée %1 n&apos;a pu être ouvert.</translation>
    </message>
</context>
<context>
    <name>QHelpSearchQueryWidget</name>
    <message>
        <location filename="../../qttools/src/assistant/help/qhelpsearchquerywidget.cpp" line="+118"/>
        <source>Search for:</source>
        <translation>Rechercher :</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Previous search</source>
        <translation>Recherche précédente</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Next search</source>
        <translation>Recherche suivante</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Search</source>
        <translation>Recherche</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Advanced search</source>
        <translation>Recherche avancée</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>words &lt;B&gt;similar&lt;/B&gt; to:</source>
        <translation>mots &lt;B&gt;semblables&lt;/B&gt; à :</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;B&gt;without&lt;/B&gt; the words:</source>
        <translation>&lt;B&gt;sans&lt;/B&gt; les mots :</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>with &lt;B&gt;exact phrase&lt;/B&gt;:</source>
        <translation>avec la &lt;B&gt;phrase exacte&lt;/B&gt; :</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>with &lt;B&gt;all&lt;/B&gt; of the words:</source>
        <translation>avec &lt;B&gt;tous&lt;/B&gt; les mots :</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>with &lt;B&gt;at least one&lt;/B&gt; of the words:</source>
        <translation>avec &lt;B&gt;au moins un&lt;/B&gt; des mots :</translation>
    </message>
</context>
<context>
    <name>QHelpSearchResultWidget</name>
    <message numerus="yes">
        <location filename="../../qttools/src/assistant/help/qhelpsearchresultwidget.cpp" line="+174"/>
        <source>%1 - %2 of %n Hits</source>
        <translation>
            <numerusform>%1 - %2 de %n résultat</numerusform>
            <numerusform>%1 - %2 de %n résultats</numerusform>
        </translation>
    </message>
    <message>
        <location line="+61"/>
        <source>0 - 0 of 0 Hits</source>
        <translation>0 - 0 de 0 résultats</translation>
    </message>
</context>
</TS>
