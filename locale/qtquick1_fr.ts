<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>Debugger::JSAgentWatchData</name>
    <message>
        <location filename="../../qtquick1/src/declarative/debugger/qjsdebuggeragent.cpp" line="+111"/>
        <source>[Array of length %1]</source>
        <translation>[Tableau de longueur %1]</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&lt;undefined&gt;</source>
        <translation>&lt;indéfini&gt;</translation>
    </message>
</context>
<context>
    <name>QDeclarativeAbstractAnimation</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativeanimation.cpp" line="+161"/>
        <source>Cannot animate non-existent property &quot;%1&quot;</source>
        <translation>Impossible d&apos;animer la propriété inexistante &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot animate read-only property &quot;%1&quot;</source>
        <translation>Impossible d&apos;animer la propriété en lecture seule &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativeutilmodule.cpp" line="+101"/>
        <location line="+50"/>
        <source>Animation is an abstract class</source>
        <translation>L&apos;animation est une classe abstraite</translation>
    </message>
</context>
<context>
    <name>QDeclarativeAnchorAnimation</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativeanimation.cpp" line="+2704"/>
        <source>Cannot set a duration of &lt; 0</source>
        <translation>Impossible de sélectionner une durée négative</translation>
    </message>
</context>
<context>
    <name>QDeclarativeAnchors</name>
    <message>
        <location filename="../../qtquick1/src/declarative/graphicsitems/qdeclarativeanchors.cpp" line="+190"/>
        <source>Possible anchor loop detected on fill.</source>
        <translation>Boucle potentielle dans les ancres détectée pour le remplissage.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Possible anchor loop detected on centerIn.</source>
        <translation>Boucle potentielle dans les ancres détectée pour le centrage.</translation>
    </message>
    <message>
        <location line="+208"/>
        <location line="+34"/>
        <location line="+646"/>
        <location line="+37"/>
        <source>Cannot anchor to an item that isn&apos;t a parent or sibling.</source>
        <translation>Impossible d&apos;ancrer à un élément qui n&apos;est pas un parent ou partage le même parent.</translation>
    </message>
    <message>
        <location line="-570"/>
        <source>Possible anchor loop detected on vertical anchor.</source>
        <translation>Boucle potentielle dans les ancres détectée pour l&apos;ancre verticale.</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Possible anchor loop detected on horizontal anchor.</source>
        <translation>Boucle potentielle dans les ancres détectée pour l&apos;ancre horizontale.</translation>
    </message>
    <message>
        <location line="+422"/>
        <source>Cannot specify left, right, and hcenter anchors.</source>
        <translation>Impossible de spécifier à la fois une ancre gauche, droite et hcenter.</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+37"/>
        <source>Cannot anchor to a null item.</source>
        <translation>Impossible d&apos;ancrer à un élément nul.</translation>
    </message>
    <message>
        <location line="-34"/>
        <source>Cannot anchor a horizontal edge to a vertical edge.</source>
        <translation>Impossible d&apos;ancrer un bord horizontal à un bord vertical.</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+37"/>
        <source>Cannot anchor item to self.</source>
        <translation>Impossible d&apos;ancrer l&apos;élément à lui même.</translation>
    </message>
    <message>
        <location line="-25"/>
        <source>Cannot specify top, bottom, and vcenter anchors.</source>
        <translation>Impossible de spécifier à la fois une ancre haut, bas et vcenter.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Baseline anchor cannot be used in conjunction with top, bottom, or vcenter anchors.</source>
        <translation>L&apos;ancre baseline ne peut pas etre combinée à l&apos;usage des ancres haut, bas ou vcenter.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot anchor a vertical edge to a horizontal edge.</source>
        <translation>Impossible d&apos;ancrer un bord vertical à un bord horizontal.</translation>
    </message>
</context>
<context>
    <name>QDeclarativeAnimatedImage</name>
    <message>
        <location filename="../../qtquick1/src/declarative/graphicsitems/qdeclarativeitemsmodule.cpp" line="+98"/>
        <location line="+108"/>
        <source>Qt was built without support for QMovie</source>
        <translation>Qt a été compilé sans support de QMovie</translation>
    </message>
</context>
<context>
    <name>QDeclarativeApplication</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativeutilmodule.cpp" line="-78"/>
        <source>Application is an abstract class</source>
        <translation>Application est une classe abstraite</translation>
    </message>
</context>
<context>
    <name>QDeclarativeBehavior</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativebehavior.cpp" line="+120"/>
        <source>Cannot change the animation assigned to a Behavior.</source>
        <translation>Impossible de changer l&apos;animation affectée à un comportement.</translation>
    </message>
</context>
<context>
    <name>QDeclarativeBinding</name>
    <message>
        <location filename="../../qtquick1/src/declarative/qml/qdeclarativebinding.cpp" line="+449"/>
        <source>Binding loop detected for property &quot;%1&quot;</source>
        <translation>Boucle détectée dans l&apos;affectation pour la propriété &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QDeclarativeCompiledBindings</name>
    <message>
        <location filename="../../qtquick1/src/declarative/qml/qdeclarativecompiledbindings.cpp" line="+388"/>
        <source>Binding loop detected for property &quot;%1&quot;</source>
        <translation>Boucle détectée dans l&apos;affectation pour la propriété &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QDeclarativeCompiler</name>
    <message>
        <location filename="../../qtquick1/src/declarative/qml/qdeclarativecompiler.cpp" line="+179"/>
        <location line="+1674"/>
        <location line="+205"/>
        <location line="+81"/>
        <location line="+81"/>
        <location line="+594"/>
        <source>Invalid property assignment: &quot;%1&quot; is a read-only property</source>
        <translation>Affectation de propriété invalide : &quot;%1&quot;est une propriété en lecture seule</translation>
    </message>
    <message>
        <location line="-2626"/>
        <source>Invalid property assignment: unknown enumeration</source>
        <translation>Affectation de propriété invalide : énumération inconnue</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Invalid property assignment: string expected</source>
        <translation>Affectation de propriété invalide : chaîne attendue</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid property assignment: url expected</source>
        <translation>Affectation de propriété invalide : url attendue</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Invalid property assignment: unsigned int expected</source>
        <translation>Affectation de propriété invalide : unsigned int attendu</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Invalid property assignment: int expected</source>
        <translation>Affectation de propriété invalide : int attendu</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+3"/>
        <source>Invalid property assignment: number expected</source>
        <translation>Affectation de propriété invalide : nombre attendu</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid property assignment: color expected</source>
        <translation>Affectation de propriété invalide : couleur attendue</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Invalid property assignment: date expected</source>
        <translation>Affectation de propriété invalide : date attendue</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Invalid property assignment: time expected</source>
        <translation>Affectation de propriété invalide : heure attendue</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Invalid property assignment: datetime expected</source>
        <translation>Affectation de propriété invalide : date et heure attendues</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Invalid property assignment: point expected</source>
        <translation>Affectation de propriété invalide : point attendu</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Invalid property assignment: size expected</source>
        <translation>Affectation de propriété invalide : taille attendue</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Invalid property assignment: rect expected</source>
        <translation>Affectation de propriété invalide : rectangle attendu</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Invalid property assignment: boolean expected</source>
        <translation>Affectation de propriété invalide : booléen attendu</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Invalid property assignment: 3D vector expected</source>
        <translation>Affectation de propriété invalide : vecteur 3D attendu</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Invalid property assignment: unsupported type &quot;%1&quot;</source>
        <translation>Affectation de propriété invalide : type &quot;%1&quot; non supporté</translation>
    </message>
    <message>
        <location line="+288"/>
        <source>Element is not creatable.</source>
        <translation>Impossible de créer l&apos;élément.</translation>
    </message>
    <message>
        <location line="+650"/>
        <source>Component elements may not contain properties other than id</source>
        <translation>Les éléments du composant ne peuvent pas contenir des propriétés autres que id</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Invalid component id specification</source>
        <translation>L&apos;id de composant spécifiée n&apos;est pas valide</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+515"/>
        <source>id is not unique</source>
        <translation>l&apos;id n&apos;est pas unique</translation>
    </message>
    <message>
        <location line="-505"/>
        <source>Invalid component body specification</source>
        <translation>Le corps de la spécification du composant n&apos;est pas valide</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Component objects cannot declare new properties.</source>
        <translation>Les objets composants ne peuvent pas déclarer de nouvelles propriétés.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Component objects cannot declare new signals.</source>
        <translation>Les objets composants ne peuvent pas déclarer de nouveaux signaux.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Component objects cannot declare new functions.</source>
        <translation>Les objets composants ne peuvent pas déclarer de nouvelles fonctions.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot create empty component specification</source>
        <translation>Impossible de créer une spécification du composant vide</translation>
    </message>
    <message>
        <location line="+88"/>
        <location line="+121"/>
        <source>&quot;%1.%2&quot; is not available in %3 %4.%5.</source>
        <translation>&quot;%1.%2&quot; n&apos;est pas disponible dans %3 %4.%5.</translation>
    </message>
    <message>
        <location line="-119"/>
        <location line="+121"/>
        <source>&quot;%1.%2&quot; is not available due to component versioning.</source>
        <translation>&quot;%1.%2&quot; n&apos;est pas disponible dans cette version du composant.</translation>
    </message>
    <message>
        <location line="-110"/>
        <source>Incorrectly specified signal assignment</source>
        <translation>L&apos;affectation du signal est incorrectement spécifiée</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Cannot assign a value to a signal (expecting a script to be run)</source>
        <translation>Impossible d&apos;assigner une valeur à un signal (un script à exécuter est attendu)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Empty signal assignment</source>
        <translation>Affectation de signal vide</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Empty property assignment</source>
        <translation>L&apos;affectation de propriété est vide</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Attached properties cannot be used here</source>
        <translation>La propriété attachée ne peut pas être utilisée ici</translation>
    </message>
    <message>
        <location line="+14"/>
        <location line="+128"/>
        <source>Non-existent attached object</source>
        <translation>L&apos;objet attaché est inexistant</translation>
    </message>
    <message>
        <location line="-124"/>
        <location line="+127"/>
        <source>Invalid attached object assignment</source>
        <translation>L&apos;affectation de l&apos;objet attaché est invalide</translation>
    </message>
    <message>
        <location line="-48"/>
        <source>Cannot assign to non-existent default property</source>
        <translation>Impossible d&apos;attacher à une propriété par défaut inexistante</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+356"/>
        <location line="+3"/>
        <source>Cannot assign to non-existent property &quot;%1&quot;</source>
        <translation>Impossible d&apos;attacher à une propriété inexistante &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="-329"/>
        <source>Invalid use of namespace</source>
        <translation>Utilisation invalide d&apos;espace de noms</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Not an attached property name</source>
        <translation>Ceci n&apos;est pas un nom de propriété attachée</translation>
    </message>
    <message>
        <location line="+182"/>
        <source>Invalid use of id property</source>
        <translation>Utilisation invalide de la propriété id</translation>
    </message>
    <message>
        <location line="+87"/>
        <location line="+2"/>
        <source>Property has already been assigned a value</source>
        <translation>Une valeur a déjà été attribuée à la propriété</translation>
    </message>
    <message>
        <location line="+18"/>
        <location line="+7"/>
        <source>Invalid grouped property access</source>
        <translation>Accès invalide à une propriété groupée</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot assign a value directly to a grouped property</source>
        <translation>Impossible d&apos;assigner directement une valeur à une propriété groupée</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Invalid property use</source>
        <translation>La propriété utilisée est invalide</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Property assignment expected</source>
        <translation>Affectation de propriété attendue</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Single property assignment expected</source>
        <translation>Une seule affectation de propriété est attendue</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unexpected object assignment</source>
        <translation>L&apos;affectation d&apos;objet est inattendue</translation>
    </message>
    <message>
        <location line="+65"/>
        <source>Cannot assign object to list</source>
        <translation>Impossible d&apos;assigner un objet à une liste</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Can only assign one binding to lists</source>
        <translation>Un seul lien peut être assigné à des listes</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot assign primitives to lists</source>
        <translation>Impossible d&apos;assigner des primitives à des listes</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot assign multiple values to a script property</source>
        <translation>Impossible d&apos;assigner plusieurs valeurs à une propriété de script</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid property assignment: script expected</source>
        <translation>Affectation de propriété invalide : script attendu</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Cannot assign multiple values to a singular property</source>
        <translation>Impossible d&apos;assigner plusieurs valeurs à une propriété singulière</translation>
    </message>
    <message>
        <location line="+93"/>
        <source>Cannot assign object to property</source>
        <translation>Impossible d&apos;assigner un objet à une propriété</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>&quot;%1&quot; cannot operate on &quot;%2&quot;</source>
        <translation>&quot;%1&quot; ne peut pas opérer sur &quot;%2&quot;</translation>
    </message>
    <message>
        <location line="+182"/>
        <source>Duplicate default property</source>
        <translation>La propriété par défaut est en double</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Duplicate property name</source>
        <translation>Le nom de propriété est en double</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Property names cannot begin with an upper case letter</source>
        <translation>Les noms des propriétés ne peuvent pas commencer par une majuscule</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Illegal property name</source>
        <translation>Le nom de la propriété est invalide</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Duplicate signal name</source>
        <translation>Le nom du signal en double</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Signal names cannot begin with an upper case letter</source>
        <translation>Les noms de signaux ne peuvent pas commencer par une majuscule</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Illegal signal name</source>
        <translation>Le nom du signal est invalide</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Duplicate method name</source>
        <translation>Le nom de la méthode est en double</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Method names cannot begin with an upper case letter</source>
        <translation>Les noms des méthodes ne peuvent pas commencer par une majuscule</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Illegal method name</source>
        <translation>Le nom de la méthode est invalide</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Property value set multiple times</source>
        <translation>La valeur de la propriété est attribuée plusieurs fois</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Invalid property nesting</source>
        <translation>Une propriété imbriquée est invalide</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>Cannot override FINAL property</source>
        <translation>Impossible de remplacer la propriété FINAL</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Invalid property type</source>
        <translation>Le type de propriété est invalide</translation>
    </message>
    <message>
        <location line="+177"/>
        <source>Invalid empty ID</source>
        <translation>Un identifiant vide est invalide</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>IDs cannot start with an uppercase letter</source>
        <translation>Les identifiants ne peuvent pas commencer par une  majuscule</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>IDs must start with a letter or underscore</source>
        <translation>Les identifiants doivent commencer par une lettre ou un tiret bas</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>IDs must contain only letters, numbers, and underscores</source>
        <translation>Les identifiants ne peuvent contenir que des lettres, des nombres ou des tirets bas</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>ID illegally masks global JavaScript property</source>
        <translation>L&apos;identifiant masque illégalement une propriété JavaScript globale</translation>
    </message>
    <message>
        <location line="+31"/>
        <location line="+9"/>
        <source>No property alias location</source>
        <translation>Aucun emplacement d&apos;aliad de propriété</translation>
    </message>
    <message>
        <location line="-4"/>
        <location line="+25"/>
        <location line="+7"/>
        <location line="+7"/>
        <location line="+6"/>
        <source>Invalid alias location</source>
        <translation>L&apos;emplacement d&apos;alias est invalide</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>Invalid alias reference. An alias reference must be specified as &lt;id&gt;, &lt;id&gt;.&lt;property&gt; or &lt;id&gt;.&lt;value property&gt;.&lt;property&gt;</source>
        <translation>La référence d&apos;alias est invalide. Les références d&apos;alias doivent être spécifiées comme &lt;id&gt;, &lt;id&gt;.&lt;propriété&gt; ou &lt;id&gt;.&lt;valeur de propriété&gt;.&lt;propriété&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid alias reference. Unable to find id &quot;%1&quot;</source>
        <translation>La référence d&apos;alias est invalide. Impossible de trouver l&apos;identifiant &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Alias property exceeds alias bounds</source>
        <translation>La propriété Alias dépasse les limites des alias</translation>
    </message>
</context>
<context>
    <name>QDeclarativeComponent</name>
    <message>
        <location filename="../../qtquick1/src/declarative/qml/qdeclarativecomponent.cpp" line="+524"/>
        <source>Invalid empty URL</source>
        <translation>Une URL vide est invalide</translation>
    </message>
    <message>
        <location line="+148"/>
        <source>createObject: value is not an object</source>
        <translation>createObject : la valeur fournie n&apos;est pas un objet</translation>
    </message>
</context>
<context>
    <name>QDeclarativeConnections</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativeconnections.cpp" line="+201"/>
        <location line="+64"/>
        <source>Cannot assign to non-existent property &quot;%1&quot;</source>
        <translation>Impossible d&apos;assigner à la propriété inexistante &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="-54"/>
        <source>Connections: nested objects not allowed</source>
        <translation>Connexions : les éléments imbriqués ne sont pas autorisés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connections: syntax error</source>
        <translation>Connexions : erreur de syntaxe</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Connections: script expected</source>
        <translation>Connexions : script attendu</translation>
    </message>
</context>
<context>
    <name>QDeclarativeEngine</name>
    <message>
        <location filename="../../qtquick1/src/declarative/qml/qdeclarativesqldatabase.cpp" line="+203"/>
        <source>executeSql called outside transaction()</source>
        <translation>executeSql a été appelé en dehors de transaction()</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Read-only Transaction</source>
        <translation>Transaction en lecture seule</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Version mismatch: expected %1, found %2</source>
        <translation>Version incompatible : %1 attendue, %2 trouvée</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>SQL transaction failed</source>
        <translation>la transaction SQL a échouée</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>transaction: missing callback</source>
        <translation>transaction : la fonction de rappel est absente</translation>
    </message>
    <message>
        <location line="+60"/>
        <location line="+14"/>
        <source>SQL: database version mismatch</source>
        <translation>SQL : la version de la base de données est incompatible</translation>
    </message>
</context>
<context>
    <name>QDeclarativeFlipable</name>
    <message>
        <location filename="../../qtquick1/src/declarative/graphicsitems/qdeclarativeflipable.cpp" line="+131"/>
        <source>front is a write-once property</source>
        <translation>front est une propriété à écriture unique</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>back is a write-once property</source>
        <translation>back est une propriété à écriture unique</translation>
    </message>
</context>
<context>
    <name>QDeclarativeGestureArea</name>
    <message>
        <location filename="../../qtquick1/src/imports/gestures/qdeclarativegesturearea.cpp" line="+176"/>
        <source>Cannot assign to non-existent property &quot;%1&quot;</source>
        <translation>Impossible d&apos;attacher à une propriété inexistante &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>GestureArea: nested objects not allowed</source>
        <translation>GestureArea : les objets imbriqués ne sont pas autorisés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>GestureArea: syntax error</source>
        <translation>GestureArea : erreur de syntaxe</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>GestureArea: script expected</source>
        <translation>GestureArea : un script est attendu</translation>
    </message>
</context>
<context>
    <name>QDeclarativeImportDatabase</name>
    <message>
        <location filename="../../qtquick1/src/declarative/qml/qdeclarativeimport.cpp" line="+373"/>
        <source>plugin cannot be loaded for module &quot;%1&quot;: %2</source>
        <translation>impossible de charger le plugin pour le module &quot;%1&quot; : %2</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>module &quot;%1&quot; plugin &quot;%2&quot; not found</source>
        <translation>le plugin &quot;%2&quot; du module &quot;%1&quot; n&apos;a pas été trouvé</translation>
    </message>
    <message>
        <location line="+144"/>
        <location line="+68"/>
        <source>module &quot;%1&quot; version %2.%3 is not installed</source>
        <translation>la version %2.%3 du module &quot;%1&quot; n&apos;est pas installée</translation>
    </message>
    <message>
        <location line="-66"/>
        <source>module &quot;%1&quot; is not installed</source>
        <translation>le module &quot;%1&quot; n&apos;est pas installé</translation>
    </message>
    <message>
        <location line="+14"/>
        <location line="+20"/>
        <source>&quot;%1&quot;: no such directory</source>
        <translation>&quot;%1&quot; : le répertoire n&apos;existe pas</translation>
    </message>
    <message>
        <location line="-2"/>
        <source>import &quot;%1&quot; has no qmldir and no namespace</source>
        <translation>l&apos;importation &quot;%1&quot; n&apos;a pas de qmldir ni d&apos;espace de noms</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>- %1 is not a namespace</source>
        <translation>- %1 n&apos;est pas un espace de noms</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>- nested namespaces not allowed</source>
        <translation>- les espaces de noms imbriqués ne sont pas autorisés</translation>
    </message>
    <message>
        <location line="+47"/>
        <location line="+4"/>
        <source>local directory</source>
        <translation>répertoire local</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>is ambiguous. Found in %1 and in %2</source>
        <translation>est ambigu. Trouvé dans %1 et dans %2</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>is ambiguous. Found in %1 in version %2.%3 and %4.%5</source>
        <translation>est ambigu. Trouvé dans %1 dans les versions %2.%3 et %4.%5</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>is instantiated recursively</source>
        <translation>est instancié récursivement</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>is not a type</source>
        <translation>n&apos;est pas un type</translation>
    </message>
    <message>
        <location line="+297"/>
        <source>File name case mismatch for &quot;%1&quot;</source>
        <translation>La casse du nom du fichier ne correspond pas pour &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QDeclarativeKeyNavigationAttached</name>
    <message>
        <location filename="../../qtquick1/src/declarative/graphicsitems/qdeclarativeitemsmodule.cpp" line="-36"/>
        <location line="+88"/>
        <source>KeyNavigation is only available via attached properties</source>
        <translation>KeyNavigation est disponible uniquement via les propriétés attachées</translation>
    </message>
</context>
<context>
    <name>QDeclarativeKeysAttached</name>
    <message>
        <location line="-87"/>
        <location line="+88"/>
        <source>Keys is only available via attached properties</source>
        <translation>Keys est disponible uniquement via les propriétés attachées</translation>
    </message>
</context>
<context>
    <name>QDeclarativeLayoutMirroringAttached</name>
    <message>
        <location filename="../../qtquick1/src/declarative/graphicsitems/qdeclarativeitem.cpp" line="+812"/>
        <source>LayoutDirection attached property only works with Items</source>
        <translation>La propriété attachée LayoutDirection ne fonctionne qu&apos;avec des éléments</translation>
    </message>
    <message>
        <location filename="../../qtquick1/src/declarative/graphicsitems/qdeclarativeitemsmodule.cpp" line="-63"/>
        <source>LayoutMirroring is only available via attached properties</source>
        <translation>LayoutMirroring est disponible uniquement via les propriétés attachées</translation>
    </message>
</context>
<context>
    <name>QDeclarativeListModel</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativelistmodel.cpp" line="+385"/>
        <source>remove: index %1 out of range</source>
        <translation>remove : l&apos;index %1 est hors de la plage de valeurs admissibles</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>insert: value is not an object</source>
        <translation>insert : la valeur n&apos;est pas un objet</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>insert: index %1 out of range</source>
        <translation>insert : l&apos;index %1 est hors de la plage de valeurs admissibles</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>move: out of range</source>
        <translation>move : hors de la plage de valeurs admissibles</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>append: value is not an object</source>
        <translation>append : la valeur n&apos;est pas un objet</translation>
    </message>
    <message>
        <location line="+71"/>
        <source>set: value is not an object</source>
        <translation>set : la valeur n&apos;est pas un objet</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+38"/>
        <source>set: index %1 out of range</source>
        <translation>set : l&apos;index %1 est hors de la plage de valeurs admissibles</translation>
    </message>
    <message>
        <location line="+37"/>
        <location line="+17"/>
        <source>ListElement: cannot contain nested elements</source>
        <translation>ListElement : ne peut pas contenir des éléments imbriqués</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>ListElement: cannot use reserved &quot;id&quot; property</source>
        <translation>ListElement : ne peut pas utiliser la propriété réservée &quot;id&quot;</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>ListElement: cannot use script for property value</source>
        <translation>ListElement : ne peut pas utiliser script comme valeur pour une propriété</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>ListModel: undefined property &apos;%1&apos;</source>
        <translation>ListModel : propriété indéfinie &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QDeclarativeLoader</name>
    <message>
        <location filename="../../qtquick1/src/declarative/graphicsitems/qdeclarativeloader.cpp" line="+400"/>
        <source>Loader does not support loading non-visual elements.</source>
        <translation>Loader n&apos;est pas compatible avec le chargement d&apos;éléments non-visuels.</translation>
    </message>
</context>
<context>
    <name>QDeclarativeParentAnimation</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativeanimation.cpp" line="-169"/>
        <source>Unable to preserve appearance under complex transform</source>
        <translation>Impossible de conserver l&apos;aspect lors d&apos;une transformation complexe</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+7"/>
        <source>Unable to preserve appearance under non-uniform scale</source>
        <translation>Impossible de conserver l&apos;aspect lors d&apos;une mise à l&apos;échelle non uniforme</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unable to preserve appearance under scale of 0</source>
        <translation>Impossible de conserver l&apos;aspect lors d&apos;une mise à l&apos;échelle égale à 0</translation>
    </message>
</context>
<context>
    <name>QDeclarativeParentChange</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativestateoperations.cpp" line="+95"/>
        <source>Unable to preserve appearance under complex transform</source>
        <translation>Impossible de conserver l&apos;aspect lors d&apos;une transformation complexe</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+7"/>
        <source>Unable to preserve appearance under non-uniform scale</source>
        <translation>Impossible de conserver l&apos;aspect lors d&apos;une mise à l&apos;échelle non uniforme</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unable to preserve appearance under scale of 0</source>
        <translation>Impossible de conserver l&apos;aspect lors d&apos;une mise à l&apos;échelle égale à 0</translation>
    </message>
</context>
<context>
    <name>QDeclarativeParser</name>
    <message>
        <location filename="../../qtquick1/src/declarative/qml/parser/qdeclarativejslexer.cpp" line="+533"/>
        <location line="+123"/>
        <location line="+54"/>
        <source>Illegal unicode escape sequence</source>
        <translation>Séquence d&apos;échappement Unicode illégale</translation>
    </message>
    <message>
        <location line="-140"/>
        <source>Illegal character</source>
        <translation>Caractère illégal</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Unclosed string at end of line</source>
        <translation>Chaîne de caractères non fermée en fin de ligne</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Illegal escape sequence</source>
        <translation>Séquence d&apos;échappement illégale</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Unclosed comment at end of file</source>
        <translation>Commentaire non fermé en fin de ligne</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Illegal syntax for exponential number</source>
        <translation>Syntaxe illégale pour un nombre exponentiel</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Identifier cannot start with numeric literal</source>
        <translation>Impossible de commencer un identifiant par un chiffre</translation>
    </message>
    <message>
        <location line="+338"/>
        <source>Unterminated regular expression literal</source>
        <translation>Élément non terminé pour l&apos;expression régulière</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Invalid regular expression flag &apos;%0&apos;</source>
        <translation>Drapeau &apos;%0&apos; invalid pour l&apos;expression régulière</translation>
    </message>
    <message>
        <location line="+16"/>
        <location line="+22"/>
        <source>Unterminated regular expression backslash sequence</source>
        <translation>Séquence antislash non terminée pour l&apos;expression régulière</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Unterminated regular expression class</source>
        <translation>Classe non terminée pour l&apos;expression régulière</translation>
    </message>
    <message>
        <location filename="../../qtquick1/src/declarative/qml/parser/qdeclarativejsparser.cpp" line="+1830"/>
        <location line="+67"/>
        <source>Syntax error</source>
        <translation>Erreur de syntaxe</translation>
    </message>
    <message>
        <location line="-65"/>
        <source>Unexpected token `%1&apos;</source>
        <translation>Jeton inattendu &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+28"/>
        <location line="+24"/>
        <source>Expected token `%1&apos;</source>
        <translation>Un jeton est attendu &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../qtquick1/src/declarative/qml/qdeclarativescriptparser.cpp" line="+246"/>
        <location line="+429"/>
        <location line="+59"/>
        <source>Property value set multiple times</source>
        <translation>La valeur de propriété est attribuée à plusieurs reprises</translation>
    </message>
    <message>
        <location line="-477"/>
        <source>Expected type name</source>
        <translation>Nom de type attendu</translation>
    </message>
    <message>
        <location line="+134"/>
        <source>Invalid import qualifier ID</source>
        <translation>Le qualificatif id d&apos;importation est invalide</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Reserved name &quot;Qt&quot; cannot be used as an qualifier</source>
        <translation>&quot;Qt&quot; est un nom réservé et ne peut pas être utilisé comme qualificatif</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Script import qualifiers must be unique.</source>
        <translation>Les qualificatifs d&apos;importation de script doivent être uniques.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Script import requires a qualifier</source>
        <translation>L&apos;importation de script exige un qualificatif</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Library import requires a version</source>
        <translation>L&apos;importation de bibliothèque exige une version</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Expected parameter type</source>
        <translation>Type de paramètre attendu</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Invalid property type modifier</source>
        <translation>Modificateur invalide pour le type de propriété</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Unexpected property type modifier</source>
        <translation>Modificateur inattendu pour le type de propriété</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Expected property type</source>
        <translation>Type de propriété attendu</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Readonly not yet supported</source>
        <translation>La lecture seule n&apos;est pas encore implémentée</translation>
    </message>
    <message>
        <location line="+218"/>
        <source>JavaScript declaration outside Script element</source>
        <translation>Déclaration JavaScript en dehors de l&apos;élément Script</translation>
    </message>
</context>
<context>
    <name>QDeclarativePauseAnimation</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativeanimation.cpp" line="-2125"/>
        <source>Cannot set a duration of &lt; 0</source>
        <translation>Impossible de définir une durée négative</translation>
    </message>
</context>
<context>
    <name>QDeclarativePixmap</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativepixmapcache.cpp" line="+303"/>
        <source>Error decoding: %1: %2</source>
        <translation>Erreur de décodage : %1 : %2</translation>
    </message>
    <message>
        <location line="+167"/>
        <location line="+360"/>
        <source>Failed to get image from provider: %1</source>
        <translation>Impossible d&apos;obtenir l&apos;image du fournisseur : %1</translation>
    </message>
    <message>
        <location line="-342"/>
        <location line="+360"/>
        <source>Cannot open: %1</source>
        <translation>Impossible d&apos;ouvrir : %1</translation>
    </message>
</context>
<context>
    <name>QDeclarativePropertyAnimation</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativeanimation.cpp" line="+1247"/>
        <source>Cannot set a duration of &lt; 0</source>
        <translation>Impossible de définir une durée négative</translation>
    </message>
</context>
<context>
    <name>QDeclarativePropertyChanges</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativepropertychanges.cpp" line="+249"/>
        <source>PropertyChanges does not support creating state-specific objects.</source>
        <translation>PropertyChanges n&apos;est pas compatible avec la création d&apos;objets spécifiques à un état.</translation>
    </message>
    <message>
        <location line="+168"/>
        <source>Cannot assign to non-existent property &quot;%1&quot;</source>
        <translation>Ne peut pas assigner à la propriété inexistante &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot assign to read-only property &quot;%1&quot;</source>
        <translation>Ne peut pas assigner à la propriété en lecture seule &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QDeclarativeTextInput</name>
    <message>
        <location filename="../../qtquick1/src/declarative/graphicsitems/qdeclarativetextinput.cpp" line="+962"/>
        <location line="+8"/>
        <source>Could not load cursor delegate</source>
        <translation>Impossible de charger le délégué de curseur</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Could not instantiate cursor delegate</source>
        <translation>Impossible d&apos;instancier le délégué de curseur</translation>
    </message>
</context>
<context>
    <name>QDeclarativeTypeLoader</name>
    <message>
        <location filename="../../qtquick1/src/declarative/qml/qdeclarativetypeloader.cpp" line="+929"/>
        <source>Script %1 unavailable</source>
        <translation>Le script %1 n&apos;est pas disponible</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Type %1 unavailable</source>
        <translation>Le type %1 n&apos;est pas disponible</translation>
    </message>
    <message>
        <location line="+189"/>
        <source>Namespace %1 cannot be used as a type</source>
        <translation>L&apos;espace de noms %1 ne peut pas être utilisé comme un type</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
</context>
<context>
    <name>QDeclarativeVME</name>
    <message>
        <location filename="../../qtquick1/src/declarative/qml/qdeclarativevme.cpp" line="+223"/>
        <source>Unable to create object of type %1</source>
        <translation>Impossible de créer un objet de type %1</translation>
    </message>
    <message>
        <location line="+443"/>
        <source>Cannot assign value %1 to property %2</source>
        <translation>Impossible d&apos;assigner la valeur %1 à la propriété %2</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Cannot assign object type %1 with no default method</source>
        <translation>Impossible d&apos;assigner un objet de type %1 sans méthode par défaut</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot connect mismatched signal/slot %1 %vs. %2</source>
        <translation>Impossible de connecter le signal/slot %1 %vs. %2 pour cause d&apos;incompatibilité</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot assign an object to signal property %1</source>
        <translation>Impossible d&apos;assigner un objet à la propriété %1 d&apos;un signal</translation>
    </message>
    <message>
        <location line="+155"/>
        <source>Cannot assign object to list</source>
        <translation>Impossible d&apos;assigner un objet à une liste</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Cannot assign object to interface property</source>
        <translation>Impossible d&apos;assigner un objet à la propriété d&apos;une interface</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Unable to create attached object</source>
        <translation>Impossible de créer un objet attaché</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Cannot set properties on %1 as it is null</source>
        <translation>Impossible d&apos;attribuer les propriétés à %1 car ce dernier est nul</translation>
    </message>
</context>
<context>
    <name>QDeclarativeVisualDataModel</name>
    <message>
        <location filename="../../qtquick1/src/declarative/graphicsitems/qdeclarativevisualitemmodel.cpp" line="+1097"/>
        <source>Delegate component must be Item type.</source>
        <translation>Un composant délégué doit être de type Item.</translation>
    </message>
</context>
<context>
    <name>QDeclarativeXmlListModel</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativeutilmodule.cpp" line="+40"/>
        <location line="+2"/>
        <location line="+49"/>
        <location line="+2"/>
        <source>Qt was built without support for xmlpatterns</source>
        <translation>Qt a été compilé sans support pour xmlpatterns</translation>
    </message>
</context>
<context>
    <name>QDeclarativeXmlListModelRole</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativexmllistmodel_p.h" line="+164"/>
        <source>An XmlRole query must not start with &apos;/&apos;</source>
        <translation>Une requête XmlRole ne doit pas commencer par &apos;/&apos;</translation>
    </message>
</context>
<context>
    <name>QDeclarativeXmlRoleList</name>
    <message>
        <location filename="../../qtquick1/src/declarative/util/qdeclarativexmllistmodel.cpp" line="+820"/>
        <source>An XmlListModel query must start with &apos;/&apos; or &quot;//&quot;</source>
        <translation>Une requête XmlListModel doit commencer par &apos;/&apos; ou &quot;//&quot;</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location line="-246"/>
        <source>&quot;%1&quot; duplicates a previous role name and will be disabled.</source>
        <translation>&quot;%1&quot; est un doublon d&apos;un nom de rôle existant et sera désactivé.</translation>
    </message>
    <message>
        <location line="+525"/>
        <location line="+4"/>
        <source>invalid query: &quot;%1&quot;</source>
        <translation>Requête invalide : &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QmlJSDebugger::QtQuick1::LiveSelectionTool</name>
    <message>
        <location filename="../../qtquick1/src/plugins/qmltooling/qmldbg_inspector/editor/liveselectiontool.cpp" line="+139"/>
        <source>Items</source>
        <translation>Éléments</translation>
    </message>
</context>
<context>
    <name>QmlJSDebugger::QtQuick1::ZoomTool</name>
    <message>
        <location filename="../../qtquick1/src/plugins/qmltooling/qmldbg_inspector/editor/zoomtool.cpp" line="+57"/>
        <source>Zoom to &amp;100%</source>
        <translation>Zoomer à &amp;100 %</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zoom In</source>
        <translation>Zoomer</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zoom Out</source>
        <translation>Dézoomer</translation>
    </message>
</context>
</TS>
