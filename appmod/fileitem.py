#!/usr/bin/env python
# -*- coding: utf-8 -*-


from PyQt5 import QtCore
from PyQt5.QtCore import Qt, QSize
from PyQt5.QtGui import QPainter, QImage, QPixmap

import mimetypes
import os
import os.path
import math, operator
from datetime import datetime

from PIL import Image
from PIL.ExifTags import TAGS

#import ImageChops
#import ImageStat

import imagehash

from applib import log
from applib import setting

TRT_STATUS_WAIT = 0
TRT_STATUS_ERROR = 1
TRT_STATUS_WARNING = 2
TRT_STATUS_OK = 3
TRT_STATUS_IGNORE = 4
TRT_STATUS_IGNORE_NOCMD = 5
TRT_STATUS_IGNORE_EXISTS = 6

"""
    aHash — average hash, for each of the pixels output 1 if the pixel is bigger or equal to the average and 0 otherwise.
    pHash — perceptive hash, does the same as aHash, but first it does a Discrete Cosine Transformation and works in the frequency domain.
    dHash — gradient hash, calculate the difference for each of the pixel and compares the difference with the average differences.
    wHash — wavelet hashing, It works in the frequency domain as pHash but it uses DWT instead of DCT.
"""
IMG_COMPARE_AVG = 0
IMG_COMPARE_GRADIENT = 1
IMG_COMPARE_PERCEPTUAL = 2
IMG_COMPARE_WAVELET = 3

class fileItem( object ):
    """
    Item Fichier
    """
    def __init__(self, parent, filePath, fileName, fileExt=[], getExif=False ):

        self.parent = parent
        self.filePath = filePath
        self.fname = fileName
        self.baseName = os.path.splitext(fileName)[0]
        self.fnameFirst = fileName.split(".")[0]
        self.fileExt = fileExt
        self.getExif = getExif

        self.initItem()

    def initItem( self ):

        self.rawInfos=[]   # (0,1) rawNode,  with (0,n) rawElem (in .childList)

        self.key = None  # dev + "_" + ino

        self.ext = ""

        self.trtStatus = TRT_STATUS_WAIT
        self.trtLib = ""

        self.ino = None
        self.dev = ""

        self.mode = ""
        self.nlink = ""
        self.uid = ""
        self.gid = ""
        self.isimmutable = ""
        self.access_w = ""

        self.islink = ""
        self.linktarget = ""

        self.fsize = ""
        self.fsizeh = ""

        self.c_date = ""
        self.m_date = ""
        self.a_date = ""
        self.c_date_stamp = ""
        self.m_date_stamp = ""
        self.a_date_stamp = ""

        self.mime = ""
        self.ftype = ""

        self.crc32 = ""
        self.sha1 = ""

        self.width = 0
        self.height = 0

        self.exifinfos = ""
        self.exif = {}

        self.tagDateTimeOriginalYear = ""
        self.tagDateTimeOriginalMonth = ""
        self.tagDateTimeOriginalDay  = ""
        self.tagDateTimeOriginalHour = ""
        self.tagDateTimeOriginalMin = ""

        self.key = ""
        self.selection = False
        self.iconLabel = ""
        self.similarity = -1



        if os.path.isfile(self.filePath  + "/" + self.fname  ) :

            import platform
            if platform.system() == "Windows":

                # Grab infos for this file ...
                (self.mode, self.ino, self.dev, self.nlink, self.uid, self.gid, self.fsize, self.fsizeh, self.c_date, self.m_date, self.a_date, self.c_date_stamp, self.m_date_stamp, self.a_date_stamp) = self.get_stat( self.filePath , self.fname )
                self.ext      = self.get_ext( self.filePath , self.fname )
                self.mime     = self.get_mime( self.filePath , self.fname )
                self.ftype    = self.get_ftype( self.filePath , self.fname )
                self.access_w = self.get_access( self.filePath , self.fname )
                (self.islink, self.linktarget) = self.get_link( self.filePath , self.fname )

                self.isimmutable = self.is_immutable(self.filePath + "/"+ self.fname)

            else :


                # Grab infos for this file ...

                (self.mode, self.ino, self.dev, self.nlink, self.uid, self.gid, self.fsize, self.fsizeh, self.c_date, self.m_date, self.a_date, self.c_date_stamp, self.m_date_stamp, self.a_date_stamp) = self.get_stat( self.filePath , self.fname )

                self.ext      = self.get_ext( self.filePath , self.fname )

                (self.islink, self.linktarget) = self.get_link( self.filePath , self.fname )

                self.isimmutable = self.is_immutable(self.filePath + "/"+ self.fname)
                """

                self.mime     = self.get_mime( self.filePath , self.fname )
                self.ftype    = self.get_ftype( self.filePath , self.fname )
                self.access_w = self.get_access( self.filePath , self.fname )

                """

            # Create unique key
            self.key = str(self.dev) + "_" + str(self.ino )

            if self.ext in self.fileExt :
                self.selection = True
            else :
                self.selection = False


            if self.getExif == True :
                try :
                    if self.ext in ("jpg", "JPG") :
                        self.fetch_exifAndSize(  self.filePath + "/"+ self.fname )
                except Exception as e:
                    pass
                    #log.Log.debug(log.ERROR, "Error %s:" % e.args[0] + " / fname=" + self.fname )


        self.iconLabel = self.fetch_label( setting.Setting.getValue("iconview/iconLabelFormat") )


    def get_ExifCreationDateTimeStamp(self) :
        # http://www.awaresystems.be/imaging/tiff/tifftags/privateifd/exif/datetimeoriginal.html
        try :
            TagDateTimeOriginal = Image.open(self.filePath + "/"+ self.fname)._getexif()[36867] # YYYY:MM:DD HH:MM:SS
            self.tagDateTimeOriginalYear = TagDateTimeOriginal[0:4]
            self.tagDateTimeOriginalMonth = TagDateTimeOriginal[5:7]
            self.tagDateTimeOriginalDay = TagDateTimeOriginal[8:10]
            self.tagDateTimeOriginalHour = TagDateTimeOriginal[11:13]
            self.tagDateTimeOriginalMin = TagDateTimeOriginal[14:16]
            self.tagDateTimeOriginalSec = TagDateTimeOriginal[14:16]

            from datetime import datetime, timezone, timedelta
            from calendar import timegm
            dt = datetime(self.tagDateTimeOriginalYear, self.tagDateTimeOriginalMonth, self.tagDateTimeOriginalDay, self.tagDateTimeOriginalHour, self.tagDateTimeOriginalMin, self.tagDateTimeOriginalSec)
            tz = timezone(timedelta(hours=5))
            self.tagDateTimeOriginalTimeStamp = timegm(dt.replace(tzinfo=tz).utctimetuple())

        except :
            pass


    def fetch_label(self, labelStr ) :

        # Creatle icon view label text

        labelStrRet = labelStr

        labelStrRet =  labelStrRet.replace ("[filePath]", str(self.filePath))
        labelStrRet =  labelStrRet.replace ("[fname]", str(self.fname))
        labelStrRet =  labelStrRet.replace ("[key]", str(self.key))
        labelStrRet =  labelStrRet.replace ("[ino]", str(self.ino))
        labelStrRet =  labelStrRet.replace ("[dev]", str(self.dev))
        labelStrRet =  labelStrRet.replace ("[ext]", str(self.ext))
        labelStrRet =  labelStrRet.replace ("[mode]", str(self.mode))
        labelStrRet =  labelStrRet.replace ("[nlink]", str(self.nlink))
        labelStrRet =  labelStrRet.replace ("[uid]", str(self.uid))
        labelStrRet =  labelStrRet.replace ("[gid]", str(self.gid))
        labelStrRet =  labelStrRet.replace ("[islink]", str(self.islink))
        labelStrRet =  labelStrRet.replace ("[linktarget]", str(self.linktarget))
        labelStrRet =  labelStrRet.replace ("[fsize]", str(self.fsize))
        labelStrRet =  labelStrRet.replace ("[fsizeh]", str(self.fsizeh))
        labelStrRet =  labelStrRet.replace ("[c_date]", str(self.c_date))
        labelStrRet =  labelStrRet.replace ("[m_date]", str(self.m_date))
        labelStrRet =  labelStrRet.replace ("[a_date]", str(self.a_date))
        labelStrRet =  labelStrRet.replace ("[c_date_stamp]", str(self.c_date_stamp))
        labelStrRet =  labelStrRet.replace ("[m_date_stamp]", str(self.m_date_stamp))
        labelStrRet =  labelStrRet.replace ("[a_date_stamp]", str(self.a_date_stamp))
        labelStrRet =  labelStrRet.replace ("[access_w]", str(self.access_w))
        labelStrRet =  labelStrRet.replace ("[isimmutable]", str(self.isimmutable))
        labelStrRet =  labelStrRet.replace ("[mime]", str(self.mime))
        labelStrRet =  labelStrRet.replace ("[ftype]", str(self.ftype))
        labelStrRet =  labelStrRet.replace ("[crc32]", str(self.crc32))
        labelStrRet =  labelStrRet.replace ("[sha1]", str(self.sha1))

        labelStrRet =  labelStrRet.replace ("[width]", str(self.width))
        labelStrRet =  labelStrRet.replace ("[height]", str(self.height))

        if self.similarity >= 0 :
            labelStrRet =  labelStrRet.replace ("[similarity]", str(self.similarity))
        else :
            labelStrRet =  labelStrRet.replace ("[similarity]", "")


        now = datetime.now()
        labelStrRet =  labelStrRet.replace ("[current_year]", str(now.year))
        labelStrRet =  labelStrRet.replace ("[current_month]", str(now.month))
        labelStrRet =  labelStrRet.replace ("[current_day]", str(now.day))
        labelStrRet =  labelStrRet.replace ("[current_hour]", str(now.hour))
        labelStrRet =  labelStrRet.replace ("[current_minute]", str(now.minute))
        labelStrRet =  labelStrRet.replace ("[current_second]", str(now.second))
        labelStrRet =  labelStrRet.replace ("[current_microsecond]", str(now.microsecond))

        return labelStrRet

    def getTagList( self) :
        #tagList = []
        #tagList.append()
        #return tagList

        return [
                "[filePath]",
                "[fname]",
                "[key]",
                "[ino]",
                "[dev]",
                "[ext]",
                "[mode]",
                "[nlink]",
                "[uid]",
                "[gid]",
                "[islink]",
                "[linktarget]",
                "[fsize]",
                "[fsizeh]",
                "[c_date]",
                "[m_date]",
                "[a_date]",
                "[c_date_stamp]",
                "[m_date_stamp]",
                "[a_date_stamp]",
                "[access_w]",
                "[isimmutable]",
                "[mime]",
                "[ftype]",
                "[crc32]",
                "[sha1]",
                "[width]",
                "[height]",
                "[current_year]",
                "[current_month]",
                "[current_day]",
                "[current_hour]",
                "[current_minute]",
                "[current_second]",
                "[current_microsecond]"
        ]

    def is_immutable(self, fname):

        import subprocess
        data = []
        try :
            p = subprocess.Popen(['lsattr', fname], bufsize=1, stdout=subprocess.PIPE)
            data, _ = p.communicate()
        except :
            return False
            pass

        if len(data) == 0 :
            return False


        return 'i' in data.decode().split(None, 1)[0]


    def fillRawInfos( self , rawDirlistSearch, rawInfoExtSearch) :

        if self.ext == "NEF" :

            newRawNode = self.appendRawInformationFromFileItem( rawDirlistSearch, rawInfoExtSearch )
            self.rawInfos.append(newRawNode)


    def getItemVal(self, nCol):

        if nCol == COL_ino:
            return self.ino
        if nCol == COL_fname:
            return self.fname
        if nCol == COL_dev:
            return self.dev
        if nCol == COL_ext:
            return self.ext
        if nCol == COL_islink:
            return self.islink
        if nCol == COL_linktarget:
            return self.linktarget
        if nCol == COL_fsize:
            return self.fsize
        if nCol == COL_fsizeh:
            return self.fsizeh
        if nCol == COL_c_date:
            return self.c_date
        if nCol == COL_a_date:
            return self.a_date
        if nCol == COL_m_date:
            return self.m_date
        if nCol == COL_access_w:
            return self.access_w
        if nCol == COL_mime:
            return self.mime
        if nCol == COL_ftype:
            return self.ftype
        if nCol == COL_crc32:
            return self.crc32
        if nCol == COL_sha1:
            return self.sha1
        if nCol == COL_exifinfos:
            return self.exifinfos

        return self.ino

    def get_ext(self, filePath, fname):

        return os.path.splitext(filePath + "/" + fname)[1][1:]

    def get_stat(self , filePath, fname ):

        returnValues = []
        (mode, ino, dev, nlink, uid, gid, fsize, a_date, m_date, c_date) = os.stat(filePath + "/" + fname)

        c_date_stamp = c_date
        m_date_stamp = m_date
        a_date_stamp = a_date
        c_date = datetime.fromtimestamp(c_date)
        m_date = datetime.fromtimestamp(m_date)
        a_date = datetime.fromtimestamp(a_date)
        fsizeh = self.human_size(fsize)

        returnValues.append(mode)
        returnValues.append(ino)
        returnValues.append(dev)
        returnValues.append(nlink)
        returnValues.append(uid)
        returnValues.append(gid)
        returnValues.append(fsize)
        returnValues.append(fsizeh)
        returnValues.append(c_date)
        returnValues.append(m_date)
        returnValues.append(a_date)
        returnValues.append(c_date_stamp)
        returnValues.append(m_date_stamp)
        returnValues.append(a_date_stamp)

        return returnValues

    def get_link(self, filePath, fname):
        returnValues = []
        islink = os.path.islink(filePath + "/" + fname)
        returnValues.append(islink)
        try:
            self.linktarget = os.path.realpath(os.readlink(filePath + "/" + fname))
            returnValues.append(self.linktarget)
        except Exception as e:
            returnValues.append("")

        return returnValues

    def get_access(self, filePath, fname):
        return os.access(filePath + "/" + fname, os.W_OK)

    def get_mime(self, filePath, fname):
        mime = mimetypes.guess_type(filePath + "/" + fname)
        if mime == None:
            mime = ""
        return mime

    def get_ftype(self, filePath, fname):
        try :
            import magic
            m = magic.open(magic.MAGIC_NONE)
            m.load()
            return m.file(filePath + "/" + fname)
        except Exception as e:
            return ""

    def get_orientation(self) :
        try :
            return self.exif["Orientation"]
        except :
            return "1"

    def human_size(self, nbytes):
        """
        Formatte la taille du fichier dans un format lisible "humainement"
        """
        import math
        suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
        human = nbytes
        rank = 0
        if nbytes != 0:
            rank = int((math.log10(nbytes)) / 3)
            rank = min(rank, len(suffixes) - 1)
            human = nbytes / (1024.0 ** rank)
        f = ('%.2f' % human).rstrip('0').rstrip('.')
        return '%s %s' % (f, suffixes[rank])

    def human_diffdate(self, d1, d2):
        """
        Human-readable date difference.
        """
        _d1 = datetime.datetime.strptime(d1, "%a %b %d %H:%M:%S %Y")
        _d2 = datetime.datetime.strptime(d2, "%a %b %d %H:%M:%S %Y")
        diff = _d2 - _d1
        return diff.days # <-- alternatively: diff.seconds

    def fetch_exifAndSize(self, filePathName):

        self.exif = {}

        img = Image.open(filePathName)

        self.width, self.height = img.size

        info = img._getexif()
        for tag, value in info.items():
            decoded = TAGS.get(tag, tag)
            #self.exif.append( { decoded  : str(value) } )
            #print( "" +  decoded + " = "+ str(value))
            #self.exif[decoded] = str(value)
            self.exif[str(decoded)] = value

    def get_crc32(self):
        """
        Calcule le hash crc32 du fichier courant
        """
        import binascii
        if os.path.isfile(self.filePath  + "/" + self.fname  ) :
            buf = open(self.filePath + "/" + self.fname, 'rb').read()
            buf = (binascii.crc32(buf) & 0xFFFFFFFF)
            self.crc32 = "%08X" % buf
            return self.crc32
        else :
            return ""

    def get_md5(self):
        """
        Calcule le hash md5 du fichier courant
        """
        if os.path.isfile(self.filePath  + "/" + self.fname  ) :
            import hashlib
            BLOCKSIZE = 100000000
            hasher = hashlib.md5()
            with open(self.filePath + "/" + self.fname, 'rb') as afile:
                buf = afile.read(BLOCKSIZE)
                while len(buf) > 0:
                    hasher.update(buf)
                    buf = afile.read(BLOCKSIZE)
            self.md5 = hasher.hexdigest()
            return self.md5
        else :
            return ""

    def get_sha1(self):
        """
        Calcule le hash sha1 du fichier courant
        """
        if os.path.isfile(self.filePath  + "/" + self.fname  ) :

            import hashlib
            BLOCKSIZE = 100000000
            hasher = hashlib.sha1()
            with open(self.filePath + "/" + self.fname, 'rb') as afile:
                buf = afile.read(BLOCKSIZE)
                while len(buf) > 0:
                    hasher.update(buf)
                    buf = afile.read(BLOCKSIZE)
            self.sha1 = hasher.hexdigest()
            return self.sha1
        else :
            return ""

    def imageDiffCompare(self, imgfile1, imgfile2, hashMethod = 2):
        """
        Compare deux images selon une méthode de hash passée en paramètre
        Retourne le taux de similarité (0..100%)
        """
        try :
            im1 = Image.open (imgfile1)
            im2 = Image.open (imgfile2)
        except Exception as e:
            return 0

        h1=0
        h2=0

        if hashMethod == IMG_COMPARE_AVG :
            h1 = imagehash.average_hash(im1)
            h2 = imagehash.average_hash(im2)
        elif hashMethod == IMG_COMPARE_GRADIENT :
            h1 = imagehash.dhash(im1)
            h2 = imagehash.dhash(im2)
        elif hashMethod == IMG_COMPARE_PERCEPTUAL :
            h1 = imagehash.phash(im1)
            h2 = imagehash.phash(im2)
        elif hashMethod == IMG_COMPARE_WAVELET :
            h1 = imagehash.whash(im1)
            h2 = imagehash.whash(im2)

        if h1 == h2 :
            return 100

        return 100-(h1-h2)


    def showHistogramRGB( self ) :
        """
        Cacule et affiche dans un pop-up l'histogramme rgb de l'image courante
        """
        # RGB Hitogram
        # This script will create a histogram image based on the RGB content of
        # an image. It uses PIL to do most of the donkey work but then we just
        # draw a pretty graph out of it.
        #
        # May 2009,  Scott McDonough, www.scottmcdonough.co.uk
        #

        from PIL import ImageDraw

        imagepath = self.filePath + "/" + self.fname  # The image to build the histogram of


        histHeight = 120            # Height of the histogram
        histWidth = 256             # Width of the histogram
        multiplerValue = 1.5        # The multiplier value basically increases
                                    # the histogram height so that low values
                                    # are easier to see, this in effect chops off
                                    # the top of the histogram.
        showFstopLines = True       # True/False to hide outline
        fStopLines = 5


        # Colours to be used
        backgroundColor = (51,51,51)    # Background color
        lineColor = (102,102,102)       # Line color of fStop Markers
        red = (255,60,60)               # Color for the red lines
        green = (51,204,51)             # Color for the green lines
        blue = (0,102,255)              # Color for the blue lines


        img = Image.open(imagepath)
        hist = img.histogram()
        histMax = max(hist)                                     #comon color
        xScale = float(histWidth)/len(hist)                     # xScaling
        yScale = float((histHeight)*multiplerValue)/histMax     # yScaling

        im = Image.new("RGBA", (histWidth, histHeight), backgroundColor)
        draw = ImageDraw.Draw(im)

        # Draw Outline is required
        if showFstopLines:
            xmarker = histWidth/fStopLines
            x =0
            for i in range(1,fStopLines+1):
                draw.line((x, 0, x, histHeight), fill=lineColor)
                x+=xmarker
            draw.line((histWidth-1, 0, histWidth-1, 200), fill=lineColor)
            draw.line((0, 0, 0, histHeight), fill=lineColor)

        # Draw the RGB histogram lines
        x=0; c=0;
        for i in hist:
            if int(i)==0: pass
            else:
                color = red
                if c>255: color = green
                if c>511: color = blue
                draw.line((x, histHeight, x, histHeight-(i*yScale)), fill=color)
            if x>255: x=0
            else: x+=1
            c+=1

        # Now save and show the histogram
        #im.save('histogram.png', 'PNG')
        im.show()



    def createThumb( self, thumbSizeWidth, thumbSizeHeight, thumbCacheDir, thumbQuality, overWrite, overlayIConLink, overlayIconSizeW, overlayIconSizeH) :
        """
        Créer la vignette d'une image

        sudo apt-get install qt5-image-formats-plugins
        """
        linkAttr = ""
        if self.islink is True :
            linkAttr="_l"

        if self.ext not in self.fileExt :
            return

        thumbFileName = self.key + linkAttr + ".JPG"
        overlayIConExt = QImage("icon/ext_"+self.ext+".png")
        overlayIConExt = overlayIConExt.scaled( QSize(overlayIconSizeW, overlayIconSizeH), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.FastTransformation ) # or .Qt.SmoothTransformation   /  QtCore.Qt.FastTransformation

        if not os.path.isfile( thumbCacheDir + thumbFileName ) or overWrite is True :

        #if overWrite is True or not os.path.isfile( thumbCacheDir + "/" + thumbFileName  )  :

            baseQimage = QImage( self.filePath + "/" + self.fname)

            if not baseQimage.isNull() :
                baseQimage = baseQimage.scaled( QSize(thumbSizeHeight, thumbSizeWidth), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.FastTransformation ) # or .Qt.SmoothTransformation   /  QtCore.Qt.FastTransformation

                overlayQimage = QImage( baseQimage.size(), QImage.Format_ARGB32_Premultiplied)

                painter = QPainter( overlayQimage )
                self.drawOverlayIconInit( painter , overlayQimage, baseQimage )

                if self.islink is True :
                    self.drawOverlayIcon( painter, overlayQimage.width() -32, overlayQimage.height()-32, overlayIConLink )

                self.drawOverlayIcon( painter, 0,0 , overlayIConExt )

                painter.end()

                overlayQimage.save( thumbCacheDir + "/" + thumbFileName  , "JPG", thumbQuality)
            else :
                log.Log.debug(log.INFO, "Erreur de création de vignettes (format incorrect): " +  pathName + "/" + fileName )


    def drawOverlayIconInit(self, painter, imageWithOverlay, baseImage ) :
        """
        Préparer le dessin d'une icône
        """
        painter.setCompositionMode(QPainter.CompositionMode_Source)
        painter.fillRect(imageWithOverlay.rect(), Qt.transparent)
        painter.setCompositionMode(QPainter.CompositionMode_SourceOver)
        painter.drawImage(0, 0, baseImage)

    def drawOverlayIcon(self, painter, posX, posY, overlayIConImage ) :
        """
        Dessiner une icône en surimpression
        """
        painter.setCompositionMode(QPainter.CompositionMode_SourceOver)
        painter.drawImage( posX , posY, overlayIConImage)


    def appendRawInformationFromFileItem (self , rawDirlistSearch, rawInfoExtSearch) :

        fileBaseName  = os.path.splitext(os.path.basename( self.fname ))[0] # Current file searched

        newRawNode = rawNode()
        newRawNode.setNodelabel( self.fname + ", ino #" + str(self.ino) )
        newRawNode.setNodeId( self.ino )
        newRawNode.childList = []

        for pathSearchElem in rawDirlistSearch :

            #print ( "------------> IN : " + pathSearchElem)
            #print ( "   search raw info for: " + fileBaseName)

            for rawInfoExtSearchElem in rawInfoExtSearch :

                if os.path.isfile( pathSearchElem + fileBaseName + "." + rawInfoExtSearchElem ) == True :
                    newRawElem = rawElem()
                    (mode, ino, dev, nlink, uid, gid, fsize, fsizeh, c_date, m_date, a_date, c_date_stamp, m_date_stamp, a_date_stamp) = self.get_stat( pathSearchElem , fileBaseName + "." + rawInfoExtSearchElem )
                    #(mode, ino, dev, nlink, uid, gid, fsize, a_date, m_date, c_date) = os.stat( pathSearchElem + fileBaseName + "." + rawInfoExtSearchElem )
                    newRawElem.ino = ino
                    newRawElem.ftype = "." + rawInfoExtSearchElem
                    newRawElem.fname = fileBaseName + "." + rawInfoExtSearchElem
                    newRawElem.fpath = pathSearchElem
                    newRawElem.setNodeId( ino )
                    newRawNode.childList.append( newRawElem )
                    #print ("     trouvé ! : " + pathSearchElem + fileBaseName + "." + rawInfoExtSearchElem)

        return newRawNode



        """
        #from timeit import default_timer as timer
        import time
        starttime = time.time()

        # ...........................

        # ...........................
        endtime = time.time()
        #self.timePerf = self.timePerf + (end - start)
        #print("self.timePerf = " + str(self.timePerf*1000))
        print ( "%5.3fs to do." % (endtime - starttime))
        """




class rawElem( ) :

    def __init__(self):

        self.ino = None
        self.ftype = ""
        self.fname = ""
        self.fpath = ""
        self.nodeId = None

    def setFtype(self, ftype) :
        self.ftype = ftype

    def setFname(self, fname) :
        self.fname = fname

    def setFpath(self, fpath) :
        self.fpath = fpath

    def setNodeId(self, nodeId) :
        self.nodeId = nodeId

class rawNode() :

    def __init__(self):

        self.nodeLabel = ""
        self.nodeId = None

    def setNodelabel(self, nodeLabel) :
        self.nodeLabel = nodeLabel

    def setNodeId(self, nodeId) :
        self.nodeId = nodeId



def reset( self) :
	self.cache = {}

def listdir (self, path) :
		try :
			cached_mtime, list = self.cache[path]
			del self.cache[key]
		except KeyError :
			cached_mtime, list = -1, []
		mtime = os.stat(path).st_mtime
		if mtime != cached_mtime :
			list = os.listdir( path )
			list.sort
		self.cache[path] = mtime, list
		return list


COL_thumb = 0
COL_selection = 1
COL_key = 2
COL_ino = 3
COL_fname = 4
COL_dev = 5
COL_ext = 6
COL_islink = 7
COL_linktarget = 8
COL_fsize = 9
COL_fsizeh = 10
COL_c_date = 11
COL_m_date = 12
COL_a_date = 13
COL_access_w = 14
COL_mime = 15
COL_ftype = 16
COL_crc32 = 17
COL_sha1 = 18
COL_exifinfos = 19
COL_isimmutable = 20
COL_width = 21
COL_height = 22
COL_trtstatus = 23
COL_trtlib = 24

COL_exifDateYear=25
COL_exifDateMonth=26
COL_exifDateDay=27



