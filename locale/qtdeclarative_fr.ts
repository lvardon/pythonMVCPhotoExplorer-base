<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>Object</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/compiler/qqmlirbuilder.cpp" line="+95"/>
        <location line="+5"/>
        <source>Duplicate method name</source>
        <translation>Nom de méthode dupliqué</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Method names cannot begin with an upper case letter</source>
        <translation>Les noms des méthodes ne peuvent pas commencer par une majuscule</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Illegal method name</source>
        <translation>Nom de méthode illégal</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Duplicate signal name</source>
        <translation>Nom de signal dupliqué</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Duplicate property name</source>
        <translation>Nom de propriété dupliqué</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Property names cannot begin with an upper case letter</source>
        <translation>Les noms des propriétés ne peuvent pas commencer par une majuscule</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Duplicate default property</source>
        <translation>Propriété par défaut dupliquée</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Property value set multiple times</source>
        <translation>La valeur de la propriété est affectée plusieurs fois</translation>
    </message>
</context>
<context>
    <name>QInputMethod</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/util/qquickutilmodule.cpp" line="+66"/>
        <source>InputMethod is an abstract class</source>
        <translation>ImputMethod est une classe abstraite</translation>
    </message>
</context>
<context>
    <name>QQmlAnonymousComponentResolver</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/compiler/qqmltypecompiler.cpp" line="+1493"/>
        <source>Component objects cannot declare new functions.</source>
        <translation>Les objets composants ne peuvent pas déclarer de nouvelles fonctions.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Component objects cannot declare new properties.</source>
        <translation>Les objets composants ne peuvent pas déclarer de nouvelles propriétés.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Component objects cannot declare new signals.</source>
        <translation>Les objets composants ne peuvent pas déclarer de nouveaux signaux.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot create empty component specification</source>
        <translation>Impossible de créer une spécification du composant vide</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Component elements may not contain properties other than id</source>
        <translation>Les éléments du composant ne peuvent pas contenir des propriétés autres que id</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Invalid component body specification</source>
        <translation>La spécification du corps du composant est invalide</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>id is not unique</source>
        <translation>la propriété id n&apos;est pas unique</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Invalid alias reference. Unable to find id &quot;%1&quot;</source>
        <translation>La référence d&apos;alias est invalide. Impossible de trouver l&apos;identifiant &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+47"/>
        <location line="+14"/>
        <location line="+9"/>
        <source>Invalid alias target location: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid alias location</source>
        <translation type="vanished">L&apos;emplacement d&apos;alias est invalide</translation>
    </message>
</context>
<context>
    <name>QQmlCodeGenerator</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/compiler/qqmlirbuilder.cpp" line="+318"/>
        <location line="+689"/>
        <source>Property value set multiple times</source>
        <translation>La valeur de la propriété est affectée plusieurs fois</translation>
    </message>
    <message>
        <location line="-629"/>
        <location line="+656"/>
        <source>Expected type name</source>
        <translation>Nom de type attendu</translation>
    </message>
    <message>
        <location line="-411"/>
        <source>Signal names cannot begin with an upper case letter</source>
        <translation>Les noms des signaux ne peuvent pas commencer par une majuscule</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Illegal signal name</source>
        <translation>Le nom du signal est invalide</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>No property alias location</source>
        <translation>Aucun emplacement d&apos;alias de propriété</translation>
    </message>
    <message>
        <location line="+18"/>
        <location line="+6"/>
        <location line="+4"/>
        <source>Invalid alias reference. An alias reference must be specified as &lt;id&gt;, &lt;id&gt;.&lt;property&gt; or &lt;id&gt;.&lt;value property&gt;.&lt;property&gt;</source>
        <translation>La référence de l&apos;alias est invalide. Les références d&apos;alias doivent être spécifiées comme &lt;id&gt;, &lt;id&gt;.&lt;propriété&gt; ou &lt;id&gt;.&lt;valeur de propriété&gt;.&lt;propriété&gt;</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>Invalid alias location</source>
        <translation>L&apos;emplacement d&apos;alias est invalide</translation>
    </message>
    <message>
        <location line="+210"/>
        <source>Invalid component id specification</source>
        <translation>L&apos;identifiant de composant spécifié n&apos;est pas valide</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Invalid empty ID</source>
        <translation>Un identifiant vide invalide</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>IDs cannot start with an uppercase letter</source>
        <translation>Les identifiants ne peuvent pas commencer par une  majuscule</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>IDs must start with a letter or underscore</source>
        <translation>Les identifiants doivent commencer par une lettre ou un tiret bas</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>IDs must contain only letters, numbers, and underscores</source>
        <translation>Les identifiants ne peuvent contenir que des lettres, des nombres ou des tirets bas</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>ID illegally masks global JavaScript property</source>
        <translation>L&apos;identifiant masque illégalement une propriété JavaScript globale</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Invalid use of id property</source>
        <translation>L&apos;utilisation de la propriété id est invalide</translation>
    </message>
    <message>
        <location line="-282"/>
        <source>Illegal property name</source>
        <translation>Le nom de la propriété est invalide</translation>
    </message>
</context>
<context>
    <name>QQmlComponent</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlcomponent.cpp" line="+661"/>
        <source>Invalid empty URL</source>
        <translation>Une URL vide est invalide</translation>
    </message>
    <message>
        <location line="+596"/>
        <location line="+117"/>
        <source>createObject: value is not an object</source>
        <translation>createObject : la valeur fournie n&apos;est pas un objet</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlincubator.cpp" line="+290"/>
        <source>Object destroyed during incubation</source>
        <translation>L&apos;objet a été détruit pendant l&apos;incubation</translation>
    </message>
</context>
<context>
    <name>QQmlConnections</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/types/qqmlconnections.cpp" line="+207"/>
        <location line="+57"/>
        <source>Cannot assign to non-existent property &quot;%1&quot;</source>
        <translation>Imposible d&apos;assigner à la propriété inexistante &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="-49"/>
        <source>Connections: nested objects not allowed</source>
        <translation>Connexions : les éléments imbriqués ne sont pas autorisés</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Connections: syntax error</source>
        <translation>Connexions : erreur de syntaxe</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connections: script expected</source>
        <translation>Connexions : un script est attendu</translation>
    </message>
</context>
<context>
    <name>QQmlDelegateModel</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/types/qqmldelegatemodel.cpp" line="+392"/>
        <source>The delegate of a DelegateModel cannot be changed within onUpdated.</source>
        <translation>Le délégué de DelegateModel ne peut pas être changé dans onUpdated.</translation>
    </message>
    <message>
        <location line="+206"/>
        <source>The maximum number of supported DelegateModelGroups is 8</source>
        <translation>Le nombre maximal de DelegateModelGroup supporté est 8</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>The group of a DelegateModel cannot be changed within onChanged</source>
        <translation>Le groupe de DelegateModel ne peut pas être changé dans onChanged</translation>
    </message>
</context>
<context>
    <name>QQmlDelegateModelGroup</name>
    <message>
        <location line="-412"/>
        <source>Group names must start with a lower case letter</source>
        <translation>Les noms de groupe doivent commencer par une minuscule</translation>
    </message>
    <message>
        <location line="+2185"/>
        <source>get: index out of range</source>
        <translation>get : l&apos;index est hors de la plage de valeurs admissibles</translation>
    </message>
    <message>
        <location line="+93"/>
        <source>insert: index out of range</source>
        <translation>insert : l&apos;index est hors de la plage de valeurs admissibles</translation>
    </message>
    <message>
        <location line="+85"/>
        <source>create: index out of range</source>
        <translation>create : l&apos;index est hors de la plage de valeurs admissibles</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>resolve: from index out of range</source>
        <translation>resolve : l&apos;index from est hors de la plage de valeurs admissibles</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>resolve: from index invalid</source>
        <translation>resolve : l&apos;index from est invalide</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>resolve: to index out of range</source>
        <translation>resolve : l&apos;index est hors de la plage de valeurs admissibles</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>resolve: to index invalid</source>
        <translation>resolve : l&apos;index est invalide</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>resolve: from is not an unresolved item</source>
        <translation>resolve : from n&apos;est pas un élément non résolu</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>resolve: to is not a model item</source>
        <translation>resolve : n&apos;est pas un élément d&apos;un modèle</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>remove: invalid index</source>
        <translation>remove : l&apos;index est invalide</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>remove: index out of range</source>
        <translation>remove : l&apos;index est hors de la plage de valeurs admissibles</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>remove: invalid count</source>
        <translation>remove : le compte est invalide</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>addGroups: index out of range</source>
        <translation>addGroups : l&apos;index est hors de la plage de valeurs admissibles</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>addGroups: invalid count</source>
        <translation>addGroups : le compte est invalide</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>removeGroups: index out of range</source>
        <translation>removeGroups : l&apos;index est hors de la plage de valeurs admissibles</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>removeGroups: invalid count</source>
        <translation>removeGroups : le compte est invalide</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>setGroups: index out of range</source>
        <translation>setGroups : l&apos;index est hors de la plage de valeurs admissibles</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>setGroups: invalid count</source>
        <translation>setGroups : le compte est invalide</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>move: invalid from index</source>
        <translation>move : l&apos;index from est invalide</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>move: invalid to index</source>
        <translation>move : l&apos;index to est invalide</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>move: invalid count</source>
        <translation>move : le compte est invalide</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>move: from index out of range</source>
        <translation>move : l&apos;index from est hors de la plage de valeurs admissibles</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>move: to index out of range</source>
        <translation>move : l&apos;index est hors de la plage de valeurs admissibles</translation>
    </message>
</context>
<context>
    <name>QQmlEngine</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlengine.cpp" line="+206"/>
        <location line="+646"/>
        <source>Locale cannot be instantiated.  Use Qt.locale()</source>
        <translation>Locale ne peut pas être instancié. Utilisez Qt.locale()</translation>
    </message>
    <message>
        <location line="-240"/>
        <source>There are still &quot;%1&quot; items in the process of being created at engine destruction.</source>
        <translation>Il reste &quot;%1&quot; éléments dans le processus qui ont été créé par la machine.</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/imports/localstorage/plugin.cpp" line="+277"/>
        <source>executeSql called outside transaction()</source>
        <translation>executeSql a été appelé en dehors de transaction()</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Read-only Transaction</source>
        <translation>La transaction est en lecture seule</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Version mismatch: expected %1, found %2</source>
        <translation>La version est incompatible : %1 attendue, %2 trouvée</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>SQL transaction failed</source>
        <translation>la transaction SQL a échouée</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>transaction: missing callback</source>
        <translation>transaction : la fonction de rappel est absente</translation>
    </message>
    <message>
        <location line="+226"/>
        <source>SQL: can&apos;t create database, offline storage is disabled.</source>
        <translation>SQL : impossible de créer la base de données, l&apos;enregistrement hors ligne est désactivé.</translation>
    </message>
    <message>
        <location line="+28"/>
        <location line="+14"/>
        <source>SQL: database version mismatch</source>
        <translation>SQL : la version de la base de données est incompatible</translation>
    </message>
</context>
<context>
    <name>QQmlEnumTypeResolver</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/compiler/qqmltypecompiler.cpp" line="-516"/>
        <source>Invalid property assignment: Enum value &quot;%1&quot; cannot start with a lowercase letter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Invalid property assignment: &quot;%1&quot; is a read-only property</source>
        <translation>Affectation de propriété invalide : &quot;%1&quot; est une propriété en lecture seule</translation>
    </message>
</context>
<context>
    <name>QQmlImportDatabase</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlimport.cpp" line="+565"/>
        <source>&quot;%1&quot; is ambiguous. Found in %2 and in %3</source>
        <translation>&quot;%1&quot; est ambigu. Il a été trouvé dans %2 et dans %3</translation>
    </message>
    <message>
        <location line="+150"/>
        <source>- %1 is not a namespace</source>
        <translation>- %1 n&apos;est pas un espace de noms</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>- nested namespaces not allowed</source>
        <translation>- les espaces de noms imbriqués ne sont pas autorisés</translation>
    </message>
    <message>
        <location line="+58"/>
        <location line="+4"/>
        <source>local directory</source>
        <translation>répertoire local</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>is ambiguous. Found in %1 and in %2</source>
        <translation>est ambigu. Trouvé dans %1 et dans %2</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>is ambiguous. Found in %1 in version %2.%3 and %4.%5</source>
        <translation>est ambigu. Trouvé dans %1 dans les versions %2.%3 et %4.%5</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>is instantiated recursively</source>
        <translation>est instancié récursivement</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>is not a type</source>
        <translation>n&apos;est pas un type</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>static plugin for module &quot;%1&quot; with name &quot;%2&quot; has no metadata URI</source>
        <translation>le plugin statique pour le module &quot;%1&quot; avec le nom &quot;%2&quot; n&apos;a pas de méta-données URI</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>module does not support the designer &quot;%1&quot;</source>
        <translation>le module ne supporte pas le designer &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>plugin cannot be loaded for module &quot;%1&quot;: %2</source>
        <translation>impossible de charger le plugin pour le module &quot;%1&quot; : %2</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>static plugin for module &quot;%1&quot; with name &quot;%2&quot; cannot be loaded: %3</source>
        <translation>le plugin statique pour le module &quot;%1&quot; avec le nom &quot;%2&quot; ne peut pas être chargé : %3</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>could not resolve all plugins for module &quot;%1&quot;</source>
        <translation>Impossible de résoudre tous les plugins pour le module &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>module &quot;%1&quot; plugin &quot;%2&quot; not found</source>
        <translation>le plugin &quot;%2&quot; du module &quot;%1&quot; n&apos;a pas été trouvé</translation>
    </message>
    <message>
        <location line="+173"/>
        <location line="+24"/>
        <source>&quot;%1&quot; version %2.%3 is defined more than once in module &quot;%4&quot;</source>
        <translation>&quot;%1&quot; en version %2.%3 est défini plus d&apos;une fois dans le module &quot;%4&quot;</translation>
    </message>
    <message>
        <location line="+15"/>
        <location line="+87"/>
        <location line="+127"/>
        <source>module &quot;%1&quot; version %2.%3 is not installed</source>
        <translation>la version %2.%3 du module &quot;%1&quot; n&apos;est pas installée</translation>
    </message>
    <message>
        <location line="-125"/>
        <location line="+127"/>
        <source>module &quot;%1&quot; is not installed</source>
        <translation>le module &quot;%1&quot; n&apos;est pas installé</translation>
    </message>
    <message>
        <location line="-80"/>
        <source>&quot;%1&quot;: no such directory</source>
        <translation>&quot;%1&quot; : le répertoire n&apos;existe pas</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>import &quot;%1&quot; has no qmldir and no namespace</source>
        <translation>l&apos;importation &quot;%1&quot; n&apos;a pas de fichier qmldir ni d&apos;espace de noms</translation>
    </message>
    <message>
        <location line="+446"/>
        <source>Module loaded for URI &apos;%1&apos; does not implement QQmlTypesExtensionInterface</source>
        <translation>Le module chargé depuis l&apos;URI &quot;%1&quot; n&apos;implémente pas la classe QQmlTypesExtensionInterface</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Module namespace &apos;%1&apos; does not match import URI &apos;%2&apos;</source>
        <translation>L&apos;espace de noms &quot;%1&quot; du module ne correspond pas à l&apos;URI d&apos;importation &quot;%2&quot;</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Namespace &apos;%1&apos; has already been used for type registration</source>
        <translation>L&apos;espace de noms &quot;%1&quot; a déjà été utilisé pour l&apos;enregistrement de type</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Module &apos;%1&apos; does not contain a module identifier directive - it cannot be protected from external registrations.</source>
        <translation>Le module &quot;%1&quot; ne contient pas de directive d&apos;identification de module - il ne peut pas être protégé contre les enregistrements externes.</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>File name case mismatch for &quot;%1&quot;</source>
        <translation>La casse du nom de fichier ne correspond pas pour &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QQmlListModel</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/types/qqmllistmodel.cpp" line="+2005"/>
        <source>unable to enable dynamic roles as this model is not empty!</source>
        <translation>impossible d&apos;activer les roles dynamiques puisque ce modèle n&apos;est pas vide !</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>unable to enable static roles as this model is not empty!</source>
        <translation>impossible d&apos;activer les roles statiques puisque ce modèle n&apos;est pas vide !</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>dynamic role setting must be made from the main thread, before any worker scripts are created</source>
        <translation>le paramétrage d&apos;un rôle dynamique doit est défini depuis le thread principal, avant que les scripts de travail ne soient créés</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>remove: indices [%1 - %2] out of range [0 - %3]</source>
        <translation>remove : les indices [%1 - %2] est hors de la plage de valeurs admissibles [0 - %3]</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>remove: incorrect number of arguments</source>
        <translation>remove : le nombre d&apos;arguments est incorrect</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>insert: index %1 out of range</source>
        <translation>insert : l&apos;index %1 est hors de la plage de valeurs admissibles</translation>
    </message>
    <message>
        <location line="+32"/>
        <location line="+3"/>
        <source>insert: value is not an object</source>
        <translation>insert : une valeur n&apos;est pas un objet</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>move: out of range</source>
        <translation>move : hors de la plage de valeurs admissibles</translation>
    </message>
    <message>
        <location line="+89"/>
        <location line="+3"/>
        <source>append: value is not an object</source>
        <translation>append : une valeur n&apos;est pas un objet</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>set: value is not an object</source>
        <translation>set : une valeur n&apos;est pas un objet</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+46"/>
        <source>set: index %1 out of range</source>
        <translation>set : l&apos;index %1 est hors de la plage de valeurs admissibles</translation>
    </message>
    <message>
        <location line="+50"/>
        <location line="+15"/>
        <source>ListElement: cannot contain nested elements</source>
        <translation>ListElement : ne peut pas contenir des éléments imbriqués</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>ListElement: cannot use reserved &quot;id&quot; property</source>
        <translation>ListElement : ne peut pas utiliser la propriété réservée &quot;id&quot;</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>ListElement: cannot use script for property value</source>
        <translation>ListElement : ne peut pas utiliser un script comme valeur pour une propriété</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>ListModel: undefined property &apos;%1&apos;</source>
        <translation>ListModel : la propriété &quot;%1&quot; n&apos;est pas définie</translation>
    </message>
</context>
<context>
    <name>QQmlObjectCreator</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlobjectcreator.cpp" line="+632"/>
        <source>Cannot assign value %1 to property %2</source>
        <translation>Impossible d&apos;assigner la valeur %1 à la propriété %2</translation>
    </message>
    <message>
        <location line="+157"/>
        <location line="+12"/>
        <source>Cannot set properties on %1 as it is null</source>
        <translation>Impossible d&apos;attribuer les propriétés à %1 car ce dernier est nul</translation>
    </message>
    <message>
        <location line="+116"/>
        <source>Cannot assign an object to signal property %1</source>
        <translation>Impossible d&apos;assigner un objet à la propriété %1 d&apos;un signal</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cannot assign object type %1 with no default method</source>
        <translation>Impossible d&apos;assigner un objet de type %1 sans méthode par défaut</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot connect mismatched signal/slot %1 %vs. %2</source>
        <translation>Impossible de connecter le signal/slot %1 %vs. %2 pour cause d&apos;incompatibilité</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Cannot assign object to interface property</source>
        <translation>Impossible d&apos;assigner un objet à la propriété d&apos;une interface</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Cannot assign object to read only list</source>
        <translation>Impossible d&apos;assigner un objet à une liste en lecture seule</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot assign primitives to lists</source>
        <translation>Impossible d&apos;assigner des primitives à des listes</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Unable to create object of type %1</source>
        <translation>Impossible de créer un objet de type %1</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Composite Singleton Type %1 is not creatable</source>
        <translation>Il n&apos;est pas possible de créer le type singleton composite %1</translation>
    </message>
</context>
<context>
    <name>QQmlObjectModel</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/types/qqmlobjectmodel.cpp" line="+370"/>
        <source>insert: index %1 out of range</source>
        <translation type="unfinished">insert : l&apos;index %1 est hors de la plage de valeurs admissibles</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>move: out of range</source>
        <translation type="unfinished">move : hors de la plage de valeurs admissibles</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>remove: indices [%1 - %2] out of range [0 - %3]</source>
        <translation type="unfinished">remove : les indices [%1 - %2] est hors de la plage de valeurs admissibles [0 - %3]</translation>
    </message>
</context>
<context>
    <name>QQmlParser</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/compiler/qqmlirbuilder.cpp" line="-517"/>
        <source>Unexpected object definition</source>
        <translation>Une définition d&apos;objet est inattendue</translation>
    </message>
    <message>
        <location line="+216"/>
        <source>Invalid import qualifier ID</source>
        <translation>Le qualificateur ID d&apos;importation est invalide</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Reserved name &quot;Qt&quot; cannot be used as an qualifier</source>
        <translation>&quot;Qt&quot; est un nom réservé et ne peut pas être utilisé comme qualificatif</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Script import qualifiers must be unique.</source>
        <translation>Les qualificateurs d&apos;importation de script doivent être uniques.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Script import requires a qualifier</source>
        <translation>L&apos;importation de script exige un qualificatif</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Library import requires a version</source>
        <translation>L&apos;importation de bibliothèque exige un numéro de version</translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+4"/>
        <source>Pragma requires a valid qualifier</source>
        <translation>Les pragma exigent un qualificatif valide</translation>
    </message>
    <message>
        <location line="+81"/>
        <source>Expected parameter type</source>
        <translation>Un type de paramètre est attendu</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Invalid signal parameter type: </source>
        <translation>Le type du paramètre du signal n&apos;est pas valide : </translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Invalid property type modifier</source>
        <translation>Le modificateur de type de propriété est invalide</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unexpected property type modifier</source>
        <translation>Le modificateur de type de propriété est inattendu</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Expected property type</source>
        <translation>Un type de propriété est attendu</translation>
    </message>
    <message>
        <location line="+111"/>
        <source>JavaScript declaration outside Script element</source>
        <translation>Déclaration JavaScript en dehors de l&apos;élément Script</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/qml/parser/qqmljslexer.cpp" line="+592"/>
        <location line="+421"/>
        <source>Illegal syntax for exponential number</source>
        <translation>La syntaxe est illégale pour un nombre exponentiel</translation>
    </message>
    <message>
        <location line="-328"/>
        <source>Stray newline in string literal</source>
        <translation>Un retour à la ligne est inopiné dans la chaîne littérale</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>End of file reached at escape sequence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+92"/>
        <location line="+25"/>
        <source>Illegal unicode escape sequence</source>
        <translation>La séquence d&apos;échappement Unicode est illégale</translation>
    </message>
    <message>
        <location line="-106"/>
        <source>Illegal hexadecimal escape sequence</source>
        <translation>La séquence d&apos;échappement hexadécimale est illégale</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Octal escape sequences are not allowed</source>
        <translation>Les séquences d&apos;échappement octales ne sont pas autorisées</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Unclosed string at end of line</source>
        <translation>Le chaîne de caractères n&apos;est pas fermée en fin de ligne</translation>
    </message>
    <message>
        <location line="+112"/>
        <source>Decimal numbers can&apos;t start with &apos;0&apos;</source>
        <translation>Les nombres décimaux ne peuvent pas commencé par &apos;0&apos;</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>At least one hexadecimal digit is required after &apos;0%1&apos;</source>
        <translation>Au moins un nombre hexadécimal est requis après &quot;0%1&quot;</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Invalid regular expression flag &apos;%0&apos;</source>
        <translation>Le drapeau &quot;%0&quot; de l&apos;expression régulière est invalide</translation>
    </message>
    <message>
        <location line="+17"/>
        <location line="+22"/>
        <source>Unterminated regular expression backslash sequence</source>
        <translation>La séquence antislash de l&apos;expression régulière n&apos;est pas terminée</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Unterminated regular expression class</source>
        <translation>La classe de l&apos;expression régulière n&apos;est pas terminée</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Unterminated regular expression literal</source>
        <translation>L&apos;élément de l&apos;expression régulière n&apos;est pas terminée</translation>
    </message>
    <message>
        <location line="+204"/>
        <location line="+10"/>
        <location line="+117"/>
        <location filename="../../qtdeclarative/src/qml/parser/qqmljsparser.cpp" line="+1840"/>
        <location line="+67"/>
        <source>Syntax error</source>
        <translation>Erreur de syntaxe</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/qml/parser/qqmljsparser.cpp" line="-65"/>
        <source>Unexpected token `%1&apos;</source>
        <translation>Le jeton &quot;%1&quot; n&apos;est pas attendu</translation>
    </message>
    <message>
        <location line="+28"/>
        <location line="+24"/>
        <source>Expected token `%1&apos;</source>
        <translation>Le jeton &quot;%1&quot; est attendu</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/qml/parser/qqmljslexer.cpp" line="-93"/>
        <source>Imported file must be a script</source>
        <translation>Les fichiers importés doivent être des scripts</translation>
    </message>
    <message>
        <location line="+55"/>
        <location line="+15"/>
        <source>File import requires a qualifier</source>
        <translation>L&apos;importation de fichier nécessite un qualificatif</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Invalid import qualifier</source>
        <translation>Le qualificatif d&apos;importation est invalide</translation>
    </message>
    <message>
        <location line="-69"/>
        <location line="+10"/>
        <location line="+12"/>
        <source>Invalid module URI</source>
        <translation>L&apos;URI du module est invalide</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Module import requires a version</source>
        <translation>L&apos;importation de module nécessite un numéro de version</translation>
    </message>
    <message>
        <location line="+16"/>
        <location line="+15"/>
        <source>Module import requires a qualifier</source>
        <translation>L&apos;importation de module nécessite un qualificatif</translation>
    </message>
</context>
<context>
    <name>QQmlPartsModel</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/types/qqmldelegatemodel.cpp" line="+58"/>
        <source>The group of a DelegateModel cannot be changed within onChanged</source>
        <translation>Le groupe de DelegateModel ne peut pas être changé dans onChanged</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Delegate component must be Package type.</source>
        <translation>Un composant délégué doit être de type Package.</translation>
    </message>
</context>
<context>
    <name>QQmlPropertyCacheCreator</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/compiler/qqmltypecompiler.cpp" line="-673"/>
        <source>Fully dynamic types cannot declare new properties.</source>
        <translation>Les types complètement dynamiques ne peuvent pas déclarer de nouvelles propriétés.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Fully dynamic types cannot declare new signals.</source>
        <translation>Les types complètement dynamiques ne peuvent pas déclarer de nouveaux signaux.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Fully Dynamic types cannot declare new functions.</source>
        <translation>Les types complètement dynamiques ne peuvent pas déclarer de nouvelles fonctions.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Non-existent attached object</source>
        <translation>Objet attaché non existant</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>Cannot override FINAL property</source>
        <translation>Impossible de surcharger la propriété FINAL</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>Invalid signal parameter type: %1</source>
        <translation>Le type du paramètre du signal n&apos;est pas valide : %1</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Duplicate signal name: invalid override of property change signal or superclass signal</source>
        <translation>Le nom du signal est utilisé plusieurs fois : la surcharge du signal de modification de propriété ou du signal d&apos;une superclasse est invalide</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Duplicate method name: invalid override of property change signal or superclass signal</source>
        <translation>Le nom de la méthode est utilisé plusieurs fois : la surcharge du signal de modification de propriété ou du signal d&apos;une superclasse est invalide</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Invalid property type</source>
        <translation>Le type de propriété n&apos;est pas valide</translation>
    </message>
</context>
<context>
    <name>QQmlPropertyValidator</name>
    <message>
        <location line="+1004"/>
        <source>Property assignment expected</source>
        <translation>Affectation de propriété attendue</translation>
    </message>
    <message>
        <location line="+83"/>
        <source>Invalid use of namespace</source>
        <translation>Utilisation invalide d&apos;espace de nommage</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid attached object assignment</source>
        <translation>Affectation de l&apos;objet joint invalide</translation>
    </message>
    <message>
        <location line="-24"/>
        <source>&quot;%1.%2&quot; is not available in %3 %4.%5.</source>
        <translation>&quot;%1.%2&quot; n&apos;est pas disponible dans %3 %4.%5.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&quot;%1.%2&quot; is not available due to component versioning.</source>
        <translation>&quot;%1.%2&quot; n&apos;est pas disponible dans cette version du composant.</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+65"/>
        <location line="+27"/>
        <source>Cannot assign a value directly to a grouped property</source>
        <translation>Impossible d&apos;affecter une valeur directement à une propriété groupée</translation>
    </message>
    <message>
        <location line="-44"/>
        <source>Attached properties cannot be used here</source>
        <translation>Les propriétés jointes ne peuvent être utilisées ici</translation>
    </message>
    <message>
        <location line="+19"/>
        <location line="+39"/>
        <source>Invalid property assignment: &quot;%1&quot; is a read-only property</source>
        <translation>Propriété affectée invalide : &quot;%1&quot; est une propriété en lecture seule</translation>
    </message>
    <message>
        <location line="-32"/>
        <source>Cannot assign multiple values to a script property</source>
        <translation>Impossible d&apos;affecter plusieurs valeurs à une propriété de script</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cannot assign multiple values to a singular property</source>
        <translation>Impossible d&apos;affecter plusieurs valeurs à une propriété singulière</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Property has already been assigned a value</source>
        <translation>La propriété a déjà une valeur affecté</translation>
    </message>
    <message>
        <location line="+20"/>
        <location line="+5"/>
        <source>Invalid grouped property access</source>
        <translation>Accès à la propriété groupé invalide</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Cannot assign to non-existent default property</source>
        <translation>Impossible d&apos;attacher à une propriété par défaut inexistante</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cannot assign to non-existent property &quot;%1&quot;</source>
        <translation>Impossible d&apos;attacher à une propriété inexistante &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Cannot assign primitives to lists</source>
        <translation>Impossible d&apos;affecter des primitives aux listes</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Invalid property assignment: unknown enumeration</source>
        <translation>Affectation de propriété invalide : énumération inconnue</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Invalid property assignment: string expected</source>
        <translation>Affectation de propriété invalide : le type &quot;string&quot; est attendu</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Invalid property assignment: string or string list expected</source>
        <translation>Affectation de propriété invalide : le type &quot;string&quot; ou &quot;string list&quot; est attendu</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Invalid property assignment: byte array expected</source>
        <translation>Affectation de propriété invalide : le type &quot;byte array&quot; est attendu</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Invalid property assignment: url expected</source>
        <translation>Affectation de propriété invalide : le type &quot;url&quot; est attendu</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Invalid property assignment: unsigned int expected</source>
        <translation>Affectation de propriété invalide : le type &quot;unsigned int&quot; est attendu</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Invalid property assignment: int expected</source>
        <translation>Affectation de propriété invalide : le type &quot;int&quot; est attendu</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+7"/>
        <source>Invalid property assignment: number expected</source>
        <translation>Affectation de propriété invalide : le type &quot;number&quot; est attendu</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Invalid property assignment: color expected</source>
        <translation>Affectation de propriété invalide : le type &quot;color&quot; est attendu</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Invalid property assignment: date expected</source>
        <translation>Affectation de propriété invalide : le type &quot;date&quot; est attendu</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Invalid property assignment: time expected</source>
        <translation>Affectation de propriété invalide : le type &quot;time&quot; est attendu</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Invalid property assignment: datetime expected</source>
        <translation>Affectation de propriété invalide : datetime attendu</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+9"/>
        <location line="+36"/>
        <source>Invalid property assignment: point expected</source>
        <translation>Affectation de propriété invalide : point attendu</translation>
    </message>
    <message>
        <location line="-27"/>
        <location line="+9"/>
        <source>Invalid property assignment: size expected</source>
        <translation>Affectation de propriété invalide : le type &quot;size&quot; est attendu</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Invalid property assignment: rect expected</source>
        <translation>Affectation de propriété invalide : rect attendu</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Invalid property assignment: boolean expected</source>
        <translation>Affectation de propriété invalide : le type &quot;boolean&quot; est attendu</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Invalid property assignment: 2D vector expected</source>
        <translation type="unfinished">Affectation de propriété invalide : le type &quot;4D vector&quot; est attendu {2D?}</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Invalid property assignment: 3D vector expected</source>
        <translation>Affectation de propriété invalide : le type &quot;3D vector&quot; est attendu</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Invalid property assignment: 4D vector expected</source>
        <translation>Affectation de propriété invalide : le type &quot;4D vector&quot; est attendu</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Invalid property assignment: quaternion expected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid property assignment: regular expression expected; use /pattern/ syntax</source>
        <translation>Affectation de propriété invalide : expression régulière attendue; utiliser la syntaxe /motif/</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid property assignment: number or array of numbers expected</source>
        <translation>Affectation de propriété invalide : le type &quot;number&quot; ou &quot;array of numbers&quot; est attendu</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Invalid property assignment: int or array of ints expected</source>
        <translation>Affectation de propriété invalide : le type &quot;int&quot; ou &quot;array of ints&quot; est attendu</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Invalid property assignment: bool or array of bools expected</source>
        <translation>Affectation de propriété invalide : le type &quot;boolean&quot; ou &quot;array of bools&quot; est attendu</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid property assignment: url or array of urls expected</source>
        <translation>Affectation de propriété invalide : le type &quot;url&quot; ou &quot;array of urls&quot; est attendu</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid property assignment: string or array of strings expected</source>
        <translation>Affectation de propriété invalide : le type &quot;string&quot; ou &quot;array of strings&quot; est attendu</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Invalid property assignment: unsupported type &quot;%1&quot;</source>
        <translation>Affectation de propriété invalide : type &quot;%1&quot; non supporté</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>&quot;%1&quot; cannot operate on &quot;%2&quot;</source>
        <translation>&quot;%1&quot; ne fonctionne pas avec &quot;%2&quot;</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Cannot assign object to list property &quot;%1&quot;</source>
        <translation>Impossible d&apos;affecter l&apos;objet à la propriété de liste &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Unexpected object assignment</source>
        <translation>Affectation d&apos;objet inattendu</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid property assignment: script expected</source>
        <translation>Affectation de propriété invalide : script attendu</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Cannot assign object to property</source>
        <translation>Impossible d&apos;affecter l&apos;objet à la propriété</translation>
    </message>
</context>
<context>
    <name>QQmlRewrite</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlpropertycache.cpp" line="+1040"/>
        <source>Signal uses unnamed parameter followed by named parameter.</source>
        <translation>Le signal utilise un paramètre non nommé suivi par un paramètre nommé.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Signal parameter &quot;%1&quot; hides global variable.</source>
        <translation>Le paramètre &quot;%1&quot; du signal masque une variable globale.</translation>
    </message>
</context>
<context>
    <name>QQmlTypeCompiler</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/compiler/qqmltypecompiler.cpp" line="-2367"/>
        <source>Composite Singleton Type %1 is not creatable.</source>
        <translation>Il n&apos;est pas possible de créer le type singleton composite %1.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Element is not creatable.</source>
        <translation>Impossible de créer l&apos;élément.</translation>
    </message>
</context>
<context>
    <name>QQmlTypeLoader</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlimport.cpp" line="-518"/>
        <source>Cannot update qmldir content for &apos;%1&apos;</source>
        <translation>Impossible de mettre à jour le contenu du fichier qmldir pour &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmltypeloader.cpp" line="+1440"/>
        <source>No matching type found, pragma Singleton files cannot be used by QQmlComponent.</source>
        <translation>Aucun paramètre correspondant n&apos;a été trouvé, les fichiers contenant le pragma Singleton ne peuvent pas être utilisé avec QQmlDocument.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>pragma Singleton used with a non composite singleton type %1</source>
        <translation>le pragma Singleton est utilisée avec un type singleton non composite %1</translation>
    </message>
    <message>
        <location line="+626"/>
        <location line="+612"/>
        <source>Script %1 unavailable</source>
        <translation>Le script %1 n&apos;est pas disponible</translation>
    </message>
    <message>
        <location line="-593"/>
        <location line="+18"/>
        <source>Type %1 unavailable</source>
        <translation>Le type %1 n&apos;est pas disponible</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>qmldir defines type as singleton, but no pragma Singleton found in type %1.</source>
        <translation>le fichier qmldir défini un type comme singleton, mais aucune directive pragma Singleton n&apos;a été trouvée dans le type %1.</translation>
    </message>
    <message>
        <location line="+139"/>
        <source>module &quot;%1&quot; is not installed</source>
        <translation>le module &quot;%1&quot; n&apos;est pas installé</translation>
    </message>
    <message>
        <location line="+127"/>
        <location line="+62"/>
        <source>Namespace %1 cannot be used as a type</source>
        <translation>L&apos;espace de noms %1 ne peut pas être utilisé comme un type</translation>
    </message>
    <message>
        <location line="-55"/>
        <location line="+62"/>
        <source>Unreported error adding script import to import database</source>
        <translation>Une erreur non signalée ajoutant un script pour importer la base de données est apparue</translation>
    </message>
    <message>
        <location line="-59"/>
        <location line="+62"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
</context>
<context>
    <name>QQuickAbstractAnimation</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/util/qquickanimation.cpp" line="+185"/>
        <source>Cannot animate non-existent property &quot;%1&quot;</source>
        <translation>Impossible d&apos;animer la propriété inexistante &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot animate read-only property &quot;%1&quot;</source>
        <translation>Impossible d&apos;animer la propriété en lecture seule &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/quick/util/qquickutilmodule.cpp" line="+2"/>
        <source>Animation is an abstract class</source>
        <translation>Animation est une classe abstraite</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Animator is an abstract class</source>
        <translation>Animator est une classe abstraite</translation>
    </message>
</context>
<context>
    <name>QQuickAccessibleAttached</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemsmodule.cpp" line="+237"/>
        <source>Accessible is only available via attached properties</source>
        <translation>Accessible est disponible uniquement via les propriétés attachées</translation>
    </message>
</context>
<context>
    <name>QQuickAnchorAnimation</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemanimation.cpp" line="+469"/>
        <source>Cannot set a duration of &lt; 0</source>
        <translation>Impossible de définir une durée négative</translation>
    </message>
</context>
<context>
    <name>QQuickAnchors</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickanchors.cpp" line="+212"/>
        <source>Possible anchor loop detected on fill.</source>
        <translation>Boucle potentielle dans les ancres détectée pour la propriété fill.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Possible anchor loop detected on centerIn.</source>
        <translation>Boucle potentielle dans les ancres détectée pour la propriété centerIn.</translation>
    </message>
    <message>
        <location line="+286"/>
        <location line="+36"/>
        <location line="+737"/>
        <location line="+37"/>
        <source>Cannot anchor to an item that isn&apos;t a parent or sibling.</source>
        <translation>Impossible d&apos;ancrer à un élément qui n&apos;est pas un parent ou partage le même parent.</translation>
    </message>
    <message>
        <location line="-664"/>
        <source>Possible anchor loop detected on vertical anchor.</source>
        <translation>Boucle potentielle dans les ancres détectée pour une ancre verticale.</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>Possible anchor loop detected on horizontal anchor.</source>
        <translation>Boucle potentielle dans les ancres détectée pour une ancre horizontale.</translation>
    </message>
    <message>
        <location line="+514"/>
        <source>Cannot specify left, right, and horizontalCenter anchors at the same time.</source>
        <translation>Impossible de spécifier à la fois des ancres à gauche, à droite et horizontalCenter.</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+37"/>
        <source>Cannot anchor to a null item.</source>
        <translation>Impossible d&apos;ancrer à un élément nul.</translation>
    </message>
    <message>
        <location line="-34"/>
        <source>Cannot anchor a horizontal edge to a vertical edge.</source>
        <translation>Impossible d&apos;ancrer un bord horizontal à un bord vertical.</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+37"/>
        <source>Cannot anchor item to self.</source>
        <translation>Impossible d&apos;ancrer l&apos;élément à lui même.</translation>
    </message>
    <message>
        <location line="-25"/>
        <source>Cannot specify top, bottom, and verticalCenter anchors at the same time.</source>
        <translation>Impossible de spécifier à la fois des ancres en haut, en bas et verticalCenter.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Baseline anchor cannot be used in conjunction with top, bottom, or verticalCenter anchors.</source>
        <translation>Une ancre Baseline ne peut pas être utilisée avec des ancres en haut, en bas ou verticalCenter.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot anchor a vertical edge to a horizontal edge.</source>
        <translation>Impossible d&apos;ancrer un bord vertical à un bord horizontal.</translation>
    </message>
</context>
<context>
    <name>QQuickAnimatedImage</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemsmodule.cpp" line="-108"/>
        <source>Qt was built without support for QMovie</source>
        <translation>Qt a été compilé sans support de QMovie</translation>
    </message>
</context>
<context>
    <name>QQuickApplication</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/qtquick2.cpp" line="+182"/>
        <source>Application is an abstract class</source>
        <translation>Application est une classe abstraite</translation>
    </message>
</context>
<context>
    <name>QQuickBehavior</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/util/qquickbehavior.cpp" line="+126"/>
        <source>Cannot change the animation assigned to a Behavior.</source>
        <translation>Impossible de changer l&apos;animation affectée à un élément Behavior.</translation>
    </message>
</context>
<context>
    <name>QQuickDragAttached</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemsmodule.cpp" line="+100"/>
        <source>Drag is only available via attached properties</source>
        <translation>Drag est disponible uniquement via les propriétés attachées</translation>
    </message>
</context>
<context>
    <name>QQuickEnterKeyAttached</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitem.cpp" line="+1690"/>
        <source>EnterKey attached property only works with Items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemsmodule.cpp" line="+46"/>
        <source>EnterKey is only available via attached properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQuickFlipable</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickflipable.cpp" line="+150"/>
        <source>front is a write-once property</source>
        <translation>front est une propriété à écriture unique</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>back is a write-once property</source>
        <translation>back est une propriété à écriture unique</translation>
    </message>
</context>
<context>
    <name>QQuickItemView</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemsmodule.cpp" line="-33"/>
        <location line="+1"/>
        <source>ItemView is an abstract base class</source>
        <translation>ItemView est une classe abstraite</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemview.cpp" line="+2312"/>
        <source>Delegate must be of Item type</source>
        <translation>Un délégué doit être de type Item</translation>
    </message>
</context>
<context>
    <name>QQuickKeyNavigationAttached</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemsmodule.cpp" line="-48"/>
        <source>KeyNavigation is only available via attached properties</source>
        <translation>KeyNavigation est disponible uniquement via les propriétés attachées</translation>
    </message>
</context>
<context>
    <name>QQuickKeysAttached</name>
    <message>
        <location line="+1"/>
        <source>Keys is only available via attached properties</source>
        <translation>Keys est disponible uniquement via les propriétés attachées</translation>
    </message>
</context>
<context>
    <name>QQuickLayoutMirroringAttached</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitem.cpp" line="-150"/>
        <source>LayoutDirection attached property only works with Items</source>
        <translation>La propriété attachée LayoutDirection est fonctionne uniquement avec des éléments</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemsmodule.cpp" line="+1"/>
        <source>LayoutMirroring is only available via attached properties</source>
        <translation>LayoutMirroring est disponible uniquement via les propriétés attachées</translation>
    </message>
</context>
<context>
    <name>QQuickLoader</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickloader.cpp" line="+949"/>
        <source>setSource: value is not an object</source>
        <translation>setSource : une valeur n&apos;est pas un objet</translation>
    </message>
</context>
<context>
    <name>QQuickOpenGLInfo</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemsmodule.cpp" line="+62"/>
        <source>OpenGLInfo is only available via attached properties</source>
        <translation>OpenGLInfo n&apos;est disponible que par des propriétés attachées</translation>
    </message>
</context>
<context>
    <name>QQuickPaintedItem</name>
    <message>
        <location line="-50"/>
        <source>Cannot create instance of abstract class PaintedItem</source>
        <translation>Impossible de créer une instance de la classe abstraite PaintedItem</translation>
    </message>
</context>
<context>
    <name>QQuickParentAnimation</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemanimation.cpp" line="-170"/>
        <source>Unable to preserve appearance under complex transform</source>
        <translation>Impossible de conserver l&apos;aspect lors d&apos;une transformation complexe</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+7"/>
        <source>Unable to preserve appearance under non-uniform scale</source>
        <translation>Impossible de conserver l&apos;aspect lors d&apos;une mise à l&apos;échelle non uniforme</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unable to preserve appearance under scale of 0</source>
        <translation>Impossible de conserver l&apos;aspect lors d&apos;une mise à l&apos;échelle égale à 0</translation>
    </message>
</context>
<context>
    <name>QQuickParentChange</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickstateoperations.cpp" line="+75"/>
        <source>Unable to preserve appearance under complex transform</source>
        <translation>Impossible de conserver l&apos;aspect lors d&apos;une transformation complexe</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+7"/>
        <source>Unable to preserve appearance under non-uniform scale</source>
        <translation>Impossible de conserver l&apos;aspect lors d&apos;une mise à l&apos;échelle non uniforme</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unable to preserve appearance under scale of 0</source>
        <translation>Impossible de conserver l&apos;aspect lors d&apos;une mise à l&apos;échelle égale à 0</translation>
    </message>
</context>
<context>
    <name>QQuickPathAnimation</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemanimation.cpp" line="+282"/>
        <source>Cannot set a duration of &lt; 0</source>
        <translation>Impossible de définir une durée négative</translation>
    </message>
</context>
<context>
    <name>QQuickPathView</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickpathview.cpp" line="+139"/>
        <source>Delegate must be of Item type</source>
        <translation>Un délégué doit être de type Item</translation>
    </message>
</context>
<context>
    <name>QQuickPauseAnimation</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/util/qquickanimation.cpp" line="+514"/>
        <source>Cannot set a duration of &lt; 0</source>
        <translation>Impossible de définir une durée négative</translation>
    </message>
</context>
<context>
    <name>QQuickPixmap</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/util/qquickpixmapcache.cpp" line="+411"/>
        <source>Error decoding: %1: %2</source>
        <translation>Erreur de décodage : %1 : %2</translation>
    </message>
    <message>
        <location line="+238"/>
        <location line="+484"/>
        <source>Invalid image provider: %1</source>
        <translation>Le fournisseur d&apos;images est invalide : %1</translation>
    </message>
    <message>
        <location line="-469"/>
        <location line="+16"/>
        <location line="+488"/>
        <source>Failed to get image from provider: %1</source>
        <translation>Impossible d&apos;obtenir des image par le fournisseur : %1</translation>
    </message>
    <message>
        <location line="-472"/>
        <source>Failed to get texture from provider: %1</source>
        <translation>Impossible d&apos;obtenir des textures du fournisseur : %1</translation>
    </message>
    <message>
        <location line="+35"/>
        <location line="+456"/>
        <source>Cannot open: %1</source>
        <translation>Impossible d&apos;ouvrir : %1</translation>
    </message>
    <message>
        <source>Invalid image data: %1</source>
        <translation type="vanished">Les données de l&apos;image sont invalides : %1</translation>
    </message>
</context>
<context>
    <name>QQuickPropertyAnimation</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/util/qquickanimation.cpp" line="+1368"/>
        <source>Cannot set a duration of &lt; 0</source>
        <translation>Impossible de définir une durée négative</translation>
    </message>
</context>
<context>
    <name>QQuickPropertyChanges</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/util/qquickpropertychanges.cpp" line="+236"/>
        <source>PropertyChanges does not support creating state-specific objects.</source>
        <translation>PropertyChanges n&apos;est pas compatible avec la création d&apos;objets spécifiques à un état.</translation>
    </message>
    <message>
        <location line="+160"/>
        <source>Cannot assign to non-existent property &quot;%1&quot;</source>
        <translation>Impossible d&apos;attacher à une propriété inexistante &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot assign to read-only property &quot;%1&quot;</source>
        <translation>Impossible d&apos;assigner à la propriété en lecture seule &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QQuickRepeater</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickrepeater.cpp" line="+420"/>
        <source>Delegate must be of Item type</source>
        <translation>Un délégué doit être de type Item</translation>
    </message>
</context>
<context>
    <name>QQuickShaderEffectMesh</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemsmodule.cpp" line="-3"/>
        <source>Cannot create instance of abstract class ShaderEffectMesh.</source>
        <translation>Impossible de créer une instance de la classe abstraite ShaderEffectMesh.</translation>
    </message>
</context>
<context>
    <name>QQuickTextUtil</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquicktextutil.cpp" line="+58"/>
        <source>%1 does not support loading non-visual cursor delegates.</source>
        <translation>%1 ne supporte pas le chargement de délégués non-visuels pour le curseur.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Could not load cursor delegate</source>
        <translation>Impossible de charger le délégué pour le curseur</translation>
    </message>
</context>
<context>
    <name>QQuickViewTransitionAttached</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemsmodule.cpp" line="-8"/>
        <source>ViewTransition is only available via attached properties</source>
        <translation>ViewTransition est disponible uniquement via les propriétés attachées</translation>
    </message>
</context>
<context>
    <name>QQuickWindow</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickwindow.cpp" line="+2638"/>
        <source>Failed to create %1 context for format %2.
This is most likely caused by not having the necessary graphics drivers installed.

Install a driver providing OpenGL 2.0 or higher, or, if this is not possible, make sure the ANGLE Open GL ES 2.0 emulation libraries (%3, %4 and d3dcompiler_*.dll) are available in the application executable&apos;s directory or in a location listed in PATH.</source>
        <extracomment>%1 Context type (Open GL, EGL), %2 format, ANGLE %3, %4 library names</extracomment>
        <translation>Erreur à la création du contexte %1 pour le format %2.
Cela peut être dû à un défaut d&apos;installation de pilotes graphiques.

Veuillez installer un pilote supportant OpenGL 2.0 ou supérieur. Si cela n&apos;est pas possible, veuillez vous assurer que les librairies d&apos;émulation ANGLE OpenGL ES 2.0 (%3, %4 et d3dcompiler_*.dll) soient accessibles dans le répertoire d&apos;exécution ou dans un emplacement listé dans la variable PATH.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Failed to create %1 context for format %2</source>
        <extracomment>%1 Context type (Open GL, EGL), %2 format specification</extracomment>
        <translation>Erreur à la création du contexte %1 pour le format %2</translation>
    </message>
</context>
<context>
    <name>QQuickWindowQmlImpl</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickwindowmodule.cpp" line="+151"/>
        <source>Conflicting properties &apos;visible&apos; and &apos;visibility&apos; for Window &apos;%1&apos;</source>
        <translation>Propriétés &apos;visible&apos; et &apos;visibility&apos; en conflit pour la fenêtre &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Conflicting properties &apos;visible&apos; and &apos;visibility&apos;</source>
        <translation>Conflit de propriété entre &apos;visible&apos; et &apos;visibilité&apos;</translation>
    </message>
</context>
<context>
    <name>QQuickXmlListModel</name>
    <message>
        <location filename="../../qtdeclarative/src/imports/xmllistmodel/qqmlxmllistmodel.cpp" line="+593"/>
        <source>&quot;%1&quot; duplicates a previous role name and will be disabled.</source>
        <translation>&quot;%1&quot; est un doublon d&apos;un nom de role existant et sera désactivé.</translation>
    </message>
    <message>
        <location line="+540"/>
        <location line="+4"/>
        <source>invalid query: &quot;%1&quot;</source>
        <translation>La requête est invalide : &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QQuickXmlListModelRole</name>
    <message>
        <location filename="../../qtdeclarative/src/imports/xmllistmodel/qqmlxmllistmodel_p.h" line="+174"/>
        <source>An XmlRole query must not start with &apos;/&apos;</source>
        <translation>Une requête XmlRole ne doit pas commencer par &apos;/&apos;</translation>
    </message>
</context>
<context>
    <name>QQuickXmlRoleList</name>
    <message>
        <location filename="../../qtdeclarative/src/imports/xmllistmodel/qqmlxmllistmodel.cpp" line="-293"/>
        <source>An XmlListModel query must start with &apos;/&apos; or &quot;//&quot;</source>
        <translation>Une requête de XmlListModel doit commencer par &apos;/&apos; ou &quot;//&quot;</translation>
    </message>
</context>
<context>
    <name>SignalHandlerConverter</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/compiler/qqmltypecompiler.cpp" line="+859"/>
        <source>Non-existent attached object</source>
        <translation>Objet joint inexistant</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Signal uses unnamed parameter followed by named parameter.</source>
        <translation>Le signal utilise le paramètre sans nom suivi par le paramètre nommé.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Signal parameter &quot;%1&quot; hides global variable.</source>
        <translation>Le signal du paramètre &quot;%1&quot; cache la variable globale.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>&quot;%1.%2&quot; is not available in %3 %4.%5.</source>
        <translation>&quot;%1.%2&quot; n&apos;est pas disponible dans %3 %4.%5.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&quot;%1.%2&quot; is not available due to component versioning.</source>
        <translation>&quot;%1.%2&quot; n&apos;est pas disponible dans cette version du composant.</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Cannot assign a value to a signal (expecting a script to be run)</source>
        <translation>Impossible d&apos;affecter une valeur à un signal (attend un script pour être exécuté)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Incorrectly specified signal assignment</source>
        <translation>Affectation du signal spécifié incorrecte</translation>
    </message>
</context>
<context>
    <name>SignalTransition</name>
    <message>
        <location filename="../../qtdeclarative/src/imports/statemachine/signaltransition.cpp" line="+123"/>
        <source>Specified signal does not exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+65"/>
        <source>Cannot assign to non-existent property &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>SignalTransition: script expected</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qmlRegisterType</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlmetatype.cpp" line="+1221"/>
        <source>Invalid QML %1 name &quot;%2&quot;</source>
        <translation>L&apos;élément QML %1 nommé &quot;%2&quot; n&apos;est pas valide</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot install %1 &apos;%2&apos; into unregistered namespace &apos;%3&apos;</source>
        <translation>Impossible d&apos;installer %1 &quot;%2&quot; dans l&apos;espace de noms non enregistré &quot;%3&quot;</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot install %1 &apos;%2&apos; into protected namespace &apos;%3&apos;</source>
        <translation>Impossible d&apos;installer %1 &quot;%2&quot; dans l&apos;espace de noms protégé &quot;%3&quot;</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Cannot install %1 &apos;%2&apos; into protected module &apos;%3&apos; version &apos;%4&apos;</source>
        <translation>Impossible d&apos;installer %1 &quot;%2&quot; dans le module protégé &quot;%3&quot; en version &quot;%4&quot;</translation>
    </message>
</context>
</TS>
