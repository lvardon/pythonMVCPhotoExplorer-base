#!/usr/bin/env python
#-*- coding: utf-8 -*-

"""

"""

import sys
import os
from PyQt5 import QtCore, QtGui, QtWidgets 
from PyQt5.QtWidgets import QApplication, QMainWindow, QDialog, QLabel, QPushButton, QVBoxLayout, QMessageBox
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QDir, QT_TRANSLATE_NOOP, QTranslator, QLocale, QSize, QPoint
from PyQt5 import uic

import sqlite3 as lite

from applib import log

class sqlite(object):

    def __init__(self, parent, sqliteFile ):        
            
        self.parent = parent
        self.sqliteFile = sqliteFile
        self.data = None       
        self.connex = None
        
        log.Log.debug( log.INFO , 'Sqlite Controler init : ' + sqliteFile)         
         

    """
    ------------------------------------------------------------------
    Sqlite version
    """ 
    def getversion( self ) :
        
        sqliteVer = None
        try:
            self.connex = lite.connect(self.sqliteFile)
            cur = self.connex.cursor()    
            cur.execute('SELECT SQLITE_VERSION()')
            sqliteVer = cur.fetchone()

        except lite.Error as e:            
            log.Log.debug( log.ERROR , "Error %s:" % e.args[0])                        
            sys.exit(1)
            
        finally:    
            if self.connex:
                self.connex.close() 
            return sqliteVer
                        
        return None
    
    """
    ------------------------------------------------------------------
    Select statement
    """    
    def select( self , sqtSt ) :
        
        self.connex = lite.connect(self.sqliteFile)        
        self.connex.row_factory = lite.Row
         
        cur = self.connex.cursor()    
        cur.execute(sqtSt)
        
        sqlData = []
        
        for row in cur:            
        #    print(' niveau 0 : {0} : {1} : {2} : {3} '.format(row['Id'], row['Name'].encode('utf-8'), row['Creation'], row['Vfloat']))
            sqlData.append( row )

        if self.connex:
            self.connex.close()                 
             
        return sqlData

    """
    ------------------------------------------------------------------
    Execute statement
    """
    def execute( self , sqlInsertSt ) : 
                
        self.connex = lite.connect(self.sqliteFile)                 
        cur = self.connex.cursor() 
           
        try:
            cur.execute(sqlInsertSt)  
            self.connex.commit()                                     
            return True        
            
        except lite.Error as e:            
            log.Log.debug( log.ERROR , "Error execute %s:" % e.args[0])                        
            return False

        finally:    
            if self.connex:
                self.connex.close()     

    """
    ------------------------------------------------------------------
    Insert statement
    """
    def insert( self , sqlInsertSt , sqlValues ) : 
               
        self.connex = lite.connect(self.sqliteFile)                 
        cur = self.connex.cursor() 
           
        try:
            cur.execute(sqlInsertSt, sqlValues)  
            self.connex.commit()           
            return True
            
        except lite.Error as e:            
            log.Log.debug( log.ERROR , "Error insert %s:" % e.args[0])                        
            return False

        finally:    
            if self.connex:
                self.connex.close()     
    """
    ------------------------------------------------------------------
    Delete statement
    """
    def delete( self , sqlInsertSt , sqlValues ) : 

        
        self.connex = lite.connect(self.sqliteFile)                  
        cur = self.connex.cursor()  
          
        try:
            cur.execute(sqlInsertSt, sqlValues)     
            self.connex.commit()           
            return True
            
        except lite.Error as e:            
            log.Log.debug( log.ERROR , "Error delete %s:" % e.args[0])                        
            return False

        finally:    
            if self.connex:
                self.connex.close()     
                

    """
    ------------------------------------------------------------------
    Commit statement
    """ 
    def commit( self ) :


        try:
            self.connex = lite.connect(self.sqliteFile)  
            self.connex.commit()
            return True
            
        except lite.Error as e:            
            log.Log.debug( log.ERROR , "Error commit %s:" % e.args[0])                        
            return False

        finally:    
            if self.connex:
                self.connex.close()     


    """
    ------------------------------------------------------------------
    Load csv datas into table (append datat or create table)
    """ 
    def create_table_from_csv(self , tableName, fullCsvPathName, fieldsList  , csvFieldsSep, dropTable, deleteFromTable,  loadFirstLine ) :

        import csv
        import time

        # Execution start time
        start = time.time()

        # Create database cx
        self.connex = lite.connect(self.sqliteFile)
        self.connex.text_factory = str
        cur = self.connex.cursor()

        tableExists = False
        cur = self.connex.cursor()    
        cur.execute("SELECT name FROM sqlite_master where name = '"+tableName+"'")
        for row in cur:                
            log.Log.debug( log.INFO , 'La table : ' + tableName + "  existe.")         
            tableExists = True

        if deleteFromTable == True :
            try :
                log.Log.debug( log.INFO , "DELETE FROM " + tableName)                         
                cur.execute( "DELETE FROM " + tableName ) # delete from Table
            except :
                pass
        
        if dropTable == True :
            try :
                log.Log.debug( log.INFO , "DROP TABLE IF EXISTS " + tableName)                                         
                cur.execute( "DROP TABLE IF EXISTS " + tableName ) # drop Table
            except :
                pass
 
        # Create table in database
        if tableExists == False or dropTable == True :            
            sqlSt = 'create table '+tableName+' '
            sqlSt = sqlSt + '('
            idxF = 0
            while idxF < len (fieldsList) :
                sqlSt = sqlSt + ' '+fieldsList[idxF]+' text'
                idxF =  idxF +1
                if idxF < len(fieldsList) :
                    sqlSt = sqlSt + ','            
            sqlSt = sqlSt + ')'
            log.Log.debug( log.INFO , "Create table : " + tableName)                                         
            cur.execute(sqlSt) # create Table

        # Insert request
        sqlSt = 'insert into '+tableName+' '
        sqlStValues = ' values ('
        sqlSt = sqlSt + '('
        idxF = 0
        while idxF < len (fieldsList) :
            sqlSt = sqlSt + ' '+fieldsList[idxF]+' '
            sqlStValues = sqlStValues + '?'                
            idxF =  idxF +1
            if idxF < len(fieldsList) :
                sqlSt = sqlSt + ', '            
                sqlStValues = sqlStValues + ', '                
        sqlStValues = sqlStValues + ')'                
        sqlSt = sqlSt + ')'
        sqlSt = sqlSt + sqlStValues
        
        # Read file and insert values into table 
        log.Log.debug( log.INFO , "Chargement du csv : " + fullCsvPathName + " / fieldDelimiter = " + csvFieldsSep + " / Première ligne = " + str(loadFirstLine) )  
        with open(fullCsvPathName, 'r') as f:
            reader = csv.reader(f, delimiter=csvFieldsSep)
            idxRow = 1
            for row in reader:
                if idxRow > 1 or loadFirstLine == True :                    
                    cur.execute(sqlSt.format(*fieldsList), row) # insert into Table
                idxRow = idxRow + 1
                
        # Commit
        self.connex.commit()

        # Execution end time
        end = time.time()

        # Start of analysis
        for row in cur.execute('select count(*) from ' + tableName):
            totalLines = row[0]
        
        log.Log.debug( log.INFO , 'Table : ' + tableName + "  : " + str(totalLines) +" lines, "+ "%.2f" % (end - start) +" sec.")         
        #print('Table : ' + tableName + "  : " + str(totalLines) +" lines, "+ "%.2f" % (end - start) +" sec.")







    """
    ------------------------------------------------------------------
    TESTS Select statements --- TO REMOVE --
    """ 

    def testDrop( self ) : 
        
        self.connex = lite.connect(self.sqliteFile)
         
        with self.connex:
         
            cur = self.connex.cursor()    
            try:
                cur.execute("DROP TABLE Users")
                cur.execute("DROP TABLE combovalue")
            except lite.Error as e:
                pass

    
    def testInsert( self ) : 
        
        self.connex = lite.connect(self.sqliteFile)
         
        with self.connex:
         
            cur = self.connex.cursor()    
            try:
                cur.execute("CREATE TABLE Users(Id INT, Name TEXT, Creation DATETIME, Vfloat FLOAT, vdecimal DECIMAL(10,5) , anotherstr TEXT ) ")
                cur.execute("INSERT INTO Users VALUES(1,'Michelle', '2012-9-25 23:39:11', 50 , 13.12345 ,'001')")
                cur.execute("INSERT INTO Users VALUES(2,'Sonya', '2007-02-01 10:00:00', 101.123, -23.120 ,'002')")
                cur.execute("INSERT INTO Users VALUES(3,'Greg' , CURRENT_TIMESTAMP, 0.99, 3.123 ,'003' )")                
                cur.execute("INSERT INTO Users VALUES(4,'avec é à' , CURRENT_TIMESTAMP, -7.23, -4.5,'004' )")                

                cur.execute("CREATE TABLE combovalue( combokey TEXT, combollib TEXT ) ")
                cur.execute("INSERT INTO combovalue VALUES( '001', 'Libellé de 001') ")
                cur.execute("INSERT INTO combovalue VALUES( '002', 'Libellé de 002') ")      
                cur.execute("INSERT INTO combovalue VALUES( '003', 'Libellé de 003') ")      
                cur.execute("INSERT INTO combovalue VALUES( '004', 'Libellé de 004') ")      
                cur.execute("INSERT INTO combovalue VALUES( '005', 'Libellé de 005') ")      
                cur.execute("CREATE UNIQUE INDEX combovalue_key on combovalue (combokey) ")
                
            except lite.Error as e:
                pass


    def testSelectByIndex( self ) : 
        
        self.connex = lite.connect(self.sqliteFile)
         
        with self.connex:
         
            cur = self.connex.cursor()    
            cur.execute("SELECT * FROM Users")
         
            rows = cur.fetchall()
         
            #for row in rows:
            #    print row
        
            return rows


    def testSelect( self ) : 
        
        self.connex = lite.connect(self.sqliteFile)
        
        self.connex.row_factory = lite.Row

        with self.connex:
         
            cur = self.connex.cursor()    
            cur.execute("SELECT * FROM Users")
            
            #for row in cursor:
            # row['name'] returns the name column in the query, row['email'] returns email column.
                #print('{0} : {1}, {2}'.format(row['name'], row['email'], row['phone']))

            for row in cur:            
                print('{0} : {1}'.format(row['Id'], row['Name']))

            #if self.connex:
            #    self.connex.close()   

            return cur

