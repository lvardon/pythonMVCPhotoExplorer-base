#!/usr/bin/env python
#-*- coding: utf-8 -*-


"""
Usage :

from applib import setting
from applib.setting import Setting

1) Object :
setting.Setting = setting.setting(self, "config.ini")
self.confirmOnQuit = setting.Setting.getValue( "app/confirmOnQuit" , "0" )

2) Aes crypt case :
Encrypt : setting.Setting.setValueAes("mykey" , "zorro")
Decrypt : print (setting.Setting.getValueAes("mykey") )

3) One line case :
modeGui = setting.setting(app, "config.ini").getValue( "app/modeGui" , "1" )



"""
from PyQt5.QtCore import QSettings, QFile
from applib import aes

Setting= None

class setting() :

    def __init__(self, parent, fileSetting):
        self.parent = parent
        self.fileSetting = fileSetting
        self.readSettings()

        import os.path
        if not os.path.isfile(self.fileSetting)  :
            self.fileExists = False
        else :
            self.fileExists = True

    def storeSettings(self):
        self.settingsWrite = QSettings(self.fileSetting , QSettings.IniFormat)
        self.settingsWrite.setFallbacksEnabled(False)
        self.settingsWrite.setValue("debug/configDebug",  self.ini.value("debug/configDebug", 0))

    def readSettings(self):
        self.ini = QSettings(self.fileSetting , QSettings.IniFormat)
        self.ini.setFallbacksEnabled(False)

    def getValue( self, paramName , defValue=None ) :
        return self.ini.value(paramName, defValue)

    def getListValue( self, paramName ) :

        listValues = self.ini.value(paramName, [], str)

        if type(listValues).__name__ == "str" or type(listValues).__name__ == "unicode" :
            retList = []
            retList.append( listValues )
            return retList
        else :
            return listValues

    def setDictValue( self, paramName) :
        with open(myfile, 'w') as f:
            for key, value in a.items():
                f.write('%s:%s\n' % (key, value))

    def getDictValue( self, paramName ) :
        data = []
        raw_data = self.getValue( paramName )
        rawDataList=[]
        #if isinstance(raw_data, unicode) or isinstance(raw_data, str) : Python 3 renamed the unicode type to str, the old str type has been replaced by bytes.
        if isinstance(raw_data, str) :
            rawDataList.append(raw_data)
        else  :
            rawDataList = raw_data

        for item in rawDataList:
            entries = item.split(',')

            for entryItem in entries :
                #print ("   entryItem=" + entryItem)
                label, icon, tooltip, command = entryItem.split(':')
                newItem = { "entry" : label, "index":0, "value":label}
                data.append( newItem )
                newItem = { "entry" : label, "index":1, "value":icon}
                data.append( newItem )
                newItem = { "entry" : label, "index":2, "value":tooltip}
                data.append( newItem )
                newItem = { "entry" : label, "index":3, "value":command}
                data.append( newItem )

        return data

    def getKeyValues( self, paramName ) :
        data = []
        raw_data = self.getValue( paramName )
        rawDataList=[]
        if isinstance(raw_data, str)  :
            rawDataList.append(raw_data)
        else  :
            rawDataList = raw_data

        if rawDataList != None :
            for item in rawDataList:
                entries = item.split(',')
                for entryItem in entries :
                    #print ("entryItem=" + entryItem )
                    if len(entryItem) > 0 :
                        key, value = entryItem.split(':')
                        newItem = { "key":key, "value":value}
                        data.append( newItem )

        return data

    def setKeyValues ( self, paramName, paramListValues) :
        paramValues = ""
        sepElem=""
        for paramValueElem in paramListValues :
            #print ( "   " + sepElem + paramValueElem["key"] + ":" + paramValueElem["value"]  )
            paramValues = paramValues + sepElem + paramValueElem["key"] + ":" + paramValueElem["value"]
            #print ("---" + paramValues)
            sepElem = ","

        self.setValue( paramName , paramValues )

    def setValue( self, paramName , value ) :
        self.ini.setValue(paramName , value);

    def setValueAes (self, paramName , value ) :
        fp = QFile("icon/app.png")
        if fp.open( QFile.ReadOnly):
            key = fp.readData(16)
        cvObj = aes.aes( key )
        cv = cvObj.encrypt(value)
        self.setValue(paramName , cv)

    def getValueAes( self, paramName , defValue=None ) :
        fp = QFile("icon/app.png")
        if fp.open( QFile.ReadOnly):
            key = fp.readData(16)
        cvObjDecode = aes.aes( key )
        cv = self.getValue( paramName , defValue )
        if cv[:6] == "crypt:" :
            self.setValueAes( paramName , cv[6:] )
            return self.getValueAes( paramName , defValue )
        else :
            return cvObjDecode.decrypt(cv).decode("utf-8")

    def fileName(self) :
        return self.ini.fileName()




