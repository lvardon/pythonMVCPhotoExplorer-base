#!/usr/bin/env python
#-*- coding: utf-8 -*-

import os
import sys
import inspect
from applib import log


PLUGIN_ACTIVE = 0
PLUGIN_NAME = 1
PLUGIN_DESCRIPTION = 2
PLUGIN_VERSION = 3
PLUGIN_MENULIB = 4
PLUGIN_STATUTBARTOOLTIP = 5
PLUGIN_MENUTOOLTIP = 6
PLUGIN_MENU_UPPER = 7
PLUGIN_MENU_TOOLBAR = 8
PLUGIN_MENU_ICONVIEW = 9
PLUGIN_MENU_DETAILVIEW = 10
PLUGIN_MULTISELECT = 11
PLUGIN_SHORTCUT = 12
PLUGIN_ICONPATH = 13
PLUGIN_ORDER = 14

class module() :

    def __init__(self, parent, className , name, active, version, description, menuLib, menuStatutBarToolTip, menuTooltip, shortCut, iconPath, order):

        self.parent = parent

        self.className = className
        self.name = name
        self.version = version
        self.description = description
        self.menuLib = menuLib
        self.menuStatutBarToolTip = menuStatutBarToolTip
        self.menuTooltip = menuTooltip
        self.shortCut = shortCut
        self.iconPath = iconPath
        self.active = active
        self.order = order


class modules() :

    def __init__(self, parent):
        self.parent = parent
        self.moduleList = []

    def listModules( self) :

        print ("Dump modules loaded:")
        for moduleListElem in  self.moduleList :
            print ("moduleListElem.className = " + moduleListElem.className)
            print ("moduleListElem.name = " + moduleListElem.name)
            print ("moduleListElem.isActive = " + str(moduleListElem.active) )
            print ("moduleListElem.version = "+moduleListElem.version)
            print ("moduleListElem.menuLib = " + moduleListElem.menuLib)
            print ("moduleListElem.menuTooltip = " + moduleListElem.menuTooltip)
            print ("moduleListElem.order = " + moduleListElem.order)
            print ("")

    def load_modules_from_path(self, path):
        """
        Import all modules from the given directory
        """
        # Check and fix the path
        if path[-1:] != '/':
           path += '/'

        # Get a list of files in the directory, if the directory exists
        if not os.path.exists(path):
            raise OSError("Directory does not exist: %s" % path)

        # Add path to the system path
        sys.path.append(path)
        # Load all the files in path
        for fileNameModule in os.listdir(path):
           #print (fileNameModule)
           # Ignore anything that isn't a .py file
           if len(fileNameModule) > 3 and fileNameModule[-3:] == '.py':
                modname = fileNameModule[:-3]
                # Import the module
                modImported = __import__(  modname, globals(), locals(), ['*'])

                for name, data in inspect.getmembers(modImported):
                    if name == '__name__':
                        prefix = str(data)
                for name, obj in inspect.getmembers(modImported):
                    if inspect.isclass(obj):
                        suffix=name
                classModule = prefix+"."+suffix
                #print ("loading class  : " + classModule)

                class_name = self.load_class_from_name(classModule)
                #print (class_name)

                # instantiate the class
                obj = class_name()

                if hasattr(obj, "main") and  hasattr(obj, "getInfo")  and hasattr(obj, "isActive")  :

                    pluginVer = obj.getInfo( PLUGIN_VERSION )
                    pluginName = obj.getInfo( PLUGIN_NAME )
                    pluginDes = obj.getInfo( PLUGIN_DESCRIPTION )
                    menuLib = obj.getInfo( PLUGIN_MENULIB )
                    menuStatutBarToolTip = obj.getInfo( PLUGIN_STATUTBARTOOLTIP )
                    menuTooltip = obj.getInfo( PLUGIN_MENUTOOLTIP )
                    iconPath = obj.getInfo( PLUGIN_ICONPATH )
                    shortCut = obj.getInfo( PLUGIN_SHORTCUT )
                    order = obj.getInfo( PLUGIN_ORDER )

                    active = obj.isActive()

                    newModule = module( self, classModule , pluginName, active, pluginVer, pluginDes, menuLib, menuStatutBarToolTip, menuTooltip, shortCut, iconPath, order)
                    self.moduleList.append(newModule)
                else :
                    log.Log.debug( log.ERROR , "ERREUR du plugin : "+classModule+" : main() ou isActive() ou getInfo() : manquant(s).")


        # Sort modules
        # Trier la liste des modules par "order"
        self.moduleList.sort( key=lambda x: x.order, reverse=False )

    def load_class_from_name(self, fqcn):

        # Break apart fqcn to get module and classname
        paths = fqcn.split('.')
        modulename = '.'.join(paths[:-1])
        classname = paths[-1]
        # Import the module
        __import__(modulename, globals(), locals(), ['*'])
        # Get the class
        cls = getattr(sys.modules[modulename], classname)
        # Check cls
        if not inspect.isclass(cls):
           log.Log.debug( log.ERREUR , "ERREUR du plugin : "+fqcn+" : n'est pas une classe !")
           #raise TypeError("%s is not a class" % fqcn)
        return cls
