

# http://qt.developpez.com/faq/?page=modules-qtcore-fichiers-lectureecriture
# https://books.google.fr/books?id=9oRa4WJLlGkC&pg=PT212&lpg=PT212&dq=python+qt+++QDataStream+write&source=bl&ots=XoiWGsKxnl&sig=W5xhkgfMjzKI-p43CEKfa0Fc5bQ&hl=en&sa=X&redir_esc=y#v=onepage&q=python%20qt%20%20%20QDataStream%20write&f=false
class dataStream() :

    def __init__(self,  parent=None):
        self.parent = parent
        print("init dataStream")

    def writeToFile(self, fileName, dataMyStreamList ) :

        printDebug("writeToFile : " + fileName)

        file = QFile(fileName)
        file.open(QIODevice.WriteOnly)
        datastream = QDataStream(file)

        printDebug("start write streamList")
        datastream.writeQVariant( dataMyStreamList.streamList )
        printDebug("start write streamListFavorite")
        datastream.writeQVariant( dataMyStreamList.streamListFavorite )
        printDebug("start write streamListAdvert")
        datastream.writeQVariant( dataMyStreamList.streamListAdvert )

        for streamItem  in dataMyStreamList.streamList :
            printDebug("save url = " + streamItem.url + " genre=" + streamItem.getGenre() + ", name =" +  streamItem.getName()  )
            for nowPlaying  in streamItem.nowPlaying :
               printDebug("nowPlaying = " + nowPlaying.getTrackItem() +", date = " + nowPlaying.getTrackItemDt()  )
        for streamItem  in dataMyStreamList.streamListFavorite :
           printDebug("* favorite track = " + streamItem.getTrackItem() )

        file.close()

    def readFromFile(self, fileName, dataMyStreamList) :

        file = QFile(fileName)
        file.open(QIODevice.ReadOnly)
        datastream = QDataStream(file)

        printDebug("start read streamList")
        dataMyStreamList.streamList = datastream.readQVariant(  )
        dataMyStreamList.streamListFavorite = datastream.readQVariant(  )
        dataMyStreamList.streamListAdvert = datastream.readQVariant(  )

        try :
            for streamItem  in dataMyStreamList.streamList :
                printDebug("loaded url = " + streamItem.url + " genre=" + streamItem.getGenre() + ", name =" +  streamItem.getName()  )
                for nowPlaying  in streamItem.nowPlaying :
                   printDebug("nowPlaying = " + nowPlaying.getTrackItem() +", date = " + nowPlaying.getTrackItemDt()  )
            for streamItem  in dataMyStreamList.streamListFavorite :
               printDebug("* favorite track = " + streamItem.getTrackItem() )
        except :
            pass
        file.close()