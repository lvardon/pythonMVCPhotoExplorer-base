# -*- coding: utf-8 -*-

"""
Model Locale

"""

from PyQt5.QtCore import QDir, QT_TRANSLATE_NOOP, QTranslator, QLocale

class locale() :

    def __init__(self, parent) :
        
        self.parent = parent        
  
        self.qmFiles = self.findQmFiles()
        self.parent.log.debug( 'INFO' , 'Locale list : ') 
        for i, qmf in enumerate(self.qmFiles):
            self.parent.log.debug( 'INFO' , 'Locale avail #' + str(i) + ' : ' + qmf) 
            pass
        
        # Pour une détection automatique de la langue locale
        #self.localecode = QLocale.system().name() # De type en_EN ou fr_FR
 
        # Et pour une version forcée :
        # locale = "fr_FR"        
        
    def findQmFiles(self):
        trans_dir = QDir('locale')
        fileNames = trans_dir.entryList(['*.qm'], QDir.Files, QDir.Name)        
        return [trans_dir.filePath(fn) for fn in fileNames]
                
    def setLocale(self, localecode) :
        self.localecode = localecode
        #self.parent.log.debug( 'INFO' , 'init locale : ' + self.localecode ) 
        translator = QTranslator() 
        #translator.load(self.qmFiles[0])        