#!/usr/bin/env python
#-*- coding: utf-8 -*-

"""

"""
import re
import os.path
import operator

from PyQt5.QtWidgets import QApplication, QMenu, QMainWindow, QDialog, QLabel, QComboBox, QPushButton, QVBoxLayout, QMessageBox, QStyledItemDelegate, QItemDelegate
from PyQt5.QtGui import QIcon, QStandardItemModel, QStandardItem, QCursor, QColor
from PyQt5.QtCore import Qt, QDir, QT_TRANSLATE_NOOP, QTranslator, QLocale, QSize, QPoint, QDateTime, QVariant, QDate, QAbstractTableModel
from PyQt5.QtCore import Qt
from PyQt5 import uic

from applib import log
from applib import setting
from applib import filecmd
from appmod import fileitem

class file_rename_controler( QDialog ) :

    def __init__(self, parent, fileItemList):

        super(file_rename_controler,self).__init__()
        self.parent = parent
        self.fileActionWindow = uic.loadUi('appmod/file_rename_view.ui', self)
        self.fileItemList = fileItemList

        self.setWindowTitle("Renommer les fichiers : " + str( len(self.fileItemList) ))

        self.extToCopyList = setting.Setting.getValue("rawsearch/rawInfoExtSearch")
        #self.fileActionWindow.checkBox_pp3.setText( "Renommer : "   + ', '.join(self.extToCopyList)  )

        self.lineEditPattern.setText( "^(.*)\.(.*)$" )
        self.lineEditFileNameValue.setText( "\\1.\\2" )

        self.lineEditFileNameValue.textChanged.connect( self.onValueChanged )

        self.lineEditPattern_numstart.setText( "1" )
        self.lineEditPattern_numstart.textChanged.connect( self.onValueChanged )
        self.lineEditPattern_numinc.setText( "1" )
        self.lineEditPattern_numinc.textChanged.connect( self.onValueChanged )
        self.lineEditPattern_format.setText( "{0:04d}" )
        self.lineEditPattern_format.textChanged.connect( self.onValueChanged )

        itemForTagList = fileitem.fileItem( self, "", "" )
        tagList = ["[num]"]
        tagList = tagList + itemForTagList.getTagList()

        self.fileActionWindow.tableView_preview.setSortingEnabled(True)

        self.comboBox_taglist.clear()
        self.comboBox_taglist.addItems( tagList )

        self.ok_pushButton.clicked.connect(self.onOk)
        self.cancel_pushButton.clicked.connect(self.onCancel)
        self.preview_pushButton.clicked.connect(self.onPreview)
        self.pushButton_tagadd.clicked.connect(self.onAddTag)
        self.reset_pushButton.clicked.connect(self.onReset)

        self.showFiles( self.fileItemList )

        self.applyRename()

    def onOk(self) :
        #myfilecmd = filecmd.filecmd(self)
        for irow in range(self.model.rowCount(self)):
            for icol in range(self.model.columnCount(self)):
                index = self.model.index(irow, icol)
                cellValue = str(self.model.data(index))
                if icol == 0 :
                    baseName = cellValue
                if icol == 1 :
                    newName = cellValue
                    baseDir  = self.fileItemList[0].filePath
                    if baseName != newName and os.path.isfile( baseDir + "/" + newName) is False:
                        print ( baseDir, "/", baseName , " -> " , baseDir, "/", newName)
                        old_file = os.path.join( baseDir, baseName)
                        new_file = os.path.join( baseDir, newName)
                        os.rename(old_file, new_file)

                        #if self.fileActionWindow.checkBox_pp3.isChecked() is True :
                        print ("rename PP3 ?")

                        (rawDirlistSearch) = setting.Setting.getListValue("rawsearch/rawDirlistSearch")
                        i = 0
                        for rawDirlistSearchElem in rawDirlistSearch :
                            rawDirlistSearch[i] = rawDirlistSearch[i].replace( "[rawdir]" , baseDir)
                            if not rawDirlistSearch[i].endswith("/") :
                                rawDirlistSearch[i] = rawDirlistSearch[i] + "/"

                            self.extToCopyList = setting.Setting.getValue("rawsearch/rawInfoExtSearch")
                            for extToCopyElem in self.extToCopyList :
                                print (extToCopyElem , " --> " , rawDirlistSearch[i]  + os.path.splitext(baseName)[0] + "." + extToCopyElem , " ?")
                                if os.path.exists( rawDirlistSearch[i]  + os.path.splitext(baseName)[0] + "." + extToCopyElem ) is True :
                                    print (
                                        "rename  " + rawDirlistSearch[i]  + os.path.splitext(baseName)[0] + "." + extToCopyElem  +
                                        " TO " + baseDir  + "/" + os.path.splitext(new_file)[0] + "." + extToCopyElem
                                    )
                                    """myfilecmd.copy(
                                        rawDirlistSearch[i], os.path.splitext(fileItem.fname)[0] + "." + extToCopyElem ,
                                        self.dstDir  , os.path.splitext(fileItem.fname)[0] + "." + extToCopyElem,
                                        self.fileActionWindow.checkBox_overwrite.isChecked()
                                    )
                                    """
                            i = i + 1

        self.model.refreshView()
        #self.ok_pushButton.setEnabled(False)

    def onCancel(self) :
        self.close()

    def onValueChanged(self ) :
        self.applyRename()

    def onPreview(self) :
        self.applyRename()

    def onReset(self) :
        self.lineEditPattern.setText( "^(.*)\.(.*)$" )
        self.lineEditFileNameValue.setText( "\\1.\\2" )
        self.applyRename()

    def dialogShow( self) :
        log.Log.debug( log.INFO , "File Rename view : showing window."  )
        self.fileActionWindow.exec_() # Modal

    def onAddTag( self ) :
        #self.lineEditFileNameValue.setText( self.lineEditFileNameValue.text() + self.comboBox_taglist.currentText() )
        self.lineEditFileNameValue.insert( self.comboBox_taglist.currentText() )

    def applyRename(self) :

        try :
            self.num = int(self.lineEditPattern_numstart.text())
        except Exception as e:
            self.num = 1

        try :
            self.inc = int(self.lineEditPattern_numinc.text())
        except Exception as e:
            self.inc = 1

        fnameFirstPrev = ""
        #self.num = self.num - 1

        print ("")
        for irow in range(self.model.rowCount(self)):

            for icol in range(self.model.columnCount(self)):

                index = self.model.index(irow, icol)
                cell = str(self.model.data(index))

                if icol == 0 :
                    baseName = cell

                if icol == 1 :

                    print ( "@" + str(irow) + " basename=" + baseName + " ---> " , cell)
                    #print ("current  : " , self.model.data(index) , " ---"   , " self.num=" , self.num )

                    try:
                        fnameFirstCurrent = re.search('(.+?)\..*', baseName).group(1)
                    except AttributeError:
                        fnameFirstCurrent = ''

                    try :

                        if irow > 0 and fnameFirstPrev != fnameFirstCurrent  :
                            self.num = self.num + self.inc

                        print ( "   [", fnameFirstPrev , "] vs [" , fnameFirstCurrent , "]  / self.num=" , self.num )

                        newName = self.getFmtNewName ( irow, baseName , self.num )

                        self.model.setData( index, newName)

                        try:
                            fnameFirstPrev = re.search('(.+?)\..*', baseName).group(1)
                        except AttributeError:
                            fnameFirstPrev = ''

                    except Exception as e:
                        self.model.setData(index,"")

    def showFiles( self , fileItemList) :

            fileList = []
            idxFile = 0
            for fileItem in fileItemList  :
                fileList.append( [fileItem.fname  , ""] )
                idxFile = idxFile + 1

            baseDir  = self.fileItemList[0].filePath
            self.model = fileListModel( fileList , baseDir, ["Fichier/image", "Nouveau nom"])
            self.model.sort(0, Qt.AscendingOrder)

            self.fileActionWindow.tableView_preview.setModel(self.model)

            self.fileActionWindow.tableView_preview.resizeColumnsToContents()
            self.fileActionWindow.tableView_preview.horizontalHeader().setStretchLastSection(True)

            self.fileActionWindow.tableView_preview.verticalHeader().setVisible(False)

    def getFmtNewName ( self, irow, baseName ,  numValue ) :
        #print ("Format : ", baseName)
        newName = re.sub( self.lineEditPattern.text(), self.lineEditFileNameValue.text() , baseName)
        #print ("newName 0: ", newName)
        newName = self.fileItemList[irow].fetch_label( newName )
        #print ("newName 1: ", newName)
        newName = newName.replace( "[num]" , self.lineEditPattern_format.text().format(numValue) )
        #print ("newName 2: ", newName)
        return newName



class fileListModel(QAbstractTableModel):
    def __init__(self, list, baseDir, headers = [], parent = None):
        QAbstractTableModel.__init__(self, parent)
        self.arraydata = list
        self.headers = headers
        self.baseDir = baseDir

    def rowCount(self, parent):
        return len(self.arraydata)

    def columnCount(self, parent):
        return len(self.arraydata[0])

    def flags(self, index):

        if not index.isValid():
            return

        column = index.column()
        if column == 0 :
            return Qt.ItemIsEnabled | Qt.ItemIsSelectable
        if column == 1 :
            return Qt.ItemIsEditable | Qt.ItemIsEnabled | Qt.ItemIsSelectable

    def data(self, index, role = Qt.DisplayRole):

        if not index.isValid():
            return QVariant()

        row = index.row()
        column = index.column()

        if role == Qt.EditRole:
            return self.arraydata[row][column]

        if role == Qt.DisplayRole:
            value = self.arraydata[row][column]
            return value

        if role == Qt.BackgroundRole:
            try :
                if column == 1 and os.path.isfile( self.baseDir + "/" + self.arraydata[row][0]) is True and os.path.isfile( self.baseDir + "/" + self.arraydata[row][column]) is True:
                    return QVariant(QColor(Qt.red))
                if column == 1 and os.path.isfile( self.baseDir + "/" + self.arraydata[row][0]) is False and os.path.isfile( self.baseDir + "/" + self.arraydata[row][column]) is True :
                    return QVariant(QColor(Qt.green))
            except Exception as e:
                pass

    def setData(self, index, value, role = Qt.EditRole):
        if role == Qt.EditRole:
            row = index.row()
            column = index.column()
            self.arraydata[row][column] = value
            self.dataChanged.emit(index, index)
            return True
        return False

    def headerData(self, section, orientation, role):

        if role == Qt.DisplayRole:

            if orientation == Qt.Horizontal:

                if section < len(self.headers):
                    return self.headers[section]
                else:
                    return "not implemented"
            else:
                return "item %d" % section

    def refreshView(self) :
        self.layoutAboutToBeChanged.emit()
        self.layoutChanged.emit()

    def sort(self, Ncol, order):
        """Sort table by given column number."""
        """
        self.layoutAboutToBeChanged.emit()
        self.list = self.list.sort_values(self.headers[Ncol],ascending=order == Qt.AscendingOrder)
        self.layoutChanged.emit()
        """

        try:
            self.layoutAboutToBeChanged.emit()
            self.arraydata = sorted(self.arraydata, key=operator.itemgetter(Ncol))
            if order == Qt.DescendingOrder:
                self.arraydata.reverse()
            self.layoutChanged.emit()

        except TypeError as e:
            print('[*] Sort on empty column')
