#!/usr/bin/env python
#-*- coding: utf-8 -*-

"""

"""

from PyQt5.QtWidgets import QApplication, QMenu, QMainWindow, QDialog, QLabel, QComboBox, QPushButton, QVBoxLayout, QMessageBox, QStyledItemDelegate, QItemDelegate
from PyQt5.QtGui import QIcon, QStandardItemModel, QStandardItem, QCursor
from PyQt5.QtCore import Qt, QDir, QT_TRANSLATE_NOOP, QTranslator, QLocale, QSize, QPoint, QDateTime, QVariant, QDate
from PyQt5.QtCore import Qt
from PyQt5 import uic


from applib import log

from applib import setting

class file_delete_controler( QDialog ) :

    def __init__(self, parent, fileItemList):

        super(file_delete_controler,self).__init__()
        self.parent = parent
        self.DetailWindow = uic.loadUi('appmod/file_delete_view.ui', self)

        nbfiles = 0
        for fileItem in fileItemList  :
            nbfiles = nbfiles + 1
            self.lineEditFileNameValue.setText( fileItem.fname )

        self.setWindowTitle("Supprimer les fichiers : " + str( nbfiles ))

        self.ok_pushButton.clicked.connect(self.onOk)
        self.cancel_pushButton.clicked.connect(self.onCancel)

    def onOk(self) :
        self.close()

    def onCancel(self) :
        self.close()

    def dialogShow( self) :
        log.Log.debug( log.INFO , "File delete view : showing window."  )
        self.DetailWindow.exec_() # Modal

    def tmp() :
        reply = QMessageBox.question(self, "Confirmez ", "Confirmez vous la suppression ? " , QMessageBox.Yes, QMessageBox.No)
        if reply == QMessageBox.Yes:

            myfilecmd = filecmd.filecmd(self)

            for fileItem in self.getFileItemListSelected () :
                resultCmd = myfilecmd.remove(  self.MainWindow.treeView.getPathSelected(), fileItem.fname )
                if resultCmd is not True :
                    QMessageBox.warning(self, 'Erreur', "Erreur de suppression : \n\n" + myfilecmd.cmdExec + "\n\n" + "Code de retour : " + str(myfilecmd.subprocess_returncode)  + "\n" + myfilecmd.subprocess_err , QMessageBox.Yes )
                else :
                    log.Log.debug(log.INFO, "Del file key : " + str(fileItem.key) )
                    self.tableData.tableModel.removeDataByKey( fileItem.key )

            self.MainWindow.nav_tableview.clearSelection()
            self.MainWindow.nav_iconview.clearSelection()
