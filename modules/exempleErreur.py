#!/usr/bin/python3
# -*- coding: utf-8 -*-

from applib.module import PLUGIN_ACTIVE,PLUGIN_NAME,PLUGIN_DESCRIPTION,PLUGIN_VERSION,PLUGIN_MENULIB,PLUGIN_MENUTOOLTIP,PLUGIN_STATUTBARTOOLTIP, PLUGIN_MENU_UPPER,PLUGIN_MENU_TOOLBAR,PLUGIN_MENU_ICONVIEW,PLUGIN_MENU_DETAILVIEW,PLUGIN_SHORTCUT, PLUGIN_ICONPATH, PLUGIN_ORDER

class ErreurExample(object):

    def __init__(self):
        self.active = True
        self.error = ""

    def main( self, self_parent, currentPath, fileItemList, fileExt ) :

        try :

            A = 1 / 0

        except Exception as e:
            self.error = e.args[0]
            return 1

        return 0

    def isActive( self ) :
        return  self.getInfo( PLUGIN_ACTIVE )

    def getInfo( self, typeInfo) :
        if typeInfo == PLUGIN_ACTIVE :
            return self.active
        elif typeInfo == PLUGIN_NAME :
            return "Plugin générant une erreur"
        elif  typeInfo == PLUGIN_DESCRIPTION :
            return "Plugin générant une erreur : division par zéro"
        elif  typeInfo == PLUGIN_VERSION :
            return "1.0.0"
        elif  typeInfo == PLUGIN_MENULIB :
            return "Erreur plugin"
        elif  typeInfo == PLUGIN_MENUTOOLTIP :
            return "Exemple de plugin générant une erreur ; division par zéro"
        elif  typeInfo == PLUGIN_STATUTBARTOOLTIP :
            return "module exemple Qt qui génère une erreur"
        elif  typeInfo == PLUGIN_SHORTCUT :
            return "Ctrl-p"
        elif  typeInfo == PLUGIN_ICONPATH :
            return "icon/statut_red.png"
        elif  typeInfo == PLUGIN_MENU_UPPER :
            return True
        elif  typeInfo == PLUGIN_MENU_TOOLBAR :
            return False
        elif  typeInfo == PLUGIN_MENU_ICONVIEW :
            return True
        elif  typeInfo == PLUGIN_MENU_DETAILVIEW :
            return True
        elif  typeInfo == PLUGIN_ORDER :
            return 510



