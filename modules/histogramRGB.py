#!/usr/bin/python3
# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import QApplication

from applib.module import PLUGIN_ACTIVE,PLUGIN_NAME,PLUGIN_DESCRIPTION,PLUGIN_VERSION,PLUGIN_MENULIB,PLUGIN_MENUTOOLTIP,PLUGIN_STATUTBARTOOLTIP, PLUGIN_MENU_UPPER,PLUGIN_MENU_TOOLBAR,PLUGIN_MENU_ICONVIEW,PLUGIN_MENU_DETAILVIEW,PLUGIN_SHORTCUT, PLUGIN_ICONPATH, PLUGIN_ORDER

class histogramRGB(object):

    def __init__(self):
        self.active = True
        self.error = ""

    def main( self, self_parent, currentPath, fileItemList, fileExt ) :

        for fileItem in fileItemList :
            fileItem.showHistogramRGB()

        return 0

    def isActive( self ) :
        return  self.getInfo( PLUGIN_ACTIVE )

    def getInfo( self, typeInfo) :
        if typeInfo == PLUGIN_ACTIVE :
            return self.active
        elif typeInfo == PLUGIN_NAME :
            return "histogramRGB"
        elif  typeInfo == PLUGIN_DESCRIPTION :
            return "Plugin histogramRGB"
        elif  typeInfo == PLUGIN_VERSION :
            return "1.0.0"
        elif  typeInfo == PLUGIN_MENULIB :
            return "Histogramme Rgb"
        elif  typeInfo == PLUGIN_MENUTOOLTIP :
            return "Executer histogramRGB"
        elif  typeInfo == PLUGIN_STATUTBARTOOLTIP :
            return "module histogramRGB"
        elif  typeInfo == PLUGIN_SHORTCUT :
            return "Ctrl-p"
        elif  typeInfo == PLUGIN_ICONPATH :
            return "icon/tool_module_histogram.png"
        elif  typeInfo == PLUGIN_MENU_UPPER :
            return True
        elif  typeInfo == PLUGIN_MENU_TOOLBAR :
            return True
        elif  typeInfo == PLUGIN_MENU_ICONVIEW :
            return True
        elif  typeInfo == PLUGIN_MENU_DETAILVIEW :
            return True
        elif  typeInfo == PLUGIN_ORDER :
            return 120


