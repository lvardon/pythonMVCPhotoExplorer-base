# -*- coding: utf-8 -*-

"""


Usage : 

from applib import log
from applib.log import Log
       
        log.Log.debug( log.INFO , 'Sqlite Controler init : ' + sqliteFile)   

"""

import sys
import datetime 
import locale
import os
import os.path
import inspect
                
Log = None

INFO = "0"
INFOSETTING = "1"
ERROR = "2"
WARNING = "3"
FATAL = "4" 
      
class log() :

    def __init__(self, parent , filelog, eraseContent = "1", outToconsole="1" ) :
       

        pathlog = filelog
        pathlog = pathlog.replace("\\", os.sep)
   
        self.filelog = pathlog
        self.filelogCopyName = None
        self.parent = parent        
        originalStdout = sys.stdout
        self.outToconsole = outToconsole
        
        try:                    
            callerframerecord = inspect.stack()[1]    # 0 represents this line, # 1 represents line at caller
            frame = callerframerecord[0]
            context = inspect.getframeinfo(frame)  
                        
            if eraseContent == "1" :
                sys.stdout = open(self.filelog, 'w')
                self.outLineMessage( INFO, 'init log file (erase): ' + self.filelog , context )                 
            else :
                sys.stdout = open(self.filelog, 'a')
                self.outLineMessage( INFO, 'init log file (append) : ' + self.filelog , context )                 
                            
            sys.stdout = originalStdout
            
        except IOError:
            sys.exit( 'Unable to write to file ' + filelog )
        
        self.debug( INFO, "self.filelog=" + self.filelog  ) 
          
    def debug(self, level, message) :

        originalStdout = sys.stdout
        
        sys.stdout = open(self.filelog, 'a+')
    
        callerframerecord = inspect.stack()[1]    # 0 represents this line, # 1 represents line at caller
        frame = callerframerecord[0]
        context = inspect.getframeinfo(frame)    
    
        self.outLineMessage( level, message, context ) 

        sys.stdout = originalStdout
        
        if self.outToconsole == "1" :
            self.outLineMessage( level, message, context ) 

    def outLineMessage( self, level, message , context) :

        now = datetime.datetime.now()
        
        levelStr = ""
        if level == INFO :
            levelStr = "INFO"
        elif  level == FATAL :
            levelStr = "FATAL"
        elif  level == WARNING :
            levelStr = "WARNING"
        elif  level == ERROR :
            levelStr = "ERREUR"
        elif  level == INFOSETTING :
            levelStr = "SETTING"
        
        print(now.strftime("%d-%m-%Y %H:%M:%S.%f") + ';' + levelStr + ';' + os.path.basename(context.filename) + ':' + context.function + ':' + str(context.lineno) + ';' + message)


    def copy( self) :
        
        import time
        import os
        import shutil

        datetime_var = time.strftime("%Y_%m_%d_%H_%M_%S")
        
        src_dir = os.path.dirname(self.filelog) 
        base=os.path.basename(self.filelog)
        src_base = os.path.splitext(base)[0]        

        """
        print ("self.filelog=" + self.filelog)
        print ("src_dir=" + src_dir)
        print ("base=" + base)
        print ("src_base=" + src_base)
        """

        dst_file = src_dir + os.sep + src_base + "_" + str(datetime_var) + ".txt"

        file_path = dst_file.split(os.path.basename(dst_file))[0]
        if not os.path.exists(file_path):
            os.makedirs(file_path)

        dst_file = dst_file.replace('\\', os.sep)
        shutil.copy2(self.filelog, dst_file)        
        
        self.filelogCopyName = dst_file
        
  
    def getFilelogCopyName( self) :
        return self.filelogCopyName

