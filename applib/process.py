# -*- coding: utf-8 -*-

"""

Usage :

        self.Process = process.process(self)
        self.Process.setOutputWidget( self.parent.textEdit_2 )



 def on_pushButton_3(self) :
        if self.Process.isStatusRunning() == False :            
            #self.Process.syncStart  = True
            self.parent.textEdit_2_update(self.Process.getInitialMessage() )
            self.parent.pushButton_3_update('Running...')
            
            self.parent.parent.log.debug('INFO', 'process call ... : ' + self.parent.getCommand() )
            self.Process.setSuccessPattern(  self.model.getSuccessPattern() ) # self.parent.getSuccessPattern()
            self.Process.setErrorPattern(  self.model.getErrorPattern() ) # self.parent.getErrorPattern()
            self.Process.start( self.getFmtCommand() ,  self.getFmtCommandArgs() )                        
            
        else:
            self.parent.parent.log.debug('INFO', 'Process stoped by user.')
            self.Process.stop()
            self.parent.pushButton_3_update('Start...')
            
            
   def onProcessStart(self ) :
        self.parent.parent.log.debug('INFO', 'onProcessStart called')

    def onProcessRunning(self, processRunning ) :
        self.parent.parent.log.debug('INFO', 'onProcessRunning called')
        if processRunning == True :
          pixmap = QPixmap("icon/process_running.png")
          scaledPixmap = pixmap.scaled(self.parent.label_2.size(), QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)          
          self.parent.label_2.setPixmap(scaledPixmap)

    def onProcessAbort(self) : 
        self.parent.parent.log.debug('INFO', 'onProcessAbort called')
          
    def onProcessSuccess(self) :
        self.parent.parent.log.debug('INFO', 'onProcessSuccess called')

    def onProcessError(self) :
        self.parent.parent.log.debug('INFO', 'onProcessError called')

    def onProcessEnd(self, statutSuccess, statutError, statusAborted , processErrorDetail) :
        self.parent.parent.log.debug('INFO', 'onProcessEnd called')
        if statutSuccess is True :
          pixmap = QPixmap("icon/process_ok.png")
          scaledPixmap = pixmap.scaled(self.parent.label_2.size(), QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)          
          self.parent.label_2.setPixmap(scaledPixmap)
        if statutError is True :
          pixmap = QPixmap("icon/process_error.png")
          scaledPixmap = pixmap.scaled(self.parent.label_2.size(), QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)          
          self.parent.label_2.setPixmap(scaledPixmap)
          self.onAboutError( "Une erreur est survenue lors de l'execution de la commande" , "" , processErrorDetail)
        if statusAborted is True :
          pixmap = QPixmap("icon/process_aborted.png")
          scaledPixmap = pixmap.scaled(self.parent.label_2.size(), QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)          
          self.parent.label_2.setPixmap(scaledPixmap)

"""
from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import QProcess

import platform, re

if platform.system() == "Windows":
    from applib import log
    from applib.log import Log
else :
    from applib import log
    from applib.log import Log
       


class process(QProcess) :

    def __init__(self,  parent):
        QProcess.__init__(self)
        self.parent = parent
                
        log.Log.debug( log.INFO , 'Process : init')
         
        #self.parent.parent.log.= log.log( self )
        #self.parent.parent.log.= parent.log

        self.outWidget=None           # Widget pour les sorties stdOut et strErr            
        self.OutLineSep = '\n'        # Séparateur de ligne dans le Widget de sortie texte

        self.colorStdout = '#000000'  # Texte StdOut
        self.colorStdErr = '#ff0000'  # Erreur détectée dans stdErr
        self.colorError = '#ff0000'   # Erreur de la commande        
        self.colorReady = '#0000ff'   # Message 'Prêt' à executer 
        self.colorEnd = '#00ff00'     # Message 'Fin' d'execution
        self.colorStop = '#ffb30B'    # Message 'Stop' (arrêt/abort)                 

        self.messageReady = 'Ready :'                
        self.messageEnd = 'End.'   
        self.messageStop = 'Stop.'   
                
        self.syncStart = False            
            
        self.waitForStarted = False
        self.waitForStartedTimeOut = 1000
            
        self.waitForFinished = False            
        self.waitForFinishedTimeOut = 2000                
                
        #self.successPattern = [ r"(perte 0%)" ]        
        #self.errorPattern = [ u"Vérifiez le nom et essayez à nouveau", u"Délai d'attente de la demande dépassé" , r"perte 100", r"a pas pu trouver"]

        self.successPattern = [ u"" ]        
        self.errorPattern = [ u"" ]


        # stdoutEncoding : ascii , latin-1 , iso8859-1 , cp850 , utf-8
        if platform.system() == "Windows":
            self.stdoutEncoding = 'cp850'
        else :
            self.stdoutEncoding = 'utf-8'

        self.statusRunning = False
        self.statusSuccess = None
        self.statusError = None        
        self.statusAborted = None

        self.cmdStart = ''
        self.processError = ''        

    def __del__(self):
        # If QApplication is closed attempt to kill the process
        self.terminate()
        # Wait for Xms and then elevate the situation to terminate
        if not self.waitForFinished :
            self.kill()

    def setSuccessPattern(self , successPattern ) :
        self.successPattern = successPattern
            
    def setErrorPattern(self , errorPattern ) :
        self.errorPattern = errorPattern

    def start(self, cmdStart ,  cmdArgs) :        
    
        self.finalReportDone = False

        if self.statusRunning == True :
          self.stop()

        self.parent.onProcessRunning( True )  

        self.chBufer=''
        self.statusRunning = True
        self.statusSuccess = None
        self.statusError = None       
        self.statusAborted = False       

        self.cmdStart = cmdStart
        self.cmdArgs = cmdArgs
        self.processCmd = QProcess(self)
        self.processCmd.readyReadStandardOutput.connect(self.stdout)
        self.processCmd.readyReadStandardError.connect(self.stderr)
        self.processCmd.finished.connect(self.cmdEnd)
        self.processCmd.error.connect(self.cmdError)

        if self.syncStart == True :
            retVal = self.processCmd.execute( cmdStart , cmdArgs) # sync'ed, and ouput is disabled
            log.Log.debug( log.INFO, 'Process execute cmd : ' + cmdStart )
            log.Log.debug( log.INFO, 'Process retVal =  '+ str(retVal) )
            # if retVal !=0 : error ....
        else :        
            self.processCmd.start( cmdStart , cmdArgs)         
            log.Log.debug( log.INFO, 'Process start cmd : ' + cmdStart )
            
        self.statusRunning = True
       
        for argv in self.cmdArgs :
            log.Log.debug( log.INFO, 'Process arg : ' + argv )
       
        for pattern in self.successPattern :        
            log.Log.debug( log.INFO, 'Pattern success : [' + pattern + ']')
        for pattern in self.errorPattern :
            log.Log.debug( log.INFO, 'Pattern error : [' + pattern  + ']')               
        
        self.parent.onProcessStart()
        
        log.Log.debug( log.INFO, 'exitStatus=' + str( self.exitStatus() ) )
        log.Log.debug( log.INFO, 'exitCode=' + str( self.exitCode() ) )
        
    def getInitialMessage(self) :
        txtColor = '<span style=\" color:'+self.colorReady+';\" >'+self.messageReady+'</span>'
        return( txtColor )

    def setOutputWidget(self, widget) :
        self.outWidget=widget

    def stop(self) :
        log.Log.debug( log.INFO, 'Process stop : ' + self.cmdStart  + ' : ' + self.messageStop)
        self.processCmd.kill()

        txtColor = '<span style=\" color:'+self.colorStop+';\" >'+self.messageStop+'</span>'
        self.appendOutWidget(txtColor)
        
        self.statusSuccess = False
        self.statusRunning = False
        self.statusAborted = True       
        self.statusError = False        
        
        self.processFinalReport()        

    def cmdError(self , error) :
        errors = ["Failed to start", "Crashed", "Timedout", "Read error", "Write Error", "Unknown Error"]
        self.processError = errors[error] 
        log.Log.debug( log.INFO, 'Process error : ' + self.cmdStart + ' : ' +  self.processError)            
        txtColor = '<span style=\" color:'+self.colorError+';\" >'+self.processError+'</span>'        
        self.outWidget.append( txtColor )
        self.outWidget.moveCursor(QtGui.QTextCursor.End)
                      
        self.statusRunning = False
        self.statusSuccess = False
        self.statusError = True       
        self.statusAborted = False  
        self.processFinalReport()  
        #self.parent.onProcessEnd(self.statusSuccess, self.statusError, self.statusAborted, self.processError )
         
            
    def cmdEnd(self):
        log.Log.debug( log.INFO, 'Process end : ' + self.cmdStart + ' : ' + self.messageEnd )

        txtColor = '<span style=\" color:'+self.colorEnd+';\" >'+self.messageEnd+'</span>'
        self.appendOutWidget(txtColor)

        self.statusRunning = False
        self.statusAborted = False       

        self.processFinalReport()

    def processFinalReport(self) :

        if self.finalReportDone == True :
            return
        else : 
            self.finalReportDone = True

        log.Log.debug( log.INFO, 'Process end statut Sucess: ' + str(self.statusSuccess))
        log.Log.debug( log.INFO, 'Process end statut Error: ' + str(self.statusError))
        log.Log.debug( log.INFO, 'Process end statut Aborted: ' + str(self.statusAborted))

        if self.statusSuccess == True :
            self.parent.onProcessSuccess()     
        if self.statusError == True :
            self.parent.onProcessError()     
        if self.statusAborted == True :
            self.parent.onProcessAbort()
        
        self.parent.onProcessEnd(self.statusSuccess, self.statusError, self.statusAborted, self.processError )
    
    def error(self, error ) :
        log.Log.debug( log.INFO, 'Last process error: ' + error())    
    
    
    def stderr(self):
        data = self.processCmd.readAllStandardError().data()
        ch = str(data.decode(self.stdoutEncoding) )
        
        self.statusError = self.checkError()    
        if self.statusError == True :
            self.parent.onProcessError()     

        self.parent.onProcessRunning( self.statusRunning )     

        arrBuffer = self.chBufer.split(self.OutLineSep)
        iArr=0
        for elemArr in arrBuffer :
            iArr = iArr + 1
            if iArr < len(arrBuffer) :
                log.Log.debug( log.INFO, 'stderr : ' + self.cmdStart + " :"  + ch)
                txtColor = '<span style=\" color:'+self.colorStdErr+';\" >'+elemArr+'</span>'
                self.appendOutWidget(txtColor)
            else :
                # keep end buffer
                self.chBufer = elemArr

        self.statusError = True

    def stdout(self):
        data = self.processCmd.readAllStandardOutput().data()
        ch = str(data.decode(self.stdoutEncoding) )
        self.chBufer = self.chBufer +  ch

        self.statusSuccess = self.checkSuccess()        
        if self.statusSuccess == True :
            self.parent.onProcessSuccess()  
        
        self.statusError = self.checkError()    
        if self.statusError == True :
            self.parent.onProcessError()     

        self.parent.onProcessRunning( self.statusRunning )     

        arrBuffer = self.chBufer.split(self.OutLineSep)
        iArr=0
        for elemArr in arrBuffer :
            iArr = iArr + 1
            if iArr < len(arrBuffer) :
                log.Log.debug( log.INFO, 'stdout ' + self.cmdStart + ':' + elemArr )
                txtColor = '<span style=\" color:'+self.colorStdout+';\" >'+elemArr+'</span>'
                self.appendOutWidget(txtColor)
            else :
                # keep end buffer
                self.chBufer = elemArr

    def checkSuccess(self ) :      
      if self.statusSuccess is not None :
        return self.statusSuccess
      for pattern in self.successPattern :        
        match = re.search( ".*(" + re.escape(pattern) + ").*", self.chBufer, re.MULTILINE|re.DOTALL)       
        if match is not None:
            log.Log.debug( log.INFO, 'success pattern found : ' + pattern)
            return True        
      return None

    def checkError(self ) :
      if self.statusError is not None :
        return self.statusSuccess
      for pattern in self.errorPattern :
        match = re.search( ".*(" + re.escape(pattern) + ").*", self.chBufer, re.MULTILINE|re.DOTALL)       
        if match is not None:
            log.Log.debug( log.INFO, 'error pattern found : ' + pattern)
            return True        
      return None

    def appendOutWidget(self, messageLine ) :
      if self.outWidget == None :
        return
      self.outWidget.append( messageLine )
      self.outWidget.moveCursor(QtGui.QTextCursor.End)

    def isStatusRunning( self ) : 
        return self.statusRunning
