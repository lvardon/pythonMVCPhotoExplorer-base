#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Controleur de la fenêtre principale
"""

import operator
import os
from os import path
import sys, traceback

from os.path import basename
from PyQt5 import QtCore, QtGui
from PyQt5 import uic

from PyQt5.QtCore import Qt, QProcess, QUrl, QRegExp, QSize, QPoint, QVariant, QItemSelectionModel, QModelIndex, QItemSelection, QAbstractTableModel, QSortFilterProxyModel, QStandardPaths, QStorageInfo
from PyQt5.QtGui import QPainter, QImage, QPixmap, QFont, QFontMetrics, QStandardItemModel, QStandardItem, QIcon, QColor, QPen, QBrush
from PyQt5.QtWidgets import QAction, QActionGroup, QApplication, QFileSystemModel, QListView, QTableView, QTreeView, QLabel, \
    QMenu, QMainWindow, QMessageBox, QAbstractItemView, QFileDialog, QGraphicsView, QGraphicsScene, QDialog, QStyledItemDelegate, QStyle

from applib import log
from applib import setting
from applib.module import module, modules
from applib.module import PLUGIN_ACTIVE,PLUGIN_NAME,PLUGIN_DESCRIPTION,PLUGIN_VERSION,PLUGIN_MENULIB,PLUGIN_MENUTOOLTIP,PLUGIN_STATUTBARTOOLTIP, PLUGIN_MENU_UPPER,PLUGIN_MENU_TOOLBAR,PLUGIN_MENU_ICONVIEW,PLUGIN_MENU_DETAILVIEW,PLUGIN_SHORTCUT, PLUGIN_ICONPATH, PLUGIN_ORDER
from applib import filecmd
from applib import dircmd


from appmod.fileitem import fileItem, COL_selection, COL_thumb, COL_key, COL_ino, COL_fname, COL_dev, COL_ext, COL_islink, COL_linktarget, \
    COL_fsize, COL_fsizeh, COL_c_date, COL_m_date, COL_a_date, COL_access_w, COL_mime, COL_ftype, COL_crc32, COL_sha1, \
    COL_exifinfos, COL_isimmutable, COL_width, COL_height, COL_trtstatus, COL_trtlib
from appmod.fileitem import rawElem, rawNode
from appmod.dirnavtreeview import dirNavTreeView
from appmod.imageviewer import imageviewer
from appmod import importExternal_controler
from appmod import duplicate_controler


# Mode de lancement du programme : lancement classqiue (fenêtre) ou bien dans le systray
STARTUPMODE_GUI = 0
STARTUPMODE_SYSTRAY = 1

# Modes de visualisation des images
STACKED_VIEW_LIST = 0
STACKED_VIEW_ICON = 1

# Liste des onglets de l'interface ui
TAB_NAVIGATION = 0
TAB_IMAGE = 1


class main_controler(QMainWindow):
    """
    Contrôleur principal de l'application
    """
    def __init__(self, parent, splash, startupMode):

        super(main_controler, self).__init__()
        self.parent = parent
        self.splash = splash
        self.startupMode = startupMode

        log.Log.debug(log.INFO, "Main view : init (load " + 'appmod/main_view.ui' + ")")
        self.MainWindow = uic.loadUi('appmod/main_view.ui', self)

        if setting.Setting.getValue("mainwindow/restoreGeometryValue", "1") is not None and setting.Setting.getValue(
                "mainwindow/restoreGeometry", 0) == "1":
            self.MainWindow.restoreGeometry(setting.Setting.getValue("mainwindow/restoreGeometryValue"))
        else:
            size = QSize(int(setting.Setting.getValue("mainwindow/initialWidth")),
                         int(setting.Setting.getValue("mainwindow/initialHeight")))
            self.MainWindow.resize(size)
            pos = QPoint(int(setting.Setting.getValue("mainwindow/initialX")),
                         int(setting.Setting.getValue("mainwindow/initialY")))
            self.MainWindow.move(pos)

        if setting.Setting.getValue("mainwindow/restoreStateValue") is not None and setting.Setting.getValue(
                "mainwindow/restoreState", 0) == "1":
            self.MainWindow.restoreState(setting.Setting.getValue("mainwindow/restoreStateValue"))

        app_icon = QtGui.QIcon()
        app_icon.addFile('icon/app16x16.png', QtCore.QSize(16, 16))
        app_icon.addFile('icon/app24x24.png', QtCore.QSize(24, 24))
        app_icon.addFile('icon/app32x32.png', QtCore.QSize(32, 32))
        app_icon.addFile('icon/app48x48.png', QtCore.QSize(48, 48))
        app_icon.addFile('icon/app256x256.png', QtCore.QSize(256, 256))
        self.setWindowIcon(app_icon)

        if setting.Setting.getValue("app/startShow", 0) == "1" or self.startupMode == STARTUPMODE_GUI:
            self.mainwindowShow()
        else:
            self.mainWindowHide()


        # App entry point -----------------------------------------------------------------------

        self.MainWindow.nav_tableview.hide()
        self.MainWindow.nav_iconview.hide()

        if setting.Setting.getValue("mainapp/lastPathRestore", "/") == "1":
            self.currentPath = setting.Setting.getValue("mainapp/lastPath")
        else:
            self.currentPath = "/"

        import platform
        if platform.system() == "Windows":
            self.defaultRoot = setting.Setting.getValue("dirtreeview/defaultRootWin" , "c:")
        else :
            self.defaultRoot = setting.Setting.getValue("dirtreeview/defaultRootNix", "/")

        log.Log.debug(log.INFO, "Tree view defaultRoot : " + self.defaultRoot)

        # Lise des dossiers : favoris
        self.favDirList = folderList(self, 10 , "dirfav/folderList" , "dirfav/folderMaxVal" )
        self.favDirList.restore()

        # Liste des dossiers  : historique des dossiers visités
        self.historyDirList = folderList(self, 12 , "dirhisto/folderList" , "dirhisto/folderMaxVal" )
        self.historyDirList.restore()

        # Tab management
        self.MainWindow.main_tabWidget.currentChanged.connect(self.onChangeTab)

        # Misc init
        self.progressBar = progressBar( self, self.MainWindow.progressBar )
        self.toolbarModules = None
        self.menuImage = None
        self.fileExt = setting.Setting.getListValue("app/fileExt")

        # Directories model
        self.dir_model = myQFileSystemModel()
        self.dir_model.setFavDirList( self.favDirList.getAll() )
        self.dir_model.setFavDirColor( setting.Setting.getValue("dirfav/folderTxtColor" , "#AB0000"), setting.Setting.getValue("dirfav/folderBgColor", "#050505") )

        dirIndex = self.dir_model.setRootPath(self.currentPath)
        self.dir_model.setFilter(QtCore.QDir.AllDirs | QtCore.QDir.NoDotAndDotDot)
        self.dir_model.directoryLoaded.connect(self.onTreeDirectoryLoaded)

        # Directories view treeview
        self.MainWindow.treeView = self.MainWindow.dirNavTreeView
        #self.MainWindow.treeView.setStyleSheet('QTreeView::item { color: red; border-left: 1px solid rgb(40,40,40);    }');
        #self.MainWindow.treeView.setStyleSheet ('QTreeView {    alternate-background-color: yellow; }')
        #self.MainWindow.treeView.setItemDelegate(redRowDelegate(self.dir_model, []))

        self.MainWindow.treeView.setModel(self.dir_model)
        rootIndex = self.MainWindow.treeView.currentIndex()
        self.MainWindow.treeView.init( self, dirIndex, rootIndex, self.dir_model)
        #rootIndex = self.dir_model.setRootPath(self.defaultRoot)
        self.lastRoot = setting.Setting.getValue("dirtreeview/lastRoot", self.defaultRoot)
        #self.MainWindow.treeView.setRootIndex(self.lastRoot)


        #dirIndex = self.dir_model.setRootPath( self.MainWindow.treeView.getPathSelected() )
        dirIndex = self.dir_model.setRootPath( self.lastRoot )
        self.MainWindow.treeView.setRootIndex(dirIndex)
        self.MainWindow.treeView.resizeColumnToContents(0)


        self.MainWindow.treeView.setPathSelected ( self.currentPath )
        log.Log.debug(log.INFO, "lastPath/current path = " + self.currentPath)


        # Toolbar actions
        self.MainWindow.toolBarMain.setIconSize(QSize(32,32))
        self.MainWindow.toolBarMain.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.MainWindow.actionTreeViewAll.triggered.connect(self.onActionTreeViewAll)
        self.MainWindow.actionTreeViewAll.setIcon(QIcon("icon/tool_viewall.png"))
        self.MainWindow.actionTreeViewAll.setIconText("Racine")

        self.MainWindow.actionTreeViewHere.triggered.connect(self.onaActionTreeViewHere)
        self.MainWindow.actionTreeViewHere.setIcon(QIcon("icon/tool_viewfromhere.png"))
        self.MainWindow.actionTreeViewHere.setIconText("Racine ici")

        self.MainWindow.actionMode_list.triggered.connect(self.onActionViewAslist)
        self.MainWindow.actionMode_list.setIcon(QIcon("icon/tool_viewlist.png"))
        self.MainWindow.actionMode_list.setIconText("Détails")
        self.MainWindow.actionMode_icon.triggered.connect(self.onActionViewAsIcon)
        self.MainWindow.actionMode_icon.setIcon(QIcon("icon/tool_viewicon.png"))
        self.MainWindow.actionMode_icon.setIconText("Icônes")
        self.MainWindow.actionScrollToSelected.triggered.connect(self.onActionTreeScrollToSelectedPath)
        self.MainWindow.actionScrollToSelected.setIcon(QIcon("icon/tool_scrollto.png"))
        self.MainWindow.actionScrollToSelected.setIconText("Selection")

        self.MainWindow.actionCreateThumbs.triggered.connect(self.constructThumbs)
        self.MainWindow.actionCreateThumbs.setIcon(QIcon("icon/tool_mkthumb.png"))
        self.MainWindow.actionCreateThumbs.setIconText("Vignettes")

        self.MainWindow.actionExpandChilds.triggered.connect(self.onActionExpandChild)
        self.MainWindow.actionExpandChilds.setIcon(QIcon("icon/tool_expand.png"))
        self.MainWindow.actionExpandChilds.setIconText("Ouvrir")

        self.MainWindow.actionCollapseChilds.triggered.connect(self.onActionCollapseChild)
        self.MainWindow.actionCollapseChilds.setIcon(QIcon("icon/tool_collapse.png"))
        self.MainWindow.actionCollapseChilds.setIconText("Fermer")

        # Toolbar modules
        self.MainWindow.actionIcon_zoomp.triggered.connect(self.onActionIconZoomPlus)
        self.MainWindow.actionIcon_zoomp.setIcon(QIcon("icon/tool_zoomplus.png"))
        self.MainWindow.actionIcon_zoomp.setIconText("Zoom +")

        self.MainWindow.actionIcon_zoomm.triggered.connect(self.onActionIconZoomMinus)
        self.MainWindow.actionIcon_zoomm.setIcon(QIcon("icon/tool_zoomminus.png"))
        self.MainWindow.actionIcon_zoomm.setIconText("Zoom -")

        self.MainWindow.actionIcon_zoomreset.triggered.connect(self.onActionIconZoomReset)
        self.MainWindow.actionIcon_zoomreset.setIcon(QIcon("icon/tool_zoomreset.png"))
        self.MainWindow.actionIcon_zoomreset.setIconText("Zoom reset")


        # Grab files from current dir and fill data model
        self.fileList = self.getfileListFromDir( self.MainWindow.treeView.getPathSelected() )

        # View #1 : files model table view  (right pan)
        self.tableData = tableData( self)
        self.tableData.tableModel = fileModelAsTable(self.fileList, self)
        self.MainWindow.nav_tableview.setSelectionMode(QAbstractItemView.ExtendedSelection)

        self.tableData.tableModel.thumbSizeWidth = int(setting.Setting.getValue("iconview/thumbSizeWidth", "100"))
        self.tableData.tableModel.thumbSizeHeight  = int(setting.Setting.getValue("iconview/thumbSizeHeight", "100"))
        self.tableData.tableModel.iconSizeWidth = int(setting.Setting.getValue("iconview/iconSizeWidth", "100"))
        self.tableData.tableModel.iconSizeHeight  = int(setting.Setting.getValue("iconview/iconSizeHeight", "100"))
        self.tableData.tableModel.cellWidth = int(setting.Setting.getValue("iconview/cellWidth", "164"))
        self.tableData.tableModel.cellHeight  = int(setting.Setting.getValue("iconview/cellHeight", "128"))
        self.tableData.tableModel.thumbQuality  = int(setting.Setting.getValue("iconview/thumbQuality", "30"))
        self.tableData.tableModel.thumbCacheDir  = setting.Setting.getValue("iconview/thumbCacheDir", "./cache")

        self.MainWindow.nav_tableview.setAlternatingRowColors(True)
        self.MainWindow.nav_tableview.horizontalHeader().setSectionsMovable(True)
        self.MainWindow.nav_tableview.verticalHeader().hide()
        self.MainWindow.nav_tableview.setSelectionBehavior(QTableView.SelectRows)

        self.MainWindow.nav_tableview.setContextMenuPolicy(Qt.CustomContextMenu)
        self.MainWindow.nav_tableview.customContextMenuRequested.connect(self.menuContextImage)

        self.MainWindow.nav_tableview.clicked.connect(self.onTableViewClicked)

        # Header context menu
        self.MainWindow.nav_tableview.horizontalHeader().setContextMenuPolicy(Qt.CustomContextMenu)
        self.MainWindow.nav_tableview.horizontalHeader().customContextMenuRequested.connect(self.menuContextHeader)

        # Files model custom sort
        self.table_proxy_model = tableviewSortProxy(self)
        self.table_proxy_model.setSourceModel(self.tableData.tableModel)
        self.MainWindow.nav_tableview.setModel(self.table_proxy_model)
        self.tableData.setProxyModel( self.table_proxy_model )
        # self.MainWindow.nav_tableview.sortByColumn( COL_fname, Qt.AscendingOrder) # AscendingOrder / DescendingOrder

        # View #2 : files model icon/list view  (right pan)
        self.MainWindow.nav_iconview.setModel(self.table_proxy_model)
        self.MainWindow.nav_iconview.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.MainWindow.nav_iconview.selectionModel().selectionChanged.connect(self.onIconViewSelectionchanged)
        self.MainWindow.nav_iconview.setContextMenuPolicy(Qt.CustomContextMenu)
        self.MainWindow.nav_iconview.customContextMenuRequested.connect(self.menuContextImage)

        self.MainWindow.nav_iconview.setViewMode(QListView.IconMode)

        self.MainWindow.nav_iconview.setSpacing( int(setting.Setting.getValue("iconview/cellSpacing", "4") ))

        self.MainWindow.nav_iconview.setWordWrap(True)
        self.MainWindow.nav_iconview.setWrapping(True)

        self.MainWindow.nav_iconview.setFont(QFont(setting.Setting.getValue("iconview/fontName", "Arial"), int(setting.Setting.getValue("iconview/fontSize", "9"))))

        # View 2 : selection
        self.MainWindow.nav_iconview.clicked.connect(self.onIconViewClicked)



        # View 1 Default sort (sort tableview will sort other views..)
        self.MainWindow.nav_tableview.setSortingEnabled(True)
        self.MainWindow.nav_tableview.selectionModel().selectionChanged.connect(self.onTableViewSelectionchanged)

        # Disposition
        mainWindowW = self.MainWindow.width()
        self.MainWindow.nav_splitter.setSizes([mainWindowW * 0.3, mainWindowW * 0.7])

        # Image view
        self.MainWindow.img_splitter.setSizes([mainWindowW * 0.85, mainWindowW * 0.15])
        self.MainWindow.imageView.setExifTableView( self.MainWindow.exif_tableView )
        self.MainWindow.imageView.setPaths( self.MainWindow.treeView.getPathSelected(), self.favDirList )

        # Divers
        self.mainwindowSetTitle( self.MainWindow.treeView.getPathSelected(), len(self.fileList))

        # Charger les modules
        self.pluginInit()

        # Connecter les actions des menus standards
        self.menuInit()

        # Construire le menu de l'historique des dossiers
        self.menuHistoInit()

        # Construire le menu des dossiers favoris
        self.menuFavInit()

        # First Display
        QtCore.QTimer.singleShot(300, self.firstDisplayInit)

        self.showNormal()

    def stateSave(self ) :
        """
        Sauvegade de la configuration de l'application.
        Emplacement de la fenêtre principale, taille de la fenêtre, dossiers ...
        """
        setting.Setting.setValue("mainapp/viewMode", self.tableData.tableModel.getViewMode())

        setting.Setting.setValue("mainapp/mainWindowState", self.MainWindow.saveState())
        setting.Setting.setValue("mainapp/mainSplitterState", self.MainWindow.nav_splitter.saveState())
        setting.Setting.setValue("mainapp/imgSplitterState", self.MainWindow.img_splitter.saveState())
        setting.Setting.setValue("mainapp/fileModeView", self.MainWindow.nav_stackedWidget.currentIndex())
        setting.Setting.setValue("mainapp/tableviewHeaderState",self.MainWindow.nav_tableview.horizontalHeader().saveState())
        setting.Setting.setValue("mainapp/exifTableViewHeaderState",self.MainWindow.exif_tableView.horizontalHeader().saveState())
        setting.Setting.setValue("mainapp/lastPath", self.MainWindow.treeView.getPathSelected())
        setting.Setting.setValue("dirtreeview/lastRoot", self.dir_model.rootPath())

        setting.Setting.setValue("iconview/iconSizeWidth", round(self.tableData.tableModel.iconSizeWidth,0))
        setting.Setting.setValue("iconview/iconSizeHeight", round(self.tableData.tableModel.iconSizeHeight,0))
        setting.Setting.setValue("iconview/cellHeight", round(self.tableData.tableModel.cellHeight,0))
        setting.Setting.setValue("iconview/cellWidth", round(self.tableData.tableModel.cellWidth,0))

        self.historyDirList.save()
        self.favDirList.save()

    def stateRestore( self ) :
        """
        Restauration de configuration de la fenêtre :
        Emplacement de la fenêtre principale, taille de la fenêtre ...
        """

        self.tableData.tableModel.setViewMode( setting.Setting.getValue("mainapp/viewMode") )

        if setting.Setting.getValue("mainapp/fileModeViewRestore", "1") == "1":
            if setting.Setting.getValue("mainapp/fileModeView", "1") == "1":
                self.onActionViewAsIcon()
            else:
                self.onActionViewAslist()
        else:
            self.onActionViewAslist()

        if setting.Setting.getValue("mainapp/mainWindowRestoreState", "0") == "1":
            self.MainWindow.restoreState(setting.Setting.getValue("mainapp/mainWindowState"))

        if setting.Setting.getValue("mainapp/mainSplitterRestoreState", "0") == "1":
            self.MainWindow.nav_splitter.restoreState(setting.Setting.getValue("mainapp/mainSplitterState"))

        if setting.Setting.getValue("mainapp/imgSplitterRestoreState", "0") == "1":
            self.MainWindow.img_splitter.restoreState(setting.Setting.getValue("mainapp/imgSplitterState"))

        if setting.Setting.getValue("mainapp/treeDirMode", "1") == "0":
            self.onaActionTreeViewHere()

        if setting.Setting.getValue("mainapp/tableviewHeaderRestoreState", "0") == "1":
            self.MainWindow.nav_tableview.horizontalHeader().restoreState(
                setting.Setting.getValue("mainapp/tableviewHeaderState"))

        if setting.Setting.getValue("mainapp/exifTableViewHeaderRestoreState", "0") == "1":
            self.MainWindow.exif_tableView.horizontalHeader().restoreState(
                setting.Setting.getValue("mainapp/exifTableViewHeaderState"))

    def onActionTreeScrollToSelectedPath(self):
        """
        Evenement : changement de dossier courant
        """
        self.currentPath = self.MainWindow.treeView.getPathSelected()
        dirIndex = self.dir_model.index(self.currentPath)
        self.MainWindow.treeView.scrollTo(dirIndex)

    # On Qfilesystem model creation (once)
    def onTreeDirectoryLoaded(self, path):
        """
        Mise en forme de l'affichage du tree view des dossiers
        Appelé automatiquement,  plusieurs fois,  au premier chargement de l'application.
        (Un appel par dossier traversé, de la racine du disque, au dossier courant)
        """
        # Ajustement de la colonne du treeview des dossiers
        if path == self.currentPath:
            self.MainWindow.treeView.resizeColumnToContents(0)
            self.MainWindow.nav_tableview.resizeColumnToContents(COL_fname)
            self.MainWindow.treeView.setPathSelected ( self.MainWindow.treeView.getPathSelected() )

    def onTreeNodeClicked(self, signal):
        """
        Evenement : à chaque selection d'un dossier du tree view.:
        """
        self.treeViewOpenPath( self.dir_model.filePath(signal) )


    def treeViewOpenPath( self, newPath  ) :
        """
        Changement de dossier de navigaton
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)

        self.MainWindow.treeView.setPathSelected ( newPath )
        # Dérouler le tree view jusqu'au nouveau dossier
        dirIndex = self.dir_model.index(newPath)
        self.MainWindow.treeView.scrollTo(dirIndex)
        self.MainWindow.treeView.setCurrentIndex(dirIndex)

        # Files populate and display
        self.fileList = self.getfileListFromDir(newPath)
        currentHeaderState = self.MainWindow.nav_tableview.horizontalHeader().saveState()
        self.tableData.tableModel.loadData(self.fileList)
        self.MainWindow.nav_tableview.horizontalHeader().restoreState( currentHeaderState )

        # Tree view
        self.MainWindow.treeView.resizeColumnToContents(0)

        # Table view
        self.MainWindow.nav_tableview.horizontalHeader().setStretchLastSection(True)
        self.MainWindow.nav_tableview.resizeColumnToContents(COL_fname)
        self.MainWindow.nav_tableview.clearSelection()

        # Icon view
        self.MainWindow.nav_iconview.clearSelection()

        # Image view
        self.MainWindow.imageView.setPaths( self.MainWindow.treeView.getPathSelected(), self.favDirList )
        self.MainWindow.imageView.setMainWindow ( self )

        # Afficher le titre de la fenêtre principale
        self.mainwindowSetTitle(newPath, len(self.fileList))

        # Affichage du texte d'infos sous chacune des vues
        self.viewSetLabelInfo()

        # Afficher dans l'onglet image de la première image disponible su dossier courrant
        self.showImage( self.table_proxy_model.geFirstDataItemSorted(self.fileExt)  )

        # Ajouter ce  nouveau dossier à l'historique
        self.historyDirList.add( newPath , "Dossier recement parcouru")
        # Recontruire le menu
        self.menuHistoInit()

        QApplication.restoreOverrideCursor()

    def onTreeNodeExpanded(self, itemIndex):
        """
        Evènement : Lorsqu'un noeud du tree view est ouvert
        """
        self.MainWindow.treeView.resizeColumnToContents(0)


    def onTableViewClicked(self, index):
        """
        Evènement : Clic souris sur la liste des fichiers du dossier
        """
        row = index.row()
        column = index.column()
        indexView = self.MainWindow.nav_tableview.currentIndex()
        indexIno = self.MainWindow.nav_tableview.model().index(row, COL_ino)
        inoCurrent = self.MainWindow.nav_tableview.model().data(indexIno, Qt.UserRole)

    def onTableViewSelectionchanged( self, itemSelectionSelected, itemSelectionDeselected) :
        """
        Evènement : Nouveaux fichiers selectionnés.
        """
        self.MainWindow.nav_iconview.selectionModel().selectionChanged.disconnect(self.onIconViewSelectionchanged)

        self.MainWindow.nav_iconview.selectionModel().clearSelection()
        self.MainWindow.nav_iconview.setSelectionMode(QAbstractItemView.MultiSelection)

        scrollIndex = None
        for model_index in self.MainWindow.nav_tableview.selectionModel().selectedRows():
            source_index = self.table_proxy_model.mapToSource(model_index)  # Le même est partagé entre les deux vues
            scrollIndex = source_index

            self.MainWindow.nav_iconview.selectionModel().select(model_index, QItemSelectionModel.Select)

        self.MainWindow.nav_iconview.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.MainWindow.nav_iconview.selectionModel().selectionChanged.connect(self.onIconViewSelectionchanged)


    def onaActionTreeViewHere(self):
        """
        Evènement menu : cacher les dossiers parents.
        """
        dirIndex = self.dir_model.setRootPath( self.MainWindow.treeView.getPathSelected() )
        self.MainWindow.treeView.setRootIndex(dirIndex)
        self.MainWindow.treeView.resizeColumnToContents(0)

    def onActionTreeViewAll(self):
        """
        Evènement menu : afficher les dossiers parents.
        """
        dirIndex = self.dir_model.setRootPath(self.defaultRoot)
        self.MainWindow.treeView.setRootIndex(dirIndex)
        self.MainWindow.treeView.resizeColumnToContents(0)

        #self.treeScrollToSelectedPath()
        self.MainWindow.treeView.scrollTo(dirIndex)

    def onActionViewAslist(self):
        """
        Evènement menu : afficher les fichiers en mode détail (liste)
        """
        self.MainWindow.nav_stackedWidget.setCurrentIndex(STACKED_VIEW_LIST)
        self.MainWindow.actionVue_liste.setChecked(True)
        self.MainWindow.actionVue_icons.setChecked(False)
        self.tableData.tableModel.setViewMode(STACKED_VIEW_LIST)

    def onActionViewAsIcon(self):
        """
        Evènement menu : afficher les fichiers en mode icônes
        """
        self.MainWindow.nav_stackedWidget.setCurrentIndex(STACKED_VIEW_ICON)
        self.MainWindow.actionVue_liste.setChecked(False)
        self.MainWindow.actionVue_icons.setChecked(True)
        self.tableData.tableModel.setViewMode(STACKED_VIEW_ICON)

    def onActionExpandChild(self):
        """
        Evènement menu : ouvrir les dossiers enfants
        """
        self.MainWindow.treeView.treeExpandNode(True)

    def onActionCollapseChild(self):
        """
        Evènement menu : fermer les dossiers enfants
        """
        self.MainWindow.treeView.treeExpandNode(False)

    def onIconViewClicked(self, index):
        """
        Evènement : Selection d'un element de la vue icône
        """
        row = index.row()
        column = index.column()
        indexView = self.MainWindow.nav_iconview.currentIndex()
        indexIno = self.MainWindow.nav_iconview.model().index(row, COL_ino)
        inoCurrent = self.MainWindow.nav_iconview.model().data(indexIno, Qt.UserRole)

    def onIconViewSelectionchanged(self, itemSelectionSelected, itemSelectionDeselected) :
        """
        Evènement : Changement de selection d'un element de la vue icône
        """
        self.MainWindow.nav_tableview.selectionModel().selectionChanged.disconnect(self.onTableViewSelectionchanged)

        self.MainWindow.nav_tableview.selectionModel().clearSelection()
        self.MainWindow.nav_tableview.setSelectionMode(QAbstractItemView.MultiSelection)

        scrollIndex = None
        for model_index in self.MainWindow.nav_iconview.selectionModel().selectedRows():
            source_index = self.table_proxy_model.mapToSource(model_index)  # Le même est partagé entre les deux vues
            scrollIndex = source_index
            self.MainWindow.nav_tableview.selectionModel().select(model_index, QItemSelectionModel.Rows | QItemSelectionModel.Select)

        if scrollIndex is not None :
            self.MainWindow.nav_tableview.scrollTo(self.table_proxy_model.index(5,0) )

        self.MainWindow.nav_tableview.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.MainWindow.nav_tableview.selectionModel().selectionChanged.connect(self.onTableViewSelectionchanged)


    def onActionIconZoomPlus( self) :
        """
        Evènement : Augmentation de la taille des icônes
        """
        self.tableData.tableModel.zoom(1.1)

    def onActionIconZoomMinus( self) :
        """
        Evènement : Diminution de la taille des icônes
        """
        self.tableData.tableModel.zoom(0.9)

    def onActionIconZoomReset( self) :
        """
        Evènement : Affichage des icônes à la taille de base
        """
        self.tableData.tableModel.zoomReset()

    def menuContextImage(self, point):
        """
        Affichage du menu contextuel pour les icônes et/ou fichiers
        """
        fileItemList = self.getFileItemListSelected ()
        if len(fileItemList) <= 0 :
            return
        newFileItem = self.tableData.tableModel.getFileItemFromKey( fileItemList[0].key )

        if self.menuImage is None :
            self.menuImage = QMenu(self)

        self.menuImage.clear()

        subMenuOpenwith = self.menuImage.addMenu("Ouvrir avec...")
        openwithValues = setting.Setting.getDictValue("openwith/openWithCmd")
        for openwithElem in openwithValues :
            if openwithElem["index"] == 0 :
                openWithAction = subMenuOpenwith.addAction(openwithElem["value"])
            if openwithElem["index"] == 1 :
                openWithAction.setIcon( QIcon("icon/" + openwithElem["value"]) )
            if openwithElem["index"] == 2 :
                openWithAction.setToolTip( openwithElem["value"] )
                openWithAction.setStatusTip( openwithElem["value"] )
            if openwithElem["index"] == 3 :
                openWithAction.triggered.connect(lambda bVal, openwithCmd=openwithElem["value"] , filePath=newFileItem.filePath , fileName= newFileItem.fname  : self.onActionOpenWith(openwithCmd, filePath, fileName ))

        subMenuActionCmd = self.menuImage.addMenu("Actions...")
        actionCmdValues = setting.Setting.getDictValue("openwith/actionCmd")
        for actionCmdElem in actionCmdValues :
            if actionCmdElem["index"] == 0 :
                actionCmdAction = subMenuActionCmd.addAction(actionCmdElem["value"])
            if actionCmdElem["index"] == 1 :
                actionCmdAction.setIcon( QIcon("icon/" + actionCmdElem["value"]) )
            if actionCmdElem["index"] == 2 :
                actionCmdAction.setToolTip( actionCmdElem["value"] )
                actionCmdAction.setStatusTip( actionCmdElem["value"] )
            if actionCmdElem["index"] == 3 :
                actionCmdAction.triggered.connect(lambda bVal, openactionCmd=actionCmdElem["value"] , filePath=newFileItem.filePath , fileName= newFileItem.fname  : self.onActionOpenWith(openactionCmd, filePath, fileName ))


        actionCreateLink = self.menuImage.addAction('Créer un lien')
        actionCreateLink.triggered.connect( lambda bVal, filePath=newFileItem.filePath , fileName= newFileItem.fname : self.onActionCreateLink( filePath, fileName))

        newFilePath = os.path.join(newFileItem.filePath + "/" + newFileItem.fname)
        if os.path.islink(newFilePath) is True :
            sourcePath = os.path.dirname(os.readlink( os.path.join( newFileItem.filePath + "/" + newFileItem.fname) ))
        else  :
            sourcePath = ""
        actionGotoSourceLink = self.menuImage.addAction('Aller à l\'emplacement d\'origine : '  + sourcePath )
        actionGotoSourceLink.triggered.connect( lambda bVal, filePath=newFileItem.filePath , fileName= newFileItem.fname : self.onActionGotoSourceLink( filePath, fileName))
        if os.path.islink(newFilePath) is False :
            actionGotoSourceLink.setEnabled(False)

        self.menuImage.addSeparator()


        actionRename = self.menuImage.addAction('Renommer')
        actionRename.triggered.connect( lambda bVal, fileName= newFileItem.fname : self.onActionFileRename( fileName ))
        if newFileItem.isimmutable == True or newFileItem.access_w == False :
            actionRename.setEnabled( False )

        actionDelete = self.menuImage.addAction('Supprimer ...')
        actionDelete.triggered.connect( lambda bVal,  fileName= newFileItem.fname : self.onActionFileDel( fileName ))
        if newFileItem.isimmutable == True or newFileItem.access_w == False :
            actionDelete.setEnabled( False )

        actionMove = self.menuImage.addAction('Déplacer')
        actionMove.triggered.connect( lambda bVal,  fileName= newFileItem.fname : self.onActionFileMove( fileName ))
        if newFileItem.isimmutable == True or newFileItem.access_w == False :
            actionMove.setEnabled( False )

        actionCopy = self.menuImage.addAction('Copier')
        actionCopy.triggered.connect( lambda bVal, fileName= newFileItem.fname : self.onActionFileCopy( fileName ))


        self.menuImage.addSeparator()
        actionCreateDir = self.menuImage.addAction('Créer un dossier')
        actionCreateDir.triggered.connect( lambda bVal, fileName= newFileItem.fname : self.onActionCreateDir( fileName ))
        actionReload = self.menuImage.addAction('Relire le dossier')
        actionReload.triggered.connect( self.onActionReload )

        self.menuImage.addSeparator()
        actionSearch = self.menuImage.addAction('Rechercher les doublons ...')
        actionSearch.triggered.connect( self.onActionSearchDuplicate )


        self.menuModulesInit()

        self.menuImage.popup(self.MainWindow.nav_tableview.mapToGlobal(point))


    def menuModulesInit( self ) :
        """
        Initialisation des menus pour les modules/plug-in utilisateur
        """
        pluginList = modules( None )
        pluginList.load_modules_from_path('modules')

        self.menuImage.addSeparator()
        menuModules = self.menuImage.addMenu("Modules ...")

        for  pluginElem in pluginList.moduleList :

            log.Log.debug(log.INFO, "Init plugin : " + pluginElem.className)
            class_name = pluginList.load_class_from_name(pluginElem.className)
            obj = class_name()

            if not obj.isActive( ) :
                log.Log.debug(log.WARNING, "Ce plugin : " + pluginElem.className + " : est désactivé.")

            if obj.getInfo(PLUGIN_MENU_TOOLBAR) is True :

                log.Log.debug(log.INFO, "Plugin ajouté dans le menu contextuel images.")

                moduleAction = QAction(QIcon(pluginElem.iconPath), pluginElem.menuLib, self)
                moduleAction.setShortcut(pluginElem.shortCut)
                moduleAction.setStatusTip(pluginElem.menuStatutBarToolTip)
                moduleAction.setToolTip(pluginElem.menuTooltip)
                import functools
                moduleAction.triggered.connect( functools.partial(self.onActionPlugin , pluginElem.className ) )

                if pluginElem.active is False :
                    moduleAction.setEnabled(False)

                menuModules.addAction(moduleAction)

    def onActionGotoSourceLink( self, filePath, fileName ) :
        """
        Ouvrir le dossier source d'un lien symbolique
        """
        sourcePath = os.path.dirname(os.readlink( os.path.join(filePath + "/" + fileName) ))

        self.MainWindow.treeView.setPathSelected ( sourcePath )

        index = self.MainWindow.treeView.dir_model.index(sourcePath)
        self.MainWindow.treeView.selectionModel().clear()
        self.MainWindow.treeView.selectionModel().setCurrentIndex(index, QItemSelectionModel.Select)

        self.treeViewOpenPath( sourcePath )


    def onActionCreateLink( self, filePath, fileName) :
        """
        Créer un lien symbolique pour les fichiers de la selection
        """
        from appmod import file_linkcreate_controler
        self.file_linkcreate_controler = file_linkcreate_controler.file_linkcreate_controler( self, self.getFileItemListSelected(), self.MainWindow.treeView.getPathSelected(), self.favDirList)
        self.file_linkcreate_controler.dialogShow()

    def onActionFileRename( self, fileName) :
        """
        Renommer un fichier
        """
        from appmod import file_rename_controler
        self.file_rename_controler = file_rename_controler.file_rename_controler( self , self.getFileItemListSelected ())
        self.file_rename_controler.dialogShow()

    def onActionCreateDir(self, fileName ) :
        dircmdWin = dircmd.dircmd(self)
        dircmdWin.select( "Créez un nouveau dossier", self.MainWindow.treeView.getPathSelected(), self.favDirList )

    def onActionReload( self ) :
        """
        Recharger le contenu du dossier courant (rafraichir les données)
        """
        self.treeViewOpenPath( self.MainWindow.treeView.getPathSelected() )

    def onActionFileDel( self, fileName) :
        """
        Supprimer les fichiers de la selection
        """
        from appmod import file_delete_controler
        self.file_delete_controler = file_delete_controler.file_delete_controler( self , self.getFileItemListSelected())
        self.file_delete_controler.dialogShow()

    def onActionFileMove( self, fileName) :
        """
        Déplacer les fichiers de la selection
        """
        from appmod import file_move_controler
        self.file_move_controler = file_move_controler.file_move_controler( self , self.getFileItemListSelected(), self.MainWindow.treeView.getPathSelected(), self.favDirList)
        self.file_move_controler.dialogShow()

    def onActionFileCopy( self, fileName) :
        """
        Copier les fichiers de la selection
        """
        from appmod import file_copy_controler
        self.file_copy_controler = file_copy_controler.file_copy_controler( self , self.getFileItemListSelected(), self.MainWindow.treeView.getPathSelected(), self.favDirList)
        self.file_copy_controler.dialogShow()

    def onActionOpenWith( self, cmdOpenWith, filePath, fileName ) :
        """
        Evènement : ouvrir le fichier selectionné avec un programme externe
        """
        cmdCommandLineFmt = cmdOpenWith
        cmdCommandLineFmt = cmdCommandLineFmt.replace( "[filefullname]", '"'+filePath + "/" + fileName+'"')
        cmdCommandLineFmt = cmdCommandLineFmt.replace( "[filepath]", '"'+filePath+'"')

        log.Log.debug(log.INFO, "Ligne de commande : " + cmdCommandLineFmt)
        import subprocess
        proc = subprocess.Popen(cmdCommandLineFmt, stdout=subprocess.PIPE, shell=True)


    def menuContextHeader(self, point):
        """
        Menu contextuel des entêtes de colonnes
        """
        column = self.MainWindow.nav_tableview.horizontalHeader().logicalIndexAt(point.x())

        menu = QMenu(self)
        hideCol = menu.addAction('Cacher : ' + self.tableData.tableModel.headerdata[column])
        hideCol.triggered.connect(lambda: self.onActionHideCol(point))

        menugroup = QActionGroup(menu)
        menusep = False
        for columnAction in range(0, self.tableData.tableModel.columnCount(self)):
            if self.MainWindow.nav_tableview.horizontalHeader().isSectionHidden(columnAction) is True:
                if menusep is False:
                    menu.addSeparator()
                    menusep = True
                action = menu.addAction('Afficher : ' + self.tableData.tableModel.headerdata[columnAction])
                action.setActionGroup(menugroup)
                action.setData(columnAction)

        menugroup.triggered.connect(self.onActionShowCol)

        menu.addSeparator()

        menugroupfilter = QActionGroup(menu)
        filtercolumnAction = QAction("Filtrer ...", self)
        filtercolumnAction.setData(column)
        menu.addAction(filtercolumnAction)
        filtercolumnAction.setActionGroup(menugroupfilter)
        menugroupfilter.triggered.connect(self.onActionFilterColumn)

        menugroupunfilter = QActionGroup(menu)
        unfiltercolumnAction = QAction("Sans filtre", self)
        unfiltercolumnAction.setData(column)
        menu.addAction(unfiltercolumnAction)
        unfiltercolumnAction.setActionGroup(menugroupunfilter)
        menugroupunfilter.triggered.connect(self.onActionUnFilterColumn)

        menu.addSeparator()
        showColAll = menu.addAction('Afficher toutes les colonnes')
        showColAll.triggered.connect(self.onActionShowColAll)

        menu.popup(self.MainWindow.nav_tableview.horizontalHeader().mapToGlobal(point))

    def onActionShowColAll(self):
        """
        Afficher toutes les colonnes
        """
        col = 0
        for colElem in self.tableData.tableModel.headerdata :
            self.MainWindow.nav_tableview.horizontalHeader().showSection(col)
            col = col + 1

    def onActionShowCol(self, action):
        """
        Afficher une colonne
        """
        column = action.data()
        self.MainWindow.nav_tableview.horizontalHeader().setSectionHidden(column, False)

    def onActionHideCol(self, point):
        """
        Cacher une colonne
        """
        column = self.MainWindow.nav_tableview.horizontalHeader().logicalIndexAt(point.x())
        self.MainWindow.nav_tableview.horizontalHeader().setSectionHidden(column, True)

    def onActionFilterColumn (self , action):
        """
        Filtrer les lignes selon une valeur
        """
        columnKey = action.data()

        from appmod import filtercol_controler
        self.filtercol_controler = filtercol_controler.filtercol_controler( self )
        self.filtercol_controler.lineEditFilterValue.setText( self.tableData.table_proxy_model.getColFilter( columnKey)  )
        self.filtercol_controler.dialogShow()


        if self.filtercol_controler.lineEditFilterValue.text() == "" :
            self.tableData.tableModel.unSetColFilter( columnKey )
            self.tableData.table_proxy_model.unSetColFilter( columnKey)
            self.tableData.tableModel.layoutAboutToBeChanged.emit()
            self.tableData.tableModel.layoutChanged.emit()
        else :
            self.tableData.tableModel.setColFilter( columnKey )
            self.tableData.table_proxy_model.setColFilter( columnKey , self.filtercol_controler.lineEditFilterValue.text() )   # "nef|jpg|NEF|JPG"
            self.tableData.tableModel.layoutAboutToBeChanged.emit()
            self.tableData.tableModel.layoutChanged.emit()

    def onActionUnFilterColumn (self , action):
        """
        Annuler le filtre d'une colonne
        """
        columnKey = action.data()
        self.tableData.tableModel.unSetColFilter( columnKey )
        self.tableData.table_proxy_model.unSetColFilter( columnKey)
        self.tableData.tableModel.layoutAboutToBeChanged.emit()
        self.tableData.tableModel.layoutChanged.emit()

    def addDirToFavorites( self, path ) :
        """
        Ajouter un dossier aux favoris
        """
        self.favDirList.add( path)
        self.menuFavInit()
        self.dir_model.setFavDirList( self.favDirList.getAll() )

    def removeDirFromFavorites( self, path ) :
        self.favDirList.remove( path)
        self.menuFavInit()
        self.dir_model.setFavDirList( self.favDirList.getAll() )


    def constructThumbs( self,  makeThumbSubDirs = False,  overWriteExistsThumbs = False, processCancel = False )  :
        """
        Construire les vignettes des images d'un dossier
        """
        pathName = self.MainWindow.treeView.getPathSelected()

        # Calcul du nombre de fichiers, afin de proposer une Progress Bar
        fileCount = 0
        if makeThumbSubDirs is True :
            for root, dirs, files in os.walk(pathName):
                fileCount += len(files)
        else :
            fileCount = len(next(os.walk(pathName))[2])

        self.progressBar.setMaximum(fileCount)
        self.progressBar.show()
        self.progressBar.setValue(0)

        thumbSizeWidth = int(setting.Setting.getValue("iconview/thumbSizeWidth", "100"))
        thumbSizeHeight  = int(setting.Setting.getValue("iconview/thumbSizeHeight", "100"))
        thumbQuality  = int(setting.Setting.getValue("iconview/thumbQuality", "30"))
        thumbCacheDir  = setting.Setting.getValue("iconview/thumbCacheDir", "./cache")
        (thumbfileExt) = setting.Setting.getValue("iconview/thumbfileExt", "jpg")

        log.Log.debug(log.INFO, "Création de vignettes pour : " + pathName)
        log.Log.debug(log.INFO, "Sous dossiers inclus : " + str(makeThumbSubDirs))
        log.Log.debug(log.INFO, "Création de vignettes pour : " + str(fileCount) + " fichiers")
        log.Log.debug(log.INFO, "Création de vignettes dans le dossier de cache : " + thumbCacheDir)
        log.Log.debug(log.INFO, "Re-créer les vignettes existantes : " + str(overWriteExistsThumbs))
        log.Log.debug(log.INFO, "Vignettes créées pour les fichiers : " + str(thumbfileExt))

        # Type d'images pris en charge par Python
        QtCore.QCoreApplication.addLibraryPath(path.join(path.dirname(QtCore.__file__), "plugins"))
        sep = ""
        imgFormatKnown = ""
        for imgGFormat in QtGui.QImageReader.supportedImageFormats():
            imgFormatKnown = imgFormatKnown  + sep + str(imgGFormat)
            sep = ", "
        log.Log.debug(log.INFO, "Formats image pris en charge : " + imgFormatKnown)

        if not os.path.exists(thumbCacheDir):
            os.makedirs(thumbCacheDir)

        self.constructThumbsFromPath( pathName , thumbSizeWidth ,thumbSizeHeight, thumbQuality, thumbCacheDir , thumbfileExt, makeThumbSubDirs, overWriteExistsThumbs )

        self.progressBar.hide()

        log.Log.debug(log.INFO, "Création des vignettes terminée")


    def constructThumbsFromPath( self, pathName, thumbSizeWidth ,thumbSizeHeight, thumbQuality, thumbCacheDir, thumbfileExt,  makeThumbSubDirs, overWriteExistsThumbs  )  :
        """
        Contruction récursive des vignettes
        """
        if os.path.isdir(pathName):
            try:

                overlayIconSizeW  = int(setting.Setting.getValue("iconview/overlayIconSizeW", "32"))
                overlayIconSizeH  = int(setting.Setting.getValue("iconview/overlayIconSizeH", "32"))

                thumbSizeWidth = int(setting.Setting.getValue("iconview/thumbSizeWidth", "100"))
                thumbSizeHeight  = int(setting.Setting.getValue("iconview/thumbSizeHeight", "100"))
                thumbQuality  = int(setting.Setting.getValue("iconview/thumbQuality", "30"))
                thumbCacheDir  = setting.Setting.getValue("iconview/thumbCacheDir", "./cache")
                overlayIconSizeW  = int(setting.Setting.getValue("iconview/overlayIconSizeW", "32"))
                overlayIconSizeH  = int(setting.Setting.getValue("iconview/overlayIconSizeH", "32"))
                overlayIConLink = QImage("icon/arrow_green.png")
                overlayIConLink = overlayIConLink.scaled( QSize(overlayIconSizeW, overlayIconSizeH), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.SmoothTransformation ) # or .Qt.SmoothTransformation   /  QtCore.Qt.FastTransformation
                #overlayIConLink = QImage("icon/arrow_green.png")
                #overlayIConLink = overlayIConLink.scaled( QSize(overlayIconSizeW, overlayIconSizeH), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.SmoothTransformation ) # or .Qt.SmoothTransformation   /  QtCore.Qt.FastTransformation
                overWrite = False

                files = os.listdir(pathName)
                for fileName in files:

                    self.progressBar.forward( 1 )

                    QApplication.processEvents()
                    if self.processCancel is True :
                        return

                    if os.path.isfile(pathName + "/" + fileName):

                        try :
                            newFileItem = fileItem(self, pathName, fileName, self.fileExt)
                            newFileItem.createThumb(  thumbSizeWidth, thumbSizeHeight, thumbCacheDir, thumbQuality, overWrite,  overlayIConLink, overlayIconSizeW, overlayIconSizeH )

                            """
                            if newFileItem.ext in thumbfileExt  :

                                # ----------
                                linkAttr = ""
                                if newFileItem.islink is True :
                                    linkAttr="_l"

                                thumbFileName = newFileItem.key + linkAttr + ".JPG"
                                overlayIConExt = QImage("icon/ext_"+newFileItem.ext+".png")
                                overlayIConExt = overlayIConExt.scaled( QSize(overlayIconSizeW, overlayIconSizeH), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.FastTransformation ) # or .Qt.SmoothTransformation   /  QtCore.Qt.FastTransformation
                                if not os.path.isfile( thumbCacheDir +thumbFileName ) or overWriteExistsThumbs is True :

                                    #if makeThumbSubDirs is True or not os.path.isfile( thumbCacheDir + "/" + thumbFileName  )  :

                                        # sudo apt-get install qt5-image-formats-plugins

                                    baseQimage = QImage( pathName + "/" + fileName)
                                    if not baseQimage.isNull() :
                                        baseQimage = baseQimage.scaled( QSize(thumbSizeHeight, thumbSizeWidth), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.FastTransformation ) # or .Qt.SmoothTransformation   /  QtCore.Qt.FastTransformation

                                        overlayQimage = QImage( baseQimage.size(), QImage.Format_ARGB32_Premultiplied)

                                        painter = QPainter( overlayQimage )
                                        self.drawOverlayIconInit( painter , overlayQimage, baseQimage )

                                        if newFileItem.islink is True :
                                            self.drawOverlayIcon( painter, overlayQimage.width() -32, overlayQimage.height()-32, overlayIConLink )

                                        self.drawOverlayIcon( painter, 0,0 , overlayIConExt )

                                        painter.end()

                                        overlayQimage.save( thumbCacheDir + "/" + thumbFileName  , "JPG", thumbQuality)
                                    else :
                                        log.Log.debug(log.INFO, "Erreur de création de vignettes (format incorrect): " +  pathName + "/" + fileName )
                            else :
                                    print("Bad ext : " + fileName)
                                # ----------------------
                            """

                        except Exception as e:
                            log.Log.debug(log.ERROR, "Error in fileItem : %s:" % e.args[0] + " / pathName=" + pathName)

                    else :
                        if os.path.isdir(pathName + "/" + fileName):
                            if makeThumbSubDirs is True :
                                self.constructThumbsFromPath( pathName + "/" + fileName, thumbSizeWidth ,thumbSizeHeight, thumbQuality, thumbCacheDir, thumbfileExt,  makeThumbSubDirs, overWriteExistsThumbs  )

            except Exception as e:
                log.Log.debug(log.ERROR, "Error %s:" % e.args[0] + " / pathName=" + pathName)

            finally:
                self.progressBar.hide()


    def keyPressEvent(self, event):
        """
        Gestion des évènements clavier
        """
        if event.key() == Qt.Key_Escape :
            self.processCancel = True
            print("ESCAPE KEY HIT")


    def drawOverlayIconInit(self, painter, imageWithOverlay, baseImage ) :
        """
        Préparer le dessin d'une icône
        """
        painter.setCompositionMode(QPainter.CompositionMode_Source)
        painter.fillRect(imageWithOverlay.rect(), Qt.transparent)
        painter.setCompositionMode(QPainter.CompositionMode_SourceOver)
        painter.drawImage(0, 0, baseImage)

    def drawOverlayIcon(self, painter, posX, posY, overlayIConImage ) :
        """
        Dessiner une icône en surimpression
        """
        painter.setCompositionMode(QPainter.CompositionMode_SourceOver)
        painter.drawImage( posX , posY, overlayIConImage)


    def getfileListFromDir(self, pathName):
        """
        Retourne la liste (liste contenenant des fileItem) des fichiers d'un dossier
        """
        if not os.path.isdir(pathName) :
            log.Log.debug(log.INFO, "non trouvé : " + pathName)
            pathName= "/"

        self.processCancel = False

        # Nombre de fichiers, afin de proposer une Progress Bar
        onlyfilesCount = len(next(os.walk(pathName))[2])
        self.progressBar.setMaximum(onlyfilesCount)
        self.progressBar.show()
        self.progressBar.setValue(0)
        barVal = 0
        self.splash.progressBarSetMaximum(onlyfilesCount)

        log.Log.debug(log.INFO, "read dir " + pathName)

        """
        (rawDirlistSearch) = setting.Setting.getListValue("rawsearch/rawDirlistSearch")

        i = 0
        for rawDirlistSearchElem in rawDirlistSearch :
            rawDirlistSearch[i] = rawDirlistSearch[i].replace( "[rawdir]" , pathName)
            if not rawDirlistSearch[i].endswith("/") :
                rawDirlistSearch[i] = rawDirlistSearch[i] + "/"
            i = i + 1

        (rawInfoExtSearch) = setting.Setting.getListValue("rawsearch/rawInfoExtSearch")
        """

        fileList = []
        if os.path.isdir(pathName):
            try:
                files = os.listdir(pathName)
                log.Log.debug(log.INFO, "read dir start : " + str(onlyfilesCount) + " files.")
                for fileName in files:

                    QApplication.processEvents()
                    if self.processCancel == True :
                        return fileList

                    if os.path.isfile(pathName + "/" + fileName):
                        try :

                            newFileItem = fileItem(self, pathName, fileName, self.fileExt, getExif=True)
                            #print ("found file "+newFileItem.fname+" key=" + str(newFileItem.key))
                            fileList.append(newFileItem)


                        except Exception as e:
                            log.Log.debug(log.ERROR, "Error in fileItem : %s:" % e.args[0] + " / pathName=" + pathName)

                        barVal = barVal + 1
                        self.progressBar.setValue( barVal)

                log.Log.debug(log.INFO, "read dir done : " + str(len(fileList)) + " files.")

            except Exception as e:
                log.Log.debug(log.ERROR, "Error %s:" % e.args[0] + " / pathName=" + pathName)

            finally:
                self.progressBar.hide()

        return fileList


    def getFileItemListSelected( self) :
        """
        Retourne la liste des images selectionnées par l'utilisateur.
        Liste contenant des éléments de type fileItem
        """
        fileItemList = []

        if self.MainWindow.nav_stackedWidget.currentIndex() == STACKED_VIEW_ICON :
            for model_index in self.MainWindow.nav_iconview.selectionModel().selectedRows():
                source_index = self.table_proxy_model.mapToSource(model_index)  # Le même est partagé entre les deux vues
                currentKeyValue = self.tableData.tableModel.getDataLineKey( source_index.row() )
                newFileItem = self.tableData.tableModel.getFileItemFromKey( currentKeyValue )
                fileItemList.append( newFileItem )
        else :
            for model_index in self.MainWindow.nav_tableview.selectionModel().selectedRows():
                source_index = self.table_proxy_model.mapToSource(model_index)  # Le même est partagé entre les deux vues
                currentKeyValue = self.tableData.tableModel.getDataLineKey( source_index.row() )
                newFileItem = self.tableData.tableModel.getFileItemFromKey( currentKeyValue )
                fileItemList.append( newFileItem )

        return fileItemList

    def viewSetLabelInfo( self) :
        """
        Formatage et affichage des textes d'information, affichés sous les vues
        """

        self.MainWindow.nav_tableview_label.setText( str(len(self.fileList)) + " fichiers." + "  - Dont : " +
             "<b>" + str( self.tableData.getFileCountByExt("Nef")) + "</b>" + " Nef, " +
             "<b>" + str( self.tableData.getFileCountByExt("pp3")) + "</b>" + " Pp3, " +
             "<b>" + str( self.tableData.getFileCountByExt("jpg")) + "</b>" + " Jpg"
        )

        self.MainWindow.nav_iconview_label.setText( str(len(self.fileList)) + " fichiers." + "  - Dont : " +
             "<b>" + str( self.tableData.getFileCountByExt("Nef")) + "</b>" + " Nef, " +
             "<b>" + str( self.tableData.getFileCountByExt("pp3")) + "</b>" + " Pp3, " +
             "<b>" + str( self.tableData.getFileCountByExt("jpg")) + "</b>" + " Jpg"
        )



    def onChangeTab(self, tabIndex ) :
        """
        Evènement : changement d'onglet
        """
        if tabIndex == TAB_IMAGE :
            newFileItem = None
            fileItemList = self.getFileItemListSelected ()
            if len(fileItemList) > 0  :
                newFileItem = self.tableData.tableModel.getFileItemFromKey( fileItemList[0].key ) # Premier de la selection
                self.showImage( newFileItem )


    def showImage(self, newFileItem = None) :
        """
        Prepare l'affichage lors de la selection d'un nouveau dossier dans le treeview
        """
        if newFileItem is not None and newFileItem.ext in self.fileExt :
            QApplication.setOverrideCursor(Qt.WaitCursor)
            log.Log.debug(log.INFO, "Affichage image : %s" % newFileItem.filePath + "/" + newFileItem.fname)
            self.MainWindow.imageView.setUpDisplay( newFileItem.filePath , newFileItem.fname  , self.table_proxy_model.getDataListSorted(self.fileExt) )
            QApplication.restoreOverrideCursor()
        else :
            log.Log.debug(log.INFO, "Affichage image : (dossier sans images)"  )
            self.MainWindow.imageView.setUpDisplay( None, None, None )


    def menuFavInit (self) :
        """
        Initialisation du menu de dossiers favoris
        """
        #print ("      init favoris dir construction :")
        #self.favDirList.dump()
        dirList = self.favDirList.getAll()
        self.MainWindow.menuDossiersFavoris.clear()
        i=0
        for dirElem in dirList :
            #print ( "Add to menu fav, dir : #" + str(i) + " :" + dirElem["key"] + " (" + dirElem["value"] + ")" )

            newAction = QAction( dirElem["key"], self)
            #newAction.setShortcut('Ctrl+Q')
            newAction.setStatusTip(dirElem["value"])
            newAction.setToolTip(dirElem["value"])
            #newAction.triggered.connect(qApp.quit)
            newAction.triggered.connect(lambda bVal, pathNav=dirElem["key"] : self.treeViewOpenPath(pathNav))

            self.MainWindow.menuDossiersFavoris.addAction(newAction)
            i= i + 1

    def menuHistoInit (self) :
        """
        Initialisation du menu de l'historique des dossiers
        """

        #self.historyDirList.dump()
        dirList = self.historyDirList.getAll()
        self.MainWindow.menuDossiersRecents.clear()
        i=0
        for dirElem in dirList :
            #print ( "Add to menu hist, dir : #" + str(i) + " :" + dirElem["key"] + " (" + dirElem["value"] + ")" )

            newAction = QAction( dirElem["key"], self)
            #newAction.setShortcut('Ctrl+Q')
            newAction.setStatusTip(dirElem["value"])
            newAction.setToolTip(dirElem["value"])
            #newAction.triggered.connect(qApp.quit)
            newAction.triggered.connect(lambda bVal, pathNav=dirElem["key"] : self.treeViewOpenPath(pathNav))

            self.MainWindow.menuDossiersRecents.addAction(newAction)
            i= i + 1

    def menuInit(self) :
        """
        Mise en place des connecteurs menus
        """
        self.MainWindow.actionQuit.triggered.connect(self.close)

        self.MainWindow.actionImportExternal.triggered.connect(self.onActionImportExternal)
        self.MainWindow.actionSearchDuplicate.triggered.connect(self.onActionSearchDuplicate)

        self.MainWindow.actionDisplayToolBar.triggered.connect(self.onActionDisplayToolBar)
        if self.MainWindow.toolBarMain.isHidden is False :
            self.MainWindow.actionDisplayToolBar.setChecked(True)
        else :
            self.MainWindow.actionDisplayToolBar.setChecked(True)

        self.MainWindow.actionDisplayModuleBar.triggered.connect(self.onActionDisplayModuleBar)
        if self.toolbarModules.isHidden() is False :
            self.MainWindow.actionDisplayModuleBar.setChecked(True)
        else :
            self.MainWindow.actionDisplayModuleBar.setChecked(True)

        self.MainWindow.actionVue_liste.triggered.connect(self.onActionViewAslist)
        self.MainWindow.actionVue_icons.triggered.connect(self.onActionViewAsIcon)

        self.MainWindow.actionIcone_zoom_minus.triggered.connect(self.onActionIconZoomMinus)
        self.MainWindow.actionIcone_zoom_reset.triggered.connect(self.onActionIconZoomReset)
        self.MainWindow.actionIcone_zoom_plus.triggered.connect(self.onActionIconZoomPlus)

    def onActionDisplayToolBar(self) :
        """
        Afficher ou cacher la barre d'outils
        """
        if self.MainWindow.toolBarMain.isHidden() is True :
            self.MainWindow.toolBarMain.show()
            self.MainWindow.actionDisplayToolBar.setChecked(True)
        else :
            self.MainWindow.toolBarMain.hide()
            self.MainWindow.actionDisplayToolBar.setChecked(False)

    def onActionDisplayModuleBar(self) :
        """
        Afficher ou cacher la barre des modules
        """
        if self.toolbarModules.isHidden() is True :
            self.toolbarModules.show()
            self.MainWindow.actionDisplayModuleBar.setChecked(True)
        else :
            self.toolbarModules.hide()
            self.MainWindow.actionDisplayModuleBar.setChecked(False)

    def pluginInit(self) :
        """
        Initialisation des modules au chargement de l'application
        """
        pluginList = modules( None )
        pluginList.load_modules_from_path('modules')

        for  pluginElem in pluginList.moduleList :

            log.Log.debug(log.INFO, "Init plugin : " + pluginElem.className)
            class_name = pluginList.load_class_from_name(pluginElem.className)
            obj = class_name()

            if not obj.isActive( ) :
                log.Log.debug(log.WARNING, "Ce plugin : " + pluginElem.className + " : est désactivé.")

            if obj.getInfo(PLUGIN_MENU_TOOLBAR) == True :

                log.Log.debug(log.INFO, "Plugin ajouté dans la toolbar.")

                if self.toolbarModules is None :
                    self.toolbarModules = self.addToolBar('Modules')
                    self.toolbarModules.setObjectName ("Modules tool bar")

                moduleAction = QAction(QIcon(pluginElem.iconPath), pluginElem.menuLib, self)
                moduleAction.setShortcut(pluginElem.shortCut)
                moduleAction.setStatusTip(pluginElem.menuStatutBarToolTip)
                moduleAction.setToolTip(pluginElem.menuTooltip)
                import functools
                moduleAction.triggered.connect( functools.partial(self.onActionPlugin , pluginElem.className ) )

                if pluginElem.active == False :
                    moduleAction.setEnabled(False)

                self.toolbarModules.addAction(moduleAction)


            if obj.getInfo(PLUGIN_MENU_UPPER) is True :
                log.Log.debug(log.INFO, "Plugin ajouté dans le menu principal.")
                pluginMenu = self.MainWindow.menuModules
                moduleAction = QAction(QIcon(pluginElem.iconPath), pluginElem.menuLib, self)
                moduleAction.setShortcut(pluginElem.shortCut)
                moduleAction.setStatusTip(pluginElem.menuStatutBarToolTip)
                moduleAction.setToolTip(pluginElem.menuTooltip)

                import functools
                moduleAction.triggered.connect( functools.partial(self.onActionPlugin , pluginElem.className ) )

                if pluginElem.active is False :
                    moduleAction.setEnabled(False)

                pluginMenu.addAction(moduleAction)


    def onActionPlugin(self, className ) :
        """
        Evènement : Execution d'un module par l'utilisateur
        """
        fileItemList = self.getFileItemListSelected ()

        try :
            log.Log.debug(log.INFO, "Execute plugin : " + className )
            pluginList = modules( None )
            pluginList.load_modules_from_path('modules')
            class_name = pluginList.load_class_from_name(className)
            obj = class_name()
            if obj.isActive( ) :
                retVal = obj.main( self, self.MainWindow.treeView.getPathSelected(), fileItemList, self.fileExt )
                log.Log.debug(log.INFO, "Execute plugin. retval = : " + str(retVal) )
                if obj.error != "" :
                    log.Log.debug(log.ERROR, "Erreur d'execution du plugin :\n\n" + obj.error)
                    QMessageBox.information( self, "Résultat de l'execution du plugin", "Erreur d'execution du plugin :\n\n" + obj.error  , QMessageBox.Ok )

            else :
                log.Log.debug(log.INFO, "Init plugin : " + className + " : est désactivé.")

        except Exception as e:
            exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
            formatted_lines = traceback.format_exc().splitlines()
            print(formatted_lines[0])
            log.Log.debug(log.ERROR, "Error d'execution du plugin : %s" % e.args[0] )
            QMessageBox.critical(self, "Error d'execution du plugin","Error d'execution du plugin : %s" % e.args[0] + "\n\n" + repr(traceback.format_exception(exceptionType, exceptionValue,exceptionTraceback)), QMessageBox.Ok )

        finally:
            pass


    def mainwindowShow(self):
        """
        Affiche de la fenêtre principale
        """
        log.Log.debug(log.INFO, "Main view : showing window.")
        self.MainWindow.show()

    def mainWindowHide(self):
        """
        Cache la fenêtre principale
        """
        self.MainWindow.hide()

    def mainwindowSetTitle(self, titleMessage, numberOfFiles):
        """
        Affiche le titre de lafenêtre principale
        """
        self.setWindowTitle(titleMessage + " (" + str(numberOfFiles) + ")")

    def firstDisplayInit(self):
        """
        Premier affichage au chargement de l'application
        """
        log.Log.debug(log.INFO, "Main view : first display.")

        # Columns width to fit contents
        self.MainWindow.nav_tableview.horizontalHeader().setStretchLastSection(True)
        self.MainWindow.nav_tableview.resizeColumnToContents(COL_fname)
        self.MainWindow.nav_tableview.clearSelection()

        self.MainWindow.treeView.expanded.connect(self.onTreeNodeExpanded)

        self.stateRestore()
        dirIndex = self.dir_model.index( self.MainWindow.treeView.getPathSelected() )
        self.MainWindow.treeView.scrollTo(dirIndex)

        # Sort
        self.table_proxy_model.setSortRole(Qt.InitialSortOrderRole)
        self.table_proxy_model.invalidate()
        self.MainWindow.nav_tableview.sortByColumn(COL_fname, Qt.AscendingOrder)  # AscendingOrder / DescendingOrder

        # Tabs label infos
        self.viewSetLabelInfo()

        # Afficher la première image du dossier
        self.showImage( self.table_proxy_model.geFirstDataItemSorted(self.fileExt)  )

        self.MainWindow.nav_tableview.show()
        self.MainWindow.nav_iconview.show()

    def closeEvent(self, event):

        if self.startupMode == STARTUPMODE_GUI:
            self.closeEventGui(event)
        else:
            self.closeEventSystray(event)

    def closeEventSystray(self, event):

        if self.parent.confirmOnHide == "1":

            messCloseAppTitle = self.tr("Confirmez", "Reduire l'application : titre fenetre")
            messCloseApp = self.tr("Reduire l'application ?", "Fermer application : message")

            box = QMessageBox()
            box.setIcon(QMessageBox.Question)
            box.setWindowTitle(messCloseAppTitle)
            box.setText(messCloseApp)
            box.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
            buttonY = box.button(QMessageBox.Yes)
            # buttonY.setText('Oui')
            buttonN = box.button(QMessageBox.No)
            # buttonN.setText('Non')
            box.exec_()

            if box.clickedButton() == buttonY:

                self.parent.saveSetting()
                self.stateSave()

                log.Log.debug(log.INFO, "Main controler : hide app.")
            elif box.clickedButton() == buttonN:
                event.ignore()
        else:
            log.Log.debug(log.INFO, "Main controler : hide app.")
            self.parent.saveSetting()
            self.stateSave()

    def closeEventGui(self, event):

        if self.parent.confirmOnQuit == "1":

            messCloseAppTitle = self.tr("Confirmez", "Fermer application : titre fenetre")
            messCloseApp = self.tr("Fermer l'application ?", "Fermer application : message")

            box = QMessageBox()
            box.setIcon(QMessageBox.Question)
            box.setWindowTitle(messCloseAppTitle)
            box.setText(messCloseApp)
            box.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
            buttonY = box.button(QMessageBox.Yes)
            # buttonY.setText('Oui')
            buttonN = box.button(QMessageBox.No)
            # buttonN.setText('Non')
            box.exec_()

            if box.clickedButton() == buttonY:
                self.parent.saveSetting()
                self.stateSave()

                log.Log.debug(log.INFO, "Main controler : close app.")
                event.accept()
            elif box.clickedButton() == buttonN:
                event.ignore()

        else:
            log.Log.debug(log.INFO, "Main controler : close app.")
            self.parent.saveSetting()
            self.stateSave()

            event.accept()

    def onActionImportExternal(self) :
        """
        Evènement menu : appel de l'option "Importation"
        """
        self.importExternalDialog = importExternal_controler.importExternal(self, self.MainWindow.treeView.getPathSelected(), self.favDirList)
        self.importExternalDialog.dialogShow()


    def onActionSearchDuplicate(self) :
        """
        Evènement menu : appel de l'option "Recherche de doublons"
        """
        self.SearchDuplicateDialog = duplicate_controler.searchDuplicate(self, self.MainWindow.treeView.getPathSelected(), self.favDirList, self.getFileItemListSelected () )
        self.SearchDuplicateDialog.dialogShow()


class fileModelAsTable(QAbstractTableModel):
    """
    Modèle de données des vues fichier détails et icones

    Qt.UserRole   : retourne la donnée brute (sans transformation)
    Qt.UserRole+1 : retourne la donnée pour le tri
    """
    def __init__(self, datain, parent=None):

        QAbstractTableModel.__init__(self, parent)
        self.arraydata = datain

        self.headerdata = setting.Setting.getListValue("listview/colHeaderNames")
        self.colFilterList = {}

        self.iconSizeWidth = int(setting.Setting.getValue("iconview/iconSizeWidth", "100"))
        self.iconSizeHeight = int(setting.Setting.getValue("iconview/iconSizeHeight", "100"))
        self.cellWidth = int(setting.Setting.getValue("iconview/cellWidth", "164"))
        self.cellHeight = int(setting.Setting.getValue("iconview/cellHeight", "128"))
        self.cellPadding = float(setting.Setting.getValue("iconview/cellPadding", "1.2"))

        self.iconSizeWidthIni = self.iconSizeWidth
        self.iconSizeHeightIni = self.iconSizeHeight
        self.cellWidthIni = self.cellWidth
        self.cellHeightIni = self.cellHeight

        self.thumbQuality  = int(setting.Setting.getValue("iconview/thumbQuality", "30"))
        self.thumbCacheDir  = setting.Setting.getValue("iconview/thumbCacheDir", "./cache")

        self.c_date_format_out = setting.Setting.getValue("listview/c_date_format_out", "dddd d MMMM yyyy hh:mm")
        self.m_date_format_out = setting.Setting.getValue("listview/c_date_format_out", "dddd d MMMM yyyy hh:mm")
        self.a_date_format_out = setting.Setting.getValue("listview/c_date_format_out", "dddd d MMMM yyyy hh:mm")

        self.setMode = 1

    def setColFilter (self , colIndex , filterActive = True) :
        self.colFilterList[colIndex] = filterActive

    def unSetColFilter (self , colIndex ) :
        if colIndex in self.colFilterList :
            del( self.colFilterList[colIndex])

    def zoom( self, zoomFactor) :

        self.layoutAboutToBeChanged.emit()

        self.iconSizeWidth = round(self.iconSizeWidth * zoomFactor)
        self.iconSizeHeight = round(self.iconSizeHeight * zoomFactor)

        font = QFont( setting.Setting.getValue("iconview/fontName", "Arial") , int(setting.Setting.getValue("iconview/fontSize", "10")))
        fm = QFontMetrics(font)
        #legendPixelsWide = fm.width(  setting.Setting.getValue("iconview/iconLabelFormat", "XXX XXXX XXXX") )
        legendPixelsHigh = fm.height()

        self.cellHeight = legendPixelsHigh + self.iconSizeHeight
        self.cellWidth = self.iconSizeWidth * self.cellPadding

        self.layoutChanged.emit()

    def zoomReset( self ) :

        self.layoutAboutToBeChanged.emit()

        self.iconSizeWidth = int(setting.Setting.getValue("iconview/iconSizeWidthInitial", "140"))
        self.iconSizeHeight = int(setting.Setting.getValue("iconview/iconSizeHeightInitial", "110"))

        self.cellHeight = int(setting.Setting.getValue("iconview/cellHeightInitial", "110"))
        self.cellWidth = int(setting.Setting.getValue("iconview/cellWidthInitial", "110"))

        self.layoutChanged.emit()

    def setViewMode(self, mode) :
        """
        Stockage du mode actuel de vue  : list (qtableview) or icons (qlistview)
        """
        self.setMode = mode

    def getViewMode(self) :
        """
        Retourne le mode actuel de vue  : list (qtableview) or icons (qlistview)
        """
        return self.setMode


    def getDataLineKey(self, row ) :
        """
        Retourne l'id (key) d'une ligne de données
        """
        if row >= 0 and row < self.rowCount(self) :
            return str(self.arraydata[row].key)
        else :
            return None

    def getDataByRowNumber(self, rowNumber) :
        return self.arraydata[ rowNumber ]

    def rowCount(self, parent):
        return len(self.arraydata)

    def columnCount(self, parent):
        if len(self.headerdata) > 0:
            return len(self.headerdata)
        return 0

    def data(self, index, role):

        if not index.isValid():
            return QVariant()

        if index.column() == COL_selection:
            return QVariant()

        if index.column() == COL_thumb:

            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].iconLabel

            if role == Qt.UserRole:
                return self.arraydata[index.row()].ino

            if role == Qt.UserRole+1:
                return self.arraydata[index.row()].fname

            if role == Qt.DecorationRole:

                iconW = self.iconSizeWidth
                iconH = self.iconSizeHeight

                linkAttr = ""
                if self.arraydata[index.row()].islink is True :
                    linkAttr="_l"

                if os.path.isfile( self.thumbCacheDir + "/" + self.arraydata[index.row()].key + linkAttr + ".JPG"    ) :
                    pixmap = QPixmap(  "data/thumbs/" + self.arraydata[index.row()].key + linkAttr + ".JPG" , "JPG" )

                    if self.arraydata[index.row()].isimmutable is True  :
                        pixmap_icon = QPixmap( QImage("icon/icon_lock.png") )
                        pixmap_icon = pixmap_icon.scaled( QSize(18, 18), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.FastTransformation )
                        painter = combined = QPainter(pixmap)
                        painter.setOpacity(0.8)
                        painter.drawPixmap(QPoint(pixmap.width() - 18,  0  ), pixmap_icon)
                        painter.end()

                    pixmap_resized = pixmap.scaled( QSize(iconH, iconW), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.FastTransformation )

                else :
                    if os.path.isfile( "icon/ext_" + self.arraydata[index.row()].ext + ".png"  ) :
                        pixmap = QPixmap("icon/ext_" + self.arraydata[index.row()].ext + ".png" , "PNG")
                        pixmap_resized = pixmap.scaled( QSize(iconH*0.4, iconW*0.4), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.FastTransformation )
                    else :
                        pixmap = QPixmap("icon/ext_generic.png" , "PNG")
                        pixmap_resized = pixmap.scaled( QSize(iconH*0.4, iconW*0.4), QtCore.Qt.KeepAspectRatio,  QtCore.Qt.FastTransformation )

                return pixmap_resized

            if role == Qt.SizeHintRole:
                cellH = self.cellWidth
                cellW = self.cellHeight

                return QSize( cellW, cellH )

        if index.column() == COL_key:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].key
            if role == Qt.UserRole:
                return self.arraydata[index.row()].key

        if index.column() == COL_ino:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].ino
            if role == Qt.UserRole:
                return self.arraydata[index.row()].ino

        if index.column() == COL_fname:

            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].fname
            if role == Qt.UserRole:
                return self.arraydata[index.row()].fname
            if role == Qt.TextAlignmentRole:
                return Qt.AlignLeft | Qt.AlignVCenter

        if index.column() == COL_dev:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].dev
            if role == Qt.UserRole:
                return self.arraydata[index.row()].dev

        if index.column() == COL_fsize:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].fsize
            if role == Qt.UserRole:
                return self.arraydata[index.row()].fsize

        if index.column() == COL_fsizeh:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].fsizeh
            if role == Qt.UserRole:
                return self.arraydata[index.row()].fsizeh
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].fsize

        if index.column() == COL_ext:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].ext
            if role == Qt.UserRole:
                return self.arraydata[index.row()].ext

        if index.column() == COL_c_date:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].c_date.strftime( self.c_date_format_out )
            if role == Qt.UserRole:
                return self.arraydata[index.row()].c_date_stamp
            if role == Qt.UserRole+1:
                return self.arraydata[index.row()].c_date_stamp

        if index.column() == COL_m_date:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].m_date.strftime(  self.m_date_format_out )
            if role == Qt.UserRole:
                return self.arraydata[index.row()].m_date_stamp
            if role == Qt.UserRole+1:
                return self.arraydata[index.row()].m_date_stamp

        if index.column() == COL_a_date:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].a_date.strftime( self.a_date_format_out)
            if role == Qt.UserRole:
                return self.arraydata[index.row()].a_date_stamp
            if role == Qt.UserRole+1:
                return self.arraydata[index.row()].a_date_stamp

        if index.column() == COL_islink:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].islink
            if role == Qt.UserRole:
                return self.arraydata[index.row()].islink
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].islink

        if index.column() == COL_linktarget:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].linktarget
            if role == Qt.UserRole:
                return self.arraydata[index.row()].linktarget
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].linktarget

        if index.column() == COL_access_w:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].access_w
            if role == Qt.UserRole:
                return self.arraydata[index.row()].access_w
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].access_w

        if index.column() == COL_mime:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].mime
            if role == Qt.UserRole:
                return self.arraydata[index.row()].mime
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].mime

        if index.column() == COL_ftype:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].ftype
            if role == Qt.UserRole:
                return self.arraydata[index.row()].ftype
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].ftype

        if index.column() == COL_crc32:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].crc32
            if role == Qt.UserRole:
                return self.arraydata[index.row()].crc32
            if role == Qt.UserRole + 1 :
                return self.arraydata[index.row()].crc32

        if index.column() == COL_sha1:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].sha1
            if role == Qt.UserRole:
                return self.arraydata[index.row()].sha1
            if role == Qt.UserRole +1 :
                return self.arraydata[index.row()].sha1

        if index.column() == COL_exifinfos:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].exifinfos
            if role == Qt.UserRole:
                return self.arraydata[index.row()].exifinfos
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].exifinfos

        if index.column() == COL_isimmutable:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].isimmutable
            if role == Qt.UserRole:
                return self.arraydata[index.row()].isimmutable
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].isimmutable

        if index.column() == COL_width:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].width
            if role == Qt.UserRole:
                return self.arraydata[index.row()].width
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].width

        if index.column() == COL_height:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].height
            if role == Qt.UserRole:
                return self.arraydata[index.row()].height
            if role == Qt.UserRole + 1:
                return self.arraydata[index.row()].height

        if index.column() == COL_trtstatus:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].trtStatus
            if role == Qt.UserRole:
                return self.arraydata[index.row()].trtStatus

        if index.column() == COL_trtlib:
            if role == Qt.DisplayRole:
                return self.arraydata[index.row()].trtLib
            if role == Qt.UserRole:
                return self.arraydata[index.row()].trtLib

        return QVariant()


    def headerData(self, col, orientation, role):

        if self.colFilterList is not None :
            for colFilterKey in self.colFilterList :
                if col == colFilterKey :
                    if orientation == Qt.Horizontal and role == Qt.DecorationRole:
                        return QPixmap("icon/filter_blue.png")

        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return QVariant(self.headerdata[col])

        return QVariant()

    def sort(self, Ncol, order):
        try:
            self.layoutAboutToBeChanged.emit()
            self.arraydata = sorted(self.arraydata, key=operator.itemgetter(Ncol))
            if order == Qt.DescendingOrder:
                self.arraydata.reverse()
            self.layoutChanged.emit()

        except TypeError as e:
            print('[*] Sort on empty column')

    def refreshData(self):
        """
        Actualise l'affichage
        """
        self.layoutAboutToBeChanged.emit()
        self.layoutChanged.emit()

    def loadData(self, fileData):
        """
        Charge les données et actualise l'affichage
        """
        self.layoutAboutToBeChanged.emit()
        self.arraydata = fileData
        self.layoutChanged.emit()

    def removeDataByKey(self, idElem):
        """
        Supprime un fichier et actualise l'affichage
        """

        self.layoutAboutToBeChanged.emit()
        idx=0
        for itemElem in self.arraydata:
            if itemElem.key == idElem:
                del self.arraydata[idx]
            idx=idx+1
        self.layoutChanged.emit()

    def getFileItemFromKey(self, idElem ):
        """
        Retourne le Fileitem pour un id (key) donné.
        """
        for itemElem in self.arraydata:
            if itemElem.key == idElem:
                return itemElem
        return []


class tableviewSortProxy(QSortFilterProxyModel):
    """
    Implementation de tris spécifiques, pour le modèle des données fichiers
    """

    def __init__(self, parent=None):

        super(tableviewSortProxy, self).__init__(parent)

        self.setFilterCaseSensitivity(Qt.CaseInsensitive)
        self.setSortCaseSensitivity(Qt.CaseInsensitive)
        self.setSortLocaleAware(True)

        self.colFilterList = {}

    def lessThan(self, left, right):

        col = left.column()

        dataleft = left.data()
        dataright = right.data()

        if col == COL_fname:
            dataleft = self.remove_accents(dataleft).lower()
            dataright = self.remove_accents(dataright).lower()

        elif col == COL_fsizeh:
            dataleft = self.sourceModel().data(left, Qt.UserRole + 1)
            dataright = self.sourceModel().data(right, Qt.UserRole + 1)

        elif col == COL_c_date:
            dataleft = self.sourceModel().data(left, Qt.UserRole + 1)
            dataright = self.sourceModel().data(right, Qt.UserRole + 1)

        elif col == COL_a_date:
            dataleft = self.sourceModel().data(left, Qt.UserRole + 1)
            dataright = self.sourceModel().data(right, Qt.UserRole + 1)

        elif col == COL_m_date:
            dataleft = self.sourceModel().data(left, Qt.UserRole + 1)
            dataright = self.sourceModel().data(right, Qt.UserRole + 1)

        return dataleft < dataright

    def remove_accents(self, input_str):
        import unicodedata
        import platform
        if platform.system() == "Windows":
            return unicodedata.normalize('NFD', input_str).encode('ascii', 'ignore')
        else:
            nkfd_form = unicodedata.normalize('NFKD', str(input_str))
            return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])

    def getColFilter (self , colIndex) :
        if colIndex in self.colFilterList :
            return self.colFilterList[colIndex]
        else :
            return ""

    def setColFilter (self , colIndex , strRegFilter) :
        self.colFilterList[colIndex] = strRegFilter

    def unSetColFilter (self , colIndex ) :
        if colIndex in self.colFilterList :
            del( self.colFilterList[colIndex])

    def filterAcceptsRow(self, source_row_number, source_parent):

        for colFilterKey  in self.colFilterList :
            indexData = self.sourceModel().index(source_row_number, colFilterKey)
            valueColCurrent = self.sourceModel().data(indexData, Qt.UserRole)
            expression = QRegExp(self.colFilterList[colFilterKey])
            index = expression.indexIn(str(valueColCurrent))
            if index < 0:
                return False

        return QSortFilterProxyModel.filterAcceptsRow(self, source_row_number, source_parent)



    def dumpSortedData( self ) :
        """
        Dump dans la console des données triées
        Appel : self.table_proxy_model.dumpSortedData()
        """
        print ("data sorted :::::::::")
        row = 0
        while row < self.rowCount() :

            indexData = self.index(row, COL_ino)
            ino = self.data(indexData, Qt.UserRole)

            indexData = self.index(row, COL_fname)
            fname = self.data(indexData, Qt.UserRole)

            print ( str(row) + " : #" + str(ino) + " -> " + fname )
            row = row + 1

    def getDataListSorted(self , fileExt= [] ) :
        """
        Retourne la liste des données triées
        """
        self.itemListSorted = []

        row = 0
        while row < self.rowCount() :

            indexKey = self.index(row, COL_key)
            rowKey = self.data(indexKey, Qt.UserRole)
            indexItem = self.sourceModel().getFileItemFromKey(rowKey)
            if indexItem.ext in fileExt :
                self.itemListSorted.append( indexItem )

            row = row + 1

        return self.itemListSorted

    def geFirstDataItemSorted( self , fileExt= []  ):
        """
        Retourne le premier Fileitem image dans le dossier
        """
        indexItem = None
        row = 0
        while row < self.rowCount() :

            indexKey = self.index(row, COL_key)
            rowKey = self.data(indexKey, Qt.UserRole)
            indexItem = self.sourceModel().getFileItemFromKey(rowKey)
            if indexItem.ext in fileExt :
                break
            row = row + 1

        return indexItem

class tableData( object ) :

    def __init__(self, parent):

        self.parent = parent
        self.clear()

    def setProxyModel(self, table_proxy_model) :
        self.table_proxy_model = table_proxy_model

    def clear(self) :
        self.tableModel = None
        self.table_proxy_model = None

    def setTableModel( self, tablemodel) :
        self.tableModel = tablemodel

    def getFileCountByExt( self, extSearch, caseSensitive = False ) :

        proxyModel = self.table_proxy_model
        if caseSensitive is False :
            extSearch = extSearch.upper()

        rowRet = 0
        row = 0
        while row < self.table_proxy_model.rowCount() :

            indexData = self.table_proxy_model.index(row, COL_ext)
            ext = self.table_proxy_model.data(indexData, Qt.UserRole)

            if caseSensitive is False :
                if ext.upper() == extSearch :
                    rowRet = rowRet + 1
            else :
                if ext == extSearch :
                    rowRet = rowRet + 1


            row = row + 1

        return rowRet

class progressBar( object ) :
    """
    Barre de progression des traitements : chargement des images du dossier, construction des vignettes
    """
    def __init__(self, parent, progressBarPtr ):
        self.parent = parent
        self.progressBarPtr = progressBarPtr

        self.clear()

    def clear(self) :
        self.progressBarPtr.setValue(0)
        self.progressBarPtr.setFormat("%p%")

    def setValue( self, value) :
        self.progressBarPtr.setValue(value)

    def setMaximum( self, maximum) :
        self.progressBarPtr.setMaximum(maximum)

    def show( self ) :
        self.progressBarPtr.show()
        self.progressBarPtr.reset()
        self.progressBarPtr.setFormat("%p%")

    def hide( self ) :
        self.progressBarPtr.hide()
        self.progressBarPtr.reset()
        self.progressBarPtr.setFormat("%p%")

    def forward( self, value = 1 ) :
        self.progressBarPtr.setValue( self.progressBarPtr.value() + value )
        self.progressBarPtr.setTextVisible(True)
        self.progressBarPtr.setFormat( str( self.progressBarPtr.value() ) + "/" + str(self.progressBarPtr.maximum())  )
        self.progressBarPtr.setAlignment(Qt.AlignCenter)


class folderList( object ) :
    """
    Gestion des listes de dossiers : favoris, historique.
    """
    def __init__(self, parent , maxValues , iniKey , iniKeyMax ):
        self.parent = parent
        self.maxValues = 10
        self.folderlist = []
        self.iniKey = iniKey
        self.iniKeyMax = iniKeyMax

    def add( self, dirPath, dirDes = "" ) :
        """
        Ajout en début de liste
        """
        self.delByKey( dirPath )
        newItem = { "key" : dirPath , "value"  : dirDes }
        self.folderlist.insert(0, newItem)
        if len (self.folderlist) > self.maxValues :
            del self.folderlist[-1]

    def remove( self, dirPath) :
        self.delByKey( dirPath )

    def setDes (self, dirPath, dirDes ) :
        i=0
        for favElem in self.folderlist :
            #print ( "#" + str(i) + " :" + favElem["key"] + " (" + favElem["value"] + ")" )
            #if favElem["key"]  = dirPath :
            i= i + 1

    def getAll( self ) :
        return self.folderlist

    def save( self ) :
        setting.Setting.setValue( self.iniKeyMax, self.maxValues)
        setting.Setting.setKeyValues( self.iniKey, self.folderlist)

    def restore( self ) :
        self.maxValues = int (setting.Setting.getValue(self.iniKeyMax, "10"))
        self.folderlist = setting.Setting.getKeyValues(self.iniKey)

    def delByKey( self, keyTest ) :
        i=0
        for favElem in self.folderlist :
            if favElem["key"]  == keyTest :
                del self.folderlist [i]
                return True
            i= i + 1
        return False

    def keyExist( self, keyTest ) :
        i=0
        for favElem in self.folderlist :
            if favElem["key"]  == keyTest :
                return True
            #print ( "#" + str(i) + " :" + favElem["key"] + " (" + favElem["value"] + ")" )
            i= i + 1
        return False

    def dump( self ) :
        i=0
        for favElem in self.folderlist :
            print ( "#" + str(i) + " :" + favElem["key"] + " (url= " + favElem["value"] + ")" )
            i= i + 1

class myQFileSystemModel(QFileSystemModel):

    def __init__(self, *args, **kwargs):
        super(myQFileSystemModel, self).__init__(*args, **kwargs)
        self.condition = None
        self.favDirList = []
        self.txtColor = "#AB0000"
        self.bgColor = "#050505"

    def setFavDirColor ( self, txtColor, bgColor ) :
        self.txtColor = txtColor
        self.bgColor = bgColor

    def setFavDirList( self, favDirList ) :
        self.favDirList = []
        for favElem in favDirList :
            self.favDirList.append( favElem["key"] )

    def data(self, index, role=Qt.DisplayRole):
        """
        Qt.DecorationRole   1   The data to be rendered as a decoration in the form of an icon. (QColor, QIcon or QPixmap)
        Qt.EditRole     2   The data in a form suitable for editing in an editor. (QString)
        Qt.ToolTipRole
        https://stackoverflow.com/questions/29732874/what-is-itemdatarole-number-31-pyside-pyqt
        """
        #text = index.data(QtCore.Qt.DisplayRole)

        if role == Qt.TextColorRole and index.column() == 0 :
            if self.isFav( self.filePath(index) ) is True  :
                return QtGui.QColor( self.txtColor)
            return QtGui.QColor("#000000")

        if role == Qt.BackgroundColorRole and index.column() == 0  :
            if self.isFav( self.filePath(index) ) is True  :
                return QtGui.QColor( self.bgColor )
            return QtGui.QColor("#FFFFFF")

        return super(myQFileSystemModel, self).data(index, role)

    def isFav( self, pathToCheck  ) :
        for favElem in self.favDirList :
            if favElem == pathToCheck :
                return True
        return False
