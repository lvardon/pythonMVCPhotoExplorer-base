#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Gestion des filigrammes images
"""

from PyQt5.QtWidgets import QApplication, QMessageBox, QLabel, QWidget, QPushButton, QComboBox, QLineEdit, QGridLayout, QDialog, QFileDialog,QFrame
from PyQt5.QtGui import QPixmap, QImage, QColor,  QDoubleValidator, QRegExpValidator, QValidator

from PyQt5.QtCore import QDir, Qt, QRegExp
from PyQt5.QtGui import QImage, QPainter, QPalette, QPixmap
from PyQt5.QtWidgets import (QAction, QApplication, QFileDialog, QLabel, QMainWindow, QMenu, QMessageBox, QScrollArea, QSizePolicy)

from applib.module import PLUGIN_ACTIVE,PLUGIN_NAME,PLUGIN_DESCRIPTION,PLUGIN_VERSION,PLUGIN_MENULIB,PLUGIN_MENUTOOLTIP,PLUGIN_STATUTBARTOOLTIP, PLUGIN_MENU_UPPER,PLUGIN_MENU_TOOLBAR,PLUGIN_MENU_ICONVIEW,PLUGIN_MENU_DETAILVIEW,PLUGIN_SHORTCUT, PLUGIN_ICONPATH, PLUGIN_ORDER
from applib import log
from applib import setting


import os
#import Image, ImageEnhance

WATERMARK_CENTER = "0"
WATERMARK_TOPLEFT = "1"
WATERMARK_TOPRIGHT = "2"
WATERMARK_BOTTOMLEFT = "3"
WATERMARK_BOTTOMRIGHT = "4"

class watermarkImage(object):

    def __init__(self):
        self.active = True
        self.error = ""

        self.watermark_pos = WATERMARK_TOPLEFT
        self.watermark_transparency = 0.5
        self.watermarkFile = os.getcwd() + "/data/watermark.png"

    def main( self, self_parent, currentPath, fileItemList, fileExt ) :

        self.watermarkFile = os.getcwd() + "/data/watermark.png"

        if not os.path.isfile( self.watermarkFile ) :
            QMessageBox.warning( self_parent, "Attention, " + self.getInfo(PLUGIN_NAME) , "Le fichier n'existe pas : \n\n" + self.watermarkFile, QMessageBox.Ok )

        # Ne garder que les images dans la liste
        fileItemListTrt = []
        i = 0
        maxFile = len(fileItemList)
        for fileItem in range(0, maxFile)  :
            fileItem = fileItemList[i]
            if fileItem.selection is not False :
                #print ("remove : " + str(i) + "/" + str(len(fileItemList)) + " : " + fileItem.fname )
                #del fileItemList[i]
                fileItemListTrt.append( fileItemList[i] )
            i = i + 1

        if len (fileItemListTrt) > 0 :
            waterMarkOptionsObj = waterMarkOptions( currentPath, fileItemListTrt, self.watermarkFile, self.watermark_pos, self.watermark_transparency )
            waterMarkOptionsObj.exec_()
        else :
            QMessageBox.warning( self_parent, "Attention, " + self.getInfo(PLUGIN_NAME), "Aucune image selectionée : la liste est vide", QMessageBox.Ok )

        return 0

    def isActive( self ) :
        return  self.getInfo( PLUGIN_ACTIVE )

    def getInfo( self, typeInfo) :
        if typeInfo == PLUGIN_ACTIVE :
            return self.active
        elif typeInfo == PLUGIN_NAME :
            return "watermarkImage"
        elif  typeInfo == PLUGIN_DESCRIPTION :
            return "Ajoute un filigramme sur l'image"
        elif  typeInfo == PLUGIN_VERSION :
            return "1.0.0"
        elif  typeInfo == PLUGIN_MENULIB :
            return "Ajouter un filigramme"
        elif  typeInfo == PLUGIN_MENUTOOLTIP :
            return "Ajoute un filigramme sur l'image"
        elif  typeInfo == PLUGIN_STATUTBARTOOLTIP :
            return "Ajoute un filigramme sur l'image"
        elif  typeInfo == PLUGIN_SHORTCUT :
            return "Ctrl+W"
        elif  typeInfo == PLUGIN_ICONPATH :
            return "icon/tool_module_watermak.png"
        elif  typeInfo == PLUGIN_MENU_UPPER :
            return True
        elif  typeInfo == PLUGIN_MENU_TOOLBAR :
            return True
        elif  typeInfo == PLUGIN_MENU_ICONVIEW :
            return True
        elif  typeInfo == PLUGIN_MENU_DETAILVIEW :
            return True
        elif  typeInfo == PLUGIN_ORDER :
            return 110


class waterMarkOptions(QDialog):

    def __init__(self, currentPath, fileItemList, watermarkFile, watermark_pos, watermark_transparency ):

        super(waterMarkOptions, self).__init__()

        self.currentPath = currentPath
        self.fileItemList = fileItemList
        self.watermarkFile  = watermarkFile
        self.watermark_pos = watermark_pos
        self.watermark_transparency = watermark_transparency
        self.offsetX = 0
        self.offsetY = 0
        self.image_quality = 100

        self.setGeometry(100, 100, 250, 150)
        self.setWindowTitle('Options du watermark')

        self.imageLabel = QLabel(self)
        self.imageLabel.setProperty( "coloredcell", True )
        self.setStyleSheet("*[coloredcell=\"true\"] {background-color:rgb(100,100,100);}")

        self.offsetXEdit = QLineEdit(self)
        self.offsetXEdit.setText( str(self.offsetX) )
        self.offsetXEdit.setMaximumSize(60,30)
        self.offsetXEdit.textChanged.connect(self.onOffsetXChanged)

        self.offsetYEdit = QLineEdit(self)
        self.offsetYEdit.setText( str(self.offsetY) )
        self.offsetYEdit.setMaximumSize(60,30)
        self.offsetYEdit.textChanged.connect(self.onOffsetYChanged)

        self.fileName = QLineEdit(self)
        self.fileName.setText(self.watermarkFile)
        #self.fileName.setMaximumSize(200,30)
        self.fileNameSelect = QPushButton('...', self)
        self.fileNameSelect.setMaximumSize(50,30)
        self.fileNameSelect.clicked.connect(self.onSelectFileWaterMark)

        self.waterPos = QComboBox(self)
        self.waterPos.addItem( "Centré" , "0")
        self.waterPos.addItem( "En haut à gauche" , "1")
        self.waterPos.addItem( "En haut à droite" ,"2")
        self.waterPos.addItem( "En bas à droite", "4")
        self.waterPos.addItem( "En bas à gauche", "3")
        self.waterPos.setCurrentIndex(self.waterPos.findData( str(self.watermark_pos)));
        self.waterPos.currentIndexChanged.connect(self.onWaterPosChanged)
        self.waterPos.setMaximumSize(200,30)

        self.imageOpacity = QLineEdit(self)
        self.imageOpacity.setText( str(watermark_transparency))
        self.imageOpacity.editingFinished.connect(self.onImageOpacityChanged)
        self.imageOpacity.setMaximumSize(100,30)

        self.buttonCancel = QPushButton("Fermer", self)
        self.buttonCancel.clicked.connect(self.onCancel)
        self.buttonCancel.setMaximumSize(100,30)

        self.buttonPrintWatermark = QPushButton("Sauver", self)
        self.buttonPrintWatermark.clicked.connect(self.onPrintWatermark)
        self.buttonPrintWatermark.setMaximumSize(100,30)

        self.imageQuality = QLineEdit(self)
        self.imageQuality.setText( str(self.image_quality))
        self.imageQuality.editingFinished.connect(self.onImageQualityChanged)
        self.imageQuality.setMaximumSize(100,30)

        self.buttonPrev = QPushButton("<<", self)
        self.buttonPrev.clicked.connect(self.onPrevImage)
        self.buttonPrev.setMaximumSize(100,30)

        self.buttonNext = QPushButton(">>", self)
        self.buttonNext.clicked.connect(self.onNextImage)
        self.buttonNext.setMaximumSize(100,30)

        self.grid = QGridLayout(self)

        self.area = QScrollArea(self)
        self.area.setWidgetResizable(True)

        self.area.setWidget(self.imageLabel)
        self.grid.addWidget(self.area, 0, 0, 1, 4)

        self.grid.addWidget(QLabel("Position : ") , 1, 0, Qt.AlignRight)
        self.grid.addWidget(self.waterPos, 1, 1)
        self.grid.addWidget(QLabel("Opacité (0..1) : ") , 1, 2, Qt.AlignRight)
        self.grid.addWidget(self.imageOpacity, 1, 3)

        self.grid.addWidget(QLabel("Offset x : ") , 2, 0, Qt.AlignRight)
        self.grid.addWidget(self.offsetXEdit , 2, 1)
        self.grid.addWidget(QLabel("Offset y : ") , 2, 2, Qt.AlignRight)
        self.grid.addWidget(self.offsetYEdit , 2, 3)

        self.grid.addWidget(QLabel("Signature : ") , 3, 0, Qt.AlignRight)
        self.grid.addWidget(self.fileName, 3, 1)
        self.grid.addWidget(self.fileNameSelect, 3, 2)

        self.grid.addWidget(self.buttonPrintWatermark, 4, 0)
        self.grid.addWidget(QLabel("Qualité Jpeg (0..100) : ") , 4, 2, Qt.AlignRight)
        self.grid.addWidget(self.imageQuality, 4, 3)

        self.grid.addWidget(self.buttonCancel, 5, 0)
        self.grid.addWidget(self.buttonPrev, 5, 2)
        self.grid.setAlignment( self.buttonPrev , Qt.AlignRight)
        self.grid.addWidget(self.buttonNext, 5, 3)
        self.initOverlay()

        self.currentImageIndex = 0

        self.showImage( self.currentImageIndex )

        dialogWidth = self.imgPix.width() + 100
        dialogHeight= self.imgPix.height() + 100
        self.resize(dialogWidth, dialogHeight)

    def onPrintWatermark(self) :
        fileItem = self.fileItemList[self.currentImageIndex]
        options = QFileDialog.Options()
        fileName, _ = QFileDialog.getSaveFileName(self,
                "Enregistrer sous ...", self.currentPath + "/" + fileItem.fname,
                "Tous (*);;Jpg (*.jpg)", options=options)

        if fileName:
            log.Log.debug(log.INFO, "Watermark créé : " + fileName)
            self.overlayQimage.save( fileName, "JPG", self.image_quality)

    def initOverlay( self ) :
        """
        Rend transparents les pixels blancs de la signature

        nb : pas necessaire en python 3

        """
        self.overlayIConExt = QImage(self.watermarkFile)
        return

        """
        self.overlayIConExt = QImage(self.watermarkFile)
        self.overlayIConExt = self.overlayIConExt.convertToFormat(QImage.Format_ARGB32) #; after loading.
        for i in range( self.overlayIConExt.width() ) :
            for j in range( self.overlayIConExt.height() ) :
                r, g, b, a = QColor(self.overlayIConExt.pixel(i ,j)).getRgb()
                if r == 255 and g == 255 and b == 255 :
                    self.overlayIConExt.setPixel(i, j, Qt.transparent)
        """
    def onPrevImage(self) :
        self.currentImageIndex = self.currentImageIndex - 1
        if self.currentImageIndex < 0 :
            self.currentImageIndex = 0
        else :
            self.showImage( self.currentImageIndex )

    def onNextImage(self) :
        self.currentImageIndex = self.currentImageIndex + 1
        if self.currentImageIndex >= len(self.fileItemList) :
            self.currentImageIndex = len(self.fileItemList)-1
        else :
            self.showImage( self.currentImageIndex )

    def onImageQualityChanged( self) :
        if self.imageQuality.isModified():
            try :
                self.image_quality = int(self.imageQuality.text())
                if self.image_quality > 100 :
                    self.image_quality = 100
                if self.image_quality < 0 :
                    self.image_quality = 0
            except :
                self.image_quality = 100

            self.imageQuality.setText( str(self.image_quality) )

    def onWaterPosChanged (self , ) :
        self.watermark_pos = self.waterPos.itemData(self.waterPos.currentIndex())
        self.showImage( self.currentImageIndex )

    def onImageOpacityChanged(self) :
        if self.imageOpacity.isModified():
            try :
                self.watermark_transparency = float(self.imageOpacity.text())
                self.imageLabel.clear()
                self.showImage( self.currentImageIndex )
            except :
                self.watermark_transparency = 1

        self.imageOpacity.setText( str(self.watermark_transparency) )

    def onOffsetXChanged(self) :
        try :
            self.offsetX = float(self.offsetXEdit.text())
            self.imageLabel.clear()
            self.showImage( self.currentImageIndex )
        except :
            self.offsetX = 0

    def onOffsetYChanged(self) :
        try :
            self.offsetY = float(self.offsetYEdit.text())
            self.imageLabel.clear()
            self.showImage( self.currentImageIndex )
        except :
            self.offsetY = 0

    def onSelectFileWaterMark(self) :
        options = QFileDialog.Options()
        fileName, _ = QFileDialog.getOpenFileName(self,
                "Selectionner la signature", self.watermarkFile,
                "Tous (*);;Jpg (*.jpg);;Png (*.png)", options=options)
        if fileName:
            self.watermarkFile = fileName
            self.fileName.setText(fileName)
            self.initOverlay()
            self.showImage( self.currentImageIndex )

    def showImage( self, indexImage ) :

        if indexImage > len(self.fileItemList) :
            return

        fileItem = self.fileItemList[indexImage]

        log.Log.debug(log.INFO, "create watermark for : " + self.currentPath + "/" + fileItem.fname)

        self.setWindowTitle( 'Créer le filigramme pour : ' + fileItem.fname + " " + str(self.currentImageIndex+1) + "/" + str(len(self.fileItemList) ))

        baseQimage = QImage( self.currentPath + "/" + fileItem.fname)
        if not baseQimage.isNull() :

            self.overlayQimage = QImage( baseQimage.size(), QImage.Format_ARGB32_Premultiplied)

            painter = QPainter( self.overlayQimage )
            painter.setRenderHints(QPainter.Antialiasing)
            painter.setRenderHints(QPainter.HighQualityAntialiasing)

            painter.setOpacity(1)
            self.drawOverlayIconInit( painter , self.overlayQimage, baseQimage )

            (watermark_x, watermark_y) = self.getWartermarkCoo( self.overlayQimage.width(), self.overlayQimage.height(), self.overlayIConExt.width(), self.overlayIConExt.height())
            try :
                painter.setOpacity( float(self.watermark_transparency))
            except :
                painter.setOpacity( 1 )

            self.drawOverlayIcon( painter, watermark_x, watermark_y, self.overlayIConExt )

            painter.end()

            self.imgPix = QPixmap.fromImage(self.overlayQimage)
            self.imageLabel.setPixmap(self.imgPix)
            self.imageLabel.setAlignment(Qt.AlignCenter)

            log.Log.debug(log.INFO, "display watermark")

    def drawOverlayIconInit(self, painter, imageWithOverlay, baseImage ) :
        painter.setCompositionMode(QPainter.CompositionMode_Source)
        painter.fillRect(imageWithOverlay.rect(), Qt.transparent)
        painter.setCompositionMode(QPainter.CompositionMode_SourceOver)
        painter.drawImage(0, 0, baseImage)

    def drawOverlayIcon(self, painter, posX, posY, overlayIConImage ) :
        painter.setCompositionMode(QPainter.CompositionMode_SourceOver)
        painter.drawImage( posX , posY, overlayIConImage)

    def getWartermarkCoo(self, imgW, imgH, ovW , ovH) :
        """
        Retourne la position de l'image overlay en fonction de la taille de l'image et de l'overlay
        """
        if self.watermark_pos == WATERMARK_CENTER :
            return ( (imgW/2)-(ovW/2) + self.offsetX , (imgH/2)-(ovH/2) + self.offsetY )

        if self.watermark_pos == WATERMARK_TOPLEFT :
            return ( 0 + self.offsetX , 0  + self.offsetY  )

        if self.watermark_pos == WATERMARK_TOPRIGHT :
            return ( imgW-ovW  + self.offsetX , 0  +  + self.offsetY)

        if self.watermark_pos == WATERMARK_BOTTOMRIGHT:
            return ( imgW-ovW  + self.offsetX , imgH-ovH + self.offsetY  )

        if self.watermark_pos == WATERMARK_BOTTOMLEFT  :
             return ( 0  + self.offsetX , imgH-ovH  + self.offsetY )

        return (0,0)

    def onCancel(self) :
        self.close()





